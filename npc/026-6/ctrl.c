// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//   The Impregnable Fortress Control Files
// Quest: General_Fortress
//  (MaxFloor+2, internal, internal)

// The antechamber has some traps you should avoid, but beyond the Blood Pact map
// effect, it only has some support NPCs (banker and healer) if you completed the
// Moubootaur Showdown earlier.

// TODO: The main NPC which lets you get out of the platform
026-6,21,70,0,	script	#026-6Gate	NPC_FANCY_CIRCLE,1,0,{
    if (getq(General_Fortress) < 6) { die(); end; }
    if ($MK_TEMPVAR >= MKIF_LV_B7F) goto L_Warp;
    mesc l(".:: Impregnable Fortress, %sF ::.", "B6"), 3;
    msObjective($MK_TEMPVAR >= MKIF_LV_B7F, l("Minimum wins: %d/%d", $MK_TEMPVAR, MKIF_LV_B7F));
    mes "";
    mesc l("Hint: The Boss Chamber lies herein ahead.");
    close;

L_Warp:
    // Not unlocked (this time, require Andrei Sakar mission completion)
    if ($GAME_STORYLINE < 4 || $MK_TEMPVAR < MKIF_LV_B7F) {
        mesc l("The gate is sealed shut."), 1;
        mesc l("The monster army is still strong on this floor!"), 1;
        mesc l("Minimum wins: %d/%d", $MK_TEMPVAR, MKIF_LV_B7F), 1;
        close;
    }
    // Busy
    if ($@MK_SCENE != MK_NONE && $@MK_SCENE != MK_SHOWDOWN) {
        dispbottom l("The Monster King is not there, it makes no sense going in the Throne Room.");
        end;
    }
    // Not a hero
    if (!$@MK_CHALLENGE && !islegendary() && strcharinfo(0) != $MOST_HEROIC$ && !is_master()) {
        dispbottom l("I should wait for %s or one of the heroes to challenge the Monster King.", $MOST_HEROIC$);
        end;
    }
	doevent "#DungeonCore::OnBleed";
    slide any(21, 22), 63;
    end;

OnGoto:
    slide 46, 114;
    end;

OnInit:
	.distance=3;
    end;
}

026-6,22,40,0,	script	#MKBossFightFire	NPC_HIDDEN,0,0,{
    end;
OnTouch:
    if ($@MK_COOLDOWN > gettimetick(2) && !is_master()) {
        dispbottom l("I should wait for %s.", ($GAME_STORYLINE >= 5 ? l("the Moubootaur arrange a new puppet") : l("the blood stains to be cleaned up")));
        end;
    }
    // NOT a fortwarp() - there is no Barbara Bonus for the Showdown!
    warp "026-7", 21, 28;
    if (!$@MK_CHALLENGE) {
        if ($GAME_STORYLINE >= 5) {
            mes l("Do you want to challenge the Monster King? If so, on which difficulty?");
            next;
            menuint
                l("No, I changed my mind."), 0,
                l("Normal"), 1,
                l("Hard"), 2,
                l("Crazy"), 3,
                l("Cadis Mode"), 5,
                l("Ultimate Pandorica"), 7,
                l("Portable Apocalypse"), 10,
                rif(REBIRTH >= 4, l("Judgment Day")), 15;
            closeclientdialog;
        }
        // Already started or no difficulty selected
        if ($@MK_CHALLENGE) end;
        if ($GAME_STORYLINE >= 5 && !@menuret) cwarp "026-6", 22, 41;
        // Begin the challenge! It's SHOWDOWN TIME!
        $@MK_CHALLENGE=max(1, @menuret);
        kamibroadcast(strcharinfo(0)+" has CHALLENGED the MONSTER KING to a SHOWDOWN!", b("WORLD HEART"));
        sleep(2500);
        mapannounce("026-6", "WORLD HEART : The teleporter is now enabled... Get to positions!", bc_map|bc_npc);
        sleep(5000);
        kamibroadcast("It is... SHOWDOWN TIME!", b("WORLD HEART"));
        sleep(2500);
        donpcevent "Impregnable#B7F::OnBegin";
    }
    end;
}

// Room Traps, only against players
026-6,0,0,0	script	#0266_Trap01	NPC_TRAP_ONLINE,0,0,{
    end;

OnTouch:
OnTouchNPC:
    WorldHeartTrap();
    sleep(500); // Wait 500ms for animation
    setnpcdisplay .name$, NPC_TRAP_ONLINE;
    // Move the trap away after it disarms (up to 30 attempts)

OnInit:
    .@e=0;
    do {
        if (.@e >= 30)
            break;
        .@x = rand2(21, 140);
        .@y = rand2(21, 120);
        .@e+=1;
    } while (!checknpccell("026-6", .@x, .@y, cell_chkpass));
    movenpc .name$, .@x, .@y;
    end;
}

// Create more traps
026-6,0,0,0	duplicate(#0266_Trap01)	#0266_Trap02	NPC_TRAP_ONLINE,0,0
026-6,0,0,0	duplicate(#0266_Trap01)	#0266_Trap03	NPC_TRAP_ONLINE,0,0
026-6,0,0,0	duplicate(#0266_Trap01)	#0266_Trap04	NPC_TRAP_ONLINE,0,0
026-6,0,0,0	duplicate(#0266_Trap01)	#0266_Trap05	NPC_TRAP_ONLINE,0,0
026-6,0,0,0	duplicate(#0266_Trap01)	#0266_Trap06	NPC_TRAP_ONLINE,0,0
026-6,0,0,0	duplicate(#0266_Trap01)	#0266_Trap07	NPC_TRAP_ONLINE,0,0
026-6,0,0,0	duplicate(#0266_Trap01)	#0266_Trap08	NPC_TRAP_ONLINE,0,0
026-6,0,0,0	duplicate(#0266_Trap01)	#0266_Trap09	NPC_TRAP_ONLINE,0,0
026-6,0,0,0	duplicate(#0266_Trap01)	#0266_Trap10	NPC_TRAP_ONLINE,0,0
026-6,0,0,0	duplicate(#0266_Trap01)	#0266_Trap11	NPC_TRAP_ONLINE,0,0
026-6,0,0,0	duplicate(#0266_Trap01)	#0266_Trap12	NPC_TRAP_ONLINE,0,0
026-6,0,0,0	duplicate(#0266_Trap01)	#0266_Trap13	NPC_TRAP_ONLINE,0,0
026-6,0,0,0	duplicate(#0266_Trap01)	#0266_Trap14	NPC_TRAP_ONLINE,0,0
026-6,0,0,0	duplicate(#0266_Trap01)	#0266_Trap15	NPC_TRAP_ONLINE,0,0

// Storage master (18, 57), healer (25, 57), etc. (125, 66)
026-6,18,57,0	script	Barzil#FT	NPC_LLOYD,{
    if (!MOUBOOTAUR_WINNER) { // EPISODE_WINNER
        mesn;
        mesq l("Hey %s. Sorry, I only take orders from Andrei Sakar. You'll need to get his permission letter from the depths of Artis before making use of my services.", strcharinfo(0));
        close;
    }
    Banker(.name$, "Impregnable Fortress", 999999);
    close;

OnInit:
    .sex = G_MALE;
    .distance = 3;
    end;
}

026-6,25,57,0	script	Alliance's Nurse	NPC_FEMALE,{
    if (!EPISODE_WINNER) {
        mesn;
        mesq l("Hey %s. Sorry, I only take orders from the Doctor. You'll need to get his permission letter from Hurnscald Hospital before making use of my services.", strcharinfo(0));
        close;
    }
    Nurse(.name$, 10, 6);
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    //setunitdata(.@npcId, UDT_HEADTOP, BrimmedFeatherHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, MiniSkirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, ShortTankTop);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 12);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 16);

    .sex = G_FEMALE;
    .distance = 3;
    end;
}

