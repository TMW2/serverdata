// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 026-6: The Impregnable Fortress (B6F) warps
026-6,88,94,0	warp	#026-6_88_94	0,0,025-2,100,25
026-6,95,121,0	script	#026-6_95_121	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 27,96; end;
}
026-6,112,121,0	script	#026-6_112_121	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 27,83; end;
}
026-6,27,82,0	script	#026-6_27_82	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 112,120; end;
}
026-6,101,105,0	script	#026-6_101_105	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 107,105; end;
}
026-6,106,105,0	script	#026-6_106_105	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 100,105; end;
}
026-6,27,97,0	script	#026-6_27_97	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 95,120; end;
}
026-6,97,65,0	script	#026-6_97_65	NPC_HIDDEN,12,0,{
	end;
OnTouch:
	slide 99,72; end;
}
026-6,99,71,0	script	#026-6_99_71	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 99,64; end;
}
