// TMW2/LOF Script.
// Author:
//    Jesusalva
// Description:
//  Part from the EPISODE and the Well Quest
//  TODO: Walking NPC, clothes, etc;

019-1-1,41,24,0	script	Miler	NPC_PLAYER,{
    .@q=getq(LoFQuest_EPISODE);
    .@w=getq(NivalisQuest_Well);

    mesn;
    if (MERC_RANK)
        mesq l("Hello, @@.", mercrank());
    else if (THIEF_RANK)
        mesq l("Hello, @@.", thiefrank());
    else
        mesq l("Hello.");

    mes "";
    menu
        l("Hello."), -,
        rif(.@w == 1, l("I need help.")), L_Well,
        rif(.@q == 3 && countitem(HerbalTea), l("The Doctor sent you some tea.")), L_Doctor,
        rif(.@q == 5 && countitem(PresentBox), l("I have a present box to you open.")), L_Box,
        rif(.@q == 4 && BaseLevel >= 50, l("So, could I help you?")), L_Quest,
        rif(.@q == 6 && !@miler_wait, l("I'm back.")), L_Continue,
        l("Do you want any monster killed?"), L_GHQ;

    // If not on Cordo quest, Miler will speak about
    if (!THIEF_RANK && !MERC_RANK)
        goto L_Rejected;

    close;

// Well Quest Subplot
L_Well:
    mes "";
    mesn;
    mesq l("What's the problem?");
    next;
    select
        l("Someone fell into the well.");
    mes "";
    mesn;
    mesq l("Ho! I'll help them!");
    getexp (JobLevel * 111), 0;
    setq NivalisQuest_Well, 2;
    close;

// Well Quest Subplot
L_Doctor:
    mes "";
    delitem HerbalTea, 1;
    getexp 111, 11;
    setq LoFQuest_EPISODE, 4;
    mesn;
    mesq l("Many thanks, the Doctor always know what's best for you."); // you or your health? Are you sure?
    next;
    mesn;
    mesq l("Lemme just fetch a small something for you....");
    next;
    mesn;
    mesq l("Oh dear, oh dear, where could I have possibly left it?!");
    close;

// Not on Cordo quest
L_Rejected:
    /*
    mesn;
    mesq l("You cannot help me at all. You lack any skill to do so.");
    next;
    */
    mesn;
    mesq l("Hey, did you know there are two mouboos which constantly fight against themselves?!");
    next;
    mesn;
    mesq l("One claims to be a constable and teach people to sell high and buy low.");
    mesq l("The other one claims to be a dangerous bandit and to teach how to steal from monsters!");
    next;
    mesn;
    mesq l("Well, I heard you needed to have some Job levels to sign up with them, and couldn't resign later.");
    mesq l("But it is a so silly fight, that whoever you join with shouldn't do much difference.");
    next;
    mesn;
    mesq l("Anyway, I heard both were disciples from Cordo-whatever, a powerful person from LoF Village.");
    //mesq l("I think you should get initiated on any side before speaking to me again.");
    close;

// Main Quest
L_Quest:

    // Force players upon Cordo quest
    /*
    if (!THIEF_RANK && !MERC_RANK)
        goto L_Rejected;
    */

    mes "";
    mesn;
    mesq l("I lost the precious ring they gave me as a gift... Who could have taken it...?");
    next;
    mesn;
    mesq l("...Of course. It was THEM. It gotta to be them!");
    next;
    mesn strcharinfo(0);
    select l("'Them' whom?");
    mes "";
    mesn;
    mesq l("They came in the night, always taking what does not belong to them.... The SLIMES!");
    next;
    mesn;
    mesq l("The ones around here are specially nasty. They steal stuff and seal them on 'present boxes', just to amuse as people try to open those...");
    next;
    mesn;
    mesq l("...But worry not, I have the right screwdriver for the job. So, are you up to bring me some Present Boxes?");
    next;
    mesn strcharinfo(0);
    select
        l("Yes, I'll help you."),
        l("Nah, not now. Slimes ruin your clothes, after all.");
    mes "";
    if (@menu == 2) {
        mesn;
        mesq l("Ah, I see, I imagine you'll wear something different then... But please come back.");
        close;
    }
    setq LoFQuest_EPISODE, 5, 0;
    mesn;
    mesq l("Great, just bring me several boxes, once one of them have the ring I'm looking for.");
    close;

// Open Present Box Loop
L_Box:
    inventoryplace NPCEyes, 1;
    .@q2=getq2(LoFQuest_EPISODE);
    delitem PresentBox, 1;
    setq2 LoFQuest_EPISODE, .@q2+1;
    mesn;
    mesc l("@@ uses his screwdriver and open the sealed gift box like a pro.", .name$);
    // Handle result
    if (.@q2 >= 70) {
        goto L_Success;
    } else if (.@q2 >= 50) {
        .@id=any(WhiteFur, Candy, Milk, Lockpicks, MaggotSlime, CandyCane, ChocolateBar, XmasCake, GingerBreadMan, CherryCake, Plushroom, Moss, Chagashroom, BugLeg, Acorn, Manana, Mashmallow, HardSpike, SilkCocoon, IceCube, CoinBag, Coal, CottonCloth, BlueDye);
        getitem .@id, 1;
        mesc l("But there was only a(n) @@ inside.", getitemlink(.@id));
    } else if (.@q2 >= 30) {
        if (rand(1,50) < .@q2) {
            .@id=any(Candy, MaggotSlime, Plushroom, Chagashroom, BugLeg, Acorn, MauveHerb);
            getitem .@id, 1;
            mesc l("But there was only a(n) @@ inside.", getitemlink(.@id));
        } else {
            .@id=rand2(2,4)+.@q2;
            Zeny=Zeny+.@id;
            mesc l("But there was only @@ GP inside.", .@id);
        }
    } else if (.@q2 >= 10) {
        .@id=rand2(3,5)+.@q2;
        Zeny=Zeny+.@id;
        mesc l("But there was only @@ GP inside.", .@id);
    } else {
        mesc l("But the box was empty.");
    }

    // Try again!
    if (countitem(PresentBox)) {
        mesn;
        mesq l("You have more boxes. Wanna try again?");
        if (askyesno() == ASK_YES)
            goto L_Box;
    }
    close;
// Quest Complete
L_Success:
    setq LoFQuest_EPISODE, 6, 0;
    getexp 25000, 0; // @Saulc DO NOT INCREASE THIS VALUE, if you think this is low, give player MobPoints.
    // TODO: Pre-Requisite Item? (eg. Lazurite or Bent Neddle)
    // TODO: White Roses quest. We could sell them high and allow drop during spring
    mesc l("His golden ring pops right out of it.");
    next;
    mesn;
    mesq l("Many thanks. I couldn't live without it. Please come back later.");
    @miler_wait=true;
    close;

L_Continue:
    if (BaseLevel < 50) goto L_Rejected;
    mesn;
    mesq l("Welcome back. Uh, no, I haven't forgot I promised you a small something... But you see, then I lost my ring, and...");
    next;
    mesn;
    mesq l("...I deposited everything on the bank. Sorry!");
    next;
    mesn;
    mesq l("Oh, but don't you worry. Sure, you can't go in Nivalis bank and take my stuff... But the Storage Fairy at Lilit might just let you.");
    next;
    mesn;
    mesq l("But she is a fairy. She won't be pleased with flowers. Instead, go and give her a %s as a token of good will.", getitemlink(SnakeSkin));
    next;
    mesn;
    mesq l("Pal, I'm counting on you. You'll like the little something I have for you!");
    setq LoFQuest_EPISODE, 7, 0;
    close;

L_GHQ:
    GHQ_Assign(Moggun, "Nivalis");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, KnitHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CreasedShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, JeansShorts);
    setunitdata(.@npcId, UDT_HAIRSTYLE, any(2,3,4,6,14,15,17,21,22,24,25,26));
    setunitdata(.@npcId, UDT_HAIRCOLOR, rand(0,20));

    .sex = G_MALE;
    .distance = 5;
    end;
}
