// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 010-2-7: Canyon Cave mobs
010-2-7,41,26,11,3	monster	Mountain Snake	1123,3,35000,150000
010-2-7,0,0,0,0	monster	Cave Maggot	1027,22,35000,150000
010-2-7,41,32,11,4	monster	Snake	1122,3,35000,150000
010-2-7,47,38,9,7	monster	Cave Snake	1035,11,35000,150000
010-2-7,37,33,7,2	monster	Black Scorpion	1074,2,35000,150000
010-2-7,0,0,0,0	monster	Plushroom Field	1011,2,35000,150000
