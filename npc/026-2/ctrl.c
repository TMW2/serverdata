// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//   The Impregnable Fortress Control Files
// Quest: General_Fortress
//  (MaxFloor+2, internal, internal)

026-2,62,91,0	script	Books#B2F	NPC_NO_SPRITE,{
    mesc l("These books seem to belong to the fortress keeper. They seem more important than the others.");
    next;
    mesc l("Most of them seem to be accounting books. There seems to be a large influx of money for the purchase of %s as a component for homunculus creation.", getitemlink(MaggotSlime));
    next;
    mesc l("The records implies a large number of economic players with ties with the Monster King operations, knowingly or not. A line also catches your attention:");
    mes l("%s - %s GP", strcharinfo(0), fnum(MonsterMoney));
    if (getq(General_Fortress) != 3) close;
    next;
    mesc l("After staring dumbfounded at the book for a while, you find something actually useful:");
    mesc l("Near the aquarium, chant the magic words; his true name. Only then, you may advance to his Throne Room."), 1;
    // The door to left leads to the living quarters area and whatnot. Both doors should be locked. The riddle should be black boxed. His true name cannot be found in the fortress, but is registered in any history bard or book.
    if (getq2(General_Fortress) == 1) {
        mesc l("There seems to be scribbled something about 'the latin part of the prophecy', whatever that means. Seems to be an old passphrase to access 'the World Heart'.");
        mesc l("This will not be found here, however."), 1;
    } else {
        mesc l("Lets try it first, otherwise it is always possible to read the book again in search of clues.");
        setq2 General_Fortress, 1;
    }
    close;

OnInit:
    .distance=3;
    end;
}

026-2,29,26,0	script	Impregnable1#B2F	NPC_HIDDEN,4,0,{
    end;

OnTouch:
    //if (getq(General_Fortress) > 3) goto L_Warp;
    mesc l(".:: Impregnable Fortress, %sF ::.", "B2"), 3;
    mesc l("Living Quarters")+" ↑", 3;
    msObjective(getq(General_Fortress) >= 4, l("* Chant magic words"));
    msObjective($MK_TEMPVAR >= MKIF_LV_B3F, l("Minimum wins: %d/%d", $MK_TEMPVAR, MKIF_LV_B3F));
    mes "";
    mesc l("Hint: Go read a book if you're bored.");
    close;

L_Warp:
    // Not unlocked
    if ($GAME_STORYLINE >= 3 && $MK_TEMPVAR < MKIF_LV_B3F) {
        mesc l("The gate is sealed shut."), 1;
        mesc l("The monster army is still strong on this floor!"), 1;
        mesc l("Minimum wins: %d/%d", $MK_TEMPVAR, MKIF_LV_B3F), 1;
        close;
    }
    //warp "026-3", X, Y;
    dispbottom l("Coming Soon, in Moubootaur Legends!");
    end;
}

026-2,86,26,0	script	Impregnable2#B2F	NPC_HIDDEN,4,0,{
    end;

OnTouch:
    if (getq(General_Fortress) > 3) goto L_Warp;
    mesc l(".:: Impregnable Fortress, %sF ::.", "B2"), 3;
    mesc l("Throne Room")+" ↑", 3;
    msObjective(getq(General_Fortress) == 4, l("* Chant magic words"));
    msObjective($MK_TEMPVAR >= MKIF_LV_B3F, l("Minimum wins: %d/%d", $MK_TEMPVAR, MKIF_LV_B3F));
    mes "";
    mesc l("Hint: Go read a book if you're bored.");
    close;

L_Warp:
    fortwarp(MKIF_LV_B3F, "026-3", 82, 19);
    end;

OnTalkNearby:
    if (getq2(General_Fortress) != 1 || getq(General_Fortress) != 3) end;
    // not very obvious stuff by gumi
    .@no_nick$ = strip(substr($@p0$, getstrlen(strcharinfo(PC_NAME)) + 3, getstrlen($@p0$) - 1));
    .@message$ = strtoupper(.@no_nick$);
    // TODO: I don't know his true name either, but the latin part is in 006-10
    if (.@message$ == "PER ASPER AD SALVOS") {
        setq General_Fortress, 4, 0, 0;
        specialeffect(FX_MGWARP, SELF, getcharid(3));
    }
    end;
OnInit:
OnInstanceInit:
    .distance=3;
    .pid=getnpcid();
    // For players
    debugmes "Fortress B2F Pattern %d", .pid;
    defpattern(.pid, "^(.*)$", "OnTalkNearby");
    activatepset(.pid);
    end;
}

