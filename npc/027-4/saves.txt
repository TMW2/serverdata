// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Mr. Saves of Scholar Class

027-4,28,28,0	script	Mr Saves	NPC_BLACKALCHEMIST,{
    function basicMagic;
    function standardMagic;
    function advancedMagic;
    if (!MAGIC_LVL) goto L_NoMagic;
    mes l(".:: Scholarship Class ::.");
    mesc l("Specialized in support, buff, debuff and strengthening skills.");
    next;
    mesn;
    mesc l("You have @@ magic skill points available.", sk_points());
    next;
    select
        l("Basic Magic"),
        l("Standard Magic"),
        l("Advanced Magic");
        //l("Mastery Magic");
    mes "";
    .@lv=@menu;
    do
    {
        // Display appropriate menu
        if (.@lv == 1)
            basicMagic();
        else if (.@lv == 2)
            standardMagic();
        else if (.@lv == 3)
            advancedMagic();

        // Handle result
        mes "";
        if (@menuret) {
            if (!learn_magic(@menuret)) {
                mesc l("You do not meet all requisites for this skill."), 1;
                next;
            }
        } else {
            closeclientdialog;
        }

    } while (@menuret);
    close;

function basicMagic {
    if (MAGIC_LVL < 1) goto L_NoMagic;
    mes l(".:: First Aid ::.");
    mesc l("Minor healing to your wounds.");
    mes "";
    mes l(".:: Accumulate Power ::.");
    mesc l("Raise damage of next skill.");
    mes "";
    mes l(".:: Provoke ::.");
    mesc l("Provoke a single monster to attack you.");
    mes "";
    mes l(".:: Windwalker ::.");
    mesc l("Increase walk speed and flee rate.");
    mes "";
    mes l(".:: Chanting ::.");
    mesc l("Reduces MP cost when using chanting-based skills.");
    mes "";
    mes l(".:: Transfer Mana ::.");
    mesc l("Drains your MP bar to replenish target's. Doesn't go past 100%.");
    mes "";
    menuint
        l("First Aid"), TMW2_FIRSTAID,
        l("Accumulate Power"), HW_MAGICPOWER,
        l("Provoke"), SM_PROVOKE,
        l("Windwalker"), SN_WINDWALK,
        l("Chanting"), TMW2_CHANT,
        l("Transfer Mana"), TMW2_MPTRANSFER,
        l("Cancel"), 0;
    return;
}

function standardMagic {
    if (MAGIC_LVL < 2) goto L_NoMagic;
    // NOTE: Alternate between First Aid + Healing for less cooldown wait
    mes l(".:: Healing ::.");
    mesc l("Minor healing to yourself or to allies.");
    mes "";
    mes l(".:: Mana Wisdom ::.");
    mesc l("(Passive) Increases Mana EXP/Control Gain rate.");
    mes "";
    mes l(".:: Last Standing Man ::.");
    mesc l("(Passive) Raise Max HP and Holy Defense.");
    mes "";
    mes l(".:: Area Provoke ::.");
    mesc l("Provokes all monsters around the target, and the target itself.");
    mes "";
    menuint
        l("Healing"), TMW2_HEALING,
        l("Mana Wisdom"), TMW2_SAGE,
        l("Last Standing Man"), CR_TRUST,
        l("Area Provoke"), EVOL_AREA_PROVOKE,
        l("Cancel"), 0;
    return;
}

function advancedMagic {
    if (MAGIC_LVL < 3) goto L_NoMagic;
    mes l(".:: Magnus Healing ::.");
    mesc l("Heals in area every friendly unit (incl. homuns and mercs). Req. Lifestone to cast.");
    mes "";
    mes l(".:: Resurrection ::.");
    mesc l("Revives an already dead allied player. Req. Lifestone to cast.");
    mes "";
    menuint
        l("Magnus Healing"), TMW2_MAGNUSHEAL,
        l("Resurrection"), TMW2_RESURRECT,
        l("Cancel"), 0;
    return;
}

L_NoMagic:
    next;
    mesn;
    mesq l("You do not have enough magic power for these classes.");
    next;
    if ($FIRESOFSTEAM < 9) {
        mesn;
        mesq l("Besides the Magic Council, Andrei Sakar have his own Mana Stone, but I doubt he would train the likes of you, or share his Mana Stone.");
        next;
    }
    mesn;
    mesq l("Perhaps, in the city, someone knows rumors about Mana Stones and can teach you. Other than that, you're on your own.");
    close;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;

}

