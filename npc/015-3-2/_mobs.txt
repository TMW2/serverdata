// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 015-3-2: Pirate Caves B2F mobs
015-3-2,35,62,2,10	monster	Grenadier	1444,1,100000,30000
015-3-2,37,61,2,10	monster	Swashbuckler	1443,2,100000,30000
015-3-2,60,54,6,3	monster	Thug	1442,2,100000,30000
015-3-2,39,85,6,3	monster	Thug	1442,2,100000,30000
015-3-2,39,53,3,11	monster	Thug	1442,2,100000,30000
015-3-2,29,83,5,3	monster	Swashbuckler	1443,2,100000,30000
015-3-2,64,51,6,4	monster	Swashbuckler	1443,2,100000,30000
015-3-2,61,51,6,2	monster	Grenadier	1444,1,100000,30000
015-3-2,31,90,6,2	monster	Grenadier	1444,1,100000,30000
015-3-2,97,47,3,11	monster	Thug	1442,2,100000,30000
015-3-2,90,57,2,10	monster	Swashbuckler	1443,2,100000,30000
015-3-2,83,40,9,5	monster	Grenadier	1444,1,100000,30000
