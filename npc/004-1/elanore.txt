// TMW-2 Script
// Author:
//    Jesusalva

004-1,80,62,0	script	Elanore the Healer	NPC_FEMALE,{
    .@price=Nurse(.name$, 10, 5, 1);
    mesq l("For you, it'll be @@ GP.", .@price);
    mes "";
    menu
        rif(Zeny >= .@price, l("Please heal me!")), L_Healing,
        l("Do you make Lifestones?"), L_Lifestones,
        l("Another time, maybe."), -;
    close;


L_Healing:
    Nurse(.name$, 10, 5, 2);
    close;

L_Lifestones:
    mes "";
    mesn;
    mesq l("Lifestones have the potential to heal the user, and thus are used for potions.");
    next;
    mesq l("I will need 3 @@, 5 @@ and 100 GP to craft a single stone.", getitemlink(BugLeg), getitemlink(MaggotSlime));
    next;
    mes "";
    menu
        l("OK, I will get them."), -,
        l("No way!"), -,
        l("Here you are!"), L_Trade,
        l("I want more than one!"), L_Trade_Input;
    close;

L_Trade:
    if (countitem(BugLeg) < 3) goto L_Trade_Missing;
    if (countitem(MaggotSlime) < 5) goto L_Trade_Missing;
    if (Zeny < 100) goto L_Trade_Missing;

    inventoryplace Lifestone, 1;

    delitem BugLeg, 3;
    delitem MaggotSlime, 5;
    set Zeny, Zeny - 100;

    getitem Lifestone, 1;
    goto L_TradeOK;

L_Trade_Input:
    mesc l("How many?");
    input .@am;
    if (.@am <= 0)
        close;

    if (countitem(BugLeg) < 3*.@am) goto L_Trade_Missing;
    if (countitem(MaggotSlime) < 5*.@am) goto L_Trade_Missing;
    if (Zeny < 100*.@am) goto L_Trade_Missing;

    inventoryplace Lifestone, 1*.@am;

    delitem BugLeg, 3*.@am;
    delitem MaggotSlime, 5*.@am;
    set Zeny, Zeny - 100*.@am;

    getitem Lifestone, 1*.@am;
    goto L_TradeOK;

L_TradeOK:
    mes "";

    // Only grant Experience on first craft
    if (getq(TulimsharQuest_Lifestone) == 0) {
        setq TulimsharQuest_Lifestone, 1;
        getexp 84, 10;
    }

    // Second tier stuff
    if (ST_TIER == 8) {
        if (gettimetick(2) > QUEST_ELEVARTEMPO) {
            mesc l(".:: Second Tier Quest - Timed Out ::."), 1;
        } else {
            getexp 44, 0;
            ST_TIER=9;
            mes l("##9.:: Second Tier Quest - Time Remaining: @@ ::.", FuzzyTime(QUEST_ELEVARTEMPO,2,2));
        }
    }

    mesn;
    mesq l("These will do just fine.");
    next;
    mesn;
    mes l("Elanore casts a chant over the items, closes her hands, and vóila! A lifestone.");
    next;
    mesq l("I guess that you could use these lifestones to gain a regeneration effect, too, but I do not know how.");
    next;
    mes l("She smiles.");
    close;

L_Trade_Missing:
    mesn;
    mesq l("This is not what I asked for.");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    //setunitdata(.@npcId, UDT_HEADTOP, 2929);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SorcererRobe);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 8);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 17);
    npcsit;

    .sex = G_FEMALE;
    .distance = 5;
    end;

}
