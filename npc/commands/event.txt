// TMW2 Script
// Author:
//    Evol Team
//    Saulc
//    Jesusalva
// Description:
//    Handles all major events on TMW2 (Christmas, Easter, Worker Day, etc.)

// WARNING, possibly dangerous.
function	script	sClear	{
        // Delete all Easter Stuff
        if ($EVENT$ != "Easter") {
            DelItemFromEveryPlayer(SilverEasteregg);
            DelItemFromEveryPlayer(GoldenEasteregg);
        } else {
        	.@nb = query_sql("SELECT c.name, i.amount FROM `inventory` AS i, `char` AS c WHERE i.nameid=834 AND i.char_id=c.char_id ORDER BY i.amount DESC LIMIT 10", $@easter_name$, $@easter_value);
            setnpcdisplay("Aurora", "Lilica#final", NPC_EASTER);
        }

        // Delete all Christmas stuff
        if ($EVENT$ != "Christmas") {
            $XMAS_MONEY=0; // Golbarez
            $XMAS_GIFTS=0; // Chief
            DelItemFromEveryPlayer(ClosedChristmasBox);
            DelItemFromEveryPlayer(OpenedChristmasBox);
        } else if ($EVENT$ == "Christmas") {
            DelAccRegFromEveryPlayer("#XMAS_LOGINBONUS");
            removemapmask "003-1", MASK_CHRISTMAS;
            removemapmask "005-1", MASK_CHRISTMAS;
            removemapmask "009-1", MASK_CHRISTMAS;
            removemapmask "012-1", MASK_CHRISTMAS;
            removemapmask "017-2", MASK_CHRISTMAS;
            removemapmask "017-2-1", MASK_CHRISTMAS;
            removemapmask "017-3", MASK_CHRISTMAS;
            removemapmask "020-2", MASK_CHRISTMAS;
        }

        // Delete all Valentine Day stuff
        if ($EVENT$ != "Valentine") {
            // ItemDB prefers account variables than quest log
            DelAccRegFromEveryPlayer("#VALENTINE_POINTS");
            DelAccRegFromEveryPlayer("#VALENTINE_RECEIVED");
            DelAccRegFromEveryPlayer("#VALENTINE_OPENED");
            DelAccRegFromEveryPlayer("#VALENTINE_SENT");
            DelAccRegFromEveryPlayer("#VALENTINE_EQUIPMENT2020");
            // Remove event items
            DelItemFromEveryPlayer(LoveLetter);
            DelItemFromEveryPlayer(BoxOfChocolates);
        } else if ($EVENT$ == "Valentine") {
            htdelete $@VALENTINE_GIFTSTACKS;
            htdelete $@VALENTINE_LOVELETTER;
            $@VALENTINE_GIFTSTACKS=0;
            $@VALENTINE_LOVELETTER=0;
            donpcevent "Aurora::OnValentine";
        }

        // Longer EXP rate controls
        if ($EVENT$ == "Anniversary") {
            $BCONFB_EXPR-=50;
            kamibroadcast("EXP Rate changed from %d%% to %d%%", $BCONFB_EXPR+50, $BCONFB_EXPR);
        }

        // Nullify Regnum Blessing
        if ($EVENT$ == "Regnum") {
            // Remove the blessing
            setmapflag($REGNUM_BLESSMAP$, mf_bexp, 100);
            $REGNUM_BLESSMAP_H$="";
            $REGNUM_BLESSMAP$="";
            kamibroadcast("Regnum's Blessing is over.");
        }

        // Hand Aurora Event Rewards
        callfunc("FYRewardEvent");
        $WORLDEXPO_ENEMY$="";
        deletearray $FYREWARD_PT;
        deletearray $FYREWARD_ID;
        deletearray $FYREWARD_AM;
        $FYLOGIN_PTS=0;

        // Simpler events
        $PATRICK_DAYCTRL=0;
        $PATRICK_DAYMAX=0;
        DelAccRegFromEveryPlayer("#PATRICK_DAY");
        DelAccRegFromEveryPlayer("#PATRICK_CTRL");
        DelAccRegFromEveryPlayer("#TMW2_LOGINBONUS");
        DelAccRegFromEveryPlayer("#THANKS_DAY");
        DelAccRegFromEveryPlayer("#THANKS_STREAK");

        // Not so simple but still needs cleanup.
        DelItemFromEveryPlayer(Event1HSword);
        DelItemFromEveryPlayer(Event2HSword);
        DelItemFromEveryPlayer(EventBow);
        DelItemFromEveryPlayer(EventWand);

        // We must remove any event drop
        charcommand("@reloadmobdb");
        SeasonReload(1);
        return;
    }

function	script	GlobalEventMenu	{

    function resetCandor {
        $@FEFE_CAVE_LEVEL=0;
        $@FEFE_DELAY=0;
        $@FEFE_DIFFICULTY=0;
        $@FEFE_CAVE_HERO$="";
        mesc "Candor Fight Reset!";
        return;
    }

    function rateManagement {
        clear;
        mes l("To get the current rate:");
        mes col("    @exprate", 7);
        next;
        mes l("To set the exp rate:");
        mes col("    @exprate ##Brate##b hours", 7);
        next;
        mes l("To reset back to normal:");
        mes col("    @exprate default", 7); // note to translators: any non-numerical value will reset so "default" is safe to translate
        next;
        return;
    }

    function dropManagement {
        clear;
        mes l("To get the current rate:");
        mes col("    @rates", 7);
        next;
        mes l("To set the drop rate:");
        mes col("    @droprate ##Brate##b hours", 7);
        next;
        mes l("To reset back to normal:");
        mes col("    @droprate default", 7); // note to translators: any non-numerical value will reset so "default" is safe to translate
        next;
        return;
    }

    // Easter is map-only, so it can be handled here instead of seasons.txt
    function sEaster {

        // Enable event
        set $EVENT$, "Easter";
        logmes "Enabled EASTER event.", LOGMES_ATCOMMAND;
        return;
    }

    // :>
    function superSpawn {
        mesc l("Monster ID, 0 to cancel");
        input .@mobid;
        if (.@mobid <= 0)
            return;
        //.@tmp=strmobinfo(4, .@mobid);
        .@tmp=getmonsterinfo(.@mobid, MOB_MAXHP);
        mesc l("Monster HP, Default %d, use 0 to cancel", .@tmp);
        input .@newhp;
        if (.@newhp <= 0)
            return;

        .@tmp=(getmonsterinfo(.@mobid, MOB_ATK1)+getmonsterinfo(.@mobid, MOB_ATK2))/2;
        mesc l("Monster ATK (varies 10%%), Default %d, use 0 to cancel", .@tmp);
        input .@newak;
        if (.@newak <= 0)
            return;
        .@minatk=.@newak*9/10;
        .@maxatk=.@newak*11/10;

        .@tmp=getmonsterinfo(.@mobid, MOB_DEF);
        mesc l("Monster DEF, Default %d, use 0 to cancel", .@tmp);
        input .@newdf;
        if (.@newdf <= 0)
            return;

        .@tmp=getmonsterinfo(.@mobid, MOB_DEX);
        mesc l("Monster DEX, Default %d, use 0 to cancel", .@tmp);
        input .@newdx;
        if (.@newdx <= 0)
            return;

        .@tmp=getmonsterinfo(.@mobid, MOB_AGI);
        mesc l("Monster AGI, Default %d, use 0 to cancel", .@tmp);
        input .@newag;
        if (.@newag <= 0)
            return;

        .@tmp=getmonsterinfo(.@mobid, MOB_RANGE);
        mesc l("Monster ATK RANGE, Default %d, use 0 to cancel", .@tmp);
        input .@newrg;
        if (.@newrg <= 0)
            return;

        mesc l("Monster SPEED, Player speed is 150, minimum is 100!");
        input .@newspd;
        if (.@newspd < 100)
            return;

        .@tmp$=strmobinfo(1, .@mobid);
        mesc l("Monster NAME, Default %s, empty to cancel", .@tmp$);
        input .@name$;
        if (.@name$ == "")
            return;

        // Spawn
        getmapxy(.@m$, .@x, .@y, 0);
        .@mob=monster(.@m$, .@x, .@y, .@name$, .@mobid, 1);

        // Reconfigure
        setunitdata(.@mob, UDT_MAXHP, .@newhp);
        setunitdata(.@mob, UDT_HP, .@newhp);
        setunitdata(.@mob, UDT_SPEED, .@newspd);
        setunitdata(.@mob, UDT_DEX, .@newdx);
        setunitdata(.@mob, UDT_AGI, .@newag);
        setunitdata(.@mob, UDT_ATKRANGE, .@newrg);
        setunitdata(.@mob, UDT_ATKMIN, .@minatk);
        setunitdata(.@mob, UDT_ATKMAX, .@maxatk);
        setunitdata(.@mob, UDT_DEF, .@newdf);

        logmes "Super spawn!", LOGMES_ATCOMMAND;

        do
        {
            select
                l("Good!"), // 1
                l("Customize attack delay!"), // 2
                l("Customize hit rate!"), // 3
                l("Customize evasion!"), // 4
                l("Modify monster level!"), // 5
                rif($EVENT$ == "Valentine", l("Send them to Valentine Island!")),
                l("Show me a report"), // 7
                l("OMG CANCEL IT!!"); // 8
            mes "";
            switch (@menu) {
            case 2:
                .@tmp=getunitdata(.@mob, UDT_ADELAY);
                mesc l("Default: %d, use 0 to cancel", .@tmp);
                input .@new;
                if (.@new > 0)
                    setunitdata(.@mob, UDT_ADELAY, .@new);
                break;
            case 3:
                .@tmp=getunitdata(.@mob, UDT_HIT);
                mesc l("Default: %d, use 0 to cancel", .@tmp);
                input .@new;
                if (.@new > 0)
                    setunitdata(.@mob, UDT_HIT, .@new);
                break;
            case 4:
                .@tmp=getunitdata(.@mob, UDT_FLEE);
                mesc l("Default: %d, use 0 to cancel", .@tmp);
                input .@new;
                if (.@new > 0)
                    setunitdata(.@mob, UDT_FLEE, .@new);
                break;
            case 5:
                .@tmp=getunitdata(.@mob, UDT_LEVEL);
                mesc l("Default: %d, use 0 to cancel", .@tmp);
                mesc l("WARNING: Might have unexpected side effects!"), 1;
                input .@new;
                if (.@new > 0)
                    setunitdata(.@mob, UDT_LEVEL, .@new);
                break;
            case 6:
                unitwarp(.@mob, "001-12", 132, 119);
                break;
            case 7:
                mesc l("Too lazy.");
                next;
                break;
            case 8:
                mesc l("Are you sure?!"), 1;
                next;
                if (askyesno() == ASK_YES) {
                    logmes "Super spawn aborted!", LOGMES_ATCOMMAND;
                    unitkill(.@mob);
                    return;
                }
                break;
            }

        } while (@menu != 1);

        // Make it a boss
        .@md=getunitdata(.@mob, UDT_MODE);
        setunitdata(.@mob, UDT_MODE, .@md|MD_BOSS);
        logmes "Super spawn completed! (GID "+.@mob+")", LOGMES_ATCOMMAND;
        return;
    }




















    function seasonManagement {
        clear;
        mes l("Current event: @@", $EVENT$);
        select
            ("Disable event"),
            ("Enable Valentine Day"),
            ("Enable St. Patrick Day"),
            ("Enable Easter"),
            ("Enable Worker's Day"),
            ("Enable Thanksgiving"),
            ("Enable Event Horizon"),
            ("Enable Christmas"),
            ("Enable Demure's Birthday"),
            ("[FY] Enable Kamelot"),
            ("[FY] Enable Regnum Blessing"),
            ("[FY] Enable Candor Season"),
            ("Reset Kill Saulc Event (Monthly)");

        switch (@menu) {
            case 1: .@r=playerattached(); sClear(); set $EVENT$, ""; attachrid(.@r); logmes "Disabled events.", LOGMES_ATCOMMAND;
                break;
            case 2:
                set $EVENT$, "Valentine"; $@VALENTINE_LOVELETTER = htnew(); $@VALENTINE_GIFTSTACKS = htnew(); logmes "Enabled VALENTINE DAY event.", LOGMES_ATCOMMAND;
                break;
            case 3:
                $EVENT$="Patrick";
                $@PATRICK_DAYMAX=31;
                enablenpc "sPatrick";
                setnpcdisplay("Aurora", "Patrick Saulc", NPC_WEIRDGREEN);
                enablenpc "St. Patrick Gold Pot";
                donpcevent "St. Patrick Gold Pot::OnForcedMove";
                logmes "Enabled ST. PATRICK DAY event (until day 31).", LOGMES_ATCOMMAND;
                break;
            case 4: sEaster(); break;
            case 5:
                set $EVENT$, "Worker";
                logmes "Enabled WORKERS DAY event.", LOGMES_ATCOMMAND;
                /*
                addmonsterdrop(Snake, Pearl, 10);
                debugmes "Snakes are now dropping Pearls.";
               */
                break;
            case 6:
                $EVENT$="Thanksgiving";
                logmes "Enabled THANKS GIVING event.", LOGMES_ATCOMMAND;
                break;
            case 7:
                $EVENT$="Event";
                logmes "Enabled EVENT (HORIZON) event.", LOGMES_ATCOMMAND;
                break;
            case 8:
                logmes "Enabled CHRISTMAS event.", LOGMES_ATCOMMAND;
                DelQuestFromEveryPlayer(SQuest_Christmas);
                sChristmas(); break;
            case 9:
                $EVENT$=any("Demure Birthday", "Blame Saulc");
                dispbottom("Maybe in future this increases everyone attack speed? Well, for now,");
                dispbottom("This is not actually an event, it just lower prices at Lua GM shop.");
                break;
            case 10:
                $EVENT$="Kamelot";
                logmes "Enabled KAMELOT event.", LOGMES_ATCOMMAND;
                break;
            case 11:
                $EVENT$="Regnum"; callfunc("FYEConf_Regnum");
                dispbottom l("Blessing applied at: %s", $REGNUM_BLESSMAP_H$);
                logmes "Enabled REGNUM event.", LOGMES_ATCOMMAND;
                break;
            case 12:
                $EVENT$="Candor";
                logmes "Enabled CANDOR event.", LOGMES_ATCOMMAND;
                break;
            case 13: DelItemFromEveryPlayer(MurdererCrown); break;
        }

        return;
    }

    do
    {
        clear;
        setnpcdialogtitle l("Event Management");
        mes l("This menu allows you to manage events and gives access to event-related tools.");
        mes "";
        mes l("What do you want to access?");

        select
            l("Experience Rate management"),
            l("Drop Rate management"),
            l("Reset Crazyfefe Fight"),
            l("Change Season Event"),
            rif(is_gm(), l("Spawn customized boss")),
            rif(getarg(0,0), menuimage("actions/home", l("Return to Super Menu"))),
            l("Close");

        //.@c = getarg(0,0) ? 2 : 1; // 1 = back to event menu, 2 = back to super menu

        switch (@menu) {
            case 1: rateManagement; break;
            case 2: dropManagement; break;
            case 3: resetCandor(); break;
            case 4: seasonManagement; break;
            case 5: superSpawn(); break;
            case 7: close; break;
            default: return;
        }

    } while (true);
}



-	script	@event	32767,{
    end;

OnCall:
    if (!is_gm()) {
        end;
    }

    GlobalEventMenu;
    closedialog;
    end;
}

function	script	CMD_toevent	{
    if (!@toeventchk) {
        @toeventval1=readparam(Hp);
        @toeventval2=readparam(Sp);
        @toeventchk=1;
        specialeffect FX_CIRCLE, SELF, getcharid(3);
        addtimer 4000, "@toevent::OnEffect";
    }
    return;
}

-	script	@toevent	32767,{
    end;

OnCall:
    CMD_toevent();
    end;

OnEffect:
    @toeventchk=0;
    removespecialeffect(FX_CIRCLE, SELF, getcharid(3));
    // Calculate
    if (BaseLevel < 10) {
        dispbottom l("You are not strong enough to survive this trip.");
        atcommand "@refresh";
        end;
    } else if (readparam(Sp) < @toeventval2) {
        dispbottom l("You must not be using mana to do this trip.");
        atcommand "@refresh";
        end;
    } else if (readparam(Hp) < @toeventval1) {
        dispbottom l("You cannot be fighting to do this trip.");
        atcommand "@refresh";
        end;
    } else if (compare(getmapname(), "001-") || (getmapinfo(MAPINFO_ZONE, getmap())) == "MMO") {
        dispbottom l("You are already at the Mana Plane of Existence.");
        atcommand "@refresh";
        end;
    } else if (getmapname() == "boss" || getmapname() == "sec_pri" || compare(getmapname(), "000-") || compare(getmapname(), "008-") || compare(getmapname(), "sore")) {
        dispbottom l("The Mana Plane is currently out of reach.");
        atcommand "@refresh";
        end;
    } else {
        if ($@MK_SCENE == MK_SIEGE_TULIM || BaseLevel > 20) {
            // Monster King events take precedence over Aeros Event
            switch ($@MK_SCENE) {
            case MK_SIEGE_TULIM:
                warp "003-1", 40, 49;
                specialeffect(63, AREA, getcharid(3));
                end;
            case MK_SIEGE_HALIN:
                warp "009-1", 27, 30;
                specialeffect(63, AREA, getcharid(3));
                end;
            case MK_SIEGE_HURNS:
                warp "012-1", 87, 70;
                specialeffect(63, AREA, getcharid(3));
                end;
            case MK_SIEGE_NIVAL:
                warp "020-1", 57, 62;
                specialeffect(63, AREA, getcharid(3));
                end;
            }
        }

        // Aeros Events takes precedence over player events
        if ($@GM_EVENT)
        {
            .@gt=$@AEROS_SPWN;
            if (.@gt == 2)
                .@gt=rand(0,1);
            switch (.@gt) {
            case 0:
                warp "001-1", 235, 26; break;
            case 1:
                warp "001-1", 23, 108; break;
            }
            specialeffect(63, AREA, getcharid(3));
            end;
        }

        // Player events takes precedence over permanent events
        // They also have an hierarchy
        // 1. Candor Battle
        if ($@FEFE_DELAY > gettimetick(2) && !$@FEFE_CAVE_LEVEL && $@FEFE_CAVE_HERO$ != "" && !$@FEFE_WAVE) {
            warp "006-1", 49, 53;
            message strcharinfo(0), l("You are now at Candor Battle Cave at @@'s request.", $@FEFE_CAVE_HERO$);
            end;
        }

        // Events are the least priority
        if ($EVENT$ == "Valentine") {
            warp "001-11", 38, 32;
            message strcharinfo(0), l("You are now at the Valentine Highlands.");
            end;
        }
        if ($EVENT$ == "Easter") {
            warp "001-4", 151, 157;
            message strcharinfo(0), l("You are now at the Magical Forest.");
            end;
        }
        if ($EVENT$ == "Worker") {
            warp "001-5", 22, 79;
            message strcharinfo(0), l("You are now at the Contributor's Cave.");
            end;
        }
        if ($EVENT$ == "Christmas" && BaseLevel >= 20) {
            warp "019-4-1", 32, 36;
            message strcharinfo(0), l("You are now at the Christmas Workshop.");
            end;
        }
    }

    // Block here
    if (!$@GM_EVENT && !$@MK_SCENE && $EVENT$ == "") {
        atcommand "@refresh";
        dispbottom l("The mana bridge is closed at the moment.");
        end;
    }

    // Seems like it was a Blame Saulc-like event...
    dispbottom l("Have a nice @@ day!", $EVENT$);
    dispbottom l("The mana bridge is closed at the moment.");
    atcommand "@refresh";
    end;

OnInit:
    bindatcmd "event", "@event::OnCall", 99, 99, 0;
    bindatcmd "toevent", "@toevent::OnCall", 0, 99, 0;
}
