// TMW2 Script
//
// @shroom <mobID> <Amount> <DisplayName>
// Plushroom Angel Script (Plush: 1011, Chaga: 1128, Clover: 1028)

-	script	@shroom	32767,{
    end;

OnShroom:
    if (.@atcmd_parameters$[0] != "")
        .@mobId=atoi(array_shift(.@atcmd_parameters$));
    if (.@atcmd_parameters$[1] != "")
        .@mobAm=atoi(array_shift(.@atcmd_parameters$));
    if (.@atcmd_parameters$[2] != "")
    	.@mobName$=implode(.@atcmd_parameters$, " ");

    // Checks
    if (!.@mobId)
        .@mobId=PlushroomField;

    if (getmonsterinfo(.@mobId, MOB_MODE) & MD_BOSS)
        Exception("Boss monsters cannot be summoned.", RB_DISPBOTTOM | RB_ISFATAL);

    if (!.@mobAm)
        .@mobAm=1;

    if (.@mobName$ == "")
        .@mobName$=strmobinfo(1, .@mobId);

    //.@gmType=(Sex ? NPC_GAMEMASTER : NPC_GAMEMISTRESS);
    .@gmType=(Sex ? GameMaster : GameMistress);
    .@gmId=monster("boss", 45, 45, strcharinfo(0), .@gmType, 1);

    // Max 40 connected players for this to work
    .@c = getunits(BL_PC, .@players, MAX_CYCLE_PC);
    for (.@i = 0; .@i < .@c; .@i++) {
        debugmes "@shroom: Attach account %d to spawn %d %s (%d)", .@players[.@i], .@mobAm, .@mobName$, .@mobId;
        attachrid(.@players[.@i]);
        getmapxy(.@m$, .@x, .@y, 0);
        unitwarp(.@gmId, .@m$, .@x, .@y);
        sleep2(20);
        unitwalk(.@gmId, .@x-1, .@y-1);
        sleep2(200);
        unitemote(.@gmId, any(E_HAPPY, E_HAPPY, E_ANGEL, E_EVILCROC));
        sleep2(150);

        // Adjust amount based on player level
        .@Ammo=limit(1, BaseLevel/15*.@mobAm, 60);
        //getmapxy(.@dm$, .@dx, .@dy, UNITTYPE_MOB, .@gmId);
        //debugmes "Spawn %d mobs at %s, (%d, %d)", .@Ammo, .@dm$, .@dx, .@dy;

        areamonster(.@m$, .@x-3, .@y-3, .@x+3, .@y+3, .@mobName$, .@mobId, .@Ammo);
        sleep2(320);
        detachrid();
    }

    // Cleanup
    unitwarp(.@gmId, "boss", 45, 45);
    unitkill(.@gmId);
    end;

OnInit:
    bindatcmd "shroom", "@shroom::OnShroom", 60, 100, 1;
}

