// TMW2 Script
//
// @rentitem <item> <time>
// Rents an <item> for <time> seconds
// 1 hour: 3600s
// 1 day: 86400s
// 1 month: 2592000s

-	script	@rentitem	32767,{
    end;

OnCall:
    //.@request$ = "";
	//.@request$ += implode(.@atcmd_parameters$, " ");

    .@item=atoi(.@atcmd_parameters$[0]);
    .@time=atoi(.@atcmd_parameters$[1]);

    if (!.@item || !.@time) {
        dispbottom l("@rentitem <item numeric id> <time in seconds>");
        end;
    }

    // Limiting to 365 days max
    .@time=limit(15, .@time, 31536000);

    rentitem(.@item, .@time);
    end;

OnInit:
    bindatcmd "rentitem", "@rentitem::OnCall", 80, 99, 1;
    end;
}

