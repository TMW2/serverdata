// TMW2 Script
// Author:
//    Jesusalva
// Description:
//    Welcome to all new players on ML! :D

-	script	@welcome	32767,{
    end;

OnCall:
    if (!@toeventchk) {
        @toeventval1=readparam(Hp);
        @toeventval2=readparam(Sp);
        @toeventchk=1;
        specialeffect FX_CIRCLE, SELF, getcharid(3);
        addtimer 4000, "@welcome::OnEffect";
    }
    end;

OnEffect:
    @toeventchk=0;
    removespecialeffect(FX_CIRCLE, SELF, getcharid(3));
    // Reversions (warp back)
    // Not in Candor: Resave your @welcome reversion snippet
    if (getmap() != "005-1") {
        getmapxy(@welc_map$, @welc_x, @welc_y, 0);
        @welc_loc$=LOCATION$;
    } else {
        // It's defined and you're in Candor, so: Warp back
        if (@welc_x && @welc_y) {
            warp @welc_map$, @welc_x, @welc_y;
            @welc_map$="";
            @welc_x=0;
            @welc_y=0;
            dispbottom l("%s: Thanks for helping.", "Nard");
            if (@welc_loc$ != "")
                LOCATION$=@welc_loc$;
            end;
        }
        // There's no saved coordinates but you're in Candor.
        // So warp you back to... Candor? I mean, what?
    }

    // Calculate
    if ($@WELCOME_TIMER < gettimetick(2)) {
        dispbottom l("There are no new players to welcome.");
        atcommand "@refresh";
        end;
    } else if (readparam(Sp) < @toeventval2) {
        dispbottom l("You must not be using mana to do this trip.");
        atcommand "@refresh";
        end;
    } else if (readparam(Hp) < @toeventval1) {
        dispbottom l("You cannot be fighting to do this trip.");
        atcommand "@refresh";
        end;
    } else if (compare(getmapname(), "001-") || (getmapinfo(MAPINFO_ZONE, getmap())) == "MMO") {
        dispbottom l("You currently could not use GM MAGIC to visit Candor.");
        atcommand "@refresh";
        end;
    } else if (getmapname() == "boss" || getmapname() == "sec_pri" || compare(getmapname(), "000-") || compare(getmapname(), "008-") || compare(getmapname(), "005-") || compare(getmapname(), "006-") || compare(getmapname(), "sore")) {
        dispbottom l("You currently could not use GM MAGIC to visit Candor.");
        atcommand "@refresh";
    } else {
        LOCATION$="Candor";
        warp "005-1", 43, 99;
        message strcharinfo(0), l("You are now at Candor.");
    }
    end;

OnInit:
    bindatcmd "welcome", "@welcome::OnCall", 0, 99, 0;
}

