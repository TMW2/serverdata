// Evol Script
// Author: Gumi, Monwarez

function	script	GlobalDebugMenu	{
    function resetAll {
        function doReset {
            resetstatus;
            resetskill;
            resetlvl 1;
            dispbottom b("ALL PERMANENT STATUS BOOSTS WERE ALSO RESET");
            dispbottom l("Reset done!");
            if (getarg(0,0) == 3) {
                closedialog;
                doevent "::OnGlobalQuestReset"; // executes in all quest npcs // FIXME: maybe have a `resetquest` buildin?
                Zeny = 0;
                BankVault = 0;
                clearitem;
                warp "000-0", 0, 0; // starting point
                end; // script must end for doevent to execute
            }
            return;
        }
        clear;
        setnpcdialogtitle l("Debug - Reset");
        mes l("What do you want to reset?");
        select
            l("Abort"),
            l("Reset stats, skills, level"),
            l("Reset EVERYTHING"),
            rif(is_admin() && debug && !@allperms, l("Set All Perms")),
            l("Return to Debug menu");

        switch (@menu) {
            case 2:
            case 3: doReset @menu; return;
            case 4:
                if (!debug) atcommand("@ban 1h "+strcharinfo(0));
                if (!is_admin()) atcommand("@ban 2h "+strcharinfo(0));
                atcommand("@addperm all_skill");
                atcommand("@addperm all_equipment");
                atcommand("@addperm skill_unconditional");
                atcommand("@addperm join_chat");
                atcommand("@addperm hide_session");
                atcommand("@addperm any_warp");
                atcommand("@addperm view_hpmeter");
                atcommand("@addperm view_equipment");
                atcommand("@addperm receive_requests");
                atcommand("@addperm can_trade_bound");
                atcommand("@addperm bypass_nostorage");
                @allperms=true;
                break;
        }

        return;
    }

    do
    {
        clear;
        setnpcdialogtitle l("Debug");
        mes l("This menu allows you to modify your account data.");
        mes "";
        mes l("What do you want to do?");
        select
            l("Change my appearance"),
            l("Change my quests"),
            l("Reset"),
            rif(getarg(0,0), l("Return to Super Menu"));

        .@c = getarg(0,0) ? 2 : 1;

        switch (@menu)
        {
            case 1: BarberDebug .@c; break;
            case 4: GlobalQuestDebug .@c; break;
            case 5: resetAll; break;
            case 6: return;
        }
    } while(1);
}



-	script	@debug	32767,{
    end;

OnCall:
    if (!is_admin()) {
        end;
    }
    GlobalDebugMenu();
    closedialog;
    end;

OnSetVar:
    if (getarraysize(.@atcmd_parameters$) != 3)
        Exception("Usage: @set-var VARIABLE INDEX VALUE", RB_DISPBOTTOM|RB_ISFATAL);

    .@cmd$=array_shift(.@atcmd_parameters$);
    .@idx=atoi(array_shift(.@atcmd_parameters$));
    if (charat(.@atcmd_parameters$[0],
               getstrlen(.@atcmd_parameters$[0])-1) == "$")
        .@str=true;

    if (.@str)
        .@val$=array_shift(.@atcmd_parameters$);
    else
        .@val=array_shift(.@atcmd_parameters$);

    if (.@str)
        setd(sprintf("%s[%d]", .@cmd$, .@idx), .@val$);
    else
        setd(sprintf("%s[%d]", .@cmd$, .@idx), .@val);

    .@msg$=sprintf("%s[%d] is now: %s", .@cmd$, .@idx,
                   getd(sprintf("%s[%d]", .@cmd$, .@idx)));

    if (!is_staff())
        atcommand("@request System Information: "+.@msg$);
    else
        dispbottom(.@msg$);
    end;

// If the char is not a staff member, it'll be sent to GM Log instead
OnGetVar:
    if (getarraysize(.@atcmd_parameters$) != 2)
        Exception("Usage: @get-var VARIABLE INDEX", RB_DISPBOTTOM|RB_ISFATAL);

    .@cmd$=array_shift(.@atcmd_parameters$);
    .@idx=atoi(array_shift(.@atcmd_parameters$));

    .@mg$=sprintf("%s[%d] == %s", .@cmd$, .@idx,
                      getd(sprintf("%s[%d]", .@cmd$, .@idx)));

    if (!is_staff())
        atcommand("@request System Information: "+.@mg$);
    else
        dispbottom(.@mg$);
    end;

OnSClear:
    sc_end SC_ALL;
    sc_end SC_DAILYSENDMAILCNT;
    dispbottom l("Status Condition Cleared");
    end;

OnInit:
    bindatcmd "debug", "@debug::OnCall", 99, 99, 1;
    bindatcmd "getvar", "@debug::OnGetVar", 80, 99, 1;
    bindatcmd "get-var", "@debug::OnGetVar", 80, 99, 1;
    bindatcmd "setvar", "@debug::OnSetVar", 99, 99, 1;
    bindatcmd "set-var", "@debug::OnSetVar", 99, 99, 1;
    bindatcmd "sclear", "@debug::OnSClear", 99, 99, 1;
    end;
}

