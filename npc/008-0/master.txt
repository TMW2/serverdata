// TMW-2 Script
// Author:
//  Jesusalva
// Description:
//  This NPC is a work on progress. It measures all players equal.
//  Controls the whole dungeon feature.

008-0,47,63,0	script	Party Master	NPC_BRGUARD_SWORD,{
    if (!is_staff())
        goto L_TODO; // TODO

    mesn;
    if (getcharid(1) > 0) {
	    mesq l("You're in the \"@@\" party, very good!", getpartyname(getcharid(1)));
        next;
    } else {
        mesq l("I protect a dungeon for PARTIES. You're not on a party, get moving.");
        close;
    }

    mesn;
    mesq l("I protect a very dangerous dungeon, and it is so dangerous, that only parties can go in.");
    next;
    if (strcharinfo(0) != getpartyleader(getcharid(1))) goto L_NotYou;
    mesn;
    mesq l("There are five floors, and they're all very dangerous. But there are riches to be found.");
    next;
    mesn;
    mesq l("You also can't stay there forever! You will have about 20 minutes to entirely clear it out and defeat the last boss.");
    if (!party_expon(getcharid(1)))
        mesc l("Note: Your party is currently not sharing experience, and will suffer a time penalty. Your time will be halved."), 6;
    next;
    // Get info about your party, and backup it
    getpartymember(getcharid(1));
	.@count = $@partymembercount;
	copyarray(.@name$[0], $@partymembername$[0], $@partymembercount);
	copyarray(.@aid[0], $@partymemberaid[0], $@partymembercount);
	copyarray(.@cid[0], $@partymembercid[0], $@partymembercount);
    .@count_online=0;

    if (.@count < 3 && !$@GM_OVERRIDE) goto L_TooSmall;
    mesn;
    mesc l("Are you and your party ready?"), 1;
    if (askyesno() != ASK_YES)
        close;

    // Loop though party to see if you can start
	for (.@i = 0; .@i < .@count; ++.@i) {
        // Online?
		if (isloggedin(.@aid[.@i], .@cid[.@i])) {
            getmapxy(.@m$, .@x, .@y, 0, .@name$[.@i]);
            // Here?
            if (.@m$ == .map$)
    			.@count_online++;
        }
    }

    // How many are logged in and here?
    if (.@count_online < 3 && !$@GM_OVERRIDE) goto L_TooSmall;

    // TODO: Query if exp sharing is enabled
    // TODO: Instance for party

    // Warp everyone and add timers
    partytimer("008-0", 1000, "Party Master::OnStart", getcharid(1));
    warpparty("008-1", 176, 20, getcharid(1), "008-0", true);

    // One bonus time
    monster("008-1", 90, 69, "Time Bonus", Mouboo, 1, "Party Master::OnMobTime");

    // Four Chests
    monster("008-1", 38, 104, "Mysterious Chest", any(BronzeChest, BronzeMimic), 1);
    monster("008-1", 41, 104, "Mysterious Chest", any(BronzeChest, BronzeMimic), 1);
    monster("008-1", 44, 104, "Mysterious Chest", any(BronzeChest, BronzeMimic), 1);
    monster("008-1", 47, 104, "Mysterious Chest", any(BronzeChest, BronzeMimic), 1);

    // We still need the SummonBoss NPC and etc.
    close;









L_TODO:
    mesn;
    mesq l("Jesusalva still have this closed because the traps are broken... %%n But we shall open soon. %%G");
    next;
    mesn;
    mesq l("It's not hard to repair, but nobody is bothering Jesusalva on #world as of late. %%n");
    mesq l("Because this, the national budget is going to other silly, under-rewarding, minor things...");
    mesc l("If you think this should be a priority, please ask Jesusalva."), 1;
    close;

// Only the party leader can start this. But you can rejoin, as long map leader is on 008-x
L_NotYou:
    getmapxy(.@m$, .@x, .@y, 0, getpartyleader(getcharid(1)));
    if (.@m$ ~= "008-*" &&
        .@m$ != "008-0" &&
        BaseLevel > 0 &&
        @pmloop) {
        mesn;
        mesq l("@@, your party leader, is inside, I'm not sure where.", getpartyleader(getcharid(1)));
        mesc l("Enter dungeons? You won't be able to join anymore when he dies."), 1;
        if (askyesno() != ASK_YES)
            close;
        // Double check
        getmapxy(.@m$, .@x, .@y, 0, getpartyleader(getcharid(1)));
        if (.@m$ ~= "008-*" && .@m$ != "008-0" && getcharid(1) > 0)
            warp "008-1", 176, 20;
    } else {
        mesn;
        mesq l("If you bring me @@, your party leader, I can let you in.", getpartyleader(getcharid(1)));
    }
    close;

// Minimum 3 players
L_TooSmall:
    mesn;
    mesq l("However, I need to see at least three volunteers here, to allow you in.");
    close;



// Main Loop
OnStart:
    @pmloop=0;
OnLoop:
    @pmloop+=1;
    .@lost="";

    // Anti-Crazyfefe™ Alpha System
    if (getcharid(1) <= 0) {
        // Left the party!
        .@lost=l("You are not a member of a party anymore.");
    }
    if (!party_expon(getcharid(1))) {
        // Party exp sharing disabled means time penalty.
        @pmloop+=1;
    }

    // Check if party master still alive and in caves.
    if (getmapxy(.@m$, .@x, .@y, 0, getpartyleader(getcharid(1))) <= 0)
        .@lost=l("Party leader is gone.");
    else if (!(.@m$ ~= "008-*"))
        .@lost=l("Party leader is not on dungeons.");

    // Add new cycle or finish.
    if (@pmloop < 1200 && .@lost == "")
        addtimer(1000, "Party Master::OnLoop");
    else
        .@lost=l("You ran out of time...");

    // See if it is time to finish
    if (.@lost != "") {
        @pmloop=0;
        warp "008-0", 47, 64;
        dispbottom str(.@lost);
    }
    end;

OnMobTime:
    getmapxy(.@m$, .@x, .@y, 0);
    areatimer(.@m$, .@x-4, .@y-4, .@x+4, .@y+4, 10, "Party Master::OnAddTime");
    end;

OnAddTime:
    @pmloop=60;
    end;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}

