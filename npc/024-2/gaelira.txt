// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Questmaker Handbook 14.0 (F = Favorable)

024-2,41,22,0	script	Gaelira	NPC_ELF_F,{
    .@q = getq(FrostiaQuest_Gaelira);
    if (BaseLevel < 52) {
        mesc l("The elf seems busy. She ignores you.");
        close;
    }
    if (.@q == 0) {
        if (frally()) goto L_Start_F;
        else goto L_Start_U;
    }
    else if (.@q == 1) {
        if (frally()) goto L_Submit_F;
        else goto L_Submit_U;
    }
    else if (.@q == 2) {
        if (frally()) goto L_Done_F;
        else goto L_Done_U;
    }
    Exception("Invalid quest state - "+str(.@q));
    end;

L_Start_F:
    mesn;
    mesq lg("Hey friend, could you do me a little favor?");
    next;
    mesn;
    mesq l("You see, I love %s! So shiny, so bright... They are my favorite!", getitemlink(Pearl));
    next;
    mesn;
    mesq l("So, if you could bring me %d of them, that would be totally awesome! Pretty please! I'm counting on you!", 20);
    setq FrostiaQuest_Gaelira, 1;
    close;

L_Submit_F:
    mesn;
    mesq l("Did you got the %d %s I've asked?", 20, getitemlink(Pearl));
    next;
    if (askyesno() == ASK_YES) {
        if (countitem(Pearl) < 20) {
            mesn;
            mesq l("You've been spending too much time with humans. You are picking up bad habits from them like lying. Be careful.");
            close;
        }
        inventoryplace AncientBlueprint, 1;
        delitem Pearl, 20;
        getitem AncientBlueprint, 1;
        Zeny+=getiteminfo(Pearl, ITEMINFO_SELLPRICE)*20*12/10; // 120% payoff
        getexp 15000, 0;
        setq FrostiaQuest_Gaelira, 2;
        mesn;
        mesq l("Thank you soooo much! Here is some compensation, and you can keep this worthless scroll of paper! I looooove Pearls!");
    }
    close;

L_Done_F:
    mesn;
    mesq l("Hey there! How are you? I hope you're doing fine!");
    next;
    mesn;
    mesq l("Did you knew about the legend of Aethyr? I've heard the path to it was somewhere to the west of our old village... And some stuff about passwords... Hehe, I don't remember!");
    next;
    mesn;
    mesq l("But I can't help but keep thinking on how many shiny treasures might be there!");
    mesc l("She mumbles something about how a custom of using crystals to mark paths was lost, and how they would be valuable if sold.");
    close;
/////////////////////////////////////////////////////////////////////////////////
L_Start_U:
    mesn;
    mesq l("Hey %s, could you do me a little favor?", get_race());
    next;
    mesn;
    mesq l("You see, I love %s! So shiny, so bright... They are my favorite!", getitemlink(Pearl));
    next;
    mesn;
    mesq l("So, if you could bring me %d of them, that would be totally awesome! I'm counting on you!", 20);
    setq FrostiaQuest_Gaelira, 1;
    close;

L_Submit_U:
    mesn;
    mesq l("Did you got the %d %s I've asked?", 20, getitemlink(Pearl));
    next;
    if (askyesno() == ASK_YES) {
        if (countitem(Pearl) < 20) {
            mesn;
            mesq l("You liar, I'll teach you a lesson you won't forget!");
            mesc l("%s stabs you with a dagger!", .name$);
            percentheal -55, 0;
            close;
        }
        inventoryplace AncientBlueprint, 1;
        delitem Pearl, 20;
        getitem AncientBlueprint, 1;
        Zeny+=getiteminfo(Pearl, ITEMINFO_SELLPRICE)*20*12/10; // 120% payoff
        getexp 15000, 0;
        setq FrostiaQuest_Gaelira, 2;
        mesn;
        mesq l("Thank you! Here is some compensation, and you can keep this worthless scroll of paper! I looooove Pearls!");
    }
    close;

L_Done_U:
    mesn;
    mesq l("Hey there! How are you?");
    next;
    mesn;
    mesq l("Did you knew about the legend of Aethyr? I've heard the path to it was somewhere to the west of our old village... And some stuff about passwords... I'm not sure if I remember!");
    next;
    mesn;
    mesq l("Well, I can't help but keep thinking on how many shiny treasures might be there!");
    mesc l("She mumbles something about how a custom of using crystals to mark paths was lost, and how they would be valuable if sold.");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonSkirt);
    setunitdata(.@npcId, UDT_HEADMIDDLE, AssassinChest);
    setunitdata(.@npcId, UDT_HEADTOP, BunnyEars);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 10);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 6);
    npcsit;

    .sex = G_FEMALE;
    .distance = 5;
    end;
}

