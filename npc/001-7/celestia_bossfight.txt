// TMW2 Scripts
// Author:
//   Jesusalva
// Description:
//   Celestia Yeti King's quest. This controls the final showdown, and brings you
//   back home safely.
//
//      If you cheated your way to here, you won't be able to interact with it.
//      No other safety measures are in place. Lone players cannot challenge the
//      Yeti King, there must be at least 2 players there to do the challenge.
//      BEWARE, the Yeti King gains stronger poisons the more people are attacking him.
//
//      $@GM_OVERRIDE allows a single player to challenge him, as usual with all
//      co-op scripts.
//
//      If you do not challenge him, the chance to challenge him again is lost.

001-7,33,39,0	script	#YetiKing	NPC_YETI_KING,0,0,{
    .@q=getq(HurnscaldQuest_Celestia);
    if (.@q == 5 && !mobcount(.map$, "#YetiKing::OnVictory")) goto L_Survivor;
    if (.@q == 6) goto L_GoHome;
    end;

L_GoHome:
    .@MLPQuest=( (##02_MLWORLD & MLP_TMW_CELESTIA) &&
                !(##02_MLWORLD & MLP_TMW_YETIKING) &&
                getvaultid());
    if (.@MLPQuest) {
        mesn strcharinfo(0);
        mesq l("Actually, have you ever heard of Yeti's kidnapping little girls?");
        next;
        goto L_VaultQuest;
    }
    mesc l("Go home now?");
    if (askyesno() == ASK_YES)
        warp "003-1-1", 94, 22;
    closedialog;
    if (!getareausers("001-7", 7))
        setnpcdisplay .name$, NPC_YETI_KING;
    close;

L_Survivor:
    if (!YETIKING_WINNER)
        YETIKING_WINNER = gettimetick(2);
    if ($YETIKING_WINNER$ == "") {
        $YETIKING_WINNER$=strcharinfo(0);
        channelmes("#world", $YETIKING_WINNER$+" is the first player to finish Yeti King Quest!! GG, dude! %%N");
        announce "All hail ##B"+$YETIKING_WINNER$+"##b, first to complete the ##3Yeti King Quest!", bc_all|bc_npc;
        getexp 0, 2000;
        getitem PrismGift, 1;
        mesc l("CONGRATULATIONS! You are the first player to finish Yeti King quest!!"), 2;
        mesc l("You just gained a Prism Gift, and 2000 Job Exp for your bravery!"), 2;
        next;
    }
    mesn col(l("The Yeti King"), 3);
    mesq l("Good job, kid. You've survived both the Cave Of Trials and Soren's Village.");
    next;
    mesn col(l("The Yeti King"), 3);
    mesq l("That was only to prove you're strong enough on yourself to do whatever you want to do. You have friends.");
    next;
    mesn col(l("The Yeti King"), 3);
    mesq l("In this world, your friends are your strength. You deserve a reward for the victory, please choose whatever you want.");
    select
        l("I want a gemstone or ore"),
        rif(!countitem(MirrorLakeArmor), l("I want experience")),
        l("I want gold"),
        l("I want coal"),
        l("I want monster points");

    mes "";
    .@r=rand2(1,100)+(@YetiKing_Challenger*5);
    switch (@menu) {
        case 1:
            if (.@r > 70)
                getitem rand2(Diamond, Amethyst), 1;
            else
                getitem rand2(CopperOre, (REBIRTH ? IridiumOre : TitaniumOre)), any(1,2);
            break;
        case 2:
            .@r+=BaseLevel;
            getexp .@r*80, .@r*2; // max 8000 xp and 200 jp (level 0)
            break;
        case 3:
            .@r+=JobLevel;
            Zeny=Zeny+.@r*75; // max 7500 gp (job 0)
            break;
        case 4:
            getitem Coal, (.@r/10); // max 10 coal
            break;
        case 5:
            .@r+=(BaseLevel+JobLevel)/2;
            Mobpt+=.@r*5; // max 500 mobpt (base/job 0)
            break;
    }
    // Completion bonus
    getexp 0, 2500;
    getitem StrangeCoin, 1;
    compareandsetq HurnscaldQuest_Celestia, 5, 6;
    mesn col(l("The Yeti King"), 3);
    mesq l("Here kid. Frostia, the elf town, is somewhere near here, but I'm not sure if you can reach it from here.");
    next;
    if ($@CINDY_STATE > gettimetick(2)) {
        mesn col(l("The Yeti King"), 3);
        mesq l("Some rogue Yetis are trying to escape to Nivalis. I can't hold them back for more than @@.", FuzzyTime($@CINDY_STATE+rand2(5,95)));
        next;
    }
    mesn col(l("The Yeti King"), 3);
    mesq l("I can warp you home now.");
    mes "";
    .@MLPQuest=( (##02_MLWORLD & MLP_TMW_CELESTIA) &&
                !(##02_MLWORLD & MLP_TMW_YETIKING) &&
                getvaultid());
    select
        rif(!.@MLPQuest, l("Please, bring me back home.")),
        rif((getareausers("001-7", 7) > 1 || $@GM_OVERRIDE) && !mobcount(.map$, "#YetiKing::OnVictory") && @YetiKing_Challenger, l("No, we challenge you to a duel!")),
        rif(.@MLPQuest, l("Actually, have you ever heard of Yeti's kidnapping little girls?")),
        l("I'll walk around here a little more.");

    mes "";
    switch (@menu) {
        case 1:
            warp "003-1-1", 94, 22;
            break;
        case 2:
            compareandsetq HurnscaldQuest_Celestia, 6, 7;
            mesn col(l("The Yeti King"), 3);
            mesq l("Foolish kids, do you think violence is the answer to everything?!");
            next;
            mesn col(l("The Yeti King"), 3);
            mesq l("I give you five minutes to defeat me. Witness my wrath!");
            if (mobcount(.map$, "#YetiKing::OnVictory"))
                close;
            setnpcdisplay .name$, NPC_NO_SPRITE;
            npctalk l("*Roaaaaaar!*");
            monster .map$, .x, .y, strmobinfo(1, YetiKing), YetiKing, 1, "#YetiKing::OnVictory";
            initnpctimer;
            break;
        case 3:
            goto L_VaultQuest;
    }
    close;

OnVictory:
    stopnpctimer;
    setnpcdisplay .name$, NPC_SUMMONING_CIRC;
    //Karma=Karma+1;
    .@mpt = 80000 / min(8, TOP3AVERAGELVL() / 5);
    Mobpt+=.@mpt;
    getitem StrangeCoin, 2;
    getmapxy(.@m$, .@x, .@y, 0);
    makeitem(StrangeCoin, 1, .@m$, .@x+rand2(-1,1), .@y+rand2(-1,1));
    npctalk l("Good job... You can keep the drops. Touch here to return home.");
    areatimer "001-7", 20, 20, 141, 171, 10, "#YetiKing::OnDefeat";
    donpcevent "Celestia::OnClock0002";
    fix_mobkill(YetiKing);
    end;

// This allows the challenger to go back home without dying.
OnDefeat:
    getexp 0, 100;
    compareandsetq HurnscaldQuest_Celestia, 7, 6;
    X24Event(X24_CELESTIA);
    end;

OnTimer60000:
    npctalk "Time left: 4 minutes";
    end;

OnTimer120000:
    npctalk "Time left: 3 minutes";
    end;

OnTimer180000:
    npctalk "Time left: 2 minutes";
    end;

OnTimer240000:
    npctalk "Time left: 1 minute";
    end;

OnTimer270000:
    npctalk "Time left: 30 seconds";
    end;

OnTimer290000:
    npctalk "Time left: 10 seconds";
    end;

OnTimer300000:
    npctalk "Time is up!";
    areatimer "001-7", 20, 20, 141, 171, 10, "#YetiKing::OnDefeat";
    killmonster(.map$, "#YetiKing::OnVictory"); // I could use "All" as label, too
    setnpcdisplay .name$, NPC_YETI_KING;
    end;

// Hourly, check if there are players and fix the sprite
OnMinute17:
    if (!getareausers("001-7", 21))
        setnpcdisplay .name$, NPC_YETI_KING;
    end;

L_VaultQuest:
    mesn col(l("The Yeti King"), 3);
    mesq l("Every once in a while, but I guess this is not common on your world, am I right.");
    next;
    select
        l("I came from The Mana World."),
        l("That's right, this is unheard of where I come from."),
        l("...How do you know I'm not from this world?");
    mes "";
    mesn col(l("The Yeti King"), 3);
    mesq l("I'm not unfamiliar with the children of Merlin, thosem whom cross the Mirror Lake. In case of The Mana World, you're lucky, we're parallel, meaning we share lots of things in common.");
    next;
    mesn col(l("The Yeti King"), 3);
    mesq l("Now, I'm sure you could find the answer you seek without the trouble of coming here, but anyway. I guess I'll explain you how things work here, first.");
    next;
    mesn col(l("The Yeti King"), 3);
    mesq l("In this world, Angela married with the Blue Sage. I had an... incident, with the Blue Sage, which is better forgetten. Anyway, seeking to cause a political instability, opposing Yetis every once in a while kidnap their daughter, Cindy.");
    next;
    mesn col(l("The Yeti King"), 3);
    mesq l("The trick at tracing parallel, is finding the difference. In this world, Cindy gives a %s, a personal belonging of her, to those whom rescue her. What is the reward on your world?", getitemlink(Earmuffs));
    next;
    select
        l("I got a Wizard Hat."),
        l("I got a Wooden Staff.");
    mes "";
    mesn col(l("The Yeti King"), 3);
    mesq l("That's your answer. Cindy is not a mage, right? This means some mage has bewitched the Yetis to do so, and if my parallel theory is correct, they're either aiming at the Blue Sage, or at her father.");
    next;
    mesn col(l("The Yeti King"), 3);
    mesq l("Therefore, children of Merlin, go back to your world, and ask the Blue Sage Nikolai about it. The blue sage may fake angerness or try to dodge the question, but they are a good person. Still, you should ensure you're on his good side.");
    next;
    mesn col(l("The Yeti King"), 3);
    mesq l("If you're still not confident enough, just tell him this: %s", col(l("*whisper whisper*"), 9));
    next;
    mesn col(l("The Yeti King"), 3);
    mesq l("Are you ready to cross the Mirror Lake and return to your world?");
    next;
    if (askyesno() == ASK_YES) {
        ##02_MLWORLD=##02_MLWORLD|MLP_TMW_YETIKING;
        MirrorLakeSendTo(MLP_TMW, 0);
    }
    close;
}





