// TMW2 Script
// Author:
//    Ernando <ernando.quirino@hotmail.com> (Creator)
//    Jesusalva <admin@tmw2.org>
// Description:
//    Hunger Games™ version for The Mana World Brazil v2, created by Ernando Quirino.

001-8,0,0,0	script	#QuirinoHUB	NPC_HIDDEN,{
    end;

    // HUBspawn( mobID, amount )
    function HUBspawn {
        areamonster "001-8", 20, 20, 80, 80, strmobinfo(1, getarg(0)), getarg(0), getarg(1);
        return; }

    // HUBscatter( itemID )
    function HUBscatter {
        makeitem(getarg(0), 1, "001-8", rand(20,80), rand(20,80));
        return; }

    // HUBarrow()
    function HUBarrow {
        makeitem(rand(TrainingArrow,BoneArrow), rand(10,30), "001-8", rand(20,80), rand(20,80));
        return; }

    // HUBrandwpn( full )
    function HUBrandwpn {
        if (getarg(0)) {
            .@x=rand(20,80); .@y=rand(20,80);
        } else {
            .@x=rand(44,55); .@y=rand(46,54);
        }

        .@r=rand(1,8);
        switch(.@r){
        case 1:
        makeitem(rand(Knife, WoodenSword), 1, "001-8", .@x, .@y); break;
        case 2:
        makeitem(rand(SharpKnife, Dagger), 1, "001-8", .@x, .@y); break;
        case 3:
        makeitem(rand(ThunderStaff,Judgment), 1, "001-8", .@x, .@y); break;
        case 4:
        makeitem(rand(BronzeGladius,Scythe), 1, "001-8", .@x, .@y); break;
        case 5:
        makeitem(rand(WoodenBow, ElficBow), 1, "001-8", .@x, .@y); break;
        case 6:
        makeitem(rand(WoodenBow, ShortBow), 1, "001-8", .@x, .@y); break;
        case 7:
        makeitem(rand(ForestBow, BansheeBow), 1, "001-8", .@x, .@y); break;
        default:
        makeitem(WoodenShield, 1, "001-8", .@x, .@y); break;
        }

        return; }

// Only one player standing! CONGRATULATIONS, YOU HAVE WON!!
OnGameOver:
    .@u=getmapusers("001-8")-1;
    debugmes "OnGameOver Check: "+str(.@u);
    if (.@u == 1) {
        $@EQ_STATUS=2;
        maptimer("001-8", 10, "#QuirinoHUB::OnVictory");
        killmonsterall("001-8");
        stopnpctimer;
        cleanmap("001-8");
    }
    end;


// Ordered by GM to start.
OnStart:
    $@EQ_STATUS=1;

    // 2 Necklaces, 1 Charm and 1 Quiver
    makeitem(rand(1000,1003), 1, "001-8", rand(44,55), rand(46,54));
    makeitem(rand(1006,1011), 1, "001-8", rand(44,55), rand(46,54));
    makeitem(rand(1150,1151), 1, "001-8", rand(44,55), rand(46,54));
    makeitem(rand(1172,1174), 1, "001-8", rand(44,55), rand(46,54));

    makeitem(GoldenRing, 1, "001-8", rand(44,55), rand(46,54));
    makeitem(SilverRing, 1, "001-8", rand(44,55), rand(46,54));
    makeitem(SilverRing, 1, "001-8", rand(44,55), rand(46,54));
    makeitem(rand(PolishedDiamond,PolishedEmerald), 1, "001-8", rand(44,55), rand(46,54));
    makeitem(rand(PolishedDiamond,PolishedEmerald), 1, "001-8", rand(44,55), rand(46,54));
    makeitem(rand(PolishedDiamond,PolishedEmerald), 1, "001-8", rand(44,55), rand(46,54));
    makeitem(rand(TrainingArrow,BoneArrow), 50, "001-8", rand(44,55), rand(46,54));

    makeitem(CandorBoots, 1, "001-8", rand(44,55), rand(46,54));
    makeitem(LousyMoccasins, 1, "001-8", rand(44,55), rand(46,54));
    makeitem(CottonBoots, 1, "001-8", rand(44,55), rand(46,54));
    makeitem(CreasedBoots, 1, "001-8", rand(44,55), rand(46,54));
    makeitem(TulimsharGuardBoots, 1, "001-8", rand(44,55), rand(46,54));

    makeitem(rand(CreasedGloves, MinerGloves), 1, "001-8", rand(44,55), rand(46,54));

    makeitem(CottonShorts, 1, "001-8", rand(44,55), rand(46,54));
    makeitem(JeansShorts, 1, "001-8", rand(44,55), rand(46,54));
    makeitem(PirateShorts, 1, "001-8", rand(44,55), rand(46,54));


    maptimer("001-8", 5000, "#QuirinoHUB::OnCount10");
    announce("##1HUNGRY QUIRIN EVENT: ##3##BCountdown: 15 seconds!", bc_all|bc_npc);
    end;

OnCount10:
    // Random arrows + weapon
    makeitem(rand(TrainingArrow,BoneArrow), 50, "001-8", rand(44,55), rand(46,54));

    // Random Weapon
    HUBrandwpn(0);

    // Throw some random, weighty stuff on the whole map.
    HUBscatter(rand(CopperOre, TitaniumOre));

    specialeffect(4, AREA, getcharid(3));
    addtimer(5000, "#QuirinoHUB::OnCount5");
    dispbottom l("10 seconds!");
    end;

OnCount5:
    specialeffect(4, AREA, getcharid(3));

    // Random Helmet
    .@r=rand(1,9);
    switch(.@r){
    case 1:
    makeitem(Bandana, 1, "001-8", rand(44,55), rand(46,54)); break;
    case 2:
    makeitem(Bucket, 1, "001-8", rand(44,55), rand(46,54)); break;
    case 3:
    makeitem(MoubooHat, 1, "001-8", rand(44,55), rand(46,54)); break;
    case 4:
    makeitem(CandleHelmet, 1, "001-8", rand(44,55), rand(46,54)); break;
    case 5:
    makeitem(PinkieHat, 1, "001-8", rand(44,55), rand(46,54)); break;
    case 6:
    makeitem(GraduationCap, 1, "001-8", rand(44,55), rand(46,54)); break;
    case 7:
    makeitem(SerfHat, 1, "001-8", rand(44,55), rand(46,54)); break;
    case 8:
    makeitem(MinerHat, 1, "001-8", rand(44,55), rand(46,54)); break;
    default:
    makeitem(PaperBag, 1, "001-8", rand(44,55), rand(46,54)); break;
    }

    // Throw some random, weighty stuff on the whole map.
    HUBscatter(rand(CopperIngot, TitaniumIngot));

    addtimer(5000, "#QuirinoHUB::OnBegin");
    dispbottom l("5 seconds!");
    end;

OnBegin:
    // Scatter some healing items, specially where the weapons are, to mess drop lists; Send armor too
    makeitem(rand(Acorn, Croconut), 1, "001-8", rand(44,55), rand(46,54));
    makeitem(rand(Manana, Carrot), 1, "001-8", rand(24,75), rand(26,74));
    makeitem(rand(RoastedMaggot, CherryCake), 1, "001-8", rand(24,75), rand(26,74));
    makeitem(rand(CreasedShirt, CandorShirt), 1, "001-8", rand(24,75), rand(26,74));
    HUBscatter(rand(BottleOfDivineWater, BottleOfSewerWater));

    // Scatter around whole map a few arrows, a training bow, a knife, a creased shirt and a creased knife
    HUBarrow();
    HUBscatter(TrainingBow);
    HUBscatter(Knife);
    HUBscatter(CreasedShirt);
    HUBscatter(CreasedShorts);

    // Throw some random stuff on the whole map.
    HUBscatter(rand(AquadaBox, WoodenLog));

    // Free player, let's start!
    delcells "qhubN";
    delcells "qhubS";
    setpcblock(PCBLOCK_SOFT, false);
    specialeffect(FX_MGSHIELD, AREA, getcharid(3));
    specialeffect(FX_CRITICAL, AREA, getcharid(3));
    dispbottom col(l("Run! Event started!"), 1);
    initnpctimer; // Restart the timer to fix any desync.
    end;

// We only have a schedule for the first 15 minutes. This accelerate stuff.
OnSendWave:
    HUBspawn(AngryRedScorpion, 3);
    HUBspawn(BlackScorpion, 1);
    HUBscatter(rand(PiberriesInfusion,LachesisBrew));
    HUBscatter(CelestiaTea);
    HUBarrow();
    // Throw some ENTIRELY random stuff on the whole map.
    HUBscatter(rand(700, 713));
    HUBscatter(rand(731, 875));
    HUBrandwpn(1);
    HUBspawn(SlimeBlast, 1);
    end;

// 15 seconds after, I'm sure it is synced, so start spawning monsters.
OnTimer15000:
    HUBspawn(WhiteSlime, 3);
    HUBspawn(SlimeBlast, 1);
    // Throw some random stuff on the whole map.
    HUBscatter(rand(731, 875));
    end;


// one minute is due.
OnTimer60000:
    HUBspawn(RedSlime, 12);
    HUBspawn(SlimeBlast, 4);
    HUBarrow();
    // Throw some random stuff on the whole map.
    HUBscatter(rand(731, 875));
    end;

// Two minutes are due.
OnTimer120000:
    HUBspawn(LavaSlime, 2);
    HUBspawn(GreenSlime, 2);
    HUBspawn(SeaSlime, 2);
    HUBspawn(CaveMaggot, 2);
    HUBspawn(Bandit, 2);
    HUBarrow();
    HUBarrow();
    HUBscatter(ElixirOfLife);
    HUBscatter(FatesPotion);
    HUBscatter(LachesisBrew);
    // Throw some random stuff on the whole map.
    HUBscatter(rand(731, 875));
    end;


// Three minutes are due.
OnTimer180000:
    HUBspawn(GreenSlime, 4);
    HUBspawn(SeaSlime, 4);
    HUBspawn(Squirrel, 4);
    HUBspawn(Snake, 1);
    HUBarrow();
    HUBarrow();
    HUBarrow();
    HUBarrow();
    HUBarrow();
    HUBarrow();
    HUBscatter(AshUrn);
    HUBscatter(Monocle);
    HUBscatter(GreenEggshellHat);
    HUBscatter(LeatherShirt);
    // Throw some random stuff on the whole map.
    HUBscatter(rand(731, 875));
    end;

// Five minutes are due.
OnTimer300000:
    HUBspawn(GreenSlime, 4);
    HUBspawn(AngryRedScorpion, 3);
    HUBspawn(Sarracenus, 1);
    HUBspawn(Snake, 2);
    HUBarrow();
    HUBarrow();
    HUBarrow();
    HUBscatter(LeatherGloves);
    HUBrandwpn(1);
    HUBrandwpn(1);
    HUBscatter(GoldenPearlRing);
    HUBscatter(ElixirOfLife);
    // Throw some random stuff on the whole map.
    HUBscatter(rand(731, 875));
    HUBscatter(rand(731, 875));
    end;

// Nine minutes are due.
OnTimer540000:
    HUBspawn(BlackScorpion, 2);
    HUBspawn(DarkLizard, 3);
    HUBspawn(Tipiou, 2);
    HUBspawn(Snake, 2);
    HUBspawn(MountainSnake, 1);
    HUBspawn(SeaSlime, 2);
    HUBarrow();
    HUBarrow();
    HUBarrow();
    HUBarrow();
    HUBarrow();
    HUBrandwpn(1);
    HUBrandwpn(1);
    HUBscatter(GoldenBlackPearlRing);
    HUBscatter(ElixirOfLife);
    // Throw some random stuff on the whole map.
    HUBscatter(rand(731, 875));
    HUBscatter(rand(731, 875));
    end;

// Fifteen minutes are due. No more random stuff will show.
OnTimer900000:
    HUBspawn(BlackScorpion, 4);
    HUBspawn(DarkLizard, 3);
    HUBspawn(Tipiou, 2);
    HUBspawn(Snake, 1);
    HUBspawn(MountainSnake, 2);
    HUBspawn(SeaSlime, 2);
    HUBarrow();
    HUBarrow();
    HUBarrow();
    HUBarrow();
    HUBarrow();
    HUBrandwpn(1);
    HUBrandwpn(1);
    HUBrandwpn(1);
    HUBscatter(ElixirOfLife);
    // Throw some random stuff on the whole map.
    HUBscatter(rand(731, 875));
    HUBscatter(rand(731, 875));
    HUBscatter(rand(731, 875));
    end;

OnVictory:
    @qhub_victor=1;
    announce(sprintf("##1HUNGRY QUIRIN EVENT: ##3##B%s has won the match! Hail!", strcharinfo(0)), bc_all|bc_npc);
    if (!QUIRINO_WINNER)
        QUIRINO_WINNER = gettimetick(2);
OnPCDieEvent:
    @qhub_died=1;
OnPCLogoutEvent:
    getmapxy(.@mapa$, .@a,.@b, 0);
    if (.@mapa$ == "001-8") {
        // Deal with the environment {
        .@u=getmapusers("001-8")-1;
        mapannounce("001-8", "Total remaining players: "+.@u,0);
        HUBscatter(rand(PiberriesInfusion,AtroposMixture));
        HUBscatter(CelestiaTea);
        HUBarrow();
        HUBspawn(SlimeBlast, 1);
        if (.@u % 2 == 1)
            HUBscatter(Piberries);
        if (.@u == 2) {
            mapannounce("001-8", "The wolves were released!",0);
            HUBscatter(ElixirOfLife);
            HUBscatter(Aquada);
            HUBscatter(Piberries);
            HUBscatter(Beer);
            HUBarrow();
            HUBspawn(Wolvern, 4);
            HUBspawn(NightScorpion, 2);
        }
        if (.@u == 1 && !@qhub_victor) {
            donpcevent "#QuirinoHUB::OnGameOver";
        }

        // } Deal with the player {
        clearitem();
        if (checkpcblock() & PCBLOCK_ATTACK)
            setpcblock(PCBLOCK_SOFT, false);

        // Check if to reduce clearitem() efficiency you've used the cart in an illegal way.
        getcartinventorylist();
    	if (@cartinventorylist_count>=1) {
            // Obviously a cheater, you should not be using the cart on the event. I HATE CHEATERS!
            // Destroy everything you had on the cart
            if ($@HAS_API) {
                query_sql("DELETE FROM `cart_inventory` WHERE `char_id`="+getcharid(0));
            } else {
                    apiasync("SQL", sprintf("DELETE FROM `cart_inventory` WHERE `char_id`='%d'", getcharid(0)));
                    apiasync("SQLRUN", "");
            }
            // Destroy the cart. Cheaters doesn't deserve it!!
            setcart(0);
            // Delete the storage register. You now need to pay it again, to don't cheat anymore!
            setq General_Banker, 0;
            // You'll also forsake any event reward.
            @qhub_victor=0;
            @qhub_died=0;
        }

        // If you are the victor (and didn't cheat), you can now hold your reward ;-)
        if (@qhub_victor) {
            X24Event(X24_VORAZ);
            getitem any(MercBoxA, MercBoxA, SilverGift, MercBoxBB, GoldenGift), 1;
            Zeny=Zeny+rand2(500, 2500);
        }
        if (@qhub_died || @qhub_victor)
            getexp rand2(100,300), BaseLevel*10; // Dying on this map is enough to get a reward. Logout = no reward.

        // You'll be revived/fully healed, and then warped.
        // FIXME: It will throw you in Nard's ship if you are on logout...
        recovery(getcharid(3));
        // If recovery() is broken:
        //atcommand "#alive \""+strcharinfo(0)+"\"";
        //percentheal 100, 100;
        warp "000-1", 22, 22;
    }

    // TODO: Checks which doesn't belong here shouldn't be here!
    HUB_Logout(@qhub_died);

    @qhub_victor=0;
    @qhub_died=0;
    end;

OnCancel:
    setpcblock(PCBLOCK_SOFT, false);
    warp "Save", 0, 0;
    end;

// No penalty override
OnNoPenaltyCommand:
    @realvalue=@deathpenalty_realvalue-readparam(BaseExp);
    @realvaljob=@deathpenalty_realvaljob-readparam(JobExp);

    // GM Report
    if (is_staff())
        debugmes("Old values: %d %d Current Values: %d %d Real Difference: %d %d", @deathpenalty_realvalue, @deathpenalty_realvaljob, readparam(BaseExp), readparam(JobExp), @realvalue, @realvaljob);

    // Revive and Warp you to save point or it'll have no effect
    recovery(getcharid(3));
    warp "Save", 0, 0;
    addtimer(500, "#QuirinoHUB::OnNoPenaltyCommand2");
    end;

OnNoPenaltyCommand2:
    // Restitute the lost experience
    if (@deathpenalty_override == 1)
        getexp @realvalue, @realvaljob;
    else if (@deathpenalty_override == 2)
        getexp @realvalue/2, @realvaljob/2;
    else
        dispbottom l("BUG, REPORT ME: QHUB PENALTY OVERRIDE INVALID SIGNAL @@", @deathpenalty_override);

    // Clear temporary variables
    @deathpenalty_override=0;
    @deathpenalty_realvalue=0;
    @deathpenalty_realvaljob=0;
    @realvalue=0;
    @realvaljob=0;
    end;
}

