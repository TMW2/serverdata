// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Forge firearms
017-4,34,23,0	script	Pyndragon	NPC_PLAYER,{
    function explainMelee;
    // craft_gun( BaseItem1, Amount, BaseItem2, Amount, BaseItem3, Amount, PrizeItem, Price )
    function craft_gun {
        .@base1=getarg(0);
        .@amon1=getarg(1);
        .@base2=getarg(2);
        .@amon2=getarg(3);
        .@base3=getarg(4);
        .@amon3=getarg(5);
        .@prize=getarg(6);
        .@price=getarg(7);

		.@price=POL_AdjustPrice(.@price);

        mesn;
        mesq l("Do you want to craft @@? For that I will need:", getitemlink(.@prize));
        mesc l("@@/@@ @@", countitem(.@base1), .@amon1, getitemlink(.@base1));
        mesc l("@@/@@ @@", countitem(.@base2), .@amon2, getitemlink(.@base2));
        mesc l("@@/@@ @@", countitem(.@base3), .@amon3, getitemlink(.@base3));
        mesc l("@@/@@ GP", format_number(Zeny), fnum(.@price));
        next;

        select
            l("Yes"),
            l("No");

        if (@menu == 2)
            return;

        if (countitem(.@base1) >= .@amon1 &&
            countitem(.@base2) >= .@amon2 &&
            countitem(.@base3) >= .@amon3 &&
            Zeny >= .@price) {
            inventoryplace .@prize, 1;
            delitem .@base1, .@amon1;
            delitem .@base2, .@amon2;
            delitem .@base3, .@amon3;
			POL_PlayerMoney(.@price);
            //getitem .@prize, 1;
            CsysNpcCraft(.@prize, IOPT_CRITDMG, rand2(20, 30), IOPT_DOUBLEATTACK, rand2(5, 15));

            mes "";
            mesn;
            mesq l("Many thanks! Come back soon.");
        } else {
            speech S_FIRST_BLANK_LINE,// | S_LAST_NEXT,
                    l("You don't have enough material, sorry.");
        }
        return;
    }

    inspectorQuest();
    .@q=getq(HurnscaldQuest_LOFPass);
    mesn;
    if (is_night())
        mesq l("Good @@. My name is @@ and I make @@.", l("evening"), .name$, l("firearms"));
    else
        mesq l("Good @@. My name is @@ and I make @@.", l("day"), .name$, l("firearms"));
    next;
    mesn strcharinfo(0);
    mesq l("Firearms? What would that be? @@", "%%4");
    next;
    mesn;
    mesq l("Oh, just an empty staff with black powder inside it. Which explodes. And then kills monsters.");
    tutmes l("%s makes specialized weapons for high level players. If you tweak with Nicholas, in Hurnscald, the weapon options, you can get really powerful.", .name$);
    next;
    if (.@q == 1) goto L_LOFBOT;
    mes "";
    if (BaseLevel < 50)
        goto L_TooWeak;
    else if (BaseLevel < 60)
        goto L_Weak;
    goto L_Menu;

L_TooWeak:
    mesn;
    mesq l("These weapons are only for masters, so you must get levels before being able to use them.");
    close;

L_Weak:
    mesn;
    mesq l("You need level 60 to use these guns, but if you want to start collecting materials, you're allowed to.");
    next;
    goto L_Menu;

L_Menu:
    menu
        l("I would like some information"), L_Info,
        l("I want a gun!"), L_Craft,
        l("Do you have non-ranged weapons?"), L_CraftMelee,
        l("I don't want anything right now, bye."), L_Close;

L_Info:
    mes "";
    mesn;
    mesq l("There are four basic class:");
    mesc l("SHORT GUNSTAFF");//aka revolver
    mesc l("* The only one hand ranged weapon you'll ever find!");
    mes l("  I cut it in half, and to fix shooting speed, I added some extra cogs.");
    mes l("  It didn't got too much lighter, and it got really weak compared to a bow.");
    mesc l("POWERFUL GUNSTAFF");
    mesc l("* Huge damage and more criticals, but slow fire rate.");
    mes l("  The standard gunstaff! It is way stronger than a bow!");
    mes l("  Oh, and it comes with added shooting range, but it is slow as hell to reload.");
    mesc l("RAPID GUNSTAFF");
    mesc l("* Low damage, highest attack speed from all.");
    mes l("  One have lots of powder to attack non-stop, but oh god that is heavy.");
    mes l("  It is also much weaker and less precise than a bow. But it is so fun using...");
    mesc l("EXPLOSIVE GUNSTAFF");
    mesc l("* Causes splash damage, and are very expensive.");
    mes l("  You know, I wanted to make the powder explode when hitting!");
    mes l("  It is the same as a bow, including in penalty. But it deals splash damage!");
    next;
    mesn;
    mesq l("Select carefully which weapon you want, so there are no regrets.");
    next;
    goto L_Menu;

L_Craft:
    select
        l("I changed my mind."),
        l("I want a SHORT GUNSTAFF."),
        l("I want a POWERFUL GUNSTAFF."),
        l("I want a RAPID GUNSTAFF."),
        l("I want an EXPLOSIVE GUNSTAFF."),
        rif(false, l("I want something more magical."));
    mes "";

    switch (@menu) {
        case 2:
            craft_gun( LeadIngot, 4, TitaniumIngot, 5, Coal, 11, PynRevolver, 3000 );
            goto L_Craft;
        case 3:
            craft_gun( LeadIngot, 6, TitaniumIngot, 7, Coal, 14, PynRifle, 3000 );
            goto L_Craft;
        case 4:
            craft_gun( LeadIngot, 6, TitaniumIngot, 7, Coal, 14, PynGatling, 4000 );
            goto L_Craft;
        case 5:
            craft_gun( LeadIngot, 9, TitaniumIngot, 10, Coal, 16, PynShotgun, 6000 );
            goto L_Craft;
        case 6:
            mesn;
            mesq l("I suppose I can make you a %s. It sometimes casts magic skills upon your foes. Well, seldomly, but it also deals more damage than normal wands.", getitemlink(PynScepter));
            next;
            craft_gun( LeadIngot, 10, TitaniumIngot, 12, Coal, 24, PynScepter, 24000 );
            goto L_Craft;
    }
    goto L_Menu;


L_CraftMelee:
    select
        l("I changed my mind."),
        l("Which melee you craft?"),
        l("I want a PORTABLE LIGHTSABER."),
        l("I want a POWERFUL LIGHTSABER."),
        rif(getskilllv(TF_STEAL) && false, l("I want a KUNAI.")),
        l("I want a WHIP.");
    mes "";

    switch (@menu) {
        case 2:
            explainMelee();
            goto L_CraftMelee;
        case 3:
            craft_gun( EverburnPowder, 30, TitaniumIngot, 7, LeadIngot, 4, Lightsaber, 25000 );
            goto L_CraftMelee;
        case 4:
            craft_gun( EverburnPowder, 30, TitaniumIngot, 7, LeadIngot, 4, PowerfulLightsaber, 25000 );
            goto L_CraftMelee;
        case 5:
            craft_gun( GoldIngot, 3, HeroCoin, 500, LOFCoin, 4, PynKunai, 25000 );
            goto L_CraftMelee;
        case 6:
            craft_gun( LOFCoin, 3, TitaniumIngot, 5, LeadIngot, 3, PynWhip, 22000 );
            goto L_CraftMelee;
    }
    goto L_Menu;


L_Close:
    closedialog;
    goodbye;
    close;

// Takes priority over craft
L_LOFBOT:
    select
        l("A friend of yours called LOF BOT asked for a coin..."),
        l("Sorry, I'm in hurry.");
    mes "";
    if (@menu == 2) close;
    mesn;
    mesq l("Ah, so LOF Bot wants a souvenir after all!");
    next;
    mesn;
    mesq l("Everyone loves the Land of Fire, it is impossible to not love it.");
    next;
    inventoryplace LOFCoin, 1;
    mesn;
    mesq l("Here, please take this to them. Tell them they are welcome here anytime! %%2");
    getitem LOFCoin, 1;
    setq HurnscaldQuest_LOFPass, 2;
    close;

function explainMelee {
    mesc l("PORTABLE LIGHTSABER");
    mesc l("* Very quick and can be used in a single hand.");
    mesc l("POWERFUL LIGHTSABER");
    mesc l("* Very quick, two handed, and evil.");
    mes "";
    mes l("  Actually, Lalica cursed one of my firestaves and it got a mind of its own D:");
    mes l("  Then I found out that I could set the staff in flames using Everburn Powder!");
    mes l("  It was quite a challenge, and I still need Lalica to keep curse them.");
    mes l("  But the result is a fast weapon which is also powerful.");
    mes "";
    next;
    mesc l("KUNAI");
    mesc l("* More for bandits than assassins or ninjas.");
    mes l("  See, I was lazy and there were only short knives around... So I made a Kunai.");
    mes l("  It can steal items or collect drops on the floor randomly. Also improves your evasion.");
    mes l("  It is not that amazing, though. I will trade with you for more useful items.");
    mes "";
    mesc l("WHIP");
    mesc l("* Has non-cumulative area of effect damage.");
    mes l("  A fair all-rounder weapon; Stronger than you would expect and not so clumsy.");
    mes l("  But its splash damage does not stack, so be careful when adding options.");
    next;
    return;
}

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, WarlordHelmet);
    setunitdata(.@npcId, UDT_HEADMIDDLE, GoldenWarlordPlate);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, BromenalPants);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 2);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    .sex=G_MALE;
    .distance=5;
    end;

}
