// TMW2 scripts.
// Authors:
//    Jesusalva and TMW Org.
// Description:
//    Sagratha (variables SAGRATHA_SCORE and SAGRATHA_FRIENDSHIP)

// SaggyScoreUpdate( amount )
function	script	SaggyScoreUpdate	{
    .@val=getarg(0);
    SAGRATHA_SCORE=limit(-50, SAGRATHA_SCORE+.@val, 50);
    return;
}

function	script	SaggyMobCount	{
    switch (killedrid) {
        // She gets angry
        case AlphaMouboo:
            SaggyScoreUpdate(-3);
            break;
        case Mouboo:
            SaggyScoreUpdate(any(-2, -3));
            break;
        case Pollet:
        case Fluffy:
        case IcedFluffy:
            SaggyScoreUpdate(any(-1, -2, -3));
            break;
        case PoisonSpikyMushroom:
        case LogHead:
        case ForestMushroom:
            SaggyScoreUpdate(any(-1, -2));
            break;
        case Squirrel:
        case SpringSquirrel:
        case LofSquirrel:
        case FrozenSquirrel:
        case FairysSquirrel:
        case MananaTree:
            SaggyScoreUpdate(-1);
            break;

        // She may get happy. Who knows.
        case ViciousSquirrel:
        case WickedMushroom:
        case Bluepar:
        case BlackScorpion:
        case Bandit:
        case RobinBandit:
            SaggyScoreUpdate(any(0,0,0,0,1));
            break;
    }
    // Full hate vs Ghosts, Undead and shadow
    .@mr=getmonsterinfo(killedrid, MOB_ELEMENT);
    switch (.@mr) {
        case Ele_Dark:
        case Ele_Ghost:
        //case Ele_Undead:
            SaggyScoreUpdate(any(0,0,0,0,1));
            break;
    }
    return;
}

014-5-1,33,37,0	script	Sagratha	NPC_SAGRATHA,{
    function teachMagic;
    function requireHelp;
    if (array_find(.SaggyHats, getequipid(EQI_HEAD_TOP)) >= 0)
        goto L_HatAttack;

    .@q=getq(HurnscaldQuest_Sagratha);
    .@m=getq(HurnscaldQuest_InjuriedMouboo);

    // Safety - Impossible Situation
    if (.@m < 2 || .@m > 3 || .@q < 6) {
        Exception("Player found cheating/breaking the rules. Character banned. Please contact GM Staff if you believe this is an error.", RB_DEFAULT|RB_SPEECH);
        //atcommand "@jailfor 7h "+strcharinfo(0);
        Exception ("Exception at exception, shutting down! (m is "+.@m+", q is "+.@q+")", RB_SPEECH|RB_ISFATAL);
        close;
    }

    mesn;

    if (SAGRATHA_FRIENDSHIP >= 2)
        mesc l("@@ nods as she notices you.", .name$);
    else if (SAGRATHA_FRIENDSHIP == 1)
        mesc l("@@ raises an eyebrow as you address her.", .name$);
    else
        mesc l("@@ glances at you, suspicion evident in her eyes.", .name$);

    mesq l("Hello.");
    next;

    if (.@m == 2 && .@q >= 6)
        goto L_Reward;
    if (.@q == 6)
        goto L_Finish;
    if (SAGRATHA_SCORE < 0)
        goto L_Unhappy;

    do
    {
        .@x=getq(General_Sagratha);
        mesn strcharinfo(0);
        select
            l("Can I return to the shrine?"),
            l("I would like some Mouboo Milk."),
            l("Can you teach me magic? Pretty please?"),
            l("Can I help you with something?"),
            l("What can you tell me about the Moubootaur?"),
            l("Good bye.");
        mes "";
        switch (@menu) {
        // XXX: Sealed Shrine
        case 1:
            mesn;
            mesq l("Why would you want to go back there?");
            mesc l("@@ snarls.", .name$);
            next;
            mesn;
            mesq l("That was a Moubootaur's temple. It is not the kind of place to give a stroll on the park.");
            next;
            mesn;
            mesq l("Did you noticed how no monster got close to it?");
            next;
            select
                l("You're right. Sorry."),
                l("Don't worry, I'll stay at the caves."),
                l("I want to pay my respects as a fellow Pink Mouboo cult member."),
                l("But I need to!");
            mes "";
            if (@menu == 3) {
                mesn;
                mesq l("As a what?");
                next;
            }
            else if (@menu == 4) {
                mesn;
                mesq l("No you don't. Don't disturb the forest for no reason.");
                close;
            }
            else if (@menu == 1) {
                mesn;
                mesq l("Hmpf. Wise choice.");
                next;
                break; // Return to the while loop
            }
            mesn;
            mesq l("As you wish. But do not mess with things you don't understand.");
            next;
            closeclientdialog;
            warp "015-8", 99, 178;
            close;
        // XXX: Mouboo Milk
        case 2:
            if (SAGRATHA_SCORE < 45) {
                mesn;
                mesq l("You don't need, nor deserve it.");
                next;
                mesn;
                mesq l("Learn milking a Mouboo. And if you harm them, I'll cast a lightning bolt at you!");
                next;
                break;
            }
            if (SAGRATHA_MILK > gettimeparam(GETTIME_HOUR)) {
                mesn;
                mesq l("I just gave you it. Come back later.");
                next;
                break;
            }
            inventoryplace Milk, 2;
            SAGRATHA_SCORE-=8; // :o
            SAGRATHA_MILK=gettimeparam(GETTIME_HOUR)+4;
            getitem Milk, any(1,1,1,1,1,1,2);
            mesn;
            mesq l("Here. You know that milking Mouboos won't cause them harm, right? It's a different story with @@.", getitemlink(MoubooSteak));
            close;
            break;
        // XXX: Teach magic / etc
        case 3:
        case 4:
            if (.@x % 2 == 0)
                teachMagic();
            else
                requireHelp();
            break;
        // XXX: Moubootaur
        case 5:
            mesn;
            mesq l("It's a scary legend of old. It's truth, but I don't know how much it is.");
            next;
            mesn;
            mesq l("Anyway, the Moubootaur certainly existed, and probably is still alive.");
            next;
            mesn;
            mesq l("How powerful they were or are, their cruelty, their judgment against the Terranite Civilization and the other bloody details of the legend... I cannot confirm them to you.");
            next;
            break;
        default:
            closeclientdialog;
            goodbye;
            close;
        }
    } while (true);

    close;

L_Finish:
    // WHAT
    if (.@m != 3) {
        Exception("Player found cheating/breaking the rules. Character banned. Please contact GM Staff if you believe this is an error.", RB_DEFAULT|RB_SPEECH);
        atcommand "@jailfor 7h "+strcharinfo(0);
        close;
    }
    mesn;
    mesq l("I haven't thanked you yet.");
    next;
    select
        l("You're welcome."),
        l("For the rescue? You didn't seemed to be in need of aid."),
        l("Yes, I'm awesome, praise me more."),
        l("For the mouboo? I was only doing my duty.");
    mes "";
    mesn;
    if (@menu != 4)
        mesq l("Not for that! For helping out the injuried mouboo, of course.");
    else
        mesq l("Hmpf, it's good to see you're at least trying to follow Wyara's example.");
    next;
    mesn;
    mesq l("Anyway, Wyara told me about the assassins. I'm surprised they followed me until the ruins.");
    next;
    mesn;
    mesq l("I thought I would be safe in the shrine, but seems like they are after the Moubootaur. Great danger will befall all forest creatures if they accomplish that.");
    next;
    mesn;
    mesq l("This will affect humans, too, so don't act as if it is not your bussiness either.");
    next;
    mesn;
    mesq l("For now, take this @@. I hope that you will use this power for something good now.", getitemlink(AlchemyBlueprintB));
    inventoryplace AlchemyBlueprintB, 1;
    getitem AlchemyBlueprintB, 1;
    getexp 24000, 0; // ~70% from level 40 (quest estimate level)
    Zeny+=3000; // About 50 carps sold
    SAGRATHA_FRIENDSHIP+=1;
    setq HurnscaldQuest_Sagratha, 7;
    close;

L_Reward:
    select
        l("Hi! My name is @@.", strcharinfo(0)),
        l("Are you Sagratha?"),
        l("Good bye.");
    mes "";
    if (@menu == 3)
        close;
    if (@menu == 1) {
        mesn;
        mesq l("Ah.");
        next;
        select
            l("Are you Sagratha?"),
            l("Erm... Good bye.");
        mes "";
        if (@menu == 2)
            close;
    }
    mesn;
    mesq l("Yes.");
    next;
    mesn;
    mesq l("Do you need something from me?");
    next;
    // A check is not needed, because quest pre-requisites
    // If you don't have the quest in clearable state... CHEAT!
    select
        l("Yep. There was a cursed mouboo on the road."),
        l("Not really.");
    mes "";
    if (@menu == 2) {
        mesn;
        mesq l("Okay then.");
        close;
    }
    mesn;
    mesq l("Yes, the cursed Mouboo you told me earlier, on the way out.");
    next;
    mesn;
    mesq l("The curse was done by nobody less than... the Moubootaur.");
    next;
    mesn;
    mesq l("Well. I'll take care of that, don't worry with that.");
    next;
    mesn;
    mesq l("It's my duty to protect the beings in the forest... Not all of them are monsters as @@s like you think.", get_race());
    compareandsetq HurnscaldQuest_InjuriedMouboo, 2, 3;
    SAGRATHA_FRIENDSHIP+=1;
    close;

L_HatAttack:
    mesn;
    mesc l("@@ seems to be trembling with disgust as she stares at your headgear.", .name$);
    next;
    mesn;
    mesq l("Do you think that is funny?");
    mesc l("@@ snarls.", .name$);
    next;
    mesn;
    mesq l("You have no idea what that poor creature felt!");
    next;
    mesn;
    mesc l("She snaps her fingers.");
    mesq l("Let me show you...");
    specialeffect 312, SELF, getcharid(3);
    percentheal -30, 0;
    close;

L_Unhappy:
    mesn;
    mesc l("@@ glares at you in anger.", .name$);
    mesq l("I wonder if you can still sleep after killing those innocent forest creatures!");
    next;
    mesn;
    mesq l("I am sure that they will come back to haunt you in your dreams!");
    close;
// Functions - TODO: Where are Mouboo Summon? Loghead summon? etc.?
function teachMagic {
    .@x=getarg(0,getq(General_Sagratha));
    switch (.@x) {
    case 0:
        if (MAGIC_LVL < 1)
            goto L_Magicless;
        mesn;
        mesc l("@@ nods.", .name$);
        mesq l("For now, yes. But you will have to prove that you really care about more than yourself.");
        next;
        mesn;
        mesq l("I'll teach you a spell. It is called CUTE HEART. It summons Fluffies. They are so cute, I want to hug them...");
	    skill TMW2_CUTEHEART, 1, 0;
        setq General_Sagratha, 1;
        break;
    case 2:
        if (MAGIC_LVL < 2)
            goto L_Magicless;
        mesn;
        mesc l("@@ nods.", .name$);
        mesq l("Be careful, because the more magic power you ask the Mana Seed, the more difficult to control it will be.");
        next;
        mesn;
        mesq l("This is how the Monster King became evil. Keep practicing magic, so you get more comfortable with it and fail less often.");
        next;
        mesn;
        mesq l("Anyway. This is the KALBOO spell. It'll summon mouboos.");
        next;
        mesn;
        mesq l("The world belong to Mouboos. They are sublime creatures!");
	    skill TMW2_KALBOO, 1, 0;
        setq General_Sagratha, 3;
        break;
    case 4:
        if (MAGIC_LVL < 3)
            goto L_Magicless;
        mesn;
        mesc l("@@ nods.", .name$);
        mesq l("You've been doing well. Be careful out there.");
        next;
        mesn;
        mesq l("Anyway. This is the KALWULF spell. It'll spawn Wolverns.");
        next;
        mesn;
        mesq l("Wolverns are fierce creatures who likes cold places. Their fur is soft.");
        next;
        mesn;
        mesq l("You can also use PLANT KINGDOM to spawn several plants at once, too. Let's make this world more green.");
	    skill TMW2_KALWULF, 1, 0;
	    skill TMW2_PLANTKINGDOM, 1, 0;
        setq General_Sagratha, 5;
        break;
    case 6:
        if (MAGIC_LVL < 4)
            goto L_Magicless;
        mesn;
        mesc l("@@ nods.", .name$);
        mesq l("You've been doing quite well, indeed. Ever visited Lilit?");
        next;
        mesn;
        mesq l("Lilit is the fairy kingdom, governed by Lilit the Fairy.");
        next;
        mesn;
        mesq l("Anyway. This is the FAIRY KINGDOM spell. It'll spawn fairies.");
        next;
        mesn;
        mesq l("Fairies usually behaves well, but they hate snakes, so avoid casting halhiss and fairy kingdom at once. You never know.");
	    skill TMW2_FAIRYKINGDOM, 1, 0;
        setq General_Sagratha, 7;
        break;
    case 8:
        if (MAGIC_LVL < 5)
            goto L_Magicless;
        mesn;
        mesc l("@@ nods.", .name$);
        mesq l("You've been doing very well, I'm surprised.");
        next;
        mesn;
        mesq l("The most powerful spells can, sometimes, summon some monster you didn't wanted to.");
        next;
        mesn;
        mesq l("This is the FROZENHEART spell. It'll summon... Yeti.");
        next;
        mesn;
        mesq l("But if you're not skilled enough, or lose control of it, a Moggun might be spawned instead. Which is a young, weak, baby Yeti.");
	    skill TMW2_FROZENHEART, 1, 0;
        setq General_Sagratha, 9;
        break;
    case 10:
        if (MAGIC_LVL < 6)
            goto L_Magicless;
        mesn;
        mesc l("@@ nods.", .name$);
        mesq l("You've been asking for way too much power from the Mana Seed.");
        next;
        mesn;
        mesq l("Too much power can corrupt you. I've seen this happening before... countless times.");
        next;
        mesn;
        mesq l("This is the STONEHEART spell. It'll summon the fierce Terranite.");
        next;
        mesn;
        mesq l("They used to be an advanced civilization long long ago, but they decided to go against the Moubootaur.");
        next;
        mesn;
        mesq l("They are now like most monsters. I pity them. Their sacrifice was not in vain, though.");
	    skill TMW2_STONEHEART, 1, 0;
        setq General_Sagratha, 11;
        break;
    case 12:
        if (MAGIC_LVL < 7)
            goto L_Magicless;
        mesn;
        mesc l("@@ nods reluctantly.", .name$);
        mesq l("If you try take any more power from the Mana Seed, I'll kill you.");
        next;
        mesn;
        mesq l("Seven levels is what the Mana Seed may give to any race but Saviors.");
        next;
        mesn;
        mesq l("Of course, the Savior race is extinct as far as history book concern with.");
        next;
        mesn;
        mesq l("This is the FAIRYEMPIRE spell. It'll summon Pixies.");
        next;
        mesn;
        mesq l("For simple-minded individuals like you, pixies are fairies.");
	    skill TMW2_FAIRYEMPIRE, 1, 0;
        setq General_Sagratha, 13;
        break;
    }
        /*
            Plants Lv 1
            Kalmurk Lv 10
            Zarkor Lv 15
            Fluffy Lv 15
            Poison Spiky Mushroom Lv 25
            Limerizer Lv 30 → Unused

            Mouboo Lv 35
            Halhiss Lv 40
            Wolvern Lv 45

            Fairy Kingdom Lv 50
            Frozenheart Lv 60
            Dragokin Lv 70 → Unused (Lilit reward)
            Stoneheart Lv 80
            Pixies Lv 90

            Never released:
            TODO Scorpions Lv 30~50
            TODO Black Mamba Lv 80
            TODO Moonshroom Lv 80
            TODO Centaur Lv 80

		    skill TMW2_DRAGOKIN, 5;
        */
    next;
    return;
}
function requireHelp {
    .@x=getarg(0,getq(General_Sagratha));
    switch (.@x) {
    case 1:
        mesn;
        mesq l("I need help because, you see, I don't carry a cauldron around.");
        next;
        mesn;
        mesq l("I need @@ @@, if you could arrange me that I would be grateful.", 35, getitemlink(PiberriesInfusion));
        if (countitem(PiberriesInfusion) < 35)
            break;
        next;
        mesc l("Deliver the potions to Sagratha?");
        if (askyesno() == ASK_YES) {
            delitem PiberriesInfusion, 35;
            getexp 800, 0;
            mesn;
            mesq l("Thanks. Do you know the KALSPIKE magic? It allows you to summon Poison Spiky Mushroom. Just use a spore.");
	        skill TMW2_KALSPIKE, 1, 0;
            setq General_Sagratha, 2;
        }
        break;
    case 3:
        .@m=Wolvern;
        if (BaseLevel < getmonsterinfo(.@m, MOB_LV)) {
            mesn;
            mesq l("When you're stronger I may consider asking for your help.");
            break;
        }
        mesn;
        mesq l("I need help because, you see, I don't carry a cauldron around.");
        next;
        mesn;
        mesq l("I need @@ @@, if you could arrange me that I would be grateful.", 1, getitemlink(GoldenApple));
        if (countitem(GoldenApple) < 1)
            break;
        next;
        mesc l("Deliver the items to Sagratha?");
        if (askyesno() == ASK_YES) {
            delitem GoldenApple, 1;
            .@xp=getmonsterinfo(.@m, MOB_BASEEXP);
            .@jp=getmonsterinfo(.@m, MOB_JOBEXP);
            getexp .@xp*20, .@jp*15;
            mesn;
            mesq l("Thanks. I am willing to share you more magic, if you're interested.");
            setq General_Sagratha, 4;
        }
        break;
    case 5:
        .@m=FireFairy;
        if (BaseLevel < getmonsterinfo(.@m, MOB_LV)) {
            mesn;
            mesq l("When you're stronger I may consider asking for your help.");
            break;
        }
        mesn;
        mesq l("I need help because, you see, I don't carry a cauldron around.");
        next;
        mesn;
        mesq l("I need @@ @@, if you could arrange me that I would be grateful.", 1, getitemlink(LilitWarpCrystal));
        if (countitem(LilitWarpCrystal) < 1)
            break;
        next;
        mesc l("Deliver the items to Sagratha?");
        if (askyesno() == ASK_YES) {
            delitem LilitWarpCrystal, 1;
            .@xp=getmonsterinfo(.@m, MOB_BASEEXP);
            .@jp=getmonsterinfo(.@m, MOB_JOBEXP);
            getexp .@xp*20, .@jp*15;
            mesn;
            mesq l("Thanks. I am willing to share you more magic, if you're interested.");
            setq General_Sagratha, 6;
        }
        break;
    case 7:
        .@m=Yeti;
        if (BaseLevel < getmonsterinfo(.@m, MOB_LV)) {
            mesn;
            mesq l("When you're stronger I may consider asking for your help.");
            break;
        }
        mesn;
        mesq l("I need help because, you see, I don't carry a cauldron around.");
        next;
        mesn;
        mesq l("I need @@ @@, if you could arrange me that I would be grateful.", 20, getitemlink(SacredManaPotion));
        if (countitem(SacredManaPotion) < 20)
            break;
        next;
        mesc l("Deliver the items to Sagratha?");
        if (askyesno() == ASK_YES) {
            delitem SacredManaPotion, 20;
            .@xp=getmonsterinfo(.@m, MOB_BASEEXP);
            .@jp=getmonsterinfo(.@m, MOB_JOBEXP);
            getexp .@xp*20, .@jp*15;
            mesn;
            mesq l("Thanks. I am willing to share you more magic, if you're interested.");
            setq General_Sagratha, 8;
        }
        break;
    case 9:
        .@m=Terranite;
        if (BaseLevel < getmonsterinfo(.@m, MOB_LV)) {
            mesn;
            mesq l("When you're stronger I may consider asking for your help.");
            break;
        }
        mesn;
        mesq l("I need help because, you see, I don't carry a cauldron around.");
        next;
        mesn;
        mesq l("I need @@ @@, if you could arrange me that I would be grateful.", 20, getitemlink(SacredImmortalityPotion));
        if (countitem(SacredImmortalityPotion) < 20)
            break;
        mesq l("Ah! I also want @@ @@.", 1, getitemlink(Arcanum));
        if (countitem(Arcanum) < 1)
            break;
        next;
        mesc l("Deliver the items to Sagratha?");
        if (askyesno() == ASK_YES) {
            delitem SacredImmortalityPotion, 20;
            delitem Arcanum, 1;
            .@xp=getmonsterinfo(.@m, MOB_BASEEXP);
            .@jp=getmonsterinfo(.@m, MOB_JOBEXP);
            getexp .@xp*20, .@jp*15;
            mesn;
            mesq l("Thanks. I am willing to share you more magic, if you're interested.");
            setq General_Sagratha, 10;
        }
        break;
    case 11:
        .@m=Tengu;
        if (BaseLevel < getmonsterinfo(.@m, MOB_LV)) {
            mesn;
            mesq l("When you're stronger I may consider asking for your help.");
            break;
        }
        mesn;
        mesq l("I need help because, you see, I don't carry a cauldron around.");
        next;
        mesn;
        mesq l("I need @@ @@, if you could arrange me that I would be grateful.", 1, getitemlink(BlackPearl));
        if (countitem(BlackPearl) < 1)
            break;
        mesq l("Ah! I also want @@ @@.", 1, getitemlink(BlueManaPearl));
        if (countitem(BlueManaPearl) < 1)
            break;
        next;
        mesc l("Deliver the items to Sagratha?");
        if (askyesno() == ASK_YES) {
            delitem BlackPearl, 1;
            delitem BlueManaPearl, 1;
            .@xp=getmonsterinfo(.@m, MOB_BASEEXP);
            .@jp=getmonsterinfo(.@m, MOB_JOBEXP);
            getexp .@xp*20, .@jp*15;
            mesn;
            mesq l("Thanks. I am willing to share you more magic, if you're interested.");
            setq General_Sagratha, 12;
        }
        break;
    default:
        mesn;
        mesq l("Maybe. Come back later.");
        next;
        break;
    }
    next;
    return;
}

L_Lie:
    mesn;
    mesq l("I'm not blind.");
    close;

L_Magicless:
    mesn;
    mesq l("Not yet. You have to ask the mana seed to give you more power.");
    next;
    mesn;
    mesq l("Your skill in magic is not great enough to use some of the spells yet, so keep practicing and visiting the mana seed until it is.");
    close;

OnInit:
    setarray .SaggyHats, FluffyHat, MoubooHat, AlphaMoubooHat;

    .distance=5;
    npcsit;
    end;

OnInstanceInit:
    disablenpc instance_npcname(.name$);
    end;
}

