// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Questmaker Handbook 14.0 (F = Favorable)

024-1,114,45,0	script	Meriel	NPC_ELF_F,{
    .@q = getq(FrostiaQuest_Meriel);
    if (BaseLevel < 42) {
        mesc l("The elf seems busy. She ignores you.");
        close;
    }
    if (.@q == 0) {
        if (frally()) goto L_Start_F;
        else goto L_Start_U;
    }
    else if (.@q == 1) {
        if (frally()) goto L_Submit_F;
        else goto L_Submit_U;
    }
    else if (.@q == 2) {
        if (frally()) goto L_Done_F;
        else goto L_Done_U;
    }
    Exception("Invalid quest state - "+str(.@q));
    end;

L_Start_F:
    mesn;
    mesq lg("Hey gal, could you do me a little favor?", "Hey pal, could you do me a little favor?");
    next;
    mesn;
    mesc l("*blushes*");
    mesq l("Bring me %d %s. I cannot tell you what for, though!", 18, getitemlink(PurpleBlobime));
    setq FrostiaQuest_Meriel, 1;
    close;

L_Submit_F:
    mesn;
    mesq l("Did you got the %d %s I've asked?", 18, getitemlink(PurpleBlobime));
    next;
    if (askyesno() == ASK_YES) {
        if (countitem(PurpleBlobime) < 18) {
            mesn;
            mesq l("You've been spending too much time with humans. You are picking up bad habits from them like lying. Be careful.");
            close;
        }
        inventoryplace EquipmentBlueprintC, 1;
        delitem PurpleBlobime, 18;
        getitem EquipmentBlueprintC, 1;
        getexp 4210, 0;
        setq FrostiaQuest_Meriel, 2;
        mesn;
        mesq l("Ahh, he'll be so happy! Thanks! You can keep this.");
    }
    close;

L_Done_F:
    mesn;
    mesq l("I've heard some elves, when they abandoned our original town, founded Aethyr somewhere up northwest. But all contact with it has since been lost.");
    close;
/////////////////////////////////////////////////////////////////////////////////
L_Start_U:
    mesn;
    mesq l("Hey, can you spare me a moment?");
    next;
    mesn;
    mesc l("*blushes*");
    mesq l("I want %d %s! Don't worry, I can pay.", 18, getitemlink(PurpleBlobime));
    setq FrostiaQuest_Meriel, 1;
    close;

L_Submit_U:
    mesn;
    mesq l("Did you got the %d %s I've asked?", 18, getitemlink(PurpleBlobime));
    next;
    if (askyesno() == ASK_YES) {
        if (countitem(PurpleBlobime) < 18) {
            mesn;
            mesq l("You liar, I'll show you to respect woman!");
            mesc l("%s slaps you!", .name$);
            percentheal -30, 0;
            close;
        }
        inventoryplace EquipmentBlueprintC, 1;
        delitem PurpleBlobime, 18;
        getitem EquipmentBlueprintC, 1;
        getexp 4210, 0;
        setq FrostiaQuest_Meriel, 2;
        mesn;
        mesq l("Hihihi... Thanks! Here, you can keep this.");
    }
    close;

L_Done_U:
    mesn;
    mesq l("I've heard some elves, when they abandoned our original town, founded Aethyr somewhere up northwest. But all contact with it has since been lost.");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonSkirt);
    setunitdata(.@npcId, UDT_HEADMIDDLE, LeatherShirt);
    setunitdata(.@npcId, UDT_HEADTOP, TrapperHat);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 8);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 11);
    npcsit;

    .sex = G_FEMALE;
    .distance = 5;
    end;
}

