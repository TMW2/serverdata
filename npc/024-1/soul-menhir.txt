// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Soul Menhir

024-1,94,41,0	script	Soul Menhir#frost	NPC_SOUL_SNOW,{
    @map$ = "024-1";
    setarray @Xs, 93, 94, 95, 93, 95, 93, 94, 95;
    setarray @Ys, 40, 40, 40, 41, 41, 42, 42, 42;
    @x = 0;
    @y = 0;
    callfunc "SoulMenhir";
    @map$ = "";
    cleararray @Xs[0], 0, getarraysize(@Xs);
    cleararray @Ys[0], 0, getarraysize(@Ys);
    @x = 0;
    @y = 0;
    close;
}
