// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 011-3: Eternal Swamps - Landbridge mobs
011-3,39,205,19,14	monster	Angry Red Scorpion	1130,10,100000,30000
011-3,40,206,19,14	monster	Fire Goblin	1067,10,100000,30000
011-3,38,207,19,14	monster	Old Snake	1199,5,100000,30000
011-3,38,174,18,13	monster	Sarracenus	1125,7,100000,30000
011-3,37,176,18,13	monster	Black Scorpion	1074,4,100000,30000
011-3,39,175,18,13	monster	Snake	1122,6,100000,30000
011-3,40,145,19,11	monster	Snake	1122,3,100000,30000
011-3,39,144,19,11	monster	Sarracenus	1125,4,100000,30000
011-3,38,146,19,11	monster	Desert Bandit	1124,5,100000,30000
011-3,40,101,36,26	monster	Fire Fairy	1183,15,100000,30000
011-3,39,99,36,26	monster	Bandit	1124,13,100000,30000
011-3,37,103,36,26	monster	Robin Bandit	1153,7,100000,30000
011-3,37,56,24,14	monster	Sea Slime	1093,5,100000,30000
011-3,39,55,24,14	monster	Red Mushroom	1042,5,100000,30000
011-3,38,54,24,14	monster	Grass Snake	1169,4,100000,30000
011-3,37,32,19,7	monster	Sea Slime	1093,5,100000,30000
011-3,39,32,19,7	monster	Tipiou	1016,3,900000,300000
011-3,38,31,19,7	monster	Vicious Squirrel	1187,5,100000,30000
011-3,51,113,3,4	monster	Fire Fairy	1183,1,100000,30000
011-3,30,89,3,4	monster	Fire Fairy	1183,1,100000,30000
