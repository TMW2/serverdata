// TMW2 Script
// Author:
//    Jesusalva

006-3,46,26,0	script	Magic Barrier#0063	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    //warp "006-3", 49, 52;
    specialeffect(5000, SELF, getcharid(3));
    dispbottom l("The power which lies in Candor rejects your strength.");
    sleep2(3000);
    specialeffect(5002, SELF, getcharid(3));
    end;
}
