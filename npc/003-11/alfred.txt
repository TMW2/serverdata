// TMW2 Script
// Author:
//    Jesusalva
// Description:
//  Tulimshar Employment Center

003-11,38,37,0	script	Alfred	NPC_LOF_RICH,{
    function _calc_wage;
    if (@alfred_st) goto L_Working;
    // Hours of Function
    if (gettime(8) >= 365) {
        mesn;
        mesq l("Sorry, we're closed for New Year festivities.");
        close;
	} else if (gettime(GETTIME_MONTH) == DECEMBER) {
		mes("It's December! The center will close for new year, be warned!");
    }
    // Main
    mesn;
    mesq l("Hello! I am Alfred, and I offer paperwork jobs. No one likes to do them, because they consist in ##Bnot doing anything for hours straight##b. Heck, most people even snooze while filling them!");
    next;
    mesn;
    mesq l("I'll offer you a payment for hours worked, I do not advise this unless you have nothing better to do.");
    next;
    mesn;
    mesq l("Do you want to work? The current wage is %s GP/hour, and if you leave the building, I'll assume you've stopped working and will emit payment.", fnum(_calc_wage()));
    mesc l("There is no payment for fraction of hours worked."), 1;
    //mesc l("If you logout without leaving the map first, fees will be deducted from your payment."), 1;
    if (askyesno() == ASK_YES) {
        dispbottom l("You've started to fill paperwork.");
        tutmes l("As long that you do not leave the map, your char will keep generating money. Do note you cannot work more than %d hours straight.", .maxHours);
        @alfred_st = gettimeparam(GETTIME_MINUTE);
        addtimer(3000, "Alfred::OnBleep");
    }
    close;

OnBleep:
    // Check if you changed the map
    if (getmap() != "003-11") {
        // Check how many hours you've worked
        .@current = max(0, gettimeparam(GETTIME_MINUTE) - @alfred_st);
        // Pay you for every 60 minutes
        .@pay = min(.maxHours, .@current / 60) * _calc_wage();
        // Pay fraction of hour if you spent over 4 hours here
        // But ONLY if you leave manually, logout won't pay you fractions.
        // There's a deflator of 50% over the fraction, however
        if ((.@current / 60) >= 4 && (.@current / 60) < .maxHours)
            .@pay += ((.@current % 60) * _calc_wage()) / 120;
        // Fraction of hour code pending .maxHours enforcement
        // i.e. preventing you from working more than .maxHours per day
        Zeny += .@pay;
        // Terminate the work
        @alfred_st = 0;
        dispbottom l("You've stopped working and was paid %s GP.", fnum(.@pay));
        end;
    }
    // Keep the variable alive and in use
    @alfred_st += 0;
    // Keep bleeping!
    addtimer(rand(1000,5000), "Alfred::OnBleep");
    end;

L_Working:
    .@current = gettimeparam(GETTIME_MINUTE) - @alfred_st;
    if (.@current / 60 >= .maxHours)
        npctalk3 l("You have worked the limit of %d hours and will not be paid for overtime.", .maxHours);
    else if (.@current / 60 >= 4)
        npctalk3 l("You have currently worked %d hour(s) and %d minute(s). Fraction of hour may be paid in half. The limit is %d hours.", .@current / 60, .@current % 60, .maxHours);
    else
        npctalk3 l("You have currently worked %d hour(s) and %d minute(s). Fraction of hour won't be paid.", .@current / 60, .@current % 60);
    end;

// Calculate your hourly wage
public function _calc_wage {
    .@wage = 0;
    /* Player Story (8 arcs) */
    // Fortress Arc Complete
    if (getq(General_Narrator) >= 23)
        .@wage += 850;
    // LoF Arc Complete
    if (getq(General_Narrator) >= 19)
        .@wage += 850;
    // Frostia Arc Complete
    if (getq(General_Narrator) >= 17)
        .@wage += 850;
    // Nivalis Arc Complete
    if (getq(General_Narrator) >= 12)
        .@wage += 850;
    // Halinarzo Arc Complete
    if (getq(General_Narrator) >= 10)
        .@wage += 850;
    // Hurnscald Arc Complete
    if (getq(General_Narrator) >= 6)
        .@wage += 850;
    // Tulimshar Arc Complete
    if (getq(General_Narrator) >= 3)
        .@wage += 850;
    // Candor Arc Complete
    if (getq(General_Narrator) >= 1)
        .@wage += 850;

    // Game Storyline Inflation Bonus
    if ($GAME_STORYLINE >= 3)
        .@wage += ($GAME_STORYLINE-2) * 200;

    // Personal Inflation Bonus
    .@wage += min((gettimetick(2) - TUT_VAR) / 86400, 500);

    // Global Inflation Bonus
    .@wage += min(TOP3AVERAGELVL()/2, 100);

    // Return the wage, capped at 8k/hour
    return .@wage;
}

OnInit:
    .sex = G_MALE;
    .distance = 5;
    .maxHours = 7 + $GAME_STORYLINE;
    end;
}
