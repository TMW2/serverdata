// TMW2 Script
// Author:
//    Jesusalva
// Description:
//    Hurnscald farmer

014-3,169,66,0	script	Oscar	NPC_RAIJIN,{
    .@q1=getq(HurnscaldQuest_Farmers);
    .@q2=getq2(HurnscaldQuest_Farmers);
    mesn;
    if (strcharinfo(0) == $MOST_HEROIC$) mesq l("Ah, if it isn't @@? You're the talk of the town!", $MOST_HEROIC$);
    if (strcharinfo(0) == $MOST_HEROIC$) next;
    if (.@q1 == 2) goto L_Hello;
    if (.@q1 == 3) goto L_Check;
    if (.@q1 == 4) goto L_Letter;
    if (.@q1 == 5) goto L_Thanks;

    mesq l("Don't bother me, I'm busy right now. Since the Monster King left, I barely got a night of rest...");
    close;

L_Hello:
    mesq l("Ah! @@! I heard you helped Hinnak with his Pinkies problem!", strcharinfo(0));
    next;
    mesn;
    mesq l("Wouldn't you like to help me too?");
    if (askyesno() != ASK_YES) {
        mesq l("Sorry, I have to go.");
        close;
    }
    next;
    mesn;
    mesq l("Great! Excellent! Because I am also diving in problems!!");
    next;
    if (readparam2(bAgi) < 30) goto L_Slow;
    if (BaseLevel < 20) goto L_Weak;
    mesn;
    // Est. kill 400 Pinkies
    mesq l("I hate so much the Pinkies on my fields, I want to do a soup! Can you provide me 10 @@? Many thanks.", getitemlink(PinkieLeg));
    setq HurnscaldQuest_Farmers, 3, 0;
    close;

L_Slow:
    mesn;
    mesq l("But you're too slow. Sorry.");
    next;
    mesc l("Raise agility to do this quest.");
    close;

L_Weak:
    mesn;
    mesq l("But you're too weak. Sorry.");
    close;

L_Check:
    mesq l("Hey, how is the monster extermination going?");
    next;
    mesn;
    // You in average need to kill 400 Pinkies. But ya killed 200~250 previously. You get this bonus for not leaving the fields.
    if (.@q2 >= 250)
        mesq l("You killed so many Pinkies! My wife and I love you!");
    else if (.@q2 >= 200)
        mesq l("You are doing GREAT! Keep slaying them, hahaah!");
    else if (.@q2 >= 150)
        mesq l("I see you are doing good. Keep slaying them, hahaah!");
    else if (.@q2 >= 100)
        mesq l("Good job, you already killed over 100! Hahah, that sure teach them a lesson!");
    else if (.@q2 >= 50)
        mesq l("Yeah, teach them a lesson! Keep going!");
    else
        mesq l("Go kill them!!");
    next;
    menu
        rif (.@q2 >= 250 && countitem(PinkieLeg) >= 10, l("Today is soup of Pinkie Legs!")), L_RealHelp,
        rif (.@q2 >= 250 && countitem(PinkieLeg) < 10, l("Thanks, they hate to drop their Legs.")), L_TrueHelp,
        rif (.@q2 >= 100 && .@q2 < 250 && countitem(PinkieLeg) >= 10, l("Here are the Pinkie Legs.")), L_Help,
        rif (.@q2 < 100 && countitem(PinkieLeg) >= 10, l("Here are the Pinkie Legs.")), L_NoKill,
        l("I'm not done yet. I'll be back."), -;
    close;

L_RealHelp:
    delitem PinkieLeg, 10;
    getexp 2000, 60;
    Zeny=Zeny+5000;
    setq HurnscaldQuest_Farmers, 4, 0;
    mes "";
    mesn;
    mesq l("Waw! You really did it, you're the savior of my farm!! You have my eternal gratitute. Here is 5000 GP for your troubles!");
    close;

L_TrueHelp:
    mes "";
    mesn;
    mesq l("I know how hard it is! I hate them with all my heart, only seeing you killing them left and right was GREAT!");
    next;
    getexp 1600, 0;
    Zeny=Zeny+2500;
    setq HurnscaldQuest_Farmers, 4, 0;
    mesn;
    mesq l("You have my eternal gratitute. Here is 2500 GP for your troubles.");
    close;

L_Help:
    delitem PinkieLeg, 10;
    getexp 1400, 0;
    Zeny=Zeny+2000;
    setq HurnscaldQuest_Farmers, 4, 0;
    mes "";
    mesn;
    mesq l("Many thanks. You have my eternal gratitute. Here is 2000 GP for your troubles.");
    close;

L_NoKill:
    mesn;
    mesq l("Ahahahah, do you really think I'll accept legs if you don't help me cleaning my fields?");
    next;
    mesn;
    mesq l("Kill at least a hundred of Pinkies here, and then we can talk.");
    close;

L_Letter:
    mesq l("You're the savior of Hurnscald crops. Half from the world would die from famine, weren't for you.");
    next;
    mesn;
    mesq l("@@ and I signed this letter. Deliver it to Airlia on the Town Hall, and she'll reward you correctly.", l("Hinnak"));
    close;

L_Thanks:
    mesq l("Many thanks for all the help!");
    close;

OnKillPinkie:
    .@q1=getq(HurnscaldQuest_Farmers);
    .@q2=getq2(HurnscaldQuest_Farmers);
    if (.@q1 == 3) {
        setq2 HurnscaldQuest_Farmers, .@q2+1;
        if (! (.@q2+1) % 10)
            dispbottom l("@@ pinkies killed on @@'s field.", .@q2+1, l("Oscar"));
    }
    fix_mobkill(Pinkie);
    end;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, FarmerHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, LeatherShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, TulimsharGuardBoots);
    setunitdata(.@npcId, UDT_WEAPON, CottonTrousers);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 2);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 0);

    .sex = G_MALE;
    .distance = 5;
    end;
}

