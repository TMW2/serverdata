// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 021-1: Ice Labyrinth warps
021-1,212,300,0	warp	#021-1_212_300	0,0,019-2,76,25
021-1,103,40,0	script	#021-1_103_40	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 56,49; end;
}
021-1,241,280,0	script	#021-1_241_280	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 266,284; end;
}
021-1,183,282,0	script	#021-1_183_282	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 158,282; end;
}
021-1,159,282,0	script	#021-1_159_282	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 184,282; end;
}
021-1,128,266,0	script	#021-1_128_266	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 115,266; end;
}
021-1,116,266,0	script	#021-1_116_266	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 129,266; end;
}
021-1,80,268,0	script	#021-1_80_268	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 55,268; end;
}
021-1,56,268,0	script	#021-1_56_268	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 81,268; end;
}
021-1,109,255,0	script	#021-1_109_255	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 106,231; end;
}
021-1,106,232,0	script	#021-1_106_232	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 109,256; end;
}
021-1,103,199,0	script	#021-1_103_199	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 92,174; end;
}
021-1,92,175,0	script	#021-1_92_175	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 103,200; end;
}
021-1,148,150,0	script	#021-1_148_150	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 128,134; end;
}
021-1,128,135,0	script	#021-1_128_135	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 148,151; end;
}
021-1,146,172,0	script	#021-1_146_172	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 135,197; end;
}
021-1,135,196,0	script	#021-1_135_196	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 146,171; end;
}
021-1,36,249,0	script	#021-1_36_249	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 35,225; end;
}
021-1,35,226,0	script	#021-1_35_226	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 36,250; end;
}
021-1,36,200,0	script	#021-1_36_200	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 26,177; end;
}
021-1,26,178,0	script	#021-1_26_178	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 36,201; end;
}
021-1,24,157,0	script	#021-1_24_157	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 31,143; end;
}
021-1,31,144,0	script	#021-1_31_144	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 24,158; end;
}
021-1,91,125,0	script	#021-1_91_125	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 115,76; end;
}
021-1,115,77,0	script	#021-1_115_77	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 91,126; end;
}
021-1,57,49,0	script	#021-1_57_49	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 104,40; end;
}
021-1,265,284,0	script	#021-1_265_284	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 240,280; end;
}
021-1,283,271,0	script	#021-1_283_271	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 269,244; end;
}
021-1,269,245,0	script	#021-1_269_245	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 283,272; end;
}
021-1,286,190,0	script	#021-1_286_190	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 289,163; end;
}
021-1,289,164,0	script	#021-1_289_164	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 286,191; end;
}
021-1,292,123,0	script	#021-1_292_123	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 296,96; end;
}
021-1,296,97,0	script	#021-1_296_97	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 292,124; end;
}
021-1,275,73,0	script	#021-1_275_73	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 283,47; end;
}
021-1,283,48,0	script	#021-1_283_48	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 275,74; end;
}
021-1,197,263,0	script	#021-1_197_263	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 187,227; end;
}
021-1,187,228,0	script	#021-1_187_228	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 197,264; end;
}
021-1,218,265,0	script	#021-1_218_265	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 227,241; end;
}
021-1,227,242,0	script	#021-1_227_242	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 218,266; end;
}
021-1,212,193,0	script	#021-1_212_193	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 207,167; end;
}
021-1,207,168,0	script	#021-1_207_168	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 212,194; end;
}
021-1,236,211,0	script	#021-1_236_211	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 235,166; end;
}
021-1,235,167,0	script	#021-1_235_167	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 236,212; end;
}
021-1,195,122,0	script	#021-1_195_122	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 208,91; end;
}
021-1,208,92,0	script	#021-1_208_92	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 195,123; end;
}
021-1,228,121,0	script	#021-1_228_121	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 232,92; end;
}
021-1,232,93,0	script	#021-1_232_93	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 228,122; end;
}
021-1,192,70,0	script	#021-1_192_70	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 200,49; end;
}
021-1,200,50,0	script	#021-1_200_50	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 192,71; end;
}
