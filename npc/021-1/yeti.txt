// TMW2 script.
// Authors:
//    Jesusalva
// Description:
//    Controls the Cindy Gateway quest node.
//    Also controls Watch Spot Yetis.

021-1,246,206,0	script	Yeti#0211Cindy	NPC_YETI,{
    // Is this the right Yeti?
    .@n=getq(NivalisQuest_Well);

    if (.@n < 2)
        goto L_NoQuest;

    CINDY_PLAYER_STRUCK=false;
    // Alright, now we need to cycle Cindy quest to decide if this NPC will have
    // a special behavior or not.
    .@q=getq(NivalisQuest_Cindy);

    if (.@q == 1) goto L_Assigned;      // Quest Assigned
    if (.@q == 2) goto L_Report;        // Quest (Sub) Accepted
    if (.@q <= 5) goto L_Instructions;  // Quest (Sub) Completed
    if (.@q >= 6) goto L_Complete;      // Quest completed at least once.

L_Thanks:
    mesn;
    mesq l("Hey, thanks for saving me. You know, from the well.");
    next;
    mesn strcharinfo(0);
    mesq l("You're wel-- Wait, it was you who was trapped on the well near Miler's house?!");
    next;
    mesn;
    mesq l("Yes, exactly. Thanks for the rescue. I must guard this area from the caves, if you need, just call me.");
    close;

L_Assigned:
    mesn;
    mesq l("Hello my friend.");
    next;
    select
        l("Hi."),
        l("Hm, do you perhaps know where to find Cindy and could help me?"),
        l("Die, your evil Yeti!");
    mes "";
    switch (@menu) {
        case 1: goto L_Thanks;
        case 2:
            mesn;
            mesq l("Hm, would \"Cindy\" be a small, little girl, kidnapped by some sturdy-looking rogue Yetis?");
            next;
            mesn strcharinfo(0);
            mesq l("Probably.");
            next;
            mesn;
            mesq l("Well, yes, I've saw them heading off to the Yeti King room. Lately, many Yetis have been uprising against your majesty.");
            next;
            mesn;
            mesq l("Unfortunately, @@s are not welcome on the Yeti Domains. You're no exception.", get_race());
            next;
            mesn strcharinfo(0);
            select
                l("Alright, good bye."),
                l("Can't I do anything, though?"),
                l("Tell me how or I'll kill you!");
            mes "";
            if (@menu == 3) {
                mesn;
                mesq l("Now, that's unfortunate. I don't think you can hit me from where you are.");
                next;
                mesn;
                mesq l("Besides, I know these caves like the back of my hand. You would have a hard time tracking me down.");
                next;
                mesn;
                mesq l("In fact, you would get lost and killed. If I were you, I would avoid the Death Penalty. It's high, ya know?");
            } else if (@menu == 2) {
                mesn;
                mesq l("Well, the Yeti King has went away to the cliffs north of this one. He'll get back here very hungry.");
                next;
                mesn;
                mesq l("I am his right-hand Yeti, so I could grant you passage to his Throne Room to slay the rogue Yetis...");
                next;
                mesn;
                mesq l("...But saving me won't be enough, so build trust by the kind gift of @@ @@. I'll be waiting.", 50, getitemlink(MoubooSteak)); // A normal Steak could do, but...
                setq NivalisQuest_Cindy, 2;
            }
            close;
        case 3:
            mesn;
            mesq l("That's rude. I never did anything to you.");
            next;
            mesn;
            mesq l("Please get moving, @@. I do not mean any harm to you.", get_race());
            close;
    }
    close;

L_Report:
    mesn;
    mesq l("Did you brought us a kind gift of good will, on the worth of %d %s, to offer to our King?", 50, getitemlink(MoubooSteak));
    next;
    select
        rif(countitem(MoubooSteak) >= 50, l("Yes, here they are.")), // Demure was here
        l("Not yet.");
    mes "";
    if (@menu == 2) {
        mesn;
        mesq l("Then please bring me this, so I can help you back.");
        close;
    }
    delitem MoubooSteak, 50;
    getexp 120000, 0; // roughly 30% from needed EXP. This quest IS boring.
    setq NivalisQuest_Cindy, 3;
    mesn;
    mesq l("Great. I hereby task you to kill the rogue Yetis on the Yeti King Throne Room. Of course, you won't be able to leave until all of them are dead.");
    next;
    mesn;
    mesq l("Getting there is tricky, so listen carefully to the instructions:");
    next;
L_Instructions:
    mesn l("ROGUE YETI SLAYING REQUEST");
    mesc l("Mission: Kill all rogue Yetis on the Throne Room. You won't be able to leave until all of them are dead.");
    next;
    mesc l("Directions: ");
    mesc l("You must flip all 5 switches on this cave, and then walk accross the cave blocked with an etheral crystal.");
    mesc l("First timers will be lead to somewhere random on this cave to keep intruders out. Just walk there again while all switches are still active.");
    mesc l("This random cave-warping only happens once, too.");
    mesc l("The Throne Room is the last room. You can reactive any switch which turns itself off.");
    next;
    mesc l("Notes: ");
    mesc l("To unlock the cell on the middle of the throne room, you need to flip all 5 switches there.");
    mesc l("You need a @@ to flip the switches on the Throne Room, don't forget to bring at least five.", getitemlink(TreasureKey));
    mesc l("EVERY switch on EVERY cave will unflip itself after about 2 minutes. So don't waste your time."), 1;
    mesc l("Keep in mind that %s are weak, baby Yetis, so kill them as well!" % getmonsterlink(Moggun));
    next;
    mesn;
    mesq l("Good luck. %%1");
    close;

L_Complete:
    //mesc l("ToDo"), 1;
    mesn;
    mesq l("Hello my friend. Thanks for slaying the rogue Yetis.");
    mesc l("Do you want to read again the instructions?");
    next;
    if (askyesno() == ASK_YES) {
        goto L_Instructions;
    }
    close;

// The other NPC
L_NoQuest:
    if (TUTORIAL)
        dispbottom l("I might want to ask an Yeti specialist about this. Maybe Celestia?");
    dispbottom l("Who's Mercury? Maybe I'm missing something? Where could he have went?");
    CINDY_PLAYER_STRUCK=true;
    @yetiagro=@yetiagro+1;
    if (@yetiagro > 5) goto L_Hit;
    if (@yetiagro > 3) goto L_Warning;
    if (@yetiagro > 1) goto L_Disrupt;
    goto L_Walking;

L_Walking:
    mesn;
    mesq l("Grr, where's Mercury... He was supposed to keep watch over this spot... It was meant to be my free time!");
    next;
    mesn;
    mesq l("And you, @@... You better get walking. Soon.", get_race());
    if (TUTORIAL)
        mesc l("WARNING: Angela Quest cannot be finished without finishing Mercury sidequest first."), 1;
        if (!.@n) {
            mesc l("(...This probably could have been a great hint... if I knew who Mercury in first place %%L)");
            mesc l("I suggest we ignore the hint and ask %s about it instead.", b("Celestia"));
        }
    close;

L_Disrupt:
    npctalk3 l("Grrr... Why are @@ on this cave?! Where did Mercury went after all?!", get_race());
    end;

L_Warning:
    npctalk3 l("Stupid @@, get out of this cave before I lose my patience! Mercury... Show up already!", get_race());
    end;

L_Hit:
    npctalk3 l("DIE, SCUM!");
    @yetiagro=0;
    percentheal -90, -100;
    close;

OnInit:
    .sex=G_OTHER;
    .distance=20;
    end;
}






/////////////////////////////////////////////////////// Random Lookout Yetis
021-1,144,203,0	script	Yeti#0211Guard	NPC_YETI,{
    dispbottom l("I better don't bother this Yeti, before it kills me.");
    @yetiagro=@yetiagro+1;
    if (@yetiagro > 5) goto L_Hit;
    if (@yetiagro > 3) goto L_Warning;
    if (@yetiagro > 1) goto L_Disrupt;
    goto L_Walking;

L_Walking:
    mesn;
    mesq l("You, @@... You better get walking. Soon.", get_race());
    close;

L_Disrupt:
    mesn;
    mesq l("Grrr... Why are @@ on this cave?! Begone, before I lose my patience!", get_race());
    close;

L_Warning:
    mesn;
    mesq l("Stupid @@, get out of this cave before I lose my patience!", get_race());
    close;

L_Hit:
    mesn;
    mesq l("DIE, SCUM!");
    percentheal -90, -100;
    @yetiagro=0;
    close;

OnInit:
    .sex=G_OTHER;
    .distance=20;
    end;
}




//021-1,25,298,0	duplicate(Yeti#0211Guard)	Yeti#0211G2	NPC_YETI

