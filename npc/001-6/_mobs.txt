// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 001-6: Cave Of Trials mobs
001-6,94,24,92,38	monster	Bif	1058,6,45000,35000
001-6,159,156,6,7	monster	King Of Trials	1079,1,60000,60000
001-6,147,139,38,42	monster	Giant Mutated Bat	1044,4,60000,60000
001-6,40,143,22,37	monster	Red Slime	1092,24,20000,60000
001-6,40,75,33,37	monster	Lava Slime	1097,12,30000,60000
001-6,88,160,30,24	monster	Yellow Slime	1091,8,60000,60000
001-6,112,52,69,41	monster	Snake	1122,17,60000,60000
001-6,119,91,57,42	monster	Black Scorpion	1074,23,30000,50000
001-6,135,117,11,12	monster	Dark Lizard	1051,2,60000,60000
001-6,30,31,11,12	monster	Dark Lizard	1051,1,60000,90000
001-6,155,159,34,35	monster	Lava Slime	1097,6,30000,30000
001-6,169,77,22,37	monster	Red Slime	1092,8,20000,60000
