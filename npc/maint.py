#!/usr/bin/python3
import traceback, os, random

w=open("maintenance.txt", 'w')
w.write("// TMW2 Scripts\n")
folders=sorted(os.listdir())
folders=folders+["guilds","botcheck","soren","sec_pri"]
for f in folders:
    if "-" in f and not "000-0" in f and f not in ["003-2-0"]:
        npc = random.choice(['NPC_CONSTR_WORKER', 'NPC_CONSTR_WORKER', 'NPC_FEMALE_WORKER', 'NPC_MAINTENANCE_A', 'NPC_MAINTENANCE_B'])
        w.write("%s,20,20,0\tscript\tMaintainer#%s\t%s,{" % (f, f, npc))
        w.write("\n\tmesn;\n\tmesq l(\"Sorry, we're doing an update at the moment, come back later.\");\n\tclose;\nOnInit:\n\t.@mx=getmapinfo(MAPINFO_SIZE_X, \"%s\")-20;\n\t.@my=getmapinfo(MAPINFO_SIZE_Y, \"%s\")-20;\n\tdo {\n\t\tif (.@e >= 20) break;\n\t\t.@x=rand2(20,.@mx);.@y=rand2(20,.@my);.@e+=1;\n\t} while (!checknpccell(\"%s\", .@x, .@y, cell_chkpass));\n\tmovenpc(.name$, .@x, .@y);\n\tend;\n}\n\n" % (f, f, f))

w.close()
print("gm use @loadnpc npc/maintenance.txt")
