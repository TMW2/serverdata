// TMW2 Script.
// Authors:
//    Jesusalva
// Description:
//    Daily Login Reward

function	script	daily_login_bonus_handler	{
    // Handle daily login bonus
    // The Strange Coin output wasn't changed, but now it relies on streaks.
    // Variables:
    //  #LOGIN_DAY
    //      Current day
    //  #LOGIN_TABLE
    //      Current month
    //  #LOGIN_STREAK
    //      Number of monthly connections
    //  #LOGIN_ALLTIME
    //      Number of times you claimed the top prize (27 days streak)
    //debugmes "DLBH";

    // GMs can receive Strange Coins
    if (GSET_AUTORECEIVE_COINS) {
        if (is_gm()) {
            if (#GMEVENT_T <= gettimetick(2)) {
                #GMEVENT_T=gettimetick(2)+(60*60*24);
                getitem StrangeCoin, 30;
            }
        } else {
            GSET_AUTORECEIVE_COINS=false;
        }
    }

    if (#LOGIN_DAY != gettime(5)) {
        // demure check: Are you on a start area?
        getmapxy(.@m$,.@x,.@y,0);
        if (compare(.@m$, "000-0"))
            return;

        //debugmes "[DLBH] Mapcheck ok";
        // Is it a new month?
        if (#LOGIN_TABLE == gettime(6)) {
            #LOGIN_STREAK=#LOGIN_STREAK+1;
        } else {
            #LOGIN_STREAK=1;
            #LOGIN_TABLE=gettime(6);
            #TMW2_LOGINBONUS=0;
        }

        // Update last day you've claimed a reward
        #LOGIN_DAY = gettime(5);
        //debugmes "[DLBH] month checks ok";

        // Handle rewards: Streaks first, daily later. Streak reward prevail over daily reward.
        if ($@NOUPDATES) {
            dispbottom col(l("Updates were disabled"), 1);
        } else if (#LOGIN_STREAK > 27) {
            getitem StrangeCoin, 2;
            getitem CasinoCoins, 1;
            dispbottom l("##2 @@ Days login bonus: ##B2x @@, 1x @@##b", #LOGIN_STREAK, getitemlink(StrangeCoin), getitemlink(CasinoCoins));
        } else if (#LOGIN_STREAK == 27) {
            #LOGIN_ALLTIME+=1;
            .@am=1;
            switch (#LOGIN_ALLTIME % 12) {
            case 1:
                if (#LOGIN_ALLTIME == 1)
                    .@prize=RightEyePatch;
                else
                    .@prize=SilverGift;
                break;
            case 2:
                .@prize=ArcmageBoxset; break;
            case 3:
                .@prize=MercBoxA; break;
            case 4:
                .@prize=AncientBlueprint; break;
            case 5:
                .@prize=StrangeCoin; .@am=120; break;
            case 6:
                if (#LOGIN_ALLTIME == 6) {
                    dispbottom l("CONGRATULATIONS! For a semester worth of logins, you're getting a pet!");
                    .@prize=PiouEgg; .@am=0;
                    makepet Piou;
                } else {
                    .@prize=GoldenGift;
                }
                break;
            case 7:
                .@prize=GoldenGift; break;
            case 8:
                .@prize=MercBoxB; break;
            case 9:
                .@prize=PrismGift; break;
            case 10:
                .@prize=StrangeCoin; .@am=150; break;
            case 11:
                .@prize=MercBoxC; break;
            case 0:
                .@prize=MysteriousFruit; break;
            default:
                .@prize=ElixirOfLife; break;
            }

            if (.@am)
                getitem .@prize, .@am;
            dispbottom l("##2 27 Days login bonus: ##B1x @@##b", getitemlink(.@prize));
        } else if (#LOGIN_STREAK == 21) {
            getitem BronzeGift, 1;
            dispbottom l("##2 21 Days login bonus: ##B1x @@##b", getitemlink(BronzeGift));
        } else if (#LOGIN_STREAK == 14) {
            getitem BronzeGift, 1;
            dispbottom l("##2 14 Days login bonus: ##B1x @@##b", getitemlink(BronzeGift));
        } else if (#LOGIN_STREAK == 7) {
            getitem StrangeCoin, 3;
            dispbottom l("##2 7 Days login bonus: ##B3x @@##b", getitemlink(StrangeCoin));
        } else if (#LOGIN_STREAK == 3) {
            getitem StrangeCoin, 1;
            dispbottom l("##2 3 Days login bonus: ##B1x @@##b", getitemlink(StrangeCoin));
        } else if (#LOGIN_STREAK % 3 == 0) {
            .@value=max(20, rand(0, (#LOGIN_STREAK*2)));
            .@value+=(BaseLevel*5/2)+rand2(JobLevel, JobLevel*7/20);
            Zeny=Zeny+.@value;
            dispbottom l("##2Daily login bonus: ##B@@ GP##b", .@value);
        } else if (#LOGIN_STREAK % 3 == 2) {
            .@value=max(5, rand2(0, (#LOGIN_STREAK/4)));
            .@value+=(BaseLevel**2);
            .@value=(.@value/2)+#LOGIN_STREAK;
            getexp .@value, 0;
            dispbottom l("##2Daily login bonus: ##B@@ EXP##b", .@value);
        } else {
            .@value=max(5, rand2(0, (#LOGIN_STREAK/4)));
            .@value+=(JobLevel**2);
            .@value=(.@value/3)+#LOGIN_STREAK;
            getexp 0, .@value;
            dispbottom l("##2Daily login bonus: ##B@@ Job Exp.##b", .@value);
        }

        // Handle event login bonus
        if (gettime(6) == JANUARY) {
            if (#TMW2_LOGINBONUS != gettime(GETTIME_YEAR) && gettime(5) == 13) {
                #TMW2_LOGINBONUS=gettime(GETTIME_YEAR);
                getitem StrangeCoin, 10;
                // TMW2 Anniversary is project, not server.
                // Therefore, contributors get an extra reward
                .@m=htget($@CONTRIBUTORS, strtolower(strcharinfo(0)), 0);
                if (.@m)
                    getitem StrangeCoin, min(9, .@m/100)+1;
                dispbottom "##B##2"+l("It's TMW2 Project anniversary!")+" "+l("We thank every developer which helped this project thus far!")+"##b##0";
            }
        }
        if (gettime(6) == MARCH) {
            if (#TMW2_LOGINBONUS != gettime(GETTIME_YEAR) && gettime(5) == 2) {
                #TMW2_LOGINBONUS=gettime(GETTIME_YEAR);
                getitem MercBoxC, 1;
                // TMW2 Day is server, not project.
                // Therefore, players get extra reward
                dispbottom "##B##2"+l("It's TMW2 Server anniversary!")+" "+l("We thank every player, because without them, this would be nothing!")+"##b##0";
            }
        }
        if (gettime(6) == JUNE) {
            if (#TMW2_LOGINBONUS != gettime(GETTIME_YEAR) && gettime(5) == 21) {
                #TMW2_LOGINBONUS=gettime(GETTIME_YEAR);
                getitem TreasureMap, 1;
                dispbottom "##B##2"+l("It's Jesusalva's anniversary!")+" "+l("Also, Summer just started. Why not taking this opportunity to go Treasure Hunting?!")+"##b##0";
            }
        }
        if (gettime(6) == JULY) {
            if (#TMW2_LOGINBONUS != gettime(GETTIME_YEAR) && gettime(5) == 7) {
                #TMW2_LOGINBONUS=gettime(GETTIME_YEAR);
                getitem ChocolateBar, 1;
                dispbottom "##B##2"+l("It's International Chocolate Day!")+"##b "+l("All monsters may drop chocolate during this period. And here is one for you!")+"##0";
            }
        }
        if (gettime(6) == SEPTEMBER) {
            if (#TMW2_LOGINBONUS != gettime(GETTIME_YEAR) && gettime(5) == 9) {
                #TMW2_LOGINBONUS=gettime(GETTIME_YEAR);
                getitem any(ScentGrenade, Grenade, SmokeGrenade), 1;
                dispbottom "##B##2"+l("It's the Free Software Day!")+" "+l("Licensing was one of the worst hassle we had, but just today, all mobs may drop Ancient Blueprints. Enjoy!")+"##b##0";
            }
        }
        if (gettime(6) == OCTOBER) {
            if (#TMW2_LOGINBONUS != gettime(GETTIME_YEAR) && gettime(5) == 1) {
                #TMW2_LOGINBONUS=gettime(GETTIME_YEAR);
                getitem Coffee, 1;
                getexp BaseLevel, JobLevel;
                dispbottom "##B##2"+l("It's the International Coffee Day!")+" "+l("Have a warm cup of Coffee on the house, and enjoy!")+"##b##0";
            }
        }
        if (gettime(6) == DECEMBER) {
            if (!#XMAS_LOGINBONUS && gettime(5) >= 24 && gettime(5) <= 26) {
                #XMAS_LOGINBONUS=1;
                getitem XmasGift, 1;
                dispbottom "##B##2"+l("Merry Christmas!")+" "+l("You have gained a special login bonus!")+"##b##0";
            }
        }

        // We're almost done with daily logins, just the optional User Interface
        if (!GSET_DAILYREWARD_SILENT) {
            setnpcdialogtitle l("Daily Login Rewards");
            setskin "daily_"+#LOGIN_STREAK;
            mes "Please keep your ManaPlus updated.";
            //mes "This is a debug message. Your manaplus version is wrong.";
            //mes "You should not be reading this. I'll call you a cheater.";
            //mes "I hope you report this (if a bug). Reading source code?";
            //mes "4144 will hear about this. You are NOT amazing by the way.";
            select("Ok");
            setskin "";
            closeclientdialog;
        }
    }

    //debugmes "[DLBH] Finished: "+#LOGIN_DAY+" ok";
    return;
}

// Gives you guild coins, but weekly (based on Guild Level)
function	script	guild_login_bonus	{
    .@g=getcharid(2);
    if (.@g < 1)
        return;

    .@c=min(5, getguildavg(.@g)/20) + min(1+getguildlvl(.@g)/5, 10);
    if (#LOGIN_GUILD_WEEK != gettimeparam(GETTIME_WEEKDAY)) {
        #LOGIN_GUILD_WEEK=gettimeparam(GETTIME_WEEKDAY);
        getitem GuildCoin, .@c;
        dispbottom l("##2Guild's Weekly login bonus: ##B%d %s##b", .@c, getitemlink(GuildCoin));
    }
    return;
}

