// TMW-2 Script
// Author:
//    Jesusalva
//  Description:
//    Nurse for all hurt players

// Name, Base Price, Price Multiplier
// getarg(3) switches functions:
//   1 - Only healing dialog (returns price)
//   2 - Only core healing
//   3 - everything (default)
function	script	Nurse	{
    // Handle redirects
    .@d=getarg(3, 3);

    // Calculate price
    .@price=(MaxHp-Hp)/getarg(1,5);
    .@price=.@price+getarg(2, 10);
    if (BaseLevel <= 15) .@price=(.@price/10);
    else if (BaseLevel <= 20) .@price=(.@price/5);
    else .@price=(.@price/2);


    // 1 - Only Healing Dialog and return .@price
    if (.@d & 1) {//&1
        mes "";
        mesn getarg(0);

        // Random message
        .@temp = rand2(1,4);
        switch (.@temp) {
            case 1:
                mesq l("You don't look too well; let me treat your wounds.");
                break;
            case 2:
                mesq l("I will make quick work of your wounds.");
                break;
            case 3:
                mesq l("Need a healing?");
                break;
            case 4:
                mesq l("Sometimes you just need to run from battle.");
                break;
        }
        mes "";

    // Skip menu flag
    if (!(.@d & 2))
        return .@price;

    mesq l("For you, it'll be @@ GP.", .@price);
    mes "";
    select
        rif(Zeny >= .@price, l("Please heal me!")),
        l("Another time, maybe.");

    if (@menu == 2) {
        closedialog;
        goodbye;
        close;
    }

    }// & 1

    // Allowed Nurse to do the healing
    if (.@d & 2) { // &2

        // Heal persona
        mes "";
        set Zeny, Zeny - .@price;
        sc_end(SC_POISON);
        sc_end(SC_SLOWPOISON);
        percentheal 100,100; // We can also use "recovery()" but that revives players :o
        mesn getarg(0);
        @temp = rand2(1,4);
        if(@temp == 1)    mesq l("Here you go!");
        if(@temp == 2)    mesq l("Painless, wasn't it?");
        if(@temp == 3)    mesq l("You should be more careful.");
        if(@temp == 4)    mesq l("Much better, right?!");
        close;

    }//&2
    return;
}
