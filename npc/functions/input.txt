// Evol functions.
// Author:
//    4144
//    Jesusalva
// Description:
//    Input utility functions
// Variables:
//    none

function	script	menuint	{
    deletearray .@vals;
    .@menustr$ = "";
    .@cnt = 0;

    for (.@f = 0; .@f < getargcount(); .@f = .@f + 2)
    {
        if (getarg(.@f) != "")
        {
            .@menustr$ = .@menustr$ + getarg(.@f) + ":";
            .@vals[.@cnt] = getarg(.@f + 1);
            .@cnt ++;
        }
    }

    .@vals[.@cnt] = -1;
    @menu = 255;
    @menuret = -1;
    select(.@menustr$);
    if (@menu == 255)
        return -1;

    @menu --;
    if (@menu < 0 || @menu >= getarraysize(.@vals) - 1)
        return -1;

    @menuret = .@vals[@menu];
    return @menuret;
}

function	script	menustr	{
    deletearray .@vals$;
    .@menustr$ = "";
    .@cnt = 0;

    for (.@f = 0; .@f < getargcount(); .@f = .@f + 2)
    {
        if (getarg(.@f) != "")
        {
            .@menustr$ = .@menustr$ + getarg(.@f) + ":";
            .@vals$[.@cnt] = getarg(.@f + 1);
            .@cnt ++;
        }
    }

    @menu = 255;
    @menuret = -1;
    select(.@menustr$);
    if (@menu == 255)
        return "";

    @menu --;
    if (@menu < 0 || @menu >= getarraysize(.@vals$))
        return "";

    @menuret$ = .@vals$[@menu];
    return @menuret$;
}

// menuint2(<array>)
function	script	menuint2	{
    .@menustr$="";

    if (!(getdatatype(getarg(0)) & DATATYPE_VAR))
        Exception("Inadequate argument type - Must be var", RB_DEFAULT|RB_ISFATAL);

    copyarray(.@ar$, getarg(0), getarraysize(getarg(0)));

    if (getarraysize(.@ar$) % 2 != 0)
        Exception("Invalid array size: "+getarraysize(.@ar$), RB_DEFAULT|RB_ISFATAL);

    freeloop(true);
    for (.@f=0; .@f < getarraysize(.@ar$); .@f++) {
        // String vs Int
        if (.@f % 2 == 0) {
            .@menustr$+=.@ar$[.@f]+":";
        } else {
            array_push(.@vals, atoi(.@ar$[.@f]));
        }
    }
    freeloop(false);

    // Do the request
    // We have: .@vals and .@menustr$
    @menu = 255;
    @menuret = -1;
    select(.@menustr$);
    //debugmes "Option %d", @menu;
    //debugmes "Array size %d", getarraysize(.@vals);

    if (@menu == 255)
        return -1;

    @menu-=1;
    if (@menu < 0 || @menu > getarraysize(.@vals) - 1)
        return -1;

    @menuret = .@vals[@menu];
    return @menuret;
}

