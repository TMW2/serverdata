// TMW2 Script
// Author: Jesusalva
// With parts from The Mana World.

function	script	SoulMenhir	{
    // Create @x and @y for this script
    @n = rand2(getarraysize(@Xs));
    @x = @Xs[@n];
    @y = @Ys[@n];
    deletearray @Xs;
    deletearray @Ys;
    @n = 0;

    mes l("[Soul Menhir]");
    mes l("(A mystical aura surrounds this stone. You feel mysteriously attracted to it. Something tells you to touch it. What do you do?)");

    menu
        rif(@x && @y, l("Touch it.")), L_Bind,
        rif($@GM_EVENT, l("Send soul to the Mana Plane for GM events")), L_Aeros,
        rif($EVENT$ == "Valentine", l("[Valentine Day] Send soul to the Valentine Highlands!")), L_Valentine,
        rif($EVENT$ == "Easter", l("[Easter] Send soul to the Mana Forest!")), L_Easter,
        rif($EVENT$ == "Worker", l("[Worker Day] Send soul to the Contributor Cave!")), L_Worker,
        rif($EVENT$ == "Christmas" && BaseLevel >= 20, l("[Christmas] Send soul to the Christmas Workshop!")), L_Xmas, // TODO: In future there'll be an event map
        l("Leave it alone."), -;

    return;

L_Bind:
    if (@map$ == "003-1" && !(TELEPORTERS & TP_TULIM))
        TELEPORTERS=TELEPORTERS|TP_TULIM;
    if (@map$ == "012-1" && !(TELEPORTERS & TP_HURNS))
        TELEPORTERS=TELEPORTERS|TP_HURNS;
    if (@map$ == "020-1" && !(TELEPORTERS & TP_NIVAL))
        TELEPORTERS=TELEPORTERS|TP_NIVAL;

    if (Menhir_Activated == 1)
        goto L_Shortversion;

    mes l("[Soul Menhir]");
    mes l("(You touch the mysterious stone. Somehow it feels warm and cold at the same time.)");
    mes l("(Suddenly a strange sensation flows through you. It feels like your soul leaves your body and becomes one with the stone.)");
    mes l("(As suddenly as the feeling started it stops. The strange attraction is away from one moment to the next and the menhir feels like just an ordinary stone.)");
    Menhir_Activated = 1;
    goto L_Save;

L_Shortversion:
    mes l("[Soul Menhir]");
    mes l("(A strange sensation flows through you. It feels like your soul leaves your body and becomes one with the stone. As suddenly as the feeling started it stops.)");
    goto L_Save;

L_Aeros:
    if (!$@GM_EVENT) goto L_DontPanic;
    .@gt=$@AEROS_SPWN;
    if (.@gt == 2) {
        select
            l("East Aeros"),
            l("West Aeros");
        .@gt=@menu-1;
    }
    switch (.@gt) {
    case 0:
        warp "001-1", 235, 26; break;
    case 1:
        warp "001-1", 23, 108; break;
    }
    specialeffect(63, AREA, getcharid(3));
    message strcharinfo(0), l("You are now at the Mana Plane of Existence, at the Floating Island of Aeros.");
    close;

L_Valentine:
    if ($EVENT$ != "Valentine") goto L_DontPanic;
    warp "001-11", 38, 32;
    message strcharinfo(0), l("You are now at the Valentine Highlands.");
    close;

L_Easter:
    if ($EVENT$ != "Easter") goto L_DontPanic;
    warp "001-4", 151, 157;
    message strcharinfo(0), l("You are now at the Mana Plane of Existence, at the Magical Forest.");
    close;

L_Worker:
    if ($EVENT$ != "Worker") goto L_DontPanic;
    warp "001-5", 22, 79;
    message strcharinfo(0), l("You are now at the Mana Plane of Existence, at the Contributor's Cave.");
    close;

L_Xmas:
    if ($EVENT$ != "Christmas") goto L_DontPanic;
    warp "019-4-1", 32, 36;
    message strcharinfo(0), l("You are now at the Christmas Workshop.");
    close;

L_DontPanic:
    message strcharinfo(0), l("(A strange barrier keeps you from touching the stone at this time.)");
    return;

L_Save:
    if (GSET_SOULMENHIR_MANUAL) {
        .@v$=MapToLoc(@map$);
        .@tp=POL_LocToTP(strtoupper(.@v$));
        if (!(#EXILED & .@tp)) {
            savepoint @map$, @x, @y;
            specialeffect(4, SELF, getcharid(3));
        } else {
            mesc l("You are exiled from this town and therefore, unable to use the Menhir."), 1;
        }
    } else {
        EnterTown(MapToLoc(@map$));
        dispbottom col(l("Your position is auto-saved when entering a town - use @ucp to change this behavior."), 1);
    }
    return;
}
