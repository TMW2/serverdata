// TMW2 Script
// Modified by Jesusalva
// Evol scripts.
// Authors:
//    gumi
//    Reid

function	script	Banking	{
    do
    {
        if (BankVault > 0) {
            speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("You currently have @@ GP on your bank account.",
                        format_number(BankVault)),
                    l("What do you want to do with your money?");
        } else {
            speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                l("What do you want to do with your money?");
        }

        select
            rif(Zeny > 0, l("Deposit.")),
            rif(BankVault > 0, l("Withdraw.")),
            l("Buy a Housing Letter"),
            l("I'm done.");

        switch (@menu)
        {
            case 1:
                speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("How much do you want to deposit?");

                menuint
                    l("Other."), -1,
                    rif(Zeny >= 1000, format_number(1000) + " GP."), 1000,
                    rif(Zeny >= 2500, format_number(2500) + " GP."), 2500,
                    rif(Zeny >= 5000, format_number(5000) + " GP."), 5000,
                    rif(Zeny >= 10000, format_number(10000) + " GP."), 10000,
                    rif(Zeny >= 25000, format_number(25000) + " GP."), 25000,
                    rif(Zeny >= 50000, format_number(50000) + " GP."), 50000,
                    rif(Zeny >= 100000, format_number(100000) + " GP."), 100000,
                    l("All of my money."), -2,
                    l("I changed my mind."), -3;

                switch (@menuret) {
                    case -1:
                        input @menuret;
                        break;
                    case -2:
                        @menuret = Zeny;
                }

                if (@menuret > 0) {
                    if (@menuret > Zeny) {
                        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                            l("You do not have enough Gold on yourself.");
                        break;
                    }

                    @menuret = min(MAX_BANK_ZENY, @menuret); // make sure the variable can't overflow
                    .@before = BankVault; // amount before the deposit
                    .@max = MAX_BANK_ZENY - BankVault; // maximum possible deposit
                    .@deposit = min(.@max, @menuret); // actual deposit

                    if (.@deposit > 0) {
                        BankVault += .@deposit; // add to bank
                        Zeny -= .@deposit; // remove from inventory

                        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                            l("You made a cash deposit of @@ GP.", format_number(.@deposit));
                    }
                }
                break;

            case 2:
                speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("How much do you want to withdraw?");

                menuint
                    l("Other."), -1,
                    rif(BankVault >= 1000, format_number(1000) + " GP."), 1000,
                    rif(BankVault >= 2500, format_number(2500) + " GP."), 2500,
                    rif(BankVault >= 5000, format_number(5000) + " GP."), 5000,
                    rif(BankVault >= 10000, format_number(10000) + " GP."), 10000,
                    rif(BankVault >= 25000, format_number(25000) + " GP."), 25000,
                    rif(BankVault >= 50000, format_number(50000) + " GP."), 50000,
                    rif(BankVault >= 100000, format_number(100000) + " GP."), 100000,
                    l("All of my money."), -2,
                    l("I changed my mind."), -3;

                switch (@menuret)
                {
                    case -1:
                        input @menuret;
                        break;
                    case -2:
                        @menuret = BankVault;
                }

                if (@menuret > 0) {
                    if (@menuret > BankVault) {
                        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                            l("You do not have enough Gold Pieces on your bank account.");
                        break;
                    }

                    @menuret = min(MAX_ZENY, @menuret); // make sure the variable can't overflow
                    .@before = Zeny; // amount before the withdrawal
                    .@max = MAX_ZENY - Zeny; // maximum possible withdrawal
                    .@withdrawal = min(.@max, @menuret); // actual withdrawal

                    if (.@withdrawal > 0) {
                        Zeny += .@withdrawal; // add to inventory
                        BankVault -= .@withdrawal; // remove from bank

                        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                            l("You withdrew a total of @@ GP.", format_number(.@withdrawal));
                    }
                }
                break;

            case 3:
                .@gp=REAL_ESTATE_CREDITS+Zeny;
                mesc l("You currently have @@ mobiliary credits + GP at your disposal.", format_number(.@gp));
                mesc l("@@ - @@ - @@", getitemlink(HousingLetterI), getitemlink(HousingLetterII), getitemlink(HousingLetterIII));
                next;
                select
                    l("Nothing"),
                    rif(.@gp >= 11000, l("Housing Letter I for 11,000 GP")),
                    rif(.@gp >= 101000, l("Housing Letter II for 101,000 GP")),
                    rif(.@gp >= 1001000, l("Housing Letter III for 1,001,000 GP"));
                mes "";
                switch (@menu) {
                    case 2:
                        realestate_payment(11000);
                        getitem HousingLetterI, 1;
                        break;
                    case 3:
                        realestate_payment(101000);
                        getitem HousingLetterII, 1;
                        break;
                    case 4:
                        realestate_payment(1001000);
                        getitem HousingLetterIII, 1;
                        break;
                }
                break;
            default: return;
        }
    } while (true);
}

function	script	BKInfo	{
    speech S_LAST_NEXT,
        l("We organize some auction and we help local merchants to launch their businesses."),
        l("We also feature some services like a storage and a bank for members."),
        l("Registration is open to everybody, but newcomers need to pay a fee for all of the paperwork.");

    narrator S_FIRST_BLANK_LINE,
        l("The bank and item storage is shared between all characters within a same account."),
        l("With it, you can safely move items and funds between your characters."),
        l("To move between characters that are on different accounts, you have to use the Trade function.");
    return;
}

// name, city, price
function	script	BKReg	{
    .@price=max(2000, getarg(2)-#BankP);
    @menu=3;
    do
    {
        mesn getarg(0);
        mesq l("Register fee is @@.", .@price);
        mesc l("The fee only need to be paid once and will work in every town.");
        next;
        select
            rif(Zeny >= .@price, l("Register")),
            l("Not at the moment"),
            l("Information");
        mes "";
        if (@menu == 1) {
            Zeny=Zeny-.@price;
            setq General_Banker, 1;
            #BankP=#BankP+rand2(150,400);
            mesn getarg(0);
            mesq l("Registered! You can now use any banking service, of any town!");
        } else if (@menu == 3) {
            BKInfo();
        }
    } while (@menu == 3);
    return;
}

// name, city, price (defaults to 10k)
function	script	Banker	{
    mesn getarg(0);
    mesq l("Welcome! My name is @@, I am a representative of the Merchant Guild on @@.", getarg(0), getarg(1));
    next;

    if (getq(General_Banker) == 0) {
        BKReg(getarg(0), getarg(1), getarg(2,10000));
    } else {
        do
        {
            select
                l("I would like to store some items."),
                l("I would like to perform money transactions."),
                l("Did I received any mail?"),
                l("What is this guild for?"),
                l("Bye.");

            switch (@menu) {
                case 1:
                    closeclientdialog;
                    openstorage;
                    close;
                    break;
                case 2:
                    Banking();
                    break;
                case 3:
                    // NOTE: This value is HARDCODED, do not try changing it!
                    mesc l("Note: Transfering items on mail cost @@ GP/item", 500);
                    mesc l("Money transference by mail is, however, free.");
                    next;
                    closeclientdialog;
                    openmail();
                    close;
                    break;
                case 4:
                    mes "";
                    BKInfo();
                    break;
            }
            if (@menu != 5) {
                speech S_FIRST_BLANK_LINE | S_LAST_NEXT | S_NO_NPC_NAME,
                    l("Something else?");
            }
        } while (@menu != 5);
    }
    closedialog;
    goodbye;
    close;

}
