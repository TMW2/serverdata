// TMW2: Moubootaur Legends scripts.
// Author:
//    Jesusalva
// Description:
//    Real Estate System
//    Primary Script Helpers
//    WARNING: They affect directly the real estate global variables!

// This function reduces payment accordingly
// realestate_payment ( amount )
function	script	realestate_payment	{
    REAL_ESTATE_CREDITS=REAL_ESTATE_CREDITS-getarg(0);
    if (REAL_ESTATE_CREDITS < 0) {
        Zeny+=REAL_ESTATE_CREDITS;
        REAL_ESTATE_CREDITS=0;
    }
    return;
}

// Generate unique name for setcells
// realestate_cellname ( estate_id, object_id )
function	script	realestate_cellname	{
    return "RESObj_"+getarg(0)+"_"+getarg(1);
}

// Generate sell price for furniture based on original price and estate ID
// realestate_sellprice ( estate_id, price )
function	script	realestate_sellprice	{
    .@timeleft=$ESTATE_RENTTIME[getarg(0)]-gettimetick(2); // Number of seconds
    .@daysleft=.@timeleft/86400; // Number of days left of rent
    .@weeksleft=.@timeleft/604800; // Number of weeks left of rent

    //debugmes "Your contract is valid for %d weeks more - %d days", .@weeksleft, .@daysleft;
    //debugmes "The divisor is %d", max(1, 8-.@weeksleft);

    return (getarg(1)/max(1, 8-.@weeksleft)) - max(0, 60-.@daysleft);
}

// This will toggle if mobilia was purchased or not, in the right group
// And as an added bonus, will tell the correct Script to reload NPCs
// realestate_togglemobilia ( estate_id, layer_id, object_id{, npc_file} )
function	script	realestate_togglemobilia	{
    switch (getarg(1)) {
        case 1:
            $ESTATE_MOBILIA_64[getarg(0)] = $ESTATE_MOBILIA_64[getarg(0)] ^ getarg(2);
            break;
        case 2:
            $ESTATE_MOBILIA_4[getarg(0)] = $ESTATE_MOBILIA_4[getarg(0)] ^ getarg(2);
            break;
        case 3:
            $ESTATE_MOBILIA_8[getarg(0)] = $ESTATE_MOBILIA_8[getarg(0)] ^ getarg(2);
            break;
        case 4:
            $ESTATE_MOBILIA_32[getarg(0)] = $ESTATE_MOBILIA_32[getarg(0)] ^ getarg(2);
            break;
        case 5:
            $ESTATE_MOBILIA_128[getarg(0)] = $ESTATE_MOBILIA_128[getarg(0)] ^ getarg(2);
            break;
        case 6:
            $ESTATE_MOBILIA_2[getarg(0)] = $ESTATE_MOBILIA_2[getarg(0)] ^ getarg(2);
            break;
        default:
            debugmes("[ERROR] [CRITICAL] [REAL ESTATE]: Object %d have Invalid Collision Type: %d (must range 1~6)", getarg(2), getarg(1));
            break;
    }
    if (getarg(3, "error") != "error") {
        // Reload NPCs on the meanwhile
        donpcevent getarg(3)+"::OnReload";
    }
    return;
}


// Like the previous function, but returns true if player have said mobilia
// realestate_hasmobilia ( estate_id, layer_id, object_id )
function	script	realestate_hasmobilia	{
    switch (getarg(1)) {
        case 1:
            return $ESTATE_MOBILIA_64[getarg(0)] & getarg(2);
        case 2:
            return $ESTATE_MOBILIA_4[getarg(0)] & getarg(2);
        case 3:
            return $ESTATE_MOBILIA_8[getarg(0)] & getarg(2);
        case 4:
            return $ESTATE_MOBILIA_32[getarg(0)] & getarg(2);
        case 5:
            return $ESTATE_MOBILIA_128[getarg(0)] & getarg(2);
        case 6:
            return $ESTATE_MOBILIA_2[getarg(0)] & getarg(2);
        default:
            debugmes("[ERROR] [CRITICAL] [REAL ESTATE]: Object %d have Invalid Collision Type: %d (must range 1~6)", getarg(2), getarg(1));
            return false;
    }
    return false;
}

