// Evol functions.
// Authors:
//    gumi
//    Reid
// Description:
//    script terminator functions



// goodbye_msg
//    Tell a random goodbye sentence.
// Variables:
//    .@rand = Random number between the number of "goodbye" choice.

function	script	goodbye_msg	{
    setarray .byemsg$[0],
        l("See you!"),
        l("See you later!"),
        l("See you soon!"),
        l("Bye!"),
        l("Farewell."),
        l("Bye then!"),
        l("Goodbye."),
        l("Bye for now."),
        l("Talk to you soon!"),
        l("Talk to you later!"),
        l("Have a good day!"),
        l("Cheers!"),
        l("Take care!");

    return any_of(.byemsg$);
}



// cwarp
//     Closes the dialog, then warps the player.
//     You almost always want to use this instead of `warp`.
// usage:
//     cwarp;
//     cwarp x, y;
//     cwarp map, x, y;

function	script	cwarp	{
    .@map$ = getarg(0, "");
    .@x = getarg(1, 0);
    .@y = getarg(2, 0);

    if (getargcount() > 0 && getargcount() < 3)
    {
        .@npc$ = strnpcinfo(0);
        .@map$ = getvariableofnpc(.map$, .@npc$);
        .@x = getarg(0);
        .@y = getarg(1);
    }

    getmapxy .@pc_map$, .@pc_x, .@pc_y, UNITTYPE_PC; // get char location

    closedialog; // XXX: maybe send closeclientdialog in the future

    if (getargcount() < 1)
    {
        warp .@pc_map$, .@pc_x, .@pc_y; // no arguments, just refresh
        close;
    }

    if (.@map$ == .@pc_map$)
    {
        if (.@pc_x == .@x && .@pc_y == .@y)
        {
            close; // same location, don't move
        }

        else
        {
            slide .@x, .@y; // same map, slide instead of full warp
            close;
        }
    }

    warp .@map$, .@x, .@y; // different map, warp to given location
    close;
}



// cshop
//     closes the dialog, then opens a shop
//     if no npc is given, calls "#<npc> $"

function	script	cshop	{
    closedialog; // XXX: maybe send closeclientdialog in the future
    shop getarg(0, "#" + strnpcinfo(0) + " $");
    //close; => the shop buildin already sends close, and is a terminator itself
}



// cstorage
//     closes the dialog, then opens storage

function	script	cstorage	{
    closedialog; // XXX: maybe send closeclientdialog in the future
    openstorage;
    close;
}



// bye
//     closes the dialog without waiting for the player to press close
//     can also display an emote

function	script	bye	{
    .@emote = getarg(0, -1);
    closedialog; // XXX: maybe send closeclientdialog in the future

    if (.@emote >= 0)
        emotion .@emote;

    close;
}



// goodbye
//     same as bye, but also displays a canned message
//     can also display an emote

function	script	goodbye	{
    npctalkonce(goodbye_msg());
    bye getarg(0, -1);
}



// goodbye2
//     Waits for the player to press close, displays a canned message,
//     ends execution.
//     Can also display an emote

function	script	goodbye2	{
    .@emote = getarg(0, -1);

    close2;
    npctalkonce(goodbye_msg());

    if (.@emote >= 0)
        emotion .@emote;

    end;
}



// fortwarp(floor ID, dest map, dest x, dest y)
//     warps player along the Fortress

function	script	fortwarp	{
    // Not unlocked
    if ($GAME_STORYLINE >= 3 && $MK_TEMPVAR < getarg(0) && !$HARDCORE) {
        mesc l("The gate is sealed shut."), 1;
        mesc l("The monster army is still strong on this floor!"), 1;
        mesc l("Minimum wins: %d/%d", $MK_TEMPVAR, getarg(0)), 1;
        close;
    }

    // If you pledged for Barbara's life, your invincibility timer is vastly better
    if (BARBARA_STATE == 3) {
        if (!@barb)
            percentheal 100, 100;
        else if ((@barb + 180) < gettimetick(2))
            percentheal 5, 5;
        if (@barb < gettimetick(2))
            sc_start SC_PRESTIGE, 4500, 3500, 5000, SCFLAG_NOAVOID|SCFLAG_FIXEDTICK|SCFLAG_FIXEDRATE, getcharid(3); // 4.5 seconds duration
        @barb = gettimetick(2) + 15;
    }

    // The actual warp within the Impregnable Fortress
    warp getarg(1), getarg(2), getarg(3);
    return;
}

