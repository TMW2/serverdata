// TMW-2 Script
// Author:
//    Jesusalva
// Description:
//    Leaderboards + GM Command Log

function	script	ScoreboardsReload	{
    debugmes "Reloading scoreboards...";
	.@nb = query_sql("select name, zeny from `char` WHERE `guild_id`!=1 ORDER BY zeny DESC LIMIT 15", $@hofortune_name$, $@hofortune_value);
	.@nb = query_sql("select name, base_level from `char` WHERE `guild_id`!=1 ORDER BY base_level DESC LIMIT 15", $@hoblvl_name$, $@hoblvl_value);
	.@nb = query_sql("select name, job_level from `char` WHERE `guild_id`!=1 ORDER BY job_level DESC LIMIT 15", $@hojlvl_name$, $@hojlvl_value);
	.@nb = query_sql("SELECT c.name, i.amount FROM `inventory` AS i, `char` AS c WHERE i.nameid="+StrangeCoin+" AND i.char_id=c.char_id ORDER BY i.amount DESC LIMIT 15", $@hoc_name$, $@hoc_value);
	.@nb = query_sql("select char_name, command from `atcommandlog` ORDER BY atcommand_id DESC LIMIT 15", $@hogm_name$, $@hogm_value$);
	.@nb = query_sql("select name, guild_lv from `guild` WHERE `guild_id`!=1 ORDER BY guild_lv DESC LIMIT 5", $@hoguild_name$, $@hoguild_value);
	.@nb = query_sql("SELECT c.name, i.value FROM `char_reg_num_db` AS i, `char` AS c WHERE i.key='CRAZYPOINTS' AND i.char_id=c.char_id ORDER BY i.value DESC LIMIT 10", $@cfefe_name$, $@cfefe_value);
	.@nb = query_sql("SELECT c.name, i.value FROM `char_reg_num_db` AS i, `char` AS c WHERE i.key='UDTRANK' AND i.char_id=c.char_id ORDER BY i.value DESC LIMIT 10", $@udt_name$, $@udt_value);
	.@nb = query_sql("SELECT c.name, i.value FROM `char_reg_num_db` AS i, `char` AS c WHERE i.key='AFKING' AND i.char_id=c.char_id ORDER BY i.value DESC LIMIT 10", $@afk_name$, $@afk_value);
	.@nb = query_sql("SELECT c.name, i.value FROM `char_reg_num_db` AS i, `char` AS c WHERE i.key='HONOR' AND i.char_id=c.char_id ORDER BY i.value DESC LIMIT 10", $@hof_name$, $@hof_value);
	.@nb = query_sql("SELECT c.name, i.value FROM `char_reg_num_db` AS i, `char` AS c WHERE i.key='ACADEMIC_RANK' AND i.char_id=c.char_id ORDER BY i.value DESC LIMIT 30", $@hoa_name$, $@hoa_value);
    .@nb = query_sql("SELECT c.name, i.value FROM `mapreg` AS i, `char` AS c WHERE i.varname='$REFERRAL_IDS' AND i.index=c.account_id ORDER BY i.value DESC LIMIT 20", $@hor_name$, $@hor_value);

    // Seasonal Scoreboards
    if (season() == SUMMER || season() == AUTUMN) {
	    .@nb = query_sql("SELECT c.name, i.count2 FROM `quest` AS i, `char` AS c WHERE i.quest_id=305 AND i.char_id=c.char_id AND i.count1="+(gettime(GETTIME_YEAR)-2000)+" ORDER BY i.count2 DESC LIMIT 10", $@ched_name$, $@ched_value);
    }
    if (season() == AUTUMN || season() == WINTER) {
	    .@nb = query_sql("SELECT c.name, i.count2 FROM `quest` AS i, `char` AS c WHERE i.quest_id="+SQuest_Autumn+" AND i.char_id=c.char_id ORDER BY i.count2 DESC LIMIT 5", $@hasn_name$, $@hasn_value);
    }
    if (season() == WINTER || season() == SPRING) {
	    .@nb = query_sql("SELECT c.name, i.count2 FROM `quest` AS i, `char` AS c WHERE i.quest_id="+SQuest_Christmas+" AND i.char_id=c.char_id ORDER BY i.count2 DESC LIMIT 5", $@xmas_box_name$, $@xmas_box_value);
	    .@nb = query_sql("SELECT c.name, i.count3 FROM `quest` AS i, `char` AS c WHERE i.quest_id="+SQuest_Christmas+" AND i.char_id=c.char_id ORDER BY i.count3 DESC LIMIT 5", $@xmas_gp_name$, $@xmas_gp_value);
    }
    if (FYEventUsesRanking()) {
	    .@nb = query_sql("SELECT c.name, i.count2 FROM `quest` AS i, `char` AS c WHERE i.quest_id="+Q_AuroraEvent+" AND i.char_id=c.char_id ORDER BY i.count2 DESC LIMIT 10", $@aurora_name$, $@aurora_value);
    }
    debugmes "Scoreboards reloaded";
    return;
}

-	script	scoreboardsGen	NPC_HIDDEN,{
    end;

// Monthly Scoreboards Rewards
OnDay0101:
OnDay0201:
OnDay0301:
OnDay0401:
OnDay0501:
OnDay0601:
OnDay0701:
OnDay0801:
OnDay0901:
OnDay1001:
OnDay1101:
OnDay1201:
    ScoreboardsReload(); // Ensure

    if ($SCOREBOARD == gettimeparam(GETTIME_MONTH)) end;

    for (.@i=0; .@i < getarraysize($@hof_name$); .@i++) {
        rodex_sendmail(gf_charnameid($@hof_name$[.@i]), "Scoreboards", "Hall Of Honor: TOP 10", "For being on the TOP 10 of this hall, you're getting some Strange Coins.", 0, StrangeCoin, 15);
    }

    for (.@i=0; .@i < getarraysize($@cfefe_name$); .@i++) {
        rodex_sendmail(gf_charnameid($@cfefe_name$[.@i]), "Scoreboards", "Hall Of Candor Battle: TOP 10", "For being on the TOP 10 of this hall, you're getting some Strange Coins.", 0, StrangeCoin, 15);
    }

    $SCOREBOARD = gettimeparam(GETTIME_MONTH);
    end;

// Hourly Reload (maybe "OnMinute00" would work better?)
OnHour00:
OnHour01:
OnHour02:
OnHour03:
OnHour04:
OnHour05:
OnHour06:
OnHour07:
OnHour08:
OnHour09:
OnHour10:
OnHour11:
OnHour12:
OnHour13:
OnHour14:
OnHour15:
OnHour16:
OnHour17:
OnHour18:
OnHour19:
OnHour20:
OnHour21:
OnHour22:
OnHour23:
OnInit:
    ScoreboardsReload();
    end;
}

function	script	HallOfGuild	{
    mes "";
	mes l("##BHall Of Guild Level: TOP5##b");
	mesf("1. %s (%d)", $@hoguild_name$[0], $@hoguild_value[0]);
	mesf("2. %s (%d)", $@hoguild_name$[1], $@hoguild_value[1]);
	mesf("3. %s (%d)", $@hoguild_name$[2], $@hoguild_value[2]);
	mesf("4. %s (%d)", $@hoguild_name$[3], $@hoguild_value[3]);
	mesf("5. %s (%d)", $@hoguild_name$[4], $@hoguild_value[4]);
    return;
}

function	script	HallOfFortune	{
    mes "";
	mes l("##BHall Of Fortune: TOP15##b");
	mesf("1. %s (%s GP)", $@hofortune_name$[0], fnum($@hofortune_value[0]));
	mesf("2. %s (%s GP)", $@hofortune_name$[1], fnum($@hofortune_value[1]));
	mesf("3. %s (%s GP)", $@hofortune_name$[2], fnum($@hofortune_value[2]));
	mesf("4. %s (%s GP)", $@hofortune_name$[3], fnum($@hofortune_value[3]));
	mesf("5. %s (%s GP)", $@hofortune_name$[4], fnum($@hofortune_value[4]));
	mesf("6. %s (%s GP)", $@hofortune_name$[5], fnum($@hofortune_value[5]));
	mesf("7. %s (%s GP)", $@hofortune_name$[6], fnum($@hofortune_value[6]));
	mesf("8. %s (%s GP)", $@hofortune_name$[7], fnum($@hofortune_value[7]));
	mesf("9. %s (%s GP)", $@hofortune_name$[8], fnum($@hofortune_value[8]));
	mesf("10. %s (%s GP)", $@hofortune_name$[9], fnum($@hofortune_value[9]));
	mesf("11. %s (%s GP)", $@hofortune_name$[10], fnum($@hofortune_value[10]));
	mesf("12. %s (%s GP)", $@hofortune_name$[11], fnum($@hofortune_value[11]));
	mesf("13. %s (%s GP)", $@hofortune_name$[12], fnum($@hofortune_value[12]));
	mesf("14. %s (%s GP)", $@hofortune_name$[13], fnum($@hofortune_value[13]));
	mesf("15. %s (%s GP)", $@hofortune_name$[14], fnum($@hofortune_value[14]));
    return;
}

function	script	HallOfLevel	{
    mes "";
	mes l("##BHall Of Level: TOP15##b");
	mesf("1. %s (%d)", $@hoblvl_name$[0], $@hoblvl_value[0]);
	mesf("2. %s (%d)", $@hoblvl_name$[1], $@hoblvl_value[1]);
	mesf("3. %s (%d)", $@hoblvl_name$[2], $@hoblvl_value[2]);
	mesf("4. %s (%d)", $@hoblvl_name$[3], $@hoblvl_value[3]);
	mesf("5. %s (%d)", $@hoblvl_name$[4], $@hoblvl_value[4]);
	mesf("6. %s (%d)", $@hoblvl_name$[5], $@hoblvl_value[5]);
	mesf("7. %s (%d)", $@hoblvl_name$[6], $@hoblvl_value[6]);
	mesf("8. %s (%d)", $@hoblvl_name$[7], $@hoblvl_value[7]);
	mesf("9. %s (%d)", $@hoblvl_name$[8], $@hoblvl_value[8]);
	mesf("10. %s (%d)", $@hoblvl_name$[9], $@hoblvl_value[9]);
	mesf("11. %s (%d)", $@hoblvl_name$[10], $@hoblvl_value[10]);
	mesf("12. %s (%d)", $@hoblvl_name$[11], $@hoblvl_value[11]);
	mesf("13. %s (%d)", $@hoblvl_name$[12], $@hoblvl_value[12]);
	mesf("14. %s (%d)", $@hoblvl_name$[13], $@hoblvl_value[13]);
	mesf("15. %s (%d)", $@hoblvl_name$[14], $@hoblvl_value[14]);
    return;
}

function	script	HallOfJob	{
    mes "";
	mes l("##BHall Of Job Level: TOP15##b");
	mesf("1. %s (%d)", $@hojlvl_name$[0], $@hojlvl_value[0]);
	mesf("2. %s (%d)", $@hojlvl_name$[1], $@hojlvl_value[1]);
	mesf("3. %s (%d)", $@hojlvl_name$[2], $@hojlvl_value[2]);
	mesf("4. %s (%d)", $@hojlvl_name$[3], $@hojlvl_value[3]);
	mesf("5. %s (%d)", $@hojlvl_name$[4], $@hojlvl_value[4]);
	mesf("6. %s (%d)", $@hojlvl_name$[5], $@hojlvl_value[5]);
	mesf("7. %s (%d)", $@hojlvl_name$[6], $@hojlvl_value[6]);
	mesf("8. %s (%d)", $@hojlvl_name$[7], $@hojlvl_value[7]);
	mesf("9. %s (%d)", $@hojlvl_name$[8], $@hojlvl_value[8]);
	mesf("10. %s (%d)", $@hojlvl_name$[9], $@hojlvl_value[9]);
	mesf("11. %s (%d)", $@hojlvl_name$[10], $@hojlvl_value[10]);
	mesf("12. %s (%d)", $@hojlvl_name$[11], $@hojlvl_value[11]);
	mesf("13. %s (%d)", $@hojlvl_name$[12], $@hojlvl_value[12]);
	mesf("14. %s (%d)", $@hojlvl_name$[13], $@hojlvl_value[13]);
	mesf("15. %s (%d)", $@hojlvl_name$[14], $@hojlvl_value[14]);
    return;
}

function	script	HallOfCoins	{
    mes "";
	mes l("##BHall Of @@: TOP15##b", getitemlink(StrangeCoin));
	mes("1."+$@hoc_name$[0]+" ("+$@hoc_value[0]+")");
	mes("2."+$@hoc_name$[1]+" ("+$@hoc_value[1]+")");
	mes("3."+$@hoc_name$[2]+" ("+$@hoc_value[2]+")");
	mes("4."+$@hoc_name$[3]+" ("+$@hoc_value[3]+")");
	mes("5."+$@hoc_name$[4]+" ("+$@hoc_value[4]+")");
	mes("6."+$@hoc_name$[5]+" ("+$@hoc_value[5]+")");
	mes("7."+$@hoc_name$[6]+" ("+$@hoc_value[6]+")");
	mes("8."+$@hoc_name$[7]+" ("+$@hoc_value[7]+")");
	mes("9."+$@hoc_name$[8]+" ("+$@hoc_value[8]+")");
	mes("10."+$@hoc_name$[9]+" ("+$@hoc_value[9]+")");
	mes("11."+$@hoc_name$[10]+" ("+$@hoc_value[10]+")");
	mes("12."+$@hoc_name$[11]+" ("+$@hoc_value[11]+")");
	mes("13."+$@hoc_name$[12]+" ("+$@hoc_value[12]+")");
	mes("14."+$@hoc_name$[13]+" ("+$@hoc_value[13]+")");
	mes("15."+$@hoc_name$[14]+" ("+$@hoc_value[14]+")");
    return;
}

function	script	HallOfGMLog	{
    mes "";
	mes l("##BLatest GM Commands##b");
	mes("1."+$@hogm_name$[0]+" ("+$@hogm_value$[0]+")");
	mes("2."+$@hogm_name$[1]+" ("+$@hogm_value$[1]+")");
	mes("3."+$@hogm_name$[2]+" ("+$@hogm_value$[2]+")");
	mes("4."+$@hogm_name$[3]+" ("+$@hogm_value$[3]+")");
	mes("5."+$@hogm_name$[4]+" ("+$@hogm_value$[4]+")");
	mes("6."+$@hogm_name$[5]+" ("+$@hogm_value$[5]+")");
	mes("7."+$@hogm_name$[6]+" ("+$@hogm_value$[6]+")");
	mes("8."+$@hogm_name$[7]+" ("+$@hogm_value$[7]+")");
	mes("9."+$@hogm_name$[8]+" ("+$@hogm_value$[8]+")");
	mes("10."+$@hogm_name$[9]+" ("+$@hogm_value$[9]+")");
	mes("11."+$@hogm_name$[10]+" ("+$@hogm_value$[10]+")");
	mes("12."+$@hogm_name$[11]+" ("+$@hogm_value$[11]+")");
	mes("13."+$@hogm_name$[12]+" ("+$@hogm_value$[12]+")");
	mes("14."+$@hogm_name$[13]+" ("+$@hogm_value$[13]+")");
	mes("15."+$@hogm_name$[14]+" ("+$@hogm_value$[14]+")");
    return;
}

function	script	HallOfCandor	{
    mes "";
	mes l("##BHall Of Crazyfefe Fight: TOP 10##b");
	mes("1."+$@cfefe_name$[0]+" ("+$@cfefe_value[0]+")");
	mes("2."+$@cfefe_name$[1]+" ("+$@cfefe_value[1]+")");
	mes("3."+$@cfefe_name$[2]+" ("+$@cfefe_value[2]+")");
	mes("4."+$@cfefe_name$[3]+" ("+$@cfefe_value[3]+")");
	mes("5."+$@cfefe_name$[4]+" ("+$@cfefe_value[4]+")");
	mes("6."+$@cfefe_name$[5]+" ("+$@cfefe_value[5]+")");
	mes("7."+$@cfefe_name$[6]+" ("+$@cfefe_value[6]+")");
	mes("8."+$@cfefe_name$[7]+" ("+$@cfefe_value[7]+")");
	mes("9."+$@cfefe_name$[8]+" ("+$@cfefe_value[8]+")");
	mes("10."+$@cfefe_name$[9]+" ("+$@cfefe_value[9]+")");
    return;
}

function	script	HallOfUDT	{
    mes "";
	mes l("##BHall Of UDT Challenge: TOP 10##b");
	mes("1."+$@udt_name$[0]+" ("+$@udt_value[0]+")");
	mes("2."+$@udt_name$[1]+" ("+$@udt_value[1]+")");
	mes("3."+$@udt_name$[2]+" ("+$@udt_value[2]+")");
	mes("4."+$@udt_name$[3]+" ("+$@udt_value[3]+")");
	mes("5."+$@udt_name$[4]+" ("+$@udt_value[4]+")");
	mes("6."+$@udt_name$[5]+" ("+$@udt_value[5]+")");
	mes("7."+$@udt_name$[6]+" ("+$@udt_value[6]+")");
	mes("8."+$@udt_name$[7]+" ("+$@udt_value[7]+")");
	mes("9."+$@udt_name$[8]+" ("+$@udt_value[8]+")");
	mes("10."+$@udt_name$[9]+" ("+$@udt_value[9]+")");
    return;
}

function	script	HallOfHonor	{
    mes "";
	mes l("##BHall Of Honor: TOP 10##b");
    mesc l("WARNING: This is experimental!!"), 1;
    mesc l("We may delete all entries and change formulas in the future."), 1;
	mes("1."+$@hof_name$[0]+" ("+$@hof_value[0]+")");
	mes("2."+$@hof_name$[1]+" ("+$@hof_value[1]+")");
	mes("3."+$@hof_name$[2]+" ("+$@hof_value[2]+")");
	mes("4."+$@hof_name$[3]+" ("+$@hof_value[3]+")");
	mes("5."+$@hof_name$[4]+" ("+$@hof_value[4]+")");
	mes("6."+$@hof_name$[5]+" ("+$@hof_value[5]+")");
	mes("7."+$@hof_name$[6]+" ("+$@hof_value[6]+")");
	mes("8."+$@hof_name$[7]+" ("+$@hof_value[7]+")");
	mes("9."+$@hof_name$[8]+" ("+$@hof_value[8]+")");
	mes("10."+$@hof_name$[9]+" ("+$@hof_value[9]+")");
    return;
}





function	script	HallOfReferral	{
    mes "";
	mes "Referral Program Report - pg. 1";
	mes("1."+$@hor_name$[0]+" ("+$@hor_value[0]+")");
	mes("2."+$@hor_name$[1]+" ("+$@hor_value[1]+")");
	mes("3."+$@hor_name$[2]+" ("+$@hor_value[2]+")");
	mes("4."+$@hor_name$[3]+" ("+$@hor_value[3]+")");
	mes("5."+$@hor_name$[4]+" ("+$@hor_value[4]+")");
	mes("6."+$@hor_name$[5]+" ("+$@hor_value[5]+")");
	mes("7."+$@hor_name$[6]+" ("+$@hor_value[6]+")");
	mes("8."+$@hor_name$[7]+" ("+$@hor_value[7]+")");
	mes("9."+$@hor_name$[8]+" ("+$@hor_value[8]+")");
	mes("10."+$@hor_name$[9]+" ("+$@hor_value[9]+")");
    next;
	mes "Referral Program Report - pg. 2";
	mes("11."+$@hor_name$[10]+" ("+$@hor_value[10]+")");
	mes("12."+$@hor_name$[11]+" ("+$@hor_value[11]+")");
	mes("13."+$@hor_name$[12]+" ("+$@hor_value[12]+")");
	mes("14."+$@hor_name$[13]+" ("+$@hor_value[13]+")");
	mes("15."+$@hor_name$[14]+" ("+$@hor_value[14]+")");
	mes("16."+$@hor_name$[15]+" ("+$@hor_value[15]+")");
	mes("17."+$@hor_name$[16]+" ("+$@hor_value[16]+")");
	mes("18."+$@hor_name$[17]+" ("+$@hor_value[17]+")");
	mes("19."+$@hor_name$[18]+" ("+$@hor_value[18]+")");
	mes("20."+$@hor_name$[19]+" ("+$@hor_value[19]+")");
    next;
    return;
}


function	script	HallOfAurora	{
    mes "";
	mes l("##BHall Of Aurora: TOP10##b");
	mesf("1. %s (%s)",  $@aurora_name$[0], fnum($@aurora_value[0]));
	mesf("2. %s (%s)",  $@aurora_name$[1], fnum($@aurora_value[1]));
	mesf("3. %s (%s)",  $@aurora_name$[2], fnum($@aurora_value[2]));
	mesf("4. %s (%s)",  $@aurora_name$[3], fnum($@aurora_value[3]));
	mesf("5. %s (%s)",  $@aurora_name$[4], fnum($@aurora_value[4]));
	mesf("6. %s (%s)",  $@aurora_name$[5], fnum($@aurora_value[5]));
	mesf("7. %s (%s)",  $@aurora_name$[6], fnum($@aurora_value[6]));
	mesf("8. %s (%s)",  $@aurora_name$[7], fnum($@aurora_value[7]));
	mesf("9. %s (%s)",  $@aurora_name$[8], fnum($@aurora_value[8]));
	mesf("10. %s (%s)", $@aurora_name$[9], fnum($@aurora_value[9]));
    return;
}


function	script	HallOfAcademic	{
    mes "";
	mes l("##BHall Of Academics: TOP30##b");
    if (playerattached()) {
        .@v=array_entries(ACADEMIC_VOUCH);
        .@mv=(ACADEMIC_RANK >= ACADEMIC_DOCTOR ? 3 : 2);
        .@mv=(ACADEMIC_RANK >= ACADEMIC_BACHELOR ? .@mv : 1);
        mesc l("Your current scholar rank: %s (%d/%d vouches)",
            academicrank(), .@v, .@mv);
    }
    mes "";
	mesf("1. %s (%s)",  $@hoa_name$[0], academicrank($@hoa_value[0]));
	mesf("2. %s (%s)",  $@hoa_name$[1], academicrank($@hoa_value[1]));
	mesf("3. %s (%s)",  $@hoa_name$[2], academicrank($@hoa_value[2]));
	mesf("4. %s (%s)",  $@hoa_name$[3], academicrank($@hoa_value[3]));
	mesf("5. %s (%s)",  $@hoa_name$[4], academicrank($@hoa_value[4]));
	mesf("6. %s (%s)",  $@hoa_name$[5], academicrank($@hoa_value[5]));
	mesf("7. %s (%s)",  $@hoa_name$[6], academicrank($@hoa_value[6]));
	mesf("8. %s (%s)",  $@hoa_name$[7], academicrank($@hoa_value[7]));
	mesf("9. %s (%s)",  $@hoa_name$[8], academicrank($@hoa_value[8]));
	mesf("10. %s (%s)", $@hoa_name$[9], academicrank($@hoa_value[9]));
    next;
	mesf("11. %s (%s)",  $@hoa_name$[10], academicrank($@hoa_value[10]));
	mesf("12. %s (%s)",  $@hoa_name$[11], academicrank($@hoa_value[11]));
	mesf("13. %s (%s)",  $@hoa_name$[12], academicrank($@hoa_value[12]));
	mesf("14. %s (%s)",  $@hoa_name$[13], academicrank($@hoa_value[13]));
	mesf("15. %s (%s)",  $@hoa_name$[14], academicrank($@hoa_value[14]));
	mesf("16. %s (%s)",  $@hoa_name$[15], academicrank($@hoa_value[15]));
	mesf("17. %s (%s)",  $@hoa_name$[16], academicrank($@hoa_value[16]));
	mesf("18. %s (%s)",  $@hoa_name$[17], academicrank($@hoa_value[17]));
	mesf("19. %s (%s)",  $@hoa_name$[18], academicrank($@hoa_value[18]));
	mesf("20. %s (%s)",  $@hoa_name$[19], academicrank($@hoa_value[19]));
    next;
	mesf("21. %s (%s)",  $@hoa_name$[20], academicrank($@hoa_value[20]));
	mesf("22. %s (%s)",  $@hoa_name$[21], academicrank($@hoa_value[21]));
	mesf("23. %s (%s)",  $@hoa_name$[22], academicrank($@hoa_value[22]));
	mesf("24. %s (%s)",  $@hoa_name$[23], academicrank($@hoa_value[23]));
	mesf("25. %s (%s)",  $@hoa_name$[24], academicrank($@hoa_value[24]));
	mesf("26. %s (%s)",  $@hoa_name$[25], academicrank($@hoa_value[25]));
	mesf("27. %s (%s)",  $@hoa_name$[26], academicrank($@hoa_value[26]));
	mesf("28. %s (%s)",  $@hoa_name$[27], academicrank($@hoa_value[27]));
	mesf("29. %s (%s)",  $@hoa_name$[28], academicrank($@hoa_value[28]));
	mesf("30. %s (%s)",  $@hoa_name$[29], academicrank($@hoa_value[29]));
    return;
}

// Hall of AFK
function	script	HallOfAFK	{
    mes "";
	mes l("##BHall Of AFK: TOP 10##b");
	mesf("1. %s (%dh%02dm)", $@afk_name$[0], $@afk_value[0]/1200, $@afk_value[0]%1200/60*3);
	mesf("2. %s (%dh%02dm)", $@afk_name$[1], $@afk_value[1]/1200, $@afk_value[1]%1200/60*3);
	mesf("3. %s (%dh%02dm)", $@afk_name$[2], $@afk_value[2]/1200, $@afk_value[2]%1200/60*3);
	mesf("4. %s (%dh%02dm)", $@afk_name$[3], $@afk_value[3]/1200, $@afk_value[3]%1200/60*3);
	mesf("5. %s (%dh%02dm)", $@afk_name$[4], $@afk_value[4]/1200, $@afk_value[4]%1200/60*3);
	mesf("6. %s (%dh%02dm)", $@afk_name$[5], $@afk_value[5]/1200, $@afk_value[5]%1200/60*3);
	mesf("7. %s (%dh%02dm)", $@afk_name$[6], $@afk_value[6]/1200, $@afk_value[6]%1200/60*3);
	mesf("8. %s (%dh%02dm)", $@afk_name$[7], $@afk_value[7]/1200, $@afk_value[7]%1200/60*3);
	mesf("9. %s (%dh%02dm)", $@afk_name$[8], $@afk_value[8]/1200, $@afk_value[8]%1200/60*3);
	mesf("10. %s (%dh%02dm)", $@afk_name$[9], $@afk_value[9]/1200, $@afk_value[9]%1200/60*3);
    return;
}


// HallOfSponsor(minimal=false)
function	script	HallOfSponsor	{
    mes l("This is in honor of all the [@@help://about-server|Contributors@@] who helped rebuilding this world, after the Monster War outbreak.");
    mes l("And also in notable mention of those who [@@https://www.patreon.com/TMW2|sponsor@@] the Alliance and its administrative structure.");
    mes "";
    mes "Saulc, Woody, Sharli, Povo";
    // List previous sponsors?
    if (!getarg(0, false)) {
        mesc "GonzoDark";
    }
    return;
}
// Returns the name of a random sponsor
function	script	rand_sponsor	{
    return any("Saulc", "Woody", "Sharli", "Povo");
}

// HallOfGame()
function	script	HallOfGame	{
    mes l("World hero: %s", $MOST_HEROIC$);
    mes l("Planted Trees: %s", fnum($TREE_PLANTED));
    mes l("Players Killed in PvP: %s", fnum($PLAYERS_KILLED));
    mes l("Monsters Killed in PvE: %s", fnum($MONSTERS_KILLED));
    mes "";
    .@s$=(season() == WINTER ? l("Winter") : .@s$);
    .@s$=(season() == AUTUMN ? l("Autumn") : .@s$);
    .@s$=(season() == SUMMER ? l("Summer") : .@s$);
    .@s$=(season() == SPRING ? l("Spring") : .@s$);
    mes l("Season: %s", .@s$);
    // weather ; game time ; world story ; etc.
    mes "";
    mes l("Notable mentions and thanks for our [@@https://www.patreon.com/TMW2|sponsors@@], we wouldn't be here without their continued support.");
    mes "";
    return;
}

function	script	HallOf2018	{
    mes "";
    if ($YETIKING_WINNER$ != "") {
        mes l(".:: FIRST PLAYER TO COMPLETE YETI KING QUEST ::.");
        mes $YETIKING_WINNER$;
        mes "";
    }
    mes l(".:: NOTABLE NAMES ON HURNSCALD LIBERATION DAY ::.");
    mes l("DragonStar, Aisen");
    mes "";
    mes l(".:: NOTABLE NAMES ON NIVALIS LIBERATION DAY ::.");
    mes l("Jesusalva");
    mes "";
    mes l(".:: FIND-THE-NPC 2018 MINI-EVENT WINNER ::.");
    mes "shab";
    next;
    mes l(".:: Easter 2018 ::.");
    mes l("In honor of DragonStar, winner of Easter 2018.");
    mesc l("Unfortunately, other victor's names weren't logged.");
    mes "";
    mes l(".:: Worker Day 2018 ::.");
    mes l("No victor appliable.");
    mes "";
    mes l(".:: Purple Day 2018 ::.");
    mes l("No victor appliable.");
    next;
    mes l(".:: Ched's Summer 2018 ::.");
    mes ("1. WarBlade - 5325");
    mes ("2. Aisen - 2000");
    mes ("3. msawis - 1000");
    mesc ("4. vilbou - 400");
    mesc ("5. Woody - 353");
    next;
    mes l(".:: Hasan Scorpion Killing Challenge 2018 ::.");
    mes ("1. Krists - 1070");
    mes ("2. Aisen - 598");
    mes ("3. AndulkaT - 212");
    mesc ("4. monking - 86");
    mesc ("5. Carbon14 - 78");
    next;
    mes l(".:: Christmas 2018 ::.");
    mes ("1. WarBlade - 324");
    mes ("2. Xtreem - 190");
    mes ("3. msawis - 110");
    mesc ("4. Krists - 75");
    mesc ("5. Mrhedx - 38");
    mes "";
    mesc ("1. WarBlade - 100,000 GP");
    mesc ("2. msawis - 7,500 GP");
    mesc ("3. LawnCable - 4,450 GP");
    return;
}

function	script	HallOf2019	{
    mes "";
    if ($HEROESHOLD_WINNER$ != "") {
        mes l(".:: FIRST PLAYER TO COMPLETE HEROES HOLD MASTER DUNGEON ::.");
        mes $HEROESHOLD_WINNER$;
        mes "";
    }
    mes l(".:: TMW-2 Anniversary ::.");
    //mesc l("Scheduled: January 13rd");
    mes l("No victor appliable.");
    mesc l("Anniversary marks the project birthdate. Do not mistake with TMW2 Day.");
    mes "";
    mes l(".:: Valentine Day ::.");
    //mesc l("Scheduled: February 12th - 15th");
    mes ("1. DragonStar - 300");
    mes ("2. Jesusalva - 121");
    mes ("3. Xanthem - 35");
    mesc ("4. Xtreem - 17");
    mesc ("5. Yuppi - 10");
    next;
    mes l(".:: TMW2 Day ::.");
    //mesc l("Scheduled: March 2nd");
    mes l("No victor appliable.");
    mesc l("TMW2 Day marks the server birthdate. Do not mistake with TMW2 Anniversary.");
    mes "";
    mes l(".:: Easter 2019 ::.");
    //mesc l("Scheduled: April 17th - 24th");
    mes l("In honor of Woody, winner of Easter 2019.");
    mesc l("Unfortunately, other victor's names weren't logged.");
    mes "";
    mes l(".:: Worker Day ::.");
    //mesc l("Scheduled: April 27th - May 3rd");
    mes l("No victor appliable.");
    next;
    mes l(".:: Ched's Summer 2019 ::.");
    //mesc l("June 21st - September 21st");
    mes ("1. Woody - 3954");
    mes ("2. Crazyfefe - 2092");
    mes ("3. Lilanna - 462");
    mesc ("4. Orby - 281");
    mesc ("5. Saulc - 246");
    mes "";
    /*
    mes l(".:: Chocolate Day ::.");
    mesc l("Event Canceled"), 1;
    //mesc l("Scheduled: July 7th");
    mes "";
    */
    mes l(".:: Open Beta 2019 ::.");
    //mesc l("Scheduled: July 20th ~ 31st");
    mes l("1. Jesusalva - 65");
    mes l("2. Kolchak - 63");
    mes l("3. Xanthem - 56");
    mesc l("4. seeds - 43");
    mesc l("5. dangerDuck - 23");
    mes "";
    mes l(".:: Free Software Day ::.");
    mesc l("Event Canceled"), 1;
    //mesc l("Scheduled: September 9th");
    mes "";
    mes l(".:: International Coffee Day ::.");
    //mesc l("Scheduled: October 1st");
    mes l("No victor appliable.");
    mes "";
    mes l(".:: Hasan Scorpion Killing Challenge 2019 ::.");
    mesc l("September 22nd - December 20th");
    mes "";
    mes l(".:: Thanksgiving 2019 ::.");
    //mesc l("November 1st - November 30th");
    mes l("No victor appliable.");
    mes "";
    mes l(".:: Christmas 2019 ::.");
    //mesc l("Scheduled: December 19th - January 2nd");
    mes l("1. Lilanna - 2498");
    mes l("2. seeds - 1167");
    mes l("3. Woody - 1066");
    mesc l("4. dangerDuck - 540");
    mesc l("5. Heaven - 501");
    return;
}

function	script	HallOf2020	{
    mesc l("This schedule is subject to change without prior notice."), 1;
    mesc l("Changes include but are not limited to festive events and dates."), 1;
    next;
    if ($REBIRTH_WINNER$ != "") {
        mes l(".:: FIRST PLAYER TO REBIRTH ::.");
        mes $REBIRTH_WINNER$;
        mes "";
    }
    mes l(".:: TMW-2 Anniversary ::.");
    //mesc l("Scheduled: January 13rd");
    mes l("No victor appliable.");
    mesc l("Anniversary marks the project birthdate. Do not mistake with TMW2 Day.");
    mes "";
    mes l(".:: Valentine Day ::.");
    //mesc l("Scheduled: February 13th - 16th");
    mes l("1. Cassio - 43098");
    mes l("2. Rill - 25955");
    mes l("3. luanaf - 20837");
    mesc l("4. Hocus - 17260");
    mesc l("5. seeds - 13395");
    next;
    mes l(".:: TMW2 Day ::.");
    //mesc l("Scheduled: March 2nd");
    mes l("No victor appliable.");
    mesc l("TMW2 Day marks the server birthdate. Do not mistake with TMW2 Anniversary.");
    mes "";
    mes l(".:: Easter 2020 ::.");
    //mesc l("Scheduled: April 17th - 24th");
    mes l("In honor of Groata, winner of Easter 2020.");
    mesc l("Unfortunately, other victor's names weren't logged.");
    mes "";
    mes l(".:: Worker Day ::.");
    //mesc l("Scheduled: April 27th - May 3rd");
    mes l("No victor appliable.");
    next;
    mes l(".:: Ched's Summer 2020 ::.");
    //mesc l("June 21st - September 21st");
    mes l("1. Mathias Cronqvist - 15000");
    mes l("2. Woody - 3637");
    mes l("3. dangerDuck - 1225");
    mesc l("4. Amdros - 1113");
    mesc l("5. Xanthem - 615");
    mes "";
    mes l(".:: Chocolate Day ::.");
    mesc l("Event Canceled"), 1;
    //mesc l("Scheduled: July 7th");
    mes "";
    mes l(".:: Free Software Day ::.");
    mesc l("Event Canceled"), 1;
    //mesc l("Scheduled: September 9th");
    mes "";
    mes l(".:: International Coffee Day ::.");
    //mesc l("Scheduled: October 1st");
    mes l("No victor appliable.");
    mes "";
    mes l(".:: Hasan Scorpion Killing Challenge 2020 ::.");
    //mesc l("September 22nd - December 20th");
    mes l("1. Leather - 4523");
    mes l("2. Manatauro - 4386");
    mes l("3. G II - 4151");
    mesc l("4. Povo - 3999");
    mesc l("5. Woolie - 3345");
    mes "";
    mes l(".:: Thanksgiving 2020 ::.");
    //mesc l("November 1st - November 30th");
    mes l("No victor appliable.");
    mes "";
    mes l(".:: Christmas 2020 ::.");
    //mesc l("Scheduled: December 19th - January 2nd");
    mes l("1. Manatauro - 29504");
    mes l("2. Povo - 10203");
    mes l("3. YuckFou - 9537");
    mesc l("4. Mathias Cronqvist - 2000");
    mesc l("5. Heaven - 806");
    mes "";
    mesc l("1. YuckFou - 10,000,010 GP");
    mesc l("2. Mathias Cronqvist - 7,423,346 GP");
    mesc l("3. Povo - 1,275,000 GP");
    return;
}

function	script	HallOf2021	{
    mes "";
    mes l(".:: TMW-2 Anniversary ::.");
    mesc l("Scheduled: January 13rd");
    //mes l("No victor appliable.");
    mesc l("Anniversary marks the project birthdate. Do not mistake with TMW2 Day.");
    mes "";
    mes l(".:: Valentine Day ::.");
    mesc l("Scheduled: February 13th - 16th");
    next;
    mes l(".:: TMW2 Day ::.");
    mesc l("Scheduled: March 2nd");
    //mes l("No victor appliable.");
    mesc l("TMW2 Day marks the server birthdate. Do not mistake with TMW2 Anniversary.");
    mes "";
    mes l(".:: Easter 2021 ::.");
    mesc l("Scheduled: April 17th - 24th");
    mes "";
    mes l(".:: Worker Day ::.");
    mesc l("Scheduled: April 27th - May 3rd");
    //mes l("No victor appliable.");
    next;
    mes l(".:: Ched's Summer 2021 ::.");
    mesc l("June 21st - September 21st");
    mes "";
    mes l(".:: Chocolate Day ::.");
    mesc l("Scheduled: July 7th");
    //mes l("No victor appliable.");
    mes "";
    mes l(".:: Free Software Day ::.");
    mesc l("Scheduled: September 9th");
    //mes l("No victor appliable.");
    mes "";
    mes l(".:: International Coffee Day ::.");
    mesc l("Scheduled: October 1st");
    //mes l("No victor appliable.");
    mes "";
    mes l(".:: Hasan Scorpion Killing Challenge 2021 ::.");
    mesc l("September 22nd - December 20th");
    mes "";
    mes l(".:: Thanksgiving 2021 ::.");
    mesc l("November 1st - November 30th");
    //mes l("No victor appliable.");
    mes "";
    mes l(".:: Christmas 2021 ::.");
    mesc l("Scheduled: December 19th - January 2nd");
    return;
}

-	script	@scoreboard	NPC_HIDDEN,{
    end;

OnCall:
    do {
        clear;
        HallOfSponsor(true);
        mes "";
        select
            l("Hall of Sponsors"),
            l("Hall Of Fortune"),
            l("Hall Of Base Level"),
            l("Hall Of Job Level"),
            l("Hall Of Strange Coins"),
            l("Hall Of GM Commands"),
            l("Hall Of Guilds"),
            l("Hall Of Crazyfefe Battle"),
            l("Hall Of Doppelganger Arena"),
            l("Hall Of AF King"),
            rif(season() == SUMMER || season() == AUTUMN, l("Hall Of Ched")),
            l("Hall Of Fame"),
            l("Hall Of Academics"),
            l("2018 Event Winners"),
            l("2019 Event Winners"),
            l("2020 Event Winners"),
            l("2021 Event Winners"),
            l("Game Statistics"),
            l("Quit");
        mes "";
        switch (@menu) {
        case 1:
            HallOfSponsor();
            next;
            break;
        case 2:
            HallOfFortune();
            next;
            break;
        case 3:
            HallOfLevel();
            next;
            break;
        case 4:
            HallOfJob();
            next;
            break;
        case 5:
            HallOfCoins();
            next;
            break;
        case 6:
            HallOfGMLog();
            next;
            break;
        case 7:
            HallOfGuild();
            next;
            break;
        case 8:
            HallOfCandor();
            next;
            break;
        case 9:
            HallOfUDT();
            next;
            break;
        case 10:
            HallOfAFK();
            next;
            break;
        case 11:
            mes b(l("Top 10 - Summer Ched's Event"));
	        mes("1."+$@ched_name$[0]+" ("+$@ched_value[0]+")");
	        mes("2."+$@ched_name$[1]+" ("+$@ched_value[1]+")");
	        mes("3."+$@ched_name$[2]+" ("+$@ched_value[2]+")");
	        mes("4."+$@ched_name$[3]+" ("+$@ched_value[3]+")");
	        mes("5."+$@ched_name$[4]+" ("+$@ched_value[4]+")");
	        mes("6."+$@ched_name$[5]+" ("+$@ched_value[5]+")");
	        mes("7."+$@ched_name$[6]+" ("+$@ched_value[6]+")");
	        mes("8."+$@ched_name$[7]+" ("+$@ched_value[7]+")");
	        mes("9."+$@ched_name$[8]+" ("+$@ched_value[8]+")");
	        mes("10."+$@ched_name$[9]+" ("+$@ched_value[9]+")");
            next;
            break;
        case 12:
            mes l("Hero: ")+$MOST_HEROIC$;
            mes "";
            HallOfHonor();
            next;
            break;
        case 13:
            HallOfAcademic();
            next;
            break;
        case 14:
            HallOf2018();
            next;
            break;
        case 15:
            HallOf2019();
            next;
            break;
        case 16:
            HallOf2020();
            next;
            break;
        case 17:
            HallOf2021();
            next;
            break;
        case 18:
            HallOfGame();
            next;
            break;
        default:
            close;
        }
    } while (true);
    end;

OnInit:
    bindatcmd "scoreboard", "@scoreboard::OnCall", 0, 100, 0;
    bindatcmd "scoreboards", "@scoreboard::OnCall", 0, 100, 0;
    end;
}

