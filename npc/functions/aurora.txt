// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    AURORA EVENT FRAMEWORK
//    Previously known as FY:Event System
//
//    Controls weekly events so Saulc, seeds and I can take vacations.
//    See also: seasons.txt, command/event.txt, event.txt, 003-1/events.txt
//    soulmenhir.txt, scoreboards.txt and, of course, the event maps (if any).
//    Aurora Event Framework functions are called by event.txt
//    specs override and is overriden by those defined in commands/event.txt

// Variables:
//      $MOST_HEROIC$
//          Updated every 2 weeks, the top 1 from previous event
//          Only the hero may begin sieges against Fortress Town.
//          Every NPC will recognize them, and Lightbringer will also
//          pay special attention to them.
//      $FYEVENT_CYCLE
//          Current cycle. I thought in using gettimeparam(WEEK) but gave up.
//      Q_AuroraEvent
//          Quest Variable: DayCtrl, Score, ClaimedControl
//
// Event Specific Variables:
//      $REGNUM_BLESSMAP$
//          Map under Regnum's Blessing
//      $REGNUM_BLESSMAP_H$
//          Human-readable form of the map under Regnum's Blessing
//
//      $FYREWARD_PT
//          Array with minimum points for rewards (primary key)
//      $FYREWARD_ID
//          Array with claimable reward IDs
//      $FYREWARD_AM
//          Array with the amount of the reward ID you'll receive
//      $FYLOGIN_PTS
//          How many event score boost you'll receive for daily login
//
//      $WORLDEXPO_ENEMY$
//          Name of the enemy responsible for ruining the World's Expo
//      $RAIDING_BOSS$
//          Name of the enemy who is raiding down the world
//      $DREAMTOWER_SAGE$
//          Name of the sage who owns the Dream Towers
//      DTOWER_DAY (CharReg)
//          Player variable which controls Dream Tower reset
//      DTOWER_ROLL (CharReg)
//          Player variable which controls Dream Ticket drop rate
//      DTOWER_FLOOR (CharReg)
//          Player variable which controls current Dream Tower floor


// AEF: BEGIN
function	script	FYNewEvent	{
    debugmes "\033[1mFY event is set to happen...\033[0m";
    // Aurora Events only begin after Liberation Day
    if ($GAME_STORYLINE < 1)
        return;
    // Update the loop
    $FYEVENT_CYCLE+=1;
    // Overrides standard event system
    $EVENT$="";
    // Delayed cleanup (Just in case)
    DelQuestFromEveryPlayer(Q_AuroraEvent);
    DelItemFromEveryPlayer(EventTreasure1);
    DelItemFromEveryPlayer(EventTreasure2);
    DelItemFromEveryPlayer(EventTreasure3);
    DelItemFromEveryPlayer(EventFish);
    DelItemFromEveryPlayer(EventOre);
    DelItemFromEveryPlayer(BrokenMedal);
    DelItemFromEveryPlayer(EventDreamTicket);
    DelItemFromEveryPlayer(EventNaftalin);
    // Olympics are costlier: Clean less often
    if ($FYEVENT_CYCLE % 12 == 1) {
        DelChrRegFromEveryPlayer("FYMOLY_ENBALL");
        DelChrRegFromEveryPlayer("FYMOLY_ICBOSS");
        DelChrRegFromEveryPlayer("FYMOLY_FLUFFY");
        DelChrRegFromEveryPlayer("FYMOLY_CHANTI");
        DelChrRegFromEveryPlayer("FYMOLY_ALCHMY");
        DelChrRegFromEveryPlayer("FYMOLY_MPWLVL");
        DelChrRegFromEveryPlayer("FYMOLY_MANAXP");
        DelChrRegFromEveryPlayer("FYMOLY_RACERS");
        DelChrRegFromEveryPlayer("FYMOLY_HOCUSM");
        DelChrRegFromEveryPlayer("FYMOLY_SURVIV");
        DelChrRegFromEveryPlayer("FYMOLY_FRIEND");
        DelChrRegFromEveryPlayer("FYMOLY_SPAMMY");
        // Same for Raids, Dream Towers and Gemini (they don't repeat)
        DelChrRegFromEveryPlayer("DTOWER_DAY");
        DelChrRegFromEveryPlayer("DTOWER_ROLL");
        DelChrRegFromEveryPlayer("DTOWER_FLOOR");
        DelChrRegFromEveryPlayer("FYRAID_LV");
        DelChrRegFromEveryPlayer("GEMINI_DAY");
    }
    deletearray $FYRAID_OWNER;
    deletearray $FYRAID_TIME;
    deletearray $FYRAID_HP;
    deletearray $FYRAID_LV;
    // Select the event
    switch ($FYEVENT_CYCLE % 12) {
    case 1:
        $EVENT$="Kamelot";
        kamibroadcast("Kamelot Season is now open!", "Aurora Events");
        break;
    case 2:
        $EVENT$="Expo";
        callfunc("FYEConf_Expo");
        kamibroadcast("World Expo is now open!", "Aurora Events");
        break;
    case 3:
        $EVENT$="Regnum";
        callfunc("FYEConf_Regnum");
        kamibroadcast("Regnum's Blessing: "+$REGNUM_BLESSMAP_H$+" is now blessed!", "Aurora Events");
        break;
    case 4:
        $EVENT$="Raid";
        callfunc("FYEConf_Raid");
        kamibroadcast("Oh noes! "+$RAIDING_BOSS$+" is attacking our world!", "Aurora Events");
        break;
    case 5:
        $EVENT$="Candor";
        kamibroadcast("Candor Battle Season is now open!", "Aurora Events");
        break;
    case 6:
        $EVENT$="Mining";
        callfunc("FYEConf_Mining");
        kamibroadcast("Miners Union Research Request is now open!", "Aurora Events");
        break;
    case 7:
        $EVENT$="Celestia";
        kamibroadcast("Celestia Season is now open!", "Aurora Events");
        break;
    case 8:
        $EVENT$="Olympics";
        callfunc("FYEConf_Olympics");
        kamibroadcast("Magic Olympics are now open!", "Aurora Events");
        break;
    case 9:
        if ($GAME_STORYLINE < 5) {
            $EVENT$="Rebirth";
            kamibroadcast("Rebirth Season has begun!", "Aurora Events");
        } else {
            $EVENT$="Siege";
            kamibroadcast("Siege Season has begun!", "Aurora Events");
        }
        break;
    case 10:
        $EVENT$="Fishing";
        callfunc("FYEConf_Fishing");
        kamibroadcast("Catch the Golden Fish is now open!", "Aurora Events");
        break;
    case 11:
        $EVENT$="Gemini";
        kamibroadcast("Gemini Season is now open!", "Aurora Events");
        break;
    default:
        $EVENT$="Tower";
        callfunc("FYEConf_Tower");
        kamibroadcast($DREAMTOWER_SAGE$+"'s Dream Towers have appeared!", "Aurora Events");
        break;
    }
    donpcevent "Aurora::OnRestore";
    return;
}






// Returns an item: (type, lv). Used by the function below (FYE_Autoset)
// types: misc, bp, warp, pot, heal, ore, magic, gift
// maxlv:  3    5     2    3    -     6     -     5
function	script	FYT	{
    .@t=getarg(0);
    .@l=getarg(1, 0);
    switch (.@t) {
    case FY_MISC:
        switch (.@l) {
        case 1:
        return any(SmokeGrenade, ScentGrenade, Grenade, DeathPotion, Lockpicks);
        case 2:
        return any(MysteriousBottle, TreasureMap, DungeonMap);
        case 3:
        return any(ArcmageBoxset, MercBoxA, ScrollSMaggot);
        }
    break;
    case FY_BP:
        switch (.@l) {
        case 1:
        return any(EquipmentBlueprintA, AlchemyBlueprintA);
        case 2:
        return any(EquipmentBlueprintB, AlchemyBlueprintB);
        case 3:
        return any(EquipmentBlueprintC, AlchemyBlueprintC);
        case 4:
        return any(EquipmentBlueprintD, AlchemyBlueprintD);
        case 5:
        return any(EquipmentBlueprintE, AlchemyBlueprintE, AncientBlueprint);
        }
    break;
    case FY_WARP:
        switch (.@l) {
        case 1:
        return any(TulimWarpCrystal, CandorWarpCrystal, HurnsWarpCrystal, LoFWarpCrystal);
        case 2:
        return any(HalinWarpCrystal, NivalWarpCrystal, FrostiaWarpCrystal);
        }
    break;
    case FY_POT:
        switch (.@l) {
        case 1:
        return any(LukPotionA, DexPotionA, IntPotionA, VitPotionA, AgiPotionA, HastePotion, StrengthPotion);
        case 2:
        return any(LukPotionB, DexPotionB, IntPotionB, VitPotionB, AgiPotionB, MoveSpeedPotion, PrecisionPotion, DodgePotion);
        case 3:
        return any(SacredImmortalityPotion, SacredLifePotion, SacredManaPotion);
        }
    break;
    case FY_ORE:
        switch (.@l) {
        case 1:
        return any(CopperIngot, IronIngot);
        case 2:
        return any(SilverIngot, GoldIngot);
        case 3:
        return any(TinIngot, LeadIngot);
        case 4:
        return any(TitaniumIngot, TerraniteIngot);
        case 5:
        return any(IridiumIngot, IridiumIngot, PlatinumIngot);
        case 6:
        return any(Diamond, Ruby, Emerald, Sapphire, Topaz, Amethyst);
        }
    break;
    case FY_GIFT:
        switch (.@l) {
        case 1:
        return BronzeGift;
        case 2:
        return SilverGift;
        case 3:
        return GoldenGift;
        case 4:
        return PrismGift;
        case 5:
        return any(SupremeGift, MysteriousFruit, PrismGift, HousingLetterIII, PrismGift);
        }
    break;
    // Single cases doesn't needs break
    case FY_HEAL:
        return any(SpearmintTea, Coffee, ClothoLiquor, BottleOfDivineWater);
    case FY_MAGIC:
        return any(FluoPowder, Quill, ScholarshipBadge, ScrollSWolvern);
    }

        return Exception("Invalid cast to FYEIT: "+.@t+" (Lv "+.@l+")", RB_IRCBROADCAST|RB_DEBUGMES|RB_GLOBALANNOUNCE, Bread);
}






function	script	FYE_Autoset	{
    // Sets 30 ranked rewards
    .@b=FY_BP; .@g=FY_GIFT; .@h=FY_HEAL; .@m=FY_MISC; .@mg=FY_MAGIC;
    .@o=FY_ORE; .@p=FY_POT; .@w=FY_WARP;
    // TODO: This function takes ~2 seconds to finish when setting the array
    // It might be improved by using a for() loop maybe
    //consolebug "FYAutoset Set %d", gettimetick(0);

    setarray $FYREWARD_ID, FYT(.@g, 1), StrangeCoin, FYT(.@m, 1), FYT(.@b, 1), FYT(.@p, 1),
                           FYT(.@h, 1), FYT(.@o, 1), FYT(.@o, 6), FYT(.@g, 2), FYT(.@w, 1),
                           FYT(.@b, 2), FYT(.@m, 2), FYT(.@o, 2), FYT(.@p, 2), FYT(.@o, 3),
                           FYT(.@b, 3), FYT(.@m, 3), StrangeCoin, FYT(.@mg, 1), FYT(.@g, 3),
                           FYT(.@w, 2), StrangeCoin, FYT(.@b, 4), FYT(.@p, 3), AncientBlueprint,
                           FYT(.@g, 4), FYT(.@o, 4), FYT(.@b, 5), FYT(.@o, 5), FYT(.@g, 5);

    //consolebug "FYAutoset IAT %d", gettimetick(0);
    // Item Amount Table
    .@hv=rand2(1,3);
    .@p1=any(1,2); .@p2=(.@p1 == 1 ? 2 : 1);
    setarray  $FYREWARD_AM, 1, 20, 1, 1, .@p1,
                            .@hv, 1, 1, 1, 1,
                            1, 1, 1, .@p2, 1,
                            1, 1, 60, 1, 1,
                            1, 100, 1, 1, 1,
                            1, 1, 1, 1, 1;
    return;
}






// Modify Kamelot
function	script	FYE_Kamelot	{
    .@g=getcharid(2);
    if ($@FYE_KAMELOT[.@g] != gettimeparam(GETTIME_DAYOFMONTH)) {
        mesc l("Kamelot Season is open!");

        // Instance still exists
        if (instanceowner($@KAMELOT_ID[.@g]) == .@g) {
            mesc l("However, your guild just challenged Kamelot Dungeons.");
            mesc l("Please wait a while.");
            return false;
        }
        mesc l("Should we?");
        next;
        askyesno();
        closeclientdialog();

        // Not going? Spoilsport
        if (@menu == ASK_NO)
            return false;

        // Someone began while you waited
        if (instanceowner($@KAMELOT_ID[.@g]) == .@g)
            return true;

        // Begin
        //callfunc("KamelotCleanup", .@g); // Not needed
        $KAMELOT_COOLDOWN[.@g] = ($KAMELOT_COOLDOWN[.@g] ? 1 : 0);
        $@FYE_KAMELOT[.@g] = gettimeparam(GETTIME_DAYOFMONTH);
        mesc l("Have fun!");
        next;
        closeclientdialog();
        return true;
    }
    return false;
}






// Configure Regnum Blessing
function	script	FYEConf_Regnum	{
    setarray .@ma$, "004-1", "007-1", "004-2", "010-2", "014-3", "014-5", "015-5",
                    "018-3", "019-1", "025-2", "025-2-1", "018-7", "019-5";
    setarray .@mb$, "Tulimshar Outskirts", "Tulimshar Mines",
                    "Tulimshar (West) Canyon", "Halinarzo (East) Canyon",
                    "Central Woodlands", "North Woodlands",
                    "Abandoned Mines (Woodlands)", "Somber Caves (LoF)",
                    "Snow Field", "Fortress Island - South",
                    "Fortress Island South Cave", "Lilit's Sanctuary",
                    "Rock Plateau (Nivalis)";
    .@m=rand2(getarraysize(.@ma$));
    $REGNUM_BLESSMAP$=.@ma$[.@m];
    $REGNUM_BLESSMAP_H$=.@mb$[.@m];
    // Apply the blessing
    setmapflag(.@ma$[.@m], mf_bexp, 300);
    setmapflag(.@ma$[.@m], mf_jexp, 300);
    return;
}






// Configure World Expo
function	script	FYEConf_Expo	{
    $WORLDEXPO_ENEMY$=any("Xakabael the Dark", "Isbamuth", "Saulc", "SUSAN", "Terogan", ($GAME_STORYLINE >= 5 ? "Moubootaur" : "Monster King"));
    setarray $FYREWARD_PT, 100, 220, 440, 880, 1320,
                           1760, 2640, 3520, 5280, 7040,
                           10560, 14080, 21120, 28160, 32000,
                           36000, 40000, 45000, 50000, 55000,
                           60000, 70000, 80000, 90000, 100000,
                           112640, 140800, 168960, 200000, 225280;
    // PS. Max Est. 225280 pts (#24 - Before Ancient Blueprint)

    FYE_Autoset();
    $FYLOGIN_PTS=rand2(18,22);
    return;
}


// Modify Treasure Chests
function	script	FYE_Expo	{
    // TODO: Merit-based random formula
    getitem EventTreasure1, 1+rand2(15); // 15
    getitem EventTreasure2, rand2(6); // 15
    getitem EventTreasure3, rand2(4); // 15
    // Total: (15+15+15)=1~45 points [AVG: 22 pts]
    return;
}






// Configure Dream Tower
function	script	FYEConf_Tower	{
    $DREAMTOWER_SAGE$=any("Jaxad", "Mr. Saves", "Tux", "Freeyorp", "Cassy", "Crush", "Rotonen", "Kage", "Bjorn", "The Elven Sage", "Hocus Pocus the Fidibus", "Elli");
    // Bertram? (https://forums.themanaworld.org/viewtopic.php?p=105296#p105296)
    setarray $FYREWARD_PT, 1, 4, 7, 10, 15,
                           22, 30, 45, 60, 75,
                           100, 120, 150, 175, 200,
                           250, 300, 350, 400, 500,
                           600, 700, 800, 900, 1000,
                           1200, 1400, 1600, 1800, 2000;
    // PS. Max Est. ?

    FYE_Autoset();
    $FYLOGIN_PTS=0;
    return;
}

// Dream Tower Ticket
function	script	FYE_Tower1	{
    .@mob=getarg(0, killedrid);
    // Obtain the monster level and roll a hidden counter
    .@lv=getmonsterinfo(.@mob, MOB_LV);
    DTOWER_ROLL+=.@lv;

    // There is no luck involved - Just get 1000 dream tower rolls
    if (DTOWER_ROLL >= 1000) {
        getitem EventDreamTicket, 1;
        DTOWER_ROLL-=1000;
    }
    return;
}






// Configure Boss Raid
function	script	FYEConf_Raid	{
    $RAIDING_BOSS$=any("Xakabael the Dark", "Janeb the Evil", "Platyna the Red", "Benjamin the Frost", "Reid the Terrific", "Nu'Rem the Destroyer", "Golbenez the Cruel", "King of Typos");
    deletearray $FYRAID_OWNER;
    deletearray $FYRAID_TIME;
    deletearray $FYRAID_HP;
    deletearray $FYRAID_LV;
    setarray $FYREWARD_PT, 10, 25, 50, 100, 150,
                           200, 300, 500, 750, 1000,
                           1250, 1500, 1750, 2000, 2500,
                           3000, 3500, 4000, 5000, 6000,
                           7500, 9000, 11000, 15000, 20000,
                           25000, 30000, 35000, 40000, 50000;
    // PS. Max Est. ?

    FYE_Autoset();
    $FYLOGIN_PTS=rand2(1, 2);
    $FYRAID_OWNER[0]=1; // System Reserve
    return;
}

// Boss Raid Appears! (What you killed is meaningless)
function	script	FYE_Raid	{
    .@mob=getarg(0, killedrid);
    // Actually - ignore system or auto-generated entities
    if (.@mob > 1490)
        return;
    .@lv=getmonsterinfo(.@mob, MOB_LV);
    // Chance goes from 10% to 1%
    // Defeating the boss lowers the chance in 0.1%
    if (rand2(1000) > max(100-FYRAID_LV, 10)) return;
    // Low level mobs may, at their level rate, abort
    // So a lv 100 mob has 1%, a lv 1 mob has 50%, etc.
    if (!rand2(.@lv)) return;

    /* A boss was found, update data */
    .@id = array_find($FYRAID_OWNER, getcharid(3));

    // Wait - Maybe you already found a boss?
    if (.@id >= 0) {
        if ($FYRAID_LV[.@id] == FYRAID_LV)
            return;
    }

    // Never found a boss before, so assign some place for you
    if (.@id < 0) {
        FYRAID_LV = 1;
        array_push($FYRAID_OWNER, getcharid(3));
        .@id = array_find($FYRAID_OWNER, getcharid(3));
    }

    // Assign the boss stats
    $FYRAID_HP[.@id]=2000+FYRAID_LV*1000+(FYRAID_LV/5*500);
    // Also declared in 001-13/main.txt
    $FYRAID_LV[.@id]=FYRAID_LV;
    $FYRAID_TIME[.@id]=gettimetick(2)+3600; // One hour

    // Announce that the boss was found!
    announce(l("%s: %s (Lv %d) has appeared!", b(l("BOSS WARNING")), $RAIDING_BOSS$, FYRAID_LV), bc_self);
    dispbottom l("Talk to Soul Menhir to engage the boss. Time limit = 1 hour.");
    // The boss is already available for anyone to kill, you get no priority
    // A new boss won't show up until you visit the Soul Menhir.
    // (This could be improved)
    return;
}







// Configure Fishing
function	script	FYEConf_Fishing	{
    setarray $FYREWARD_PT, 42, 63, 94, 141, 212,
                           318, 478, 717, 1076, 1345,
                           1614, 2020, 2421, 3026, 3632,
                           4540, 5000, 6000, 7000, 8500,
                           10000, 11000, 12000, 13500, 15000;
    // PS. Max Est. 14400 pts (#21 - Before 100 Strange Coins)

    FYE_Autoset();
    $FYLOGIN_PTS=rand2(6,8);
    return;
}


// Modify Fishing
function	script	FYE_Fishing	{
    // TODO: Merit-based random formula [AVG: 3.5] about 10pts/min
    getitem EventFish, rand2(1, 6);
    return;
}






// Configure Mining Union Research Event
function	script	FYEConf_Mining	{
    setarray $FYREWARD_PT, 100, 220, 440, 880, 1320,
                           1760, 2640, 3520, 5275, 7040,
                           10560, 14080, 21120, 28160, 35200,
                           42240, 57000, 70400, 85000, 112640,
                           140800, 169000, 197120, 225300, 281160,
                           337920, 394240, 450560, 550000, 675000;
    // PS. Max Est. 705740 pts (overflow)

    FYE_Autoset();
    $FYLOGIN_PTS=rand2(16,20);
    return;
}


// Modify Bifs
function	script	FYE_Mining	{
    .@mob=getarg(0, killedrid);
    // Only ores
    if (getmonsterinfo(.@mob, MOB_RACE) != RC_Mineral)
        return;

    // TODO: Merit-based random formula [AVG: 5] about 70pts/min
    // TODO: Big vs Small bifs (regex OK, size OK)
    .@ore=rand2(3, 6);
    if (.@mob == EleniumBif)
        .@ore-=1;
    getitem EventOre, .@ore;
    return;
}






// Configure Magic Olympics
function	script	FYEConf_Olympics	{
    setarray $FYREWARD_PT, 10, 22, 44, 88, 132,
                           176, 264, 352, 527, 704,
                           1056, 1408, 2112, 2816, 3520,
                           4224, 5700, 7040, 8500, 11264,
                           14080, 16900, 19712, 22530, 28116,
                           33792, 39424, 45056, 55000, 67500;
    // PS. Max Est. ? pts

    FYE_Autoset();
    $FYLOGIN_PTS=0;
    return;
}


// Modify Magic Skills
function	script	FYE_Olympics_SK	{
    if ($EVENT$ != "Olympics") return;

    .@sk=getarg(0, @skillId);
    .@sl=max(getarg(1, @skillLv), 1);
    .@st=getarg(2, @skillTarget);
    if (.@st < 1)
        consolebug("Skill target not set for FYE_OLYMPICS_SK Skill ID %d", .@sk);

    .@bl=getunittype(.@st);
    .@cl = $@MSK_CLASS[.@sk];

    // Supportive
    if (.@cl == CLASS_SCHOLARSHIP &&
        .@st != getcharid(3) &&
        .@st > 1 &&
        .@bl != BL_MOB &&
        .@bl != BL_PET &&
        .@bl != BL_NPC &&
        .@sk != SM_PROVOKE &&
        .@sk != EVOL_AREA_PROVOKE)
            FYMOLY_FRIEND += 1;

    // Generic
    if (.@sk != TMW2_FAKESKILL &&
        .@sk != TMW2_FAKESKILL2 &&
        .@sk != AM_CALLHOMUN &&
        .@sk != AM_REST &&
        .@sk != AM_RESURRECTHOMUN &&
        .@sk != TMW2_TRANSMIGRATION &&
        .@sk != TMW2_OVHFIRE)
            FYMOLY_SPAMMY += .@sl;

    setq2 Q_AuroraEvent, (getq2(Q_AuroraEvent) + rand2(.@sl + 1));
    return;
}

// Modify Mana EXP Gain
function	script	FYE_Olympics_MX	{
    if ($EVENT$ != "Olympics") return;
    .@var=getarg(0, 0);

    FYMOLY_MANAXP += .@var;
    setq2 Q_AuroraEvent, (getq2(Q_AuroraEvent) + rand2(.@var + 1));
    return;
}

// Count Chanting Usage
function	script	FYE_Olympics_CH	{
    if ($EVENT$ != "Olympics") return;

    // TODO: Change based on using different verbs/adjectives?
    FYMOLY_CHANTI += 1;
    setq2 Q_AuroraEvent, (getq2(Q_AuroraEvent) + 1);
    return;
}

// Count Alchemy Usage
function	script	FYE_Olympics_AL	{
    if ($EVENT$ != "Olympics") return;

    .@units = getarg(0, 1);
    FYMOLY_ALCHMY += .@units;
    setq2 Q_AuroraEvent, (getq2(Q_AuroraEvent) + .@units);
    return;
}

// Send to Porthos, returns false on failure, true on success
function	script	FYE_Olympics_TO	{
    if ($EVENT$ != "Olympics") return false;

    .@m$ = "moly@"+getcharid(0);
    if (instanceowner(@olympics) != getcharid(3)) {
        .@id = instance_create("moly@"+getcharid(0), getcharid(3), IOT_CHAR);
        if (.@id < 0) return false;
        .@mp$ = instance_attachmap("001-14", .@id, 0, .@m$);
        @olympics = .@id;
        // It'll be self-destroyed when time runs out (30 minutes)
        instance_set_timeout(1800, 1800, .@id);
        instance_init(.@id);
    } else {
        instance_set_timeout(1800, 1800, @olympics);
    }
    warp .@m$, 92, 90;
    return true;
}







// "Submit" button from 003-1/events.txt
// Don't forget to enable it in npc/utils.txt as well!!
function	script	FYE_Submit	{
    .@day=getq(Q_AuroraEvent);
    .@pts=getq2(Q_AuroraEvent);

    // Handle daily login score rewards
    if (.@day != gettimeparam(GETTIME_DAYOFMONTH)) {
        setq1 Q_AuroraEvent, gettimeparam(GETTIME_DAYOFMONTH);
        setq2 Q_AuroraEvent, .@pts+$FYLOGIN_PTS;
        .@pts=getq2(Q_AuroraEvent);
        if ($FYLOGIN_PTS)
            dispbottom l("Daily Event Bonus: %d Points!", $FYLOGIN_PTS);
    }

    // Give you points
    if ($EVENT$ == "Expo") {
        // .:: WORLD EXPO EVENT ::.
        .@pts+=countitem(EventTreasure1)*1;
        .@pts+=countitem(EventTreasure2)*3;
        .@pts+=countitem(EventTreasure3)*5;

        setq2 Q_AuroraEvent, .@pts;
        delitem EventTreasure1, countitem(EventTreasure1);
        delitem EventTreasure2, countitem(EventTreasure2);
        delitem EventTreasure3, countitem(EventTreasure3);
    } else if ($EVENT$ == "Fishing") {
        // .:: CATCH THE GOLD FISH EVENT ::.
        .@pts+=countitem(EventFish)*1;

        setq2 Q_AuroraEvent, .@pts;
        delitem EventFish, countitem(EventFish);
    } else if ($EVENT$ == "Mining") {
        // .:: MINION UNION'S RESEARCH REQUEST EVENT ::.
        .@pts+=countitem(EventOre)*1;

        setq2 Q_AuroraEvent, .@pts;
        delitem EventOre, countitem(EventOre);
    } else if ($EVENT$ == "Tower") {
        // .:: DREAM TOWER APPEARS ::.
        .@pts+=countitem(BrokenMedal)*1;

        setq2 Q_AuroraEvent, .@pts;
        delitem BrokenMedal, countitem(BrokenMedal);
    } else if ($EVENT$ == "Raid") {
        // .:: BOSS RAID ::.
        .@pts+=countitem(EventNaftalin)*1;

        setq2 Q_AuroraEvent, .@pts;
        delitem EventNaftalin, countitem(EventNaftalin);
    } else if ($EVENT$ == "Olympics") {
        // .:: MAGIC OLYMPICS ::.
        // Handled separately
    } else {
        // Wut? This is not an Aurora Event
        Exception($EVENT$+" is NOT a valid Aurora Event; Misdefinition.\n\nPlease ensure that it is defined in utils, aurora, news, and command/event.\n\nFYE_Submit - FYEventUsesRanking - FYE_* - FYEConf_* - FYStopEvent", RB_DEFAULT|RB_ISFATAL);
    }
    return;
}






// Stops any Aurora Event
function	script	FYStopEvent	{
    consolebug "FYStopEvent %d", gettimetick(0);
    setarray .@av$, "Kamelot", "Regnum", "Expo", "Fishing", "Candor", "Mining", "Tower", "Raid", "Olympics", "Celestia", "Rebirth", "Siege", "Gemini";
    if (array_find(.@av$, $EVENT$) >= 0) {
        sClear();
        $EVENT$="";
    }
    consolebug "FYStopEvent end %d", gettimetick(0);
    return;
}


// Handle the ending of Aurora Events (see also functions/util.txt)
function	script	FYRewardEvent	{
    consolebug "FYRewardEvent %d", gettimetick(0);
    if (FYEventUsesRanking()) {
        debugmes("Rewards are due");
        // This code absolutely can't fail:
        if ($EVENT$ == "Olympics") {
            callfunc("HocusScoreNew");
            copyarray $@aurora_name$, $@moly_n$, 10;
            copyarray $@aurora_value, $@moly_v, 10;
            for (.@i=0; .@i < 10; .@i++) {
                if ($@aurora_name$[.@i] == "") break;
                $@aurora_charid[.@i] = gf_charnameid($@aurora_name$[.@i]);
            }
        } else {
    	    .@nb = query_sql("SELECT c.name, i.count2, c.char_id FROM `quest` AS i, `char` AS c WHERE i.quest_id="+Q_AuroraEvent+" AND i.char_id=c.char_id ORDER BY i.count2 DESC LIMIT 10", $@aurora_name$, $@aurora_value, $@aurora_charid);
        }
        $MOST_HEROIC$=$@aurora_name$[0];
        for (.@i=0;.@i < getarraysize($@aurora_charid);.@i++) {
            switch (.@i+1) {
                case 1:
                    .@prize=120; break;
                case 2:
                    .@prize=100; break;
                case 3:
                    .@prize=80; break;
                case 4:
                case 5:
                    .@prize=60; break;
                case 6:
                case 7:
                    .@prize=40; break;
                default:
                    .@prize=20;
            }
            rodex_sendmail($@aurora_charid[.@i], "Aurora Events", $EVENT$+" Reward!", "Final Ranking: #"+(.@i+1)+". Congratulations on making "+$@aurora_value[.@i]+" points on the event!", 0, StrangeCoin, .@prize);
        }
        // Send results copy to syslog
        .@score$=sprintf("%s event finished:\n#01 - %s (%d)\n#02 - %s (%d)\n#03 - %s (%d)\n#04 - %s (%d)\n#05 - %s (%d)\n", $EVENT$, $@aurora_name$[0], $@aurora_value[0], $@aurora_name$[1], $@aurora_value[1], $@aurora_name$[2], $@aurora_value[2], $@aurora_name$[3], $@aurora_value[3], $@aurora_name$[4], $@aurora_value[4]);
        consoleinfo(.@score$);
        // Magic Olympics has extra rewards
        if ($EVENT$ == "Olympics") {
            if ($@aurora_charid[0] > 0)
                rodex_sendmail($@aurora_charid[0], "Hocus Pocus", "A Small Gift", "I wanted to send you cake but the cat ate it.", 0, BlackyCatFix, 1, ScholarshipTuition, 1);
            if ($@aurora_charid[1] > 0)
                rodex_sendmail($@aurora_charid[1], "Hocus Pocus", "A Small Gift", "Here is some noob cake for a bright mind.", 0, ApanaCake, 1, ScholarshipTuition, 1);
        }
        consolebug "FYRewardEvent due %d", gettimetick(0);
        // Destroy the quest
        DelQuestFromEveryPlayer(Q_AuroraEvent);
        deletearray $@aurora_name$;
        deletearray $@aurora_value;
        deletearray $@aurora_charid;
        /*
        DelItemFromEveryPlayer(EventTreasure1);
        DelItemFromEveryPlayer(EventTreasure2);
        DelItemFromEveryPlayer(EventTreasure3);
        DelItemFromEveryPlayer(EventFish);
        DelItemFromEveryPlayer(EventOre);
        */
        // Specing wants scoreboards to be preserved
        // Needs sanitization, or "\n" breaks it
        .@score$=replacestr(.@score$, "\n", "\\n");
        api_send(505, "[79, 21698, \"\", \""+.@score$+"\"]");
    }
    consolebug "FYRewardEvent end %d", gettimetick(0);
    return;
}








// Modify Mob Killing
// Serves for multiple events, preserves killedrid
function	script	AuroraMobkill	{
    if ($EVENT$ == "Mining")
        FYE_Mining(killedrid);
    if ($EVENT$ == "Tower")
        FYE_Tower1(killedrid);
    if ($EVENT$ == "Raid")
        FYE_Raid(killedrid);
    return;
}


// Normalize Aurora Event after a server restart
function	script	FYE_Normalize	{
    if ($EVENT$ == "Regnum") {
        // Reapply the Regnum's blessing
        setmapflag($REGNUM_BLESSMAP$, mf_bexp, 300);
        setmapflag($REGNUM_BLESSMAP$, mf_jexp, 300);
    }
    return;
}

