// TMW 2 Script
// Author:
//  Jesusalva
//  Micksha
// Description:
//  Controls sewers.
//  FIXME: People should not be able to return here once they leave to 042-10
//  Spawn monsters and respawns them.

// A simple random treasure chest - to be sure players were introduced to this
// awesome system. Same rules as any treasure box still applies.
042-8,60,40,0	script	#chest_0428	NPC_CHEST,{
	KamelotTreasure(2);
	specialeffect(.dir == 0 ? 24 : 25, AREA, getnpcid()); // closed ? opening : closing
	close;

OnInit:
	.distance = 2;
	end;

OnInstanceInit:
    // Yes, we just hope it works out of box
    explode(.@map$, .map$, "@");
    .@g=atoi(.@map$[1]);
    if (.@g < 1) {
        debugmes "[ERROR] [KAMELOT] Unable to spawn for Kamelot %s", .map$;
        debugmes "[ERROR] [KAMELOT] Using dummy data (returned: %d)", .@g;
        .@g=0;
    }
    debugmes "Spawning monsters for guild %d", .@g;
    .@mx=getguildavg(.@g);

    // Corritors
    KamelotCaveSpawn(2, 35, 58,  54,  80, .@mx, "042-8"); // Sewer
    KamelotCaveSpawn(4, 36, 22,  75,  32, .@mx, "042-8"); // North
    KamelotCaveSpawn(6, 65, 20,  84,  70, .@mx, "042-8"); // Exit
    KamelotCaveSpawn(5, 20, 20,  41,  57, .@mx, "042-8"); // West

    // Boss Chamber
    KamelotCaveSpawn(5, 40, 32, 62, 50, .@mx, "042-8");
    KamelotBoss("042-8", 50, 42, .@mx+1, .name$);

    // Boss monster
    // TODO

    // Neutral monsters
    areamonster(.map$, 20, 20, 85, 80, strmobinfo(1, YellowSlime), YellowSlime, 5);
    areamonster(.map$, 20, 20, 85, 80, strmobinfo(1, ManaGhost), ManaGhost, max(1, .@mx/10));
    areamonster(.map$, 20, 20, 85, 80, strmobinfo(1, CaveMaggot), CaveMaggot, 30);

    // Bonus monsters
    if (!rand2(2))
        areamonster(.map$, 45, 20, 85, 80, strmobinfo(1, MagicBif), MagicBif, 2);
    if (!rand2(2))
        areamonster(.map$, 20, 20, 85, 80, strmobinfo(1, GoldenChest), GoldenChest, 1);
    if (!rand2(2))
        areamonster(.map$, 20, 20, 85, 80, strmobinfo(1, SilverChest), SilverChest, 2);
    if (!rand2(2))
        areamonster(.map$, 20, 20, 85, 80, strmobinfo(1, BronzeChest), BronzeChest, 3);
    end;

OnKillBoss:
    if (!playerattached())
        goto OnRespawn;
    // Maybe a reward is due
    .@g=getcharid(2);
    if (.@g < 1) die();
    getitem GuildCoin, 2+min(13, $KAMELOT_MX[.@g]/10);
    getexp $KAMELOT_MX[.@g]*14, $KAMELOT_MX[.@g]*8;
    mapannounce getmap(), strcharinfo(0)+" has defeated the boss!", 0;
    .@delay=max(7000, 42000-$KAMELOT_PC[.@g]*2000);
    goto OnRespawn;

OnKillMob:
    if (!playerattached())
        goto OnRespawn;
    // Maybe a reward is due
    .@g=getcharid(2);
    if (.@g < 1) die();
    getexp $KAMELOT_MX[.@g]*7, $KAMELOT_MX[.@g]*4;
    .@delay=max(7000, 42000-$KAMELOT_PC[.@g]*2000);
    // FALLTHROUGH

OnRespawn:
    .@delay=(.@delay ? .@delay : 7000);
    sleep(.@delay);
    // Yes, we just hope it works out of box
    explode(.@map$, .map$, "@");
    .@g=atoi(.@map$[1]);
    if (.@g < 1) {
        debugmes "[ERROR] [KAMELOT] Unable to respawn for Kamelot %s", .map$;
        .@g=0;
    }
    KamelotCaveSpawn(1, 20, 20, 115, 100, $KAMELOT_MX[.@g], "042-8");
    end;

}


// The exit only works before chest is looted
042-8,51,79,0	script	#KDoor0428	NPC_HIDDEN,2,0,{
    end;

OnTouch:
    .@g=getcharid(2);
    if (.@g < 1) die();
    if ($KAMELOT_KEYMASK[.@g] & 2) goto L_NoAccess;
    warp "042-5@"+.@g, 44, 21;
    end;


L_NoAccess:
    dispbottom l("OH NOES! The ceiling seems to have collapsed when the chest was open! We are forced to go forward!!");
    end;
}

