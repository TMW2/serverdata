// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//   Leads the Alliance in the Fortress Town

025-1,96,26,0	script	Commander Povo	NPC_BRGUARD_SPEAR,{
    .@q=getq(General_Narrator);
    if (.@q < 21) { legiontalk(); end; } // No cutting corners!
    mesn;
    mesq l("Greetings %s, I am %s, the man in charge for the Alliance occupation of Fortress Town.", (strcharinfo(0) == $MOST_HEROIC$ ? lg("Hero") : lg("Adventurer")), .name$);
    next;

    // All this is only relevant if Moubootaur wasn't unsealed (yet)
    if ($GAME_STORYLINE < 5) {
        // Povo is worried with the upcoming siege
        if ($FIRESOFSTEAM && gettime(4) == MONDAY) {
            mesn;
            mesq l("It's a matter of hours before the Impregnable Fortress send enough monsters to overrun us, so we're preparing a strategic withdraw before this. I'm sorry, but I cannot spare any time for you.");
            close;
        // Povo is worried with the upcoming siege, but he doesn't know when
        } else if (!$FIRESOFSTEAM && gettime(4) < TUESDAY) {
            mesn;
            mesq l("It's a matter of hours before the Impregnable Fortress send enough monsters to overrun us, and we still haven't understood the pattern. We must be ready to withdraw at any time, which precludes casual talk.");
            mesc l("%s clearly thinks you're distracting him. It's better to talk to him another day.", .name$);
            close;
        }
    }

    // Depending on how you finished Barbara's Quest, claim your reward now
    if (BARBARA_STATE == 2) {
        inventoryplace NPCEyes, 1;
        mesn;
        mesq l("Sir Pyndragon and Lady Lalica asked to thank thee for the situation with the thief.");
        next;
        mesn;
        mesq l("Therefore, please choose a permanent stat-boosting fruit.");
        next;
        menuint
            l("I'll decide later"), 0,
            l("Strength"), StrengthFruit,
            l("Agility"), AgilityFruit,
            l("Vitality"), VitalityFruit,
            l("Intelligence"), IntelligenceFruit,
            l("Dexterity"), DexterityFruit,
            l("Luck"), LuckFruit;
        mes "";
        if (@menuret) {
            getitembound @menuret, 1, 4;
            BARBARA_STATE += 10;
            getexp 0, 250;
            mesn;
            mesq l("Here you go. Please, keep fighting and growing strong. Improving oneself is... Damn, I forgot the proverb.");
            next;
        }
    }

    // Switch the quest
    if (.@q == 21) goto L_Intro;
    if (.@q == 22) goto L_Short;
    if (.@q == 23) goto L_Storage;
    // ...

    // Endtrail
    mes "";
    mesc l("@@ You need to wait further releases to continue this quest!", b(l("WARNING:"))), 1;
    close;

// Dispatch you to the abandoned encampment.
L_Intro:
    mesn;
    mesq l("Don't worry, I was briefed. You're looking for the Mana Fragment Expedition, right? The one which your parents supposedly participated or something?");
    next;
    mesn;
    mesq l("We scoured this place. Found notes left behind by the expedition, they seem to have had set an encampment southeast of here.");
    next;
    mesn;
    mesq l("I have no idea how successful they were; the place we found the notes in was... well... lets just say the wood walls were red, and have been that way for a long time.");
    next;
    mesn;
    mesq l("However, it was mentioned that there is a small tunnel with a closed passage to reach the encampment; I assume they weren't exactly welcomed by the local monster population and the Fortress Town if anything was swarming with monsters back then.");
    next;
    if ($FIRESOFSTEAM < 10) {
        mesn;
        mesq l("And truth be told, it still is - the Impregnable Fortress seems to periodically release massive batches of monsters, trying to hold the ground is a death wish.");
        next;
        mesn;
        if ($FIRESOFSTEAM)
            mesq l("Hopefully Andrei Sakar will find what he's looking for in Artis. I believe Nard was in charge of ferrying adventurers to his exploration, consider helping him.");
        else
            mesq l("I cannot let go of a single combat capable person - even volunteer adventurers like yourself - until we can confirm the Impregnable Fortress north of us has a pattern to their spawns.");
        close;
    }
    mesn;
    mesq l("I don't have anyone to send ahead to scout the place, and even if you go, I'm not expecting you to find anything alive on that encampment. But I understand that finding out the truth is important to you and that you'll go all the same.");
    next;
    mesn;
    mesq l("So, here is the key. Well, I say \"key\", but it is really just a magic pattern which you can draw on your hand and use it to open the passage. I can only wish you luck... Godspeed, %s, may the wind be in your favor.", (strcharinfo(0) == $MOST_HEROIC$ ? lg("Hero") : lg("Adventurer")));
    next;
    mesc b(l(".:: Main Quest ::.")), 3;
    msObjective($FORTRESS_STATE, l("* Invade the Fortress Town"));
    msObjective(true, l("* Find clues"));
    msObjective(false, l("  ** Explore the encampment southeast of Fortress Town."));
    msObjective(false, l("  ** Unveil the truth."));
    msObjective(false, l("* (optional) Save the world!"));
    setq General_Narrator, 22;
    getexp 0, 2225; // You gain some Job Experience for completing this talk
    close;

// Reminder to go to the Fortress Encampment
L_Short:
    mesn;
    mesq l("Why are you hesitating to explore the abandoned encampment southeast of this town? Well, not that I blame you; the dangers likely outweights the benefits.");
    next;
    mesn;
    mesq l("They might have sealed the direct path, but there's a tunnel in a cave which you can go in now that you have the magic key to open it.");
    close;

L_Storage:
    if ($GAME_STORYLINE < 5) close;
    if (REBIRTH < 4) close;
    mesn;
    mesq l("Actually, given you are a key part of our defenses and the Monster King is dead, you have acquired a special leave to use our storage.");
    next;
    closeclientdialog;
    openstorage 3;
    close;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}

/////////////////////////////////////////////////////////////////////////////////
025-1,59,86,0	script	Commander Cadis	NPC_BRGUARD_SWORD,{
    function cadisReward;
    mesn;
    mesq l("Greetings %s, I am %s, I am in charge of monster extermination.", (strcharinfo(0) == $MOST_HEROIC$ ? lg("Hero") : lg("Adventurer")), .name$);
    next;
    // Check for ongoing quests
    if (getq(FortressQuest_SlimeHunter) == 1)
        goto L_SlimeHunter;
    if (getq(FortressQuest_Over100) == 1)
        goto L_Over100;
    if (getq(FortressQuest_RangedHunt) == 1)
        goto L_RangedHunt;
    mesn;
    mesq l("I have extremely difficult quests for you; They are more painful than a Grand Hunter Quest, because I don't admit wimps fighting with me.");
    next;
    mesn;
    mesq l("You will be rewarded by me shall you succeed in any of the tasks.");
    mesc l("%s: Once accepted, you must complete them before taking another one.", b(l("WARNING")));
    // Well, we *could* make it more GHQ-like but lazy devs are lazy.
    next;
    do
    {
        select
            l("I'm not interested."),
            rif(!getq(FortressQuest_SlimeHunter), l("The Great Slime Hunt")),
            rif(!getq(FortressQuest_Over100), l("The Great Over 100 Hunt")),
            rif(!getq(FortressQuest_RangedHunt), l("The Great Ranged Hunt"));
        mes "";
        switch (@menu) {
        case 1:
            close;
        case 2:
            mesn;
            mesq l("You'll have to slay %s %s for me! I don't care which ones, just SLAY THEM!", fnum(.million), l("slimes"));
            next;
            select
                l("Accept"),
                l("Reject");
            mes "";
            if (@menu == 1) {
                setq1 FortressQuest_SlimeHunter, 1;
                mesn;
                mesq l("Then get to work already!");
                close;
            }
            break;
        case 3:
            mesn;
            mesq l("You'll have to slay %s %s for me! I don't care which ones, just SLAY THEM!", fnum(.million), l("monsters of at least level 100"));
            mesc l("Each monster will be counted as %d kills.", 2);
            next;
            select
                l("Accept"),
                l("Reject");
            mes "";
            if (@menu == 1) {
                setq1 FortressQuest_Over100, 1;
                mesn;
                mesq l("Then get to work already!");
                close;
            }
            break;
        case 4:
            mesn;
            mesq l("You'll have to slay %s %s for me! I don't care which ones, just SLAY THEM!", fnum(.million), l("ranged monsters"));
            mesc l("Minimum range: %d", 3);
            next;
            select
                l("Accept"),
                l("Reject");
            mes "";
            if (@menu == 1) {
                setq1 FortressQuest_RangedHunt, 1;
                mesn;
                mesq l("Then get to work already!");
                close;
            }
            break;
        }
    } while (true);
    close;

L_SlimeHunter:
    cadisReward(FortressQuest_SlimeHunter, l("Slimes"));
    close;

L_Over100:
    cadisReward(FortressQuest_Over100, l("over 100 mobs"));
    close;

L_RangedHunt:
    cadisReward(FortressQuest_RangedHunt, l("ranged mobs"));
    close;

function cadisReward {
    .@kill=getq2(getarg(0));
    .@rewa=getq3(getarg(0));
    mesn;
    mesq l("Thus far you've slain %s/%s %s for me!", fnum(.@kill), fnum(.million), b(getarg(1, "monsters")));
    inventoryplace NPCEyes, 4, Iten, 2;

    // ***** ***** *****   Rewards   ***** ***** *****
    if (.@kill >= .tier1 && .@rewa < 1) {
        mesc l("Milestone reached: %s kills", fnum(.tier1));
        setq3 getarg(0), 1;
        getitem MercenaryBoxsetDD, 3;
        getitem SacredImmortalityPotion, 2;
        getitem MercenaryBoxsetE, 1;
    }

    if (.@kill >= .tier2 && .@rewa < 2) {
        mesc l("Milestone reached: %s kills", fnum(.tier2));
        setq3 getarg(0), 2;
        getitem MercenaryBoxsetEE, 1;
        getitem MagicApple, 1;
        getitem EquipmentBlueprintE, 1;
        getitem IridiumOre, 2;
    }

    if (.@kill >= .tier3 && .@rewa < 3) {
        mesc l("Milestone reached: %s kills", fnum(.tier3));
        setq3 getarg(0), 3;
        getitem MercenaryBoxsetEE, 2;
        getitem StrangeCoin, 50;
        getitem EarthPowder, 2;
        getitem PlatinumOre, 1;
    }

    if (.@kill >= .tier4 && .@rewa < 4) {
        mesc l("Milestone reached: %s kills", fnum(.tier4));
        setq3 getarg(0), 4;
        getitem MercenaryBoxsetEE, 3;
        getitem ElixirOfLife, 5;
        getitem EarthPowder, 2;
        getitem PlatinumOre, 1;
    }


    // Quest complete
    if (.@kill >= .million) {
        mesc b(l("Quest complete: Congratulations!")), 3;
        setq1 getarg(0), 2;
        setq3 getarg(0), 5;
        getitem MercenaryBoxsetEE, 5;
        getitem StrangeCoin, 100;
        getitem EarthPowder, 3;
        getitem any(LuckFruit, DexterityFruit, IntelligenceFruit, VitalityFruit, AgilityFruit, StrengthFruit), 1;
        getitem MysteriousFruit, 1;
    }
    return;
}

OnInit:
    .sex = G_MALE;
    .distance = 5;
    .million = 1000000;

    .tier1 =  10000;
    .tier2 =  50000;
    .tier3 = 250000;
    .tier4 = 500000;
    end;
}

// Commander Cadis Questcheck
function	script	CadisQuestCheck	{
    if (!playerattached())
        return;

    .@mobId=getarg(0, killedrid);

    if (getq(FortressQuest_SlimeHunter) == 1) {
        // Register the kill
        if (compare(strtolower(strmobinfo(1, .@mobId)), "slime")) {
            setq2 FortressQuest_SlimeHunter, getq2(FortressQuest_SlimeHunter)+1;
        // Report every 1000 kills
        if (getq2(FortressQuest_SlimeHunter) % 1000 == 0)
            dispbottom l("Cadis : You have slain %s %s out of a million.", fnum(getq2(FortressQuest_SlimeHunter)), l("slimes"));
        }
    }

    if (getq(FortressQuest_Over100) == 1) {
        // Register the kill as doubled (to make easier)
        if (getmonsterinfo(.@mobId, MOB_LV) >= 100) {
            setq2 FortressQuest_Over100, getq2(FortressQuest_Over100)+2;
        // Report every 1000 kills
        if (getq2(FortressQuest_Over100) % 1000 == 0)
            dispbottom l("Cadis : You have slain %s %s out of a million.", fnum(getq2(FortressQuest_Over100)), l("mobs over lv 100"));
        }
    }

    if (getq(FortressQuest_RangedHunt) == 1) {
        // Register the kill
        if (getmonsterinfo(.@mobId, MOB_RANGE) >= 3) {
            setq2 FortressQuest_RangedHunt, getq2(FortressQuest_RangedHunt)+1;
        // Report every 1000 kills
        if (getq2(FortressQuest_RangedHunt) % 1000 == 0)
            dispbottom l("Cadis : You have slain %s %s out of a million.", fnum(getq2(FortressQuest_RangedHunt)), l("ranged mobs"));
        }
    }

    return;
}

