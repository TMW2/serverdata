// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 025-1: Fortress Town - Holy Land conf

025-1,99,112,0	script	#025-1_99_112	NPC_HIDDEN,{
	end;
OnDisable:
	delcells "025-1_99_112"; end;
OnEnable:
OnInit:
	setcells "025-1", 99, 112, 100, 112, 1, "025-1_99_112";
}

025-1,100,122,0	script	#025-1_100_122	NPC_HIDDEN,7,0,{
	end;
OnTouch:
	doevent "#DungeonCore::OnCurse";
	end;
}

025-1,100,22,0	script	#025-1_100_22	NPC_HIDDEN,3,0,{
	end;
OnTouch:
	doevent "#DungeonCore::OnCurse";
	end;
}
