// TMW-2 Script.
// Author:
//    Jesusalva
// Notes:
//    Fortress Island South (Lv 70~150 Area)

025-2	mapflag	mask	5

025-2,97,122,0	script	Airship	NPC_AIRSHIP,{
	mesc l(".:: Fortress Island ::."), 1;
	mes "";
	mesc l("Do you want to return to Land Of Fire Village?"), 1;
	next;
	if (askyesno() == ASK_YES) {
        specialeffect FX_CIRCLE, SELF, getcharid(3);
        closeclientdialog;
		sleep2(5000);
	    removespecialeffect(FX_CIRCLE, SELF, getcharid(3));
		if (ispcdead()) end;
		warp "017-10", 64, 25;
	}
	close;

OnInit:
    .sex = G_OTHER;
    .distance = 8;
	end;

}


