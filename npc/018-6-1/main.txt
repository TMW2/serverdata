// TMW2 Scripts
// Author:
//    Jesusalva
// Description:
//    Controls Elenium Mines, specially the instances

// Main Controller for Instanced maps
018-6-1,0,0,0	script	#01861_InstCtrl	NPC_HIDDEN,{
    end;

// Map, x, y, width, height, mob, amount
function AreaMonsterB {
    .@m$=getarg(0);
    .@x1=getarg(1);
    .@x2=.@x1+getarg(3);
    .@y1=getarg(2);
    .@y2=.@y1+getarg(4);
    .@mi=getarg(5);
    .@am=getarg(6);
    areamonster(.@m$, .@x1, .@y1, .@x2, .@y2, strmobinfo(1, .@mi), .@mi, .@am, instance_npcname(.name$)+"::OnKill"+.@mi);
    return;
}

// (var, value)
function SetIfVoid {
    if (!getarg(0))
        return getarg(1);
    return 0;
}

// Bifs won't respawn
OnKill1226:
OnKill1227:
OnKill1228:
    end;

// Others: Wait 30 seconds and respawn
OnKill1178:
    .@mi=SetIfVoid(.@mi, 1178);
OnKill1051:
    .@mi=SetIfVoid(.@mi, 1051);
OnKill1074:
    .@mi=SetIfVoid(.@mi, 1074);
OnKill1123:
    .@mi=SetIfVoid(.@mi, 1123);
OnKill1176:
    .@mi=SetIfVoid(.@mi, 1176);
OnKill1026:
    .@mi=SetIfVoid(.@mi, 1026);

    // Common utils
    /*
    // Respawn monster after 30s
    getmapxy(.@m$, .@x, .@y, 0);
    sleep(30000);
    AreaMonsterB(.@m$, .@x-2, .@y-2, 4, 4, .@mi, 1);
    */
    end;

OnInstanceInit:
    // Generate map ID
    getmapxy(.@m$, .@x, .@y, UNITTYPE_NPC);

    // Bif monsters
    AreaMonsterB(.@m$,  40,  49, 7, 8, BigEleniumBif, 5);
    AreaMonsterB(.@m$,  95,  51, 7, 8, BigEleniumBif, 5);
    AreaMonsterB(.@m$, 145,  83, 6, 5, EleniumBif, 5);
    AreaMonsterB(.@m$,  62,  86, 6, 5, EleniumBif, 5);
    AreaMonsterB(.@m$,  65, 130, 6, 5, EleniumBif, 5);
    AreaMonsterB(.@m$, 125, 158, 6, 5, SmallEleniumBif, 4);
    AreaMonsterB(.@m$, 129, 183, 6, 5, SmallEleniumBif, 4);
    AreaMonsterB(.@m$,  34, 181, 5, 5, SmallEleniumBif, 4);

    // Common Monsters
    AreaMonsterB(.@m$,  80, 172, 54, 25, BlackSlime, 16);
    AreaMonsterB(.@m$,  89, 167, 54, 25, DarkLizard, 8);
    AreaMonsterB(.@m$,  96, 103, 56, 29, BlackScorpion, 17);
    AreaMonsterB(.@m$, 101, 105, 54, 25, MountainSnake, 9);
    AreaMonsterB(.@m$,  70,  45, 44, 25, WickedMushroom, 14);
    AreaMonsterB(.@m$,  68,  45, 31, 21, Archant, 7);
    end;

}
/////////////////////////////
018-6-1,83,26,0	script	#ToForgottenChamber	NPC_SUMMONING_CIRC,0,0,{
    dispbottom l("It looks dangerous.");
    end;

OnTouch:
    .@q=getq(LoFQuest_Barbara);
    .@q2=getq2(LoFQuest_Barbara);
    .@q3=getq3(LoFQuest_Barbara);
    // Skipped cutscenes, show you the EXIT
    if (.@q == 1 && .@q3 != 1)
        cwarp "017-1", 32, 45;

    if (.@q >= 1) {
        warp BarbaraInstCheck(0), 90, 91;
        // Complete the first arc if possible
        if (.@q == 1) {
            setnpcdialogtitle l("Forgotten Chamber");
            mesn l("Forgotten Chamber");
            mesq l("You see yourself in a strange chamber. Strange drawings litter the walls.");
            next;
            mesn l("Forgotten Chamber");
            mesq l("You can see in distance two portals working, and two portals offline.");
            next;
            mesn l("Forgotten Chamber");
            mesq l("It seems to be a puzzle, you can only imagine what needs to be done in order to enable the right portals.");
            next;
            setq1 LoFQuest_Barbara, 2;
            setq3 LoFQuest_Barbara, 0;
            closeclientdialog;
        }
        // Update M0 NPCs based on quest state
        if (.@q > 2)
            enablenpc instance_npcname("#ToForgottenShrine", .@q2);
        if (.@q == 2) {
            // Note: state 64 and 128 are temporary and must be removed
            if (.@q3 & 64)
                setq3 LoFQuest_Barbara, .@q3^64;
            // Safety update
            .@q3=getq3(LoFQuest_Barbara);

            if (.@q3 & 128)
                setq3 LoFQuest_Barbara, .@q3^128;
            // Safety update
            .@q3=getq3(LoFQuest_Barbara);

            if (.@q3 == 5)
                enablenpc instance_npcname("#ToForgottenShrine", .@q2);
            if (.@q3 == 3)
                enablenpc instance_npcname("#ToSouthHall", .@q2);
        }

    } else {
        Exception("ERROR, YOU SHOULD NOT BEEN SEEING THIS. 018-6-1.TFC");
    }
    end;

OnInit:
    disablenpc .name$;
    end;

}

///////////////////////////// Minievents
018-6-1,128,131,0	script	Wounded Soldier#01861	NPC_GUARD_DEAD,{
    //npctalk3 l("Please find her and don't worry with me! And be careful!");
    npctalk3 l("*scream in pain*");
    close;

OnMain:
    mesn l("Wounded Soldier");
    mesq l("Hey, you! Are you here on Kenton's orders?!");
    next;
    setnpcdialogtitle l("Benjamin, Wounded Soldier");
    mesn l("Benjamin, Wounded Soldier");
    mesq l("My name is Benjamin, and I was in charge of an incursion here to capture Barbara.");
    next;
    mesn l("Benjamin, Wounded Soldier");
    mesq l("My unit, however, was attacked! An assassin snuck up on us!");
    mesc l("You help Benjamin in getting up.");
    next;
    mesn l("Benjamin, Wounded Soldier");
    mesq l("I hope my men are safe, that was yesterday. I am barely alive.");
    mesc l("You know what is worse? THIS WHOLE CAVE HAVE DEATH PENALTY ON!!!"), 1;
    next;
    mesn l("Benjamin, Wounded Soldier");
    mesq l("I shall report to Kenton. I now entrust this quest in your hands!");
    next;
    mesn l("Benjamin, Wounded Soldier");
    mesq l("And please, bring those criminals to justice! Barbara and the Assassin!");
    next;
    mesc l("With a wave, Benjamin returns to report Kenton about the outcome.");
    setq3 LoFQuest_Barbara, 1;
    disablenpc instance_npcname(.name$); // NPC will now go to rest
    close;

OnInit:
    .sex=G_MALE;
    disablenpc .name$;
    end;
OnInstanceInit:
    if ($@GM_OVERRIDE) setnpcdisplay instance_npcname(.name$), NPC_WOUNDEDSOLDIER;
    disablenpc instance_npcname(.name$);
    end;
}

// Event trigger
018-6-1,121,132,0	script	#01861TriggerWS1	NPC_HIDDEN, 0, 10,{
    end;
OnTouch:
    .@q=getq(LoFQuest_Barbara);
    .@q2=getq2(LoFQuest_Barbara);
    .@q3=getq3(LoFQuest_Barbara);
    if (.@q == 1 && .@q3 == 0) {
        enablenpc instance_npcname("Wounded Soldier#01861", .@q2);
        addtimer2(500, instance_npcname("Wounded Soldier#01861", .@q2)+"::OnMain");
    }
    end;

OnInit:
    disablenpc .name$;
    end;
}

///////////////////////////// Minievents
018-6-1,89,70,0	script	Barbara#01861	NPC_INJURIED_GIRL,{
    //npctalk3 l("Please find her and don't worry with me! And be careful!");
    npctalk3 l("*scream in pain*");
    close;

OnMain:
    slide 84, 71; // Safety
    showavatar NPC_INJURIED_GIRL;  // this is handled by avatars.xml

    .@q=getq(LoFQuest_Barbara);
    .@q2=getq2(LoFQuest_Barbara);
    .@q3=getq3(LoFQuest_Barbara);
    sleep2(50);

    // Barbara was taken hostage or murdered - save this data
    @evil=0; // Temporary variable to show the murder option or not
    setnpcdialogtitle l("Wounded Girl");
    mesn l("Wounded Girl");
    mesq l("...");
    next;
    mesn l("Wounded Girl");
    mesq l("...I guess I can't hide anymore...");
    next;
    sshake(rand2(3,5), false);
    mesn l("Wounded Girl");
    mesq l("Ah!");
    next;
    setnpcdialogtitle l("Barbara, Wounded Girl");
    mesn l("Barbara, Wounded Girl");
    mesq l("Sorry, I haven't seen you. My name is Barbara.");
    next;
    select
        l("My name is ")+strcharinfo(0),
        l("I'm here to arrest you.");
    mes "";
    if (@menu == 1) {
        mesn l("Barbara, Wounded Girl");
        mesq l("Pleased to meet you. What brings you down here?");
        next;
        mesn strcharinfo(0);
        mesq l("I'm here to arrest a criminal.");
        next;
    } else {
        @evil+=1;
    }
    mesn l("Barbara, Wounded Girl");
    mesq l("Please spare me. I'm gravely wounded. I'm innocent, I swear!");
    next;
    select
        l("Why should I spare you?"),
        l("Where is the item you've stolen?");
    mes "";
    mesn l("Barbara, Wounded Girl");
    mesq l("I... I confess. I did steal an apple. I was hungry! I am very poor.");
    next;
    mesn l("Barbara, Wounded Girl");
    mesq l("My family died when I was just a little girl... I have been trying to survive...");
    next;
    select
        l("Cut the chat short and go straight to the point!"),
        l("I'm listening, but don't hope me to buy your story.");
    mes "";
    if (@menu == 2) {
        mesn l("Barbara, Wounded Girl");
        mesq l("You know, if you're poor, you can't get too close to the Noble district.");
        next;
        mesn l("Barbara, Wounded Girl");
        mesq l("They left me no inheritance, either. Even surviving has been a struggle. I'm not strong enough to kill the living potatoes for @@.", getitemlink(Potatoz));
        next;
        mesn l("Barbara, Wounded Girl");
        mesq l("The monsters here are too strong, and as I said before, I can't get close to the Transcendence Portal nor to get training.");
        next;
        select
            l("I'm not here to listen stories, I need you to return what you've stole.");
    } else {
        @evil+=1;
    }
    mesn l("Barbara, Wounded Girl");
    mesq l("I can't give you the Apple I've stole. Because, I was stolen first!");
    next;
    select
        l("I don't believe you."),
        l("Let's say I believe you. Who stole you? Benjamin?");
    mes "";
    if (@menu == 1) {
        mesn l("Barbara, Wounded Girl");
        mesq l("But- But you have to! It is the ")+b(l("truth"))+"!";
        next;
        select
            l("And whom do you claim to be the thief?"),
            rif(@evil, l("[Kill her]")),
            l("Barbara. You're under arrest by Kenton's orders. Please surrend peacefully.");
        mes "";
        @evil+=1;
        switch (@menu) {
        case 2:
            mesn l("Barbara, Wounded Girl");
            mesq l("OH MY, PLEASE DON'T!");
            next;
            mesn l("Barbara, Wounded Girl");
            mesq l("I NEVER TOUCHED THE SOUL MENHIR! I SWEAR YOU, I'M INNOCENT! PLEASE HAVE MERCY!!");
            next;
            mesc l("Really kill Barbara?"), 1;
            mesc l("WARNING: THIS ACTION IS IRREVERSIBLE."), 1;
            if (askyesno() == ASK_YES) {
                mes "";
                mesc l("She falls in a single blow. You'll attribute her death to the Mysterious Assassin.");
                BARBARA_STATE=1;
                setq1 LoFQuest_Barbara, 5;
                disablenpc instance_npcname(.name$); // NPC will now go to rest
                close;
            } else {
                @evil=false;
                mesn l("Barbara, Wounded Girl");
                mesc l("*sigh*");
                mesq l("Thank you...");
                next;
            }
            break;
        case 3:
            mesn l("Barbara, Wounded Girl");
            mesq l("...I have no other choice, do I?");
            next;
            mesn l("Barbara, Wounded Girl");
            mesq l("But be warned, I don't have the Apple with me. It must be further in.");
            // The quest is not over yet - do not set BARBARA_STATE
            setq1 LoFQuest_Barbara, 5;
            disablenpc instance_npcname(.name$); // NPC will now go to rest
            close;
            break;
        }
    } else {
        mesn l("Barbara, Wounded Girl");
        mesq l("Oh no, Benjamin is a city guard, he would never do that. I hope.");
        next;
    }
L_MainStory:
    mesn l("Barbara, Wounded Girl");
    mesq l("A hooded man attacked me, and left me in this state.");
    next;
    mesn l("Barbara, Wounded Girl");
    mesq l("The hooded man was about to deliver me a final blow, but then he saw the Apple I've stole...");
    next;
    mesn l("Barbara, Wounded Girl");
    mesq l("...And for some reason he took the apple and went further in the caves.");
    next;
    mesn l("Barbara, Wounded Girl");
    mesq l("Maybe if you go back the way you're coming, you find him...");
    next;
    mesn l("Barbara, Wounded Girl");
    mesq l("By the way, I can't say for sure it was a man. He smelled like onions, and his face was shinning.");
    next;
    // FIXME: Should she be so blunt?
    mesn l("Barbara, Wounded Girl");
    mesq l("I'm only assuming it was a man because the lack of boobs.");
    next;
    mesn l("Barbara, Wounded Girl");
    mesq l("So... I'm at your hands now... What will you do with me?");
    next;
    select
        l("Listen to her story again."),
        rif(@evil, l("[Kill her]")),
        l("You're under arrest. I have to bring you back to Kenton."),
        l("I have some healing items with me, we should tend your wounds first.");
    mes "";
    switch (@menu) {
    // Murder
    case 2:
        mesn l("Barbara, Wounded Girl");
        mesq l("OH MY, PLEASE DON'T!");
        next;
        mesn l("Barbara, Wounded Girl");
        mesq l("I NEVER TOUCHED THE SOUL MENHIR! I SWEAR YOU, I'M INNOCENT! PLEASE HAVE MERCY!!");
        next;
        mesc l("Really kill Barbara?"), 1;
        mesc l("WARNING: THIS ACTION IS IRREVERSIBLE."), 1;
        if (askyesno() == ASK_YES) {
            mes "";
            mesc l("She falls in a single blow. You'll attribute her death to the Mysterious Assassin.");
            BARBARA_STATE=1;
            setq1 LoFQuest_Barbara, 5;
            disablenpc instance_npcname(.name$); // NPC will now go to rest
            close;
        } else {
            @evil=false;
            mesn l("Barbara, Wounded Girl");
            mesc l("*sigh*");
            mesq l("Thank you...");
            next;
        }
    // Repeat
    case 1:
        goto L_MainStory;
        break;
    // Healing
    case 4:
        mesn l("Barbara, Wounded Girl");
        mesq l("I'm afraid my wounds are too critical. I'm bleeding. I was cursed. And I can't even move.");
        next;
        if (countitem(ElixirOfLife)) {
            mesc l("You have an @@ - Should you give her that?", getitemlink(ElixirOfLife));
            if (askyesno() == ASK_YES) {
                mes "";
                mesn l("Barbara, Wounded Girl");
                mesq l("It's true enough an @@ could heal even a dead tree, and would dispel almost every status ailment I could have.", getitemlink(ElixirOfLife));
                next;
                mesn l("Barbara, Wounded Girl");
                mesq l("But my case requires extended treatment. I need to be hospitalized. I'm afraid it is too late for the Elixir to have effect...");
                next;
            }
        }
        mesn strcharinfo(0);
        mesq l("...You're right. My medicine cannot heal you.");
        next;
        mesn l("Barbara, Wounded Girl");
        mesq l("I think the Doctor, on Dimond's Cove could help me, but that means passing by Kenton...");
        next;
    // Arrest
    case 3:
        mesn l("Barbara, Wounded Girl");
        mesq l("...I have no other choice, do I?");
        next;
        mesn l("Barbara, Wounded Girl");
        mesc l("*sigh*");
        mesq l("But be warned, I don't have the Apple with me. It must be further in.");
        mes "";
        mesc l("Barbara was apprehended. It would be weird to show her on your inventory, so we won't.");
        // The quest is not over yet - do not set BARBARA_STATE
        setq1 LoFQuest_Barbara, 5;
        disablenpc instance_npcname(.name$); // NPC will now go to rest
        close;
    }
    Exception("Invalid Barbara Dialog State - 0");
    close;

OnInit:
    .sex=G_FEMALE;
    disablenpc .name$;
    end;
OnInstanceInit:
    disablenpc instance_npcname(.name$);
    end;
}

// Event trigger
018-6-1,84,71,0	script	#01861TriggerBB1	NPC_HIDDEN, 0, 10,{
    end;
OnTouch:
    .@q=getq(LoFQuest_Barbara);
    .@q2=getq2(LoFQuest_Barbara);
    .@q3=getq3(LoFQuest_Barbara);
    if (.@q == 4) {
        enablenpc instance_npcname("Barbara#01861", .@q2);
        addtimer2(500, instance_npcname("Barbara#01861", .@q2)+"::OnMain");
    }
    end;

OnInit:
    disablenpc .name$;
    end;
}

