// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//   The Impregnable Fortress Control Files
// Quest: General_Fortress
//  (MaxFloor+1, internal, internal)

025-3,105,83,0	script	Impregnable#0F	NPC_HIDDEN,2,0,{
    end;

OnTouch:
    if (getq(General_Fortress)) goto L_Warp;
    mes l(".:: The Impregnable Fortress ::.");
    mes "";
    mes l("The fortress goes below until the Monster King Throne is established.");
    mes l("To descend you must finish the task on the floor. Sometimes it'll be outspoken, other times, it'll not.");
    mes l("The tasks may vary from collecting certain items, walking in a specific order, lighting candles, killing a specific monster or all monsters in the floor, et cetera.");
    next;
    mes l("The tasks are individual, but help from other players is acceptable in most cases.");
    mesc l("(Do note that there may be filters to decide when the player helped you and when the player did the quest for you. In some cases, you'll need to redo the stage.)");
    next;
    mes l("After finishing the task, the floor below will be liberated for your access only. Other players will need to complete their tasks.");
    mes l("Due to the sheer amount of monsters in the army, most floors are not immediately available! The Monster Army must be weakened before accessing the deeper floors by repeatedly defeating them at Fortress Town Siege.");
    next;
    mesc l(".:: Impregnable Fortress, %sF ::.", "0"), 3;
    msObjective(getq(General_Fortress) == 1, l("* Obtain the Fortress Key"));
    msObjective($MK_TEMPVAR < MKIF_LV_B0F, l("Minimum wins: %d/%d", $MK_TEMPVAR, MKIF_LV_B0F));
    mes "";
    mesc l("Hint: Eek! Eek! Eek!");
    tutmes l("Explore all map elements (including monsters) with the hint in mind to fulfill the mission objectives. They'll never demand something outside the mission map!");
    close;

L_Warp:
    // Not unlocked
    if ($GAME_STORYLINE >= 3 && $MK_TEMPVAR < MKIF_LV_B0F) {
        mesc l("The gate is sealed shut."), 1;
        mesc l("The monster army is still strong on this floor!"), 1;
        mesc l("Minimum wins: %d/%d", $MK_TEMPVAR, MKIF_LV_B0F), 1;
        close;
    }
    warp "026-0", 64, 98;
    end;

OnMoubiKill:
    .@q=getq(General_Fortress);
    .@q2=getq2(General_Fortress);
    .@q3=getq3(General_Fortress);
    if (.@q != 0) end;

    setq2 General_Fortress, .@q2+1;
    if (rand2(10000) <= .@q2) {
        dispbottom b(l("You have found the Impregnable Fortress Key. Access to B0F granted."));
        setq General_Fortress, 1, 0, 0;
        getexp 0, 50000;
    }
    if (!killedrid)
        fix_mobkill(AngryBat);
    else
        fix_mobkill(killedrid);
    end;
}


