// TMW2 scripts.
// Authors:
//    Saulc
//    Jesusalva
//    Reid
//    Travolta
// Description:
//    Tamiloc is the barber.

003-6,46,30,0	script	Tamiloc	NPC_ELVEN_FEMALE_ARMOR_SHOP,{
    function setRace {
        clear;
        setnpcdialogtitle l("%s - Modify Race", .name$);
        mes l("Race") + ": " + get_race();
        next;
        mes l("Please select the desired race.");
        select
            l("Kaizei Human"),
            l("Argaes Human"),
            l("Tonori Human"),
            l("Elf"),
            l("Orc"),
            l("Raijin"),
            l("Tritan"),
            l("Ukar"),
            l("Redy"),
            l("Savior");
        switch (@menu)
        {
            default:
                jobchange max(0, @menu-1);
        }
        return;
    }


    mesn;
    mesq l("Hi! Do you want a hair cut?");

    do
    {
        select
            l("What is my current hairstyle and hair color?"),
            l("I'd like to get a different style."),
            l("Can you do something with my color?"),
            rif(is_gm() || REBIRTH >= 5, l("I want to change my Race!")),
            l("I'm fine for now, thank you.");

        switch (@menu)
        {
            case 1:
                BarberSayStyle 3;
                break;
            case 2:
                BarberChangeStyle;
                speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("Enjoy your new style.");
                    l("Anything else?");
                break;
            case 3:
                BarberChangeColor;
                speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("I hope you like this color.");
                    l("Anything else?");
                break;
            case 4:
                setRace;
                break;
            case 5:
                speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("Feel free to come visit me another time.");

                goodbye;
        }
    } while (1);
    close;


OnInit:
    .sex = G_FEMALE;
    .distance = 5;
    end;
}
