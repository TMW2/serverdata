// TMW 2 Script
// Author:
//  Jesusalva
//  Micksha
// Description:
//  Controls sewers.
//  FIXME: People should not be able to return here once they leave to 042-10
//  Spawn monsters and respawns them.

// A simple random treasure chest - to be sure players were introduced to this
// awesome system. Same rules as any treasure box still applies.
042-9,48,76,0	script	#chest_0429	NPC_CHEST,{
	KamelotTreasure(1);
	specialeffect(.dir == 0 ? 24 : 25, AREA, getnpcid()); // closed ? opening : closing
	close;

OnInit:
	.distance = 2;
	end;

OnInstanceInit:
    // Yes, we just hope it works out of box
    explode(.@map$, .map$, "@");
    .@g=atoi(.@map$[1]);
    if (.@g < 1) {
        consolewarn "[ERROR] [KAMELOT] Unable to spawn for Kamelot %s", .map$;
        debugmes "[ERROR] [KAMELOT] Using dummy data (returned: %d)", .@g;
        .@g=0;
    }
    debugmes "Spawning monsters for guild %d", .@g;
    .@mx=getguildavg(.@g);

    // Corritors
    KamelotCaveSpawn(3,  67, 60, 100,  85, .@mx, "042-9"); // Sewer
    KamelotCaveSpawn(12, 20, 20, 100,  50, .@mx, "042-9"); // West+Exit
    KamelotCaveSpawn(3,  28, 50,  67,  67, .@mx, "042-9"); // East

    // Boss Chamber
    KamelotCaveSpawn(5, 21, 60, 51, 80, .@mx, "042-9");
    KamelotBoss("042-9", 40, 74, .@mx+1, .name$);

    // Boss monster
    // TODO

    // Neutral monsters
    areamonster(.map$, 20, 20, 100, 85, strmobinfo(1, YellowSlime), YellowSlime, 5);
    areamonster(.map$, 20, 20, 100, 85, strmobinfo(1, ManaGhost), ManaGhost, max(1, .@mx/10));
    areamonster(.map$, 20, 20, 100, 85, strmobinfo(1, CaveMaggot), CaveMaggot, 30);

    // Bonus monsters
    if (!rand2(2))
        areamonster(.map$, 45, 20, 100, 85, strmobinfo(1, MagicBif), MagicBif, 2);
    if (!rand2(2))
        areamonster(.map$, 20, 20, 100, 85, strmobinfo(1, GoldenChest), GoldenChest, 1);
    if (!rand2(2))
        areamonster(.map$, 20, 20, 100, 85, strmobinfo(1, SilverChest), SilverChest, 2);
    if (!rand2(2))
        areamonster(.map$, 20, 20, 100, 85, strmobinfo(1, BronzeChest), BronzeChest, 3);
    end;

OnKillBoss:
    if (!playerattached())
        goto OnRespawn;
    // Maybe a reward is due
    .@g=getcharid(2);
    if (.@g < 1) die();
    getitem GuildCoin, 2+min(13, $KAMELOT_MX[.@g]/10);
    getexp $KAMELOT_MX[.@g]*14, $KAMELOT_MX[.@g]*8;
    mapannounce getmap(), strcharinfo(0)+" has defeated the boss!", 0;
    .@delay=max(3000, 21000-$KAMELOT_PC[.@g]*1000);
    goto OnRespawn;

OnKillMob:
    if (!playerattached())
        goto OnRespawn;
    // Maybe a reward is due
    .@g=getcharid(2);
    if (.@g < 1) die();
    getexp $KAMELOT_MX[.@g]*7, $KAMELOT_MX[.@g]*4;
    .@delay=max(7000, 42000-$KAMELOT_PC[.@g]*2000);
    // FALLTHROUGH

OnRespawn:
    .@delay=(.@delay ? .@delay : 7000);
    sleep(.@delay);
    // Yes, we just hope it works out of box
    explode(.@map$, .map$, "@");
    .@g=atoi(.@map$[1]);
    if (.@g < 1) {
        consolewarn "[ERROR] [KAMELOT] Unable to respawn for Kamelot %s", .map$;
        .@g=0;
    }
    KamelotCaveSpawn(1, 20, 20, 115, 100, $KAMELOT_MX[.@g], "042-9");
    end;

}


// The exit only works before chest is looted
042-9,99,80,0	script	#KDoor0429	NPC_HIDDEN,0,6,{
    end;

OnTouch:
    .@g=getcharid(2);
    if (.@g < 1) die();
    if ($KAMELOT_KEYMASK[.@g] & 1) goto L_NoAccess;
    warp "042-5@"+.@g, 21, 43;
    end;


L_NoAccess:
    dispbottom l("OH NOES! The ceiling seems to have collapsed when the chest was open! We are forced to go forward!!");
    end;
}

