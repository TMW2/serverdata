// TMW2 scripts.
// Authors:
//    Diogo_RBG
//    Jesusalva
// Description:
//    Pirate Caves gateway main logic
//    Adapted from TMW-BR for Moubootaur Legends

015-3,128,161,0	script	#0153WG1	NPC_HIDDEN,0,0,{
    end;
OnTouch:
    compareandsetq HurnscaldQuest_PirateCave, 3, 4;
    compareandsetq HurnscaldQuest_PirateCave, 5, 6;
    slide 166, 38;
    end;
}

015-3,132,23,0	script	#0153WG2	NPC_HIDDEN,0,0,{
    end;
OnTouch:
    if (getq(HurnscaldQuest_PirateCave) >= 6) {
        slide 98, 162;
    }
    end;
}


