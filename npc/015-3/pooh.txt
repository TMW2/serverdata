// TMW2 scripts.
// Author:
//    Diogo RBG from TMW-BR
//    Jesusalva
// Description:
//    This is a very cute bear, it'll definitely earn your sympathy.
//    Pooh (original) has became Public Domain in January 2022.

015-3,127,181,0	script	Pooh	NPC_POOH,{
    function poohNear;
    function poohFood;
    function poohSmell;
    inventoryplace BearHat, 1;
    .@q = getq(HurnscaldQuest_TeddyBear);
    mesc l("OMG so cute! This must be the cutest bear you ever seen your whole life.");
    // Minimum level not met (does it even make sense? It is within Pirate Map)
    if (BaseLevel < 39)
        close;
    mesc l("It seems to be concentrated on its small pot... So concentrated, it doesn't even realizes you're there.");
    mesc l("This is so amusing, you decide to stare for a while longer.");
    next;
    // Quest already finished
    if (.@q >= 3) {
        mesc l("It then looks at you with a pleasant smile, you definitely earned a new cute friend!");
        close;
    }
    mesc l("And then you notice the small teddy is opening and closing the lid of this pot repeated times, as if it were looking for something.");
    mesc l("You ask yourself: What is the teddy looking for? How can I help him?");
    compareandsetq HurnscaldQuest_TeddyBear, 0, 1;
    next;
    .@cont = false;
    // Main menu
    do
    {
        mesn strcharinfo(0);
        select
            l("Sing a song to brighten its mood."),
            l("Pet his wonderfully fluffy pelt."),
            l("Approach it softly."),
            l("Offer food to it."),
            l("Leave the poor thing in peace.");
        mes "";
        switch (@menu) {
        case 1:
            mesc l("You sing a pretty song to the teddy, and this makes it very happy.");
            mesc l("You know this, because their big round eyes were shining.");
            next;
            mesc l("However, this only distracts him for a short while. Soon after, he returns to his small ritual with the pot.");
            next;
            mesc l("Poor teddy, not even a pretty music can comfort him for long. What can we do now?");
            next;
            break;
        case 2:
            mesc l("With a big smile, you stretch your hand in the teddy's direction; But the little one gets scared with the movement and starts to move away from you.");
            next;
            mesc l("You give up after seeing its reaction, and try your best to resist the temptation of hugging this fluffy ball. Maybe we have another idea?");
            next;
            break;
        case 3:
            poohNear();
            next;
            break;
        case 4:
            poohFood();
            .@cont = true;
            break;
        case 5:
            mesc l("You leave the teddy alone with its pot.");
            .@cont = true;
            break;
        default:
            mesc "FIXME", 1;
            next;
            break;
        }
    } while (!.@cont);
    close;

function poohNear {
    mesc l("You take off your bag and put it on the floor, as you lower yourself and try to slowly get close to it.");
    next;
    mesc l("But the teddy, noticing your movement, starts getting farther from you. It doesn't seems scared, but it doesn't allows you to approach either.");
    next;
    if (countitem(Honey) || countitem(BeeStinger)) {
        poohSmell();
        return;
    }
    mesc l("Insisting will do us no good, maybe we should try to gain its trust first?");
    return;
}

function poohFood {
    mesc l("Maybe the teddy is hungry? What could we offer to it?");
    .@honey = false;
    do
    {
        mes "";
        mes "##B" + l("Drag and drop a healing item from your inventory.") + "##b";

        .@id = requestitem();

        // If ID is invalid, there's not enough items, it is bound, etc;
        if (.@id < 1) return;
        if (.@id < 1 || countitem(.@id) < 1 || checkbound(.@id) || getiteminfo(.@id, ITEMINFO_TYPE) != IT_HEALING) {
            if (checkbound(.@id)) {
                mesc l("You cannot part with this item!");
            } else if (.@id == DeathPotion || .@id == NymphPoison) {
                mesc l("...Are you crazy?");
            } else if (.@id == MagicApple || .@id == MysteriousFruit) {
                mesc l("This is too precious to offer, I think.");
            } else if (getiteminfo(.@id, ITEMINFO_TYPE) != IT_HEALING) {
                mesc l("I'm not sure if that's edible. For teddies, at least.");
            } else {
                mesc l("You give up.");
                mesc l("Maybe it is not hungry, after all.");
                return;
            }
            next;
            continue;
        }

        // Bad healing items
        if (.@id == Beer ||
            .@id == RedPlushWine ||
            .@id == DwarvenSake ||
            .@id == CrazyRum ||
            .@id == WhiskeyAle) {
            mesc l("Offering alcohol to a teddy seems like a ugly thing to do. We should think on something else.");
            next;
            continue;
        } else if (.@id == ElixirOfLife) {
            mesc l("...I don't think the teddy is sick, so there's no point giving it an Elixir.");
            next;
            continue;
        } else if (.@id == BottleOfSewerWater) {
            mesc l("...I refuse to comment on this ridiculous idea.");
            next;
            continue;
        } else if (.@id == Honey) {
            .@honey = true;
            break;
        }

        mesc l("Really give your %s to the teddy?", getitemlink(.@id)), 1;
        mesc l("The item will be lost forever.");
        if (askyesno() == ASK_YES) {
            mes "";
            mesc l("The teddy stares at it but does not approach. Maybe it is not hungry, or it doesn't want the food you offered.");
            // Item is NOT deleted because it did not approach
            next;
        }
    } while (!.@honey);
    // You're offering HONEY! But we must check if you CAN do this
    if (getq(HurnscaldQuest_TeddyBear) < 2) {
        percentheal -2, 0;
        SC_Bonus(15, SC_POISON, 1);
        getmapxy(.@m$, .@x, .@y, 0);
        .@u = monster(.@m$, .@x, .@y, strmobinfo(1, Bee), Bee, 1);
        unitattack(.@u, getcharid(3));
        closeclientdialog;
        dispbottom l("Suddenly, a bee appears and attacks you out of NOWHERE!");
        end; // TERM
    }
    // You can, so we go ahead.
    mesc l("Our plushy friend knows what is really sweet on this life. It likes honey a great deal, and by the way it looks you, it probably wants more.");
    next;
    mesc l("It is... SO CUTE! You won't refuse honey to a teddy, will you?");
    mesc l("How much honey will you give it? (You have: %d)", countitem(Honey)), 3;
    input .@honey, 0, countitem(Honey);
    if (.@honey < 1) return;
    delitem Honey, .@honey;
    setq2 HurnscaldQuest_TeddyBear, getq2(HurnscaldQuest_TeddyBear) + .@honey;
    getexp 0, .@honey * 14;
    mesc l("The teddy quickly takes the honey and fills the pot. It is really hungry!");
    if (getq2(HurnscaldQuest_TeddyBear) >= 27) {
        mesc l("...And at long last, the teddy's pot is full of honey.");
        mesc l("It seems happy, but it quickly goes to a dark honey after finishing.");
        next;
        mesc l("When it returns, it is holding a cute %s. Understanding it is a gift, you take it.", getitemlink(BearHat));
        mesc l("Doesn't it feel good to help?");
        getitem BearHat, 1;
        setq HurnscaldQuest_TeddyBear, 3;
        // Half of needed exp, but treasure map may require more level
        getexp 15750, 9200; // Job Exp: Lv 28 as reference
    }
    return;
}

function poohSmell {
    mesc l("But the teddy unexpectedly starts moving towards your bag, possibly attracted by some smell. You give up on approaching it, and starts watching it.");
    next;
    if (@stingpooh) {
        mesc l("However, it stops abruptly and quickly moves away from it.");
        mesc l("I guess it still remembers the sting on the snout. Ouch!");
        return;
    }
    mesc l("Without any ceremony or further ado, the teddy starts poking on your things.");
    next;
    if (countitem(BeeStinger)) {
        mesc l("However, it comes back crying! Poor teddy, it got attracted by a sweet smell but it found a %s instead!", getitemlink(BeeStinger));
        delitem BeeStinger, 1;
        @stingpooh = true;
        next;
        mesc l("Unsatisfied, it goes away from you and your bag. Such a shame, we almost had got its trust.");
        return;
    }
    mesc l("After turning it upside down, it finds something which makes it really happy: %s! And there's nothing a teddy likes more than honey.", getitemlink(Honey));
    next;
    mesc l("The teddy picks it and goes running to somewhere it can eat in peace. Now we know how we can please our little friend - with %s! How we didn't think on that before?!", getitemlink(Honey));
    delitem Honey, 1;
    compareandsetq HurnscaldQuest_TeddyBear, 1, 2;
    setq2 HurnscaldQuest_TeddyBear, getq2(HurnscaldQuest_TeddyBear) + 1;
    getexp 0, 14;
    return;
}

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;
}
