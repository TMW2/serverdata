// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 004-2-2: Canyon Cave mobs
004-2-2,50,84,14,5	monster	Old Snake	1199,2,35000,150000
004-2-2,0,0,0,0	monster	Cave Maggot	1027,15,35000,150000
004-2-2,56,55,4,26	monster	Scorpion	1071,6,35000,150000
004-2-2,36,79,4,2	monster	Maggot	1030,2,35000,150000
004-2-2,55,25,4,3	monster	Desert Maggot	1083,2,35000,150000
004-2-2,44,28,12,5	monster	Old Snake	1199,2,35000,150000
004-2-2,0,0,0,0	monster	Small Topaz Bif	1101,1,35000,150000
