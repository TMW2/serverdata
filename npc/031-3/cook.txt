// TMW-2 Script.
// Author:
//    Saulc
//    Jesusalva
// Notes:
//    Teaches cooking, explains seasoning minigame, expiration, etc.
//    Doesn't teach any recipe, though. Legit max level for cooking skill is 6.

031-3,35,55,0	script	Ashley Loisl	NPC_SAVIOR_F,{
    mesn;
    mesq l("Ho... Congratulations in making this far.");
    mesc l("Ashley raises an eyebrown at you.");
    if (getskilllv(TMW2_COOKING) > REBIRTH) close;
    next;
    mesn;
    mesq l("Even with my poor skills, I used to be considered a decent warrior, only because I mastered the Art of Cooking. I would be willing to teach you - but first, you need to wear a %s so we can begin.", getitemlink(ChefHat));
    if (!isequipped(ChefHat)) close;
    next;
    .@lv = getskilllv(TMW2_COOKING);
    .@price = 5000 + (25000 * .@lv);
    mesn;
    mesq l("As you're wearing one, I can acknowledge you as a %s being and tell you that I'll be charging you %s GP and that you need to have finished %d/%02d%% of the quests in this town.", get_race(), fnum(.@price), reputation("Aethyr"), .@lv*20);
    if (Zeny < .@price) close;
    if (reputation("Aethyr") < .@lv*20) close;
    next;
    mesn;
    mesq l("So, do you want to learn how to cook plates suitable for combat? Proper cuisine will actually help you in combat more than drinking potions.");
    if (askyesno() == ASK_NO) {
        mesn;
        mesq l("Hmpf, getting cold feet? Your loss, then.");
        close;
    }
    mes "";
    mesn;
    mesq l("Good, so the first thing you should have in mind is that cooked food is different from just \"food\". It will stick for longer and grant effects actually useful in combat, not just status conditions which expire when you die. Think on them like a special kind of equipment.");
    next;
    mesn;
    mesq l("The better your cooking skill, the longer the food last and the better the effects seasoning can draw out of the plate. Most chefs in the world forgot what \"cuisine for combat\" means, and as a result, the effects last only a few seconds or just replenish some health, barely remarkable at all, hah!");
    next;
    mesn;
    mesq l("Look and learn and ask if you have any questions. I'll also be adding some important notes to your %s if you own it.", getitemlink(JesusalvaGrimorium));
    .@lv = getskilllv(TMW2_COOKING);
    .@price = 5000 + (25000 * .@lv);
    Zeny -= .@price;
    if (!isequipped(ChefHat)) end; // Put here on purpose
    skill TMW2_COOKING, .@lv+1, 0;
    next;
    mesn;
    mesq l("And I'll teach you no recipes. However, there are recipes to be learnt in Candor, Halinarzo and Land of Fire, shall you find the chef and mention me.");
    if (TUTORIAL) {
        dnext;
        goto OnCookHelp;
    }
    close;

OnCookHelp:
    mesn;
    mesq l("The most important thing to keep in mind is that %s.", b(l("cooked food expire")));
    next;
    mesn;
    mesq l("So if you cook too much, most of it will go to waste. Only cook what you intend to eat.");
    next;
    mesn;
    mesq l("Coincidentally, the food you're eating %s but instead will expire at same time as it would if you hadn't eat it.", b(l("will not expire on death")));
    next;
    mesn;
    mesq l("You can only have one food active at a time, and eating another plate will delete the current one... Because you'll scarf it all down in one go.");
    next;
    mesn;
    mesq l("You can \"unequip\" a food - in other words, scarf it down - if you don't want its effects.");
    next;
    mesn;
    mesq l("You can't sell, trade or drop food on the ground. If you are not holding a similar plate in your hand already, then you'll be given the option to %s when finishing the plate.", b(l("season it")));
    next;
    mesn;
    mesq l("If you season too much or too little, it'll taste worse and cause maluses if you're eating it. But if you season correctly, it'll gain unexpected additional properties.");
    next;
    mesn;
    mesq l("You'll receive messages as if you had \"rented\" the food to keep you aware of when it is going to expire. This apply both for the food you are currently eating - shown in the equipment window - as for the food in your inventory.");
    next;
    mesn;
    mesq l("Eating the food most suitable for your battle style is the secret for having an edge over your opponents. Don't waste precious food while so many people starve at Halinarzo and it'll be a profitable passtime.");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADBOTTOM, AlchemistArmor);
    setunitdata(.@npcId, UDT_HEADMIDDLE, FurBoots);
    setunitdata(.@npcId, UDT_HEADTOP, ChefHat);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 8);
    setunitdata(.@npcId, UDT_HAIRCOLOR, any(3, 5, 6, 7, 11)); // TODO: hair 21?

    .sex = G_MALE;
    .distance = 5;
    end;

}

