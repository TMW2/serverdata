// TMW2 Script
// Author:
//    Crazyfefe
//    Jesusalva
// This warp is enabled after waiting for lua

003-2,28,41,0	script	#LeaveComplaints	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    if ((BaseLevel >= 40 && getq(General_Narrator) >= 2 && getq2(Q_DragonFarm) < gettimetick(2)) || is_gm())
        warp "003-2-1", 47, 34;
    else
        npctalk3 l("Complaints Depto. temporaly closed, come back later");
    if (getq2(Q_DragonFarm) > gettimetick(2))
        npctalk3 l("Time remaining: %s", FuzzyTime(getq2(Q_DragonFarm)));
    end;
}
