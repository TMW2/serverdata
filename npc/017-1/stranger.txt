// TMW2/LoF scripts.
// Authors:
//    var
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Special quest, can be done every three months, on days 18~25.
//    Months: February, May, August, November

017-1,44,37,0	script	Stranger#LoF	NPC_LOF_STRANGER,{
    if (!$@GM_OVERRIDE && (gettime(GETTIME_MONTH) % 3 != 2 || (gettime(GETTIME_DAYOFMONTH) < 18 && gettime(GETTIME_DAYOFMONTH) > 25))) {
        dispbottom l("An error with Stranger#LoF happened! Please report!!");
        disablenpc .name$;
        end;
    }

    .@q=getq(LoFQuest_Hitchhiker);
    if (.@q == 2) goto L_Complete;
    if (.@q == 1) goto L_Quizz;
    mesn;
    mesq l("Hello, hello! It's great to see you. Maybe you can help me with a little problem I have.");
    next;
    mesn;
    mesq l("I need some materials to repair my spacesh- ahm, it's not important why I need it."); // Translators: Spacesh(ip)
    next;
    mesn;
    mesq l("Anyway, I'd be really happy if you could give me @@ @@, @@ @@, @@ @@ and @@ @@.",
            100, getitemlink(Chagashroom),
            3, getitemlink(FluoPowder),
            1, getitemlink(TinIngot),
            1, getitemlink(TitaniumIngot));

    if (countitem(Chagashroom) < 100 ||
        countitem(FluoPowder) < 3 ||
        countitem(TinIngot) < 1 ||
        countitem(TitaniumIngot) < 1)
        close;
    next;
    mesn;
    mesq l("Do you have that for me?");
    if (askyesno() == ASK_NO)
        close;
    mes "";
    // We already checked, cheaters will have their inventory ripped apart and I don't care ~ Jesusalva
    delitem Chagashroom, 100;
    delitem FluoPowder, 3;
    delitem TinIngot, 1;
    delitem TitaniumIngot, 1;
    getexp (BaseLevel**2)*5, JobLevel*2;
    setq LoFQuest_Hitchhiker, 1;
    mesn;
    mesq l("Great! Thank you!");
    next;
    mesn;
    mesq l("I have a lot of work to do now. But before you leave...");
    goto L_Quizz;

L_Quizz:
    inventoryplace OldTowel, 1;
    mesn;
    mesq l("Did you knew May 25th it is a very special day - the Towel Day?");
    next;
    mesn;
    mesq l("To celebrate this I'll ask you some questions about my favourite book, The Hitchhiker's Guide to the Galaxy. If you can answer them, I'll give you something very useful.");
    next;
    mesn;
    mesq l("The answers might need to have whitespaces. DO NOT, BY ALL MEANS, insert two whitespaces instead of one.");
    mesq l("I'll get so mad at you that even if you are correct, I'll say otherwise. Also, mind special characters.");
    mesq l("If I don't know the answer in your language, try answering in English. That always works.");
    mesc l("Are you ready?"), 1;
    mesc l("There's no time limit on this riddle.");
    next;
    if (askyesno() == ASK_NO)
        close;
    goto L_Easy;

L_Easy:
    mes "";
    mesn;
    mesq l("The first one is easy.");
    next;
    mes "";
    if (0171_Stranger_BlackBox(EASY))
        goto L_Medium;
    else
        goto L_Fail;

L_Medium:
    mes "";
    mesn;
    mesq l("You're absolutely right. The next one will be more tricky.");
    next;
    mes "";
    if (0171_Stranger_BlackBox(MEDIUM))
        goto L_Hard;
    else
        goto L_Fail;

L_Hard:
    mes "";
    mesn;
    mesq l("Not bad, not bad. Let's see if you can answer an even more difficult one.");
    next;
    mes "";
    if (0171_Stranger_BlackBox(HARD))
        goto L_Reward;
    else
        goto L_Fail;


// Failure
L_Fail:
    mes "";
    mesn;
    if (rand2(1,2) == 1) {
        mesq l("Sorry, but that's not right.");
        next;
        mesn;
        mesq l("Come try again, some other time.");
    } else {
        mesq l("Nah, that's wrong. Come back if you want to try again.");
    }
    mesc l("If you are sure your answer is correct, please contact Jesusalva for bug fixing.");
    close;

// Reward
L_Reward:
    mes "";
    mesn;
    mesq l("Unbelievable! You're really a wise person.");
    next;
    mesn;
    mesq l("One last question, but this shouldn't be a problem for you.");
    next;
    mesn;
    mesq l("What is the most important item for every hitchhiker to have?");
    input .@answer$;
    if (strip(strtolower(.@answer$)) != "towel" && strip(strtolower(.@answer$)) != l("towel"))
        goto L_Fail;
    mes "";
    mesn;
    mesq l("There you are! A towel is really the most important item for a hitchhiker to have.");

    getitem OldTowel, 1;
    // If you complete the quest in May, you get 50% more experience
    if (gettime(GETTIME_MONTH) == MAY)
        getexp JobLevel*BaseLevel*3/2, BaseLevel*3;
    else
        getexp JobLevel*BaseLevel, BaseLevel*2;
    setq LoFQuest_Hitchhiker, 2;
    next;
    mesn;
    mesq l("It really comes in handy that I have my towel with me. A towel is about the most massively useful thing an inte- ahm, a person can have.");
    close;

// TODO: Enable another quest after main quest has been completed
// TODO: Completing sub quest on May shall give better bonus (gettime(GETTIME_MONTH) == MAY)
// TODO: Keep your Towel well guarded; It can only be obtained once and is required in further quests.
L_Complete:
    mesn;
    mesq l("I have a lot of work to do now.");
    if (rand2(3) == 2) {
        next;
        mesn;
        mesq l("Annoying Paxel, for example %%o");
    }
    close;

// Proccessing Core
OnInit:
    .sex=G_OTHER;
    .distance=5;
OnClock0000:
    if (gettime(GETTIME_MONTH) % 3 == 2) {
        if (gettime(GETTIME_DAYOFMONTH) >= 18 && gettime(GETTIME_DAYOFMONTH) <= 25) {
            enablenpc .name$;
            end;
        }
    }
    disablenpc .name$;
    end;

}

