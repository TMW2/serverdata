// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Pachua's Cousin. On LoF/TMWA he also crafted Iron Powder, Sulfur Powder, and Yellow Powder (for magic)
//    IMPORTANT @Saulc : DO NOT GIVE ANY EXPERIENCE ON THIS NPC.
//    This is sort of a bet, you know. “Go hunt Mountain Snakes”, or bet some precious
//    crafting material on this NPC. Two choices. The easy way is RISKY, not REWARDING.
//    The refund is the exact fair price, retrieved from database. The only "loss" here
//    is that you would rather do something else with Leather Patch but instead, you "sold" it.
//    If you sell items you don't get experience, so you shouldn't get any exp here either.
//
//    This NPC could as well do Jeans Shorts, but I'm too lazy to calculate formula now.
//    The price is Cave Snake Skin in proportion to Jean Shorts rarity, and cost in proportion
//    to sell price difference.

017-1,155,162,0	script	Nowhere Man	NPC_KAYLO,{
    mesn;
    mesq l("Welcome! I come from nowhere, and my family hunt Snakes. We also make fine leather items from their skin!");
    next;
    mesn;
    mesq l("Next time an annoying snake refuses to drop their precious leather drops, come talk to us!");
    // 160 * 1 = 160 GP vs 500 GP (340 GP)
    mesq l("And if you bring me 1 @@ and 440 GP, I can make a @@ for you.", getitemlink(SnakeSkin), getitemlink(LeatherPatch));
    mesq l("Or I could recover the @@ for 10 @@ and the modest fee of 4400 GP.", getitemlink(SnakeSkin), getitemlink(LeatherPatch));
    next;
    select
        l("Nice to know. I'll come to you when random numbers try to kill me."),
        rif(countitem(SnakeSkin) >= 1 && Zeny >= 440, l("I want Leather Patch")),
        rif(countitem(LeatherPatch) >= 10 && Zeny >= 4400, l("I want Snake Skin"));

    switch (@menu) {
        case 2:
            inventoryplace LeatherPatch, 1;
            delitem SnakeSkin, 1;
            Zeny=Zeny-440;
            getitem LeatherPatch, 1;
            mesn;
            mesq l("Many thanks!");
            break;
        case 3:
            inventoryplace SnakeSkin, 1;
            delitem LeatherPatch, 10;
            Zeny=Zeny-4400;
            getitem SnakeSkin, 1;
            mesn;
            mesq l("Many thanks!");
            break;
    }
    close;

OnInit:
    .sex=G_MALE;
    .distance=5;
    end;
}



017-1,174,169,0	script	Nowhere Jeane	NPC_SAMANTHA,{
    mesn;
    // 95 * 2 = 190 GP vs 400 GP (210 GP)
    mesq l("If you bring me 2 @@ and 210 GP, I can make a @@ for you.", getitemlink(CaveSnakeSkin), getitemlink(JeansShorts));
    mesq l("Or I can swap the @@ with a @@ - for only 2100 GP.", getitemlink(JeansShorts), getitemlink(CaveSnakeSkin));
    next;
    select
        l("Nice to know. I'll come to you when random numbers try to kill me."),
        rif(countitem(CaveSnakeSkin) >= 2 && Zeny >= 210, l("I want it.")),
        rif(countitem(JeansShorts) && Zeny >= 2100, l("I don't care for equipment, gimme a snake skin."));

    switch (@menu) {
        case 2:
            inventoryplace JeansShorts, 1;
            delitem CaveSnakeSkin, 2;
            Zeny=Zeny-210;
            getitem JeansShorts, 1;
            mesn;
            mesq l("Many thanks!");
            break;
        case 3:
            inventoryplace CaveSnakeSkin, 1;
            delitem JeansShorts, 1;
            Zeny=Zeny-2100;
            getitem CaveSnakeSkin, 1;
            mesn;
            mesq l("Many thanks!");
            break;
    }
    close;


OnInit:
    .sex=G_FEMALE;
    .distance=5;
    end;

}


017-1,143,161,0	script	Nowhere Gluvine	NPC_SERENA,{
    mesn;
    // 320 * 6 = 1920 GP vs 450 GP (-)
    mesq l("If you bring me 6 @@ I can make a @@ for you.", getitemlink(MountainSnakeSkin), getitemlink(LeatherGloves));
    mesq l("Or I can swap the @@ with a @@ - for free.", getitemlink(LeatherGloves), getitemlink(MountainSnakeSkin));
    next;
    select
        l("Nice to know. I'll come to you when random numbers try to kill me."),
        rif(countitem(MountainSnakeSkin) >= 6, l("I want it.")),
        rif(countitem(LeatherGloves), l("I don't care for shorts, gimme a snake skin."));

    switch (@menu) {
        case 2:
            inventoryplace LeatherGloves, 1;
            delitem MountainSnakeSkin, 6;
            getitem LeatherGloves, 1;
            mesn;
            mesq l("Many thanks!");
            break;
        case 3:
            inventoryplace MountainSnakeSkin, 1;
            delitem LeatherGloves, 6;
            getitem MountainSnakeSkin, 1;
            mesn;
            mesq l("Many thanks!");
            break;
    }
    close;


OnInit:
    .sex=G_FEMALE;
    .distance=5;
    end;

}

