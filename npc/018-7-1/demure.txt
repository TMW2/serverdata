// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Demure is the Queen of Dragons


018-7-1,23,20,0	script	Demure, Queen of Dragons	NPC_DEMURE,{
    mesn;
    mesq l("Greetings mortal; I am Demure, Queen of Dragons.");
    if (BaseLevel < 40) close;
    next;
    mesn strcharinfo(0);
    select
        l("Thanks. It is my pleasure."),
        rif($DEMUR_HOLDER$ == "", l("I've heard rumors about a legendary axe.")),
        l("I want to challenge you.");
    mes "";
    if (@menu == 1) { closeclientdialog; close;}
    if (@menu == 2) {
        mesn;
        mesq l("The %s, yes. I have it. It is a legendary weapon, only one of it exist on the world.", getitemlink(DemureAxe));
        next;
        if ($GAME_STORYLINE < 3) {
            mesn;
            mesq l("But it is still too early to give it away to a %s children.", get_race());
            close;
        }
        mesn;
        mesq l("I'll give it to someone who challenge me and best me in an impressive manner. If it was easy, someone would already have claimed it.");
        mesn;
        mesq l("So, will you challenge me?");
        next;
        if (askyesno() == ASK_NO) close;
    }
    mesc l("Mode?");
    menuint
        l("Solo"), MODE_SOLO,
        rif(getcharid(1), l("Party")), MODE_PARTY;
    .@mode = @menuret;
    if (.@mode == MODE_PARTY) {
        if (getcharid(1) < 1)
            kick(getcharid(3), 3);
        if (strcharinfo(0) != getpartyleader(getcharid(1))) {
            mesn;
            mesq l("Hold your horses, %s. You are not the party leader.", strcharinfo(0));
            close;
        }
    }
    mes "";
    mesc l("Difficulty?");
    menuint
        l("I want to challenge the Yetifly."), 1,
        l("I want to challenge you."), 2,
        l("I want to challenge you both."), 3;
    .@mode2 = @menuret;
    mes "";
    mesc l("Monster Density and Boss HP?");
    menuint
        l("Meager."), 1,
        l("Normal."), 2,
        l("Doubled."), 4,
        l("Hunter."), 8,
        l("Ultimate"), 16;
    .@mode3 = @menuret;
    mes "";
    if (.@mode == MODE_SOLO) {
    	.@mapn$="demu@"+getcharid(0);
    	.@inst = instance_create("Demured "+getcharid(0), getcharid(3), IOT_CHAR);
    } else {
    	.@mapn$="demu@"+getcharid(1);
    	.@inst = instance_create("Demured "+getcharid(1), getcharid(1), IOT_PARTY);
    }
    instance_attachmap("018-7-1", .@inst, false, .@mapn$);
    // Instance lasts 10 minutes + 15s grace time
	instance_set_timeout(615, 615, .@inst);
	instance_init(.@inst);

    // Create walls
    setcells .@mapn$, 75, 66, 75, 67, 1, .@mapn$+"A";
    setcells .@mapn$, 85, 84, 86, 84, 1, .@mapn$+"B";

    // Adjust the proper monster count to be spawned
    if (.@mode == MODE_PARTY) {
        getpartymember(getcharid(1));
	    .@count = max($@partymembercount, 1);
    } else {
        .@count = 1;
    }

    // Setup the boss monsters. Non-exclusive (bitwise)
    if (.@mode2 & 1) {
        .@m=monster(.@mapn$, 85, 66, strmobinfo(1, Yetifly), Yetifly, 1, strnpcinfo(NPC_NAME_UNIQUE)+"::OnYeti");
        .@p=getunitdata(.@m, UDT_MAXHP);
        setunitdata(.@m, UDT_MAXHP, .@p*.@count*.@mode3);
        setunitdata(.@m, UDT_HP,    .@p*.@count*.@mode3);
    }
    if (.@mode2 & 2) {
        .@m=monster(.@mapn$, 87, 57, strmobinfo(1, DemureFirstForm), DemureFirstForm, 1, strnpcinfo(NPC_NAME_UNIQUE)+"::OnDem1");
        .@p=getunitdata(.@m, UDT_MAXHP);
        setunitdata(.@m, UDT_MAXHP, .@p*.@count*.@mode3);
        setunitdata(.@m, UDT_HP,    .@p*.@count*.@mode3);
    }

    // Setup the reinforcements, in appropriate number and mode
    for (.@i = 0; .@i < (.@count*.@mode3); .@i++) {
        .@mob = any(EarthFairy, FireFairy, WaterFairy, WindFairy, PoisonFairy);
        .@m = areamonster(.@mapn$, 76, 47, 95, 83, strmobinfo(1, .@mob), .@mob, 1);
        .@p = getunitdata(.@m, UDT_MODE);
        setunitdata(.@m, UDT_MODE, (.@p | MD_AGGRESSIVE));
    }
    for (.@i = 0; .@i < .@count; .@i++) {
        .@mob = any(EliteDuck, Archant, Wolvern, BlueSlimeMother);
        areamonster .@mapn$, 76, 47, 95, 83, strmobinfo(1, .@mob), .@mob, any(1,2);
    }

    // Warp you and/or your party
    if (.@mode == MODE_SOLO)
        warp(.@mapn$, 85, 70);
    else
        warpparty(.@mapn$, 85, 70, getcharid(1), "018-7-1", true);

    // Begin tracking time and metadata (blackbox needs this)
    @elapsed = 0;
    @d_mode1 = .@mode;
    @d_mode2 = .@mode2;
    @d_mode3 = .@mode3;
    @d_count = .@count;
    addtimer 5000, strnpcinfo(NPC_NAME_UNIQUE)+"::OnBeet";
    close;

OnBeet:
    if (!compare(getmap(), "demu@") && !compare(getmap(), "018-7-1")) end;
    @elapsed += 5;
    if (@elapsed % 60 < 5)
        mapannounce(getmap(), l("Time left: %d minutes", 10-(@elapsed/60)), bc_map | bc_pc);
    if (@elapsed > 600)
        end;
    addtimer2 5000, strnpcinfo(NPC_NAME_UNIQUE)+"::OnBeet";
    end;

// Check victory conditions
OnYeti:
    fix_mobkill(Yetifly);
OnDem2:
    if (!playerattached()) {
        consolebug "ERROR - PLAYER NOT ATTACHED, INVALID KILL, CANNOT RESOLVE MAP.";
        end;
    }
    .@m$ = getmap();
    .@left=0;
    .@left+=mobcount(.@m$, strnpcinfo(NPC_NAME_UNIQUE)+"::OnYeti");
    .@left+=mobcount(.@m$, strnpcinfo(NPC_NAME_UNIQUE)+"::OnDem1");
    .@left+=mobcount(.@m$, strnpcinfo(NPC_NAME_UNIQUE)+"::OnDem2");
    // You win!
    if (!.@left) {
        mapannounce(getmap(), "CONGRATULATIONS - YOU WIN", bc_map | bc_pc);
        Zeny += rand2(500, 1500)*max(1, @d_mode2);
        // FIXME: Add a proper reward
        X24Event(X24_DEMURE);
        maptimer2(.@m$, 1000, strnpcinfo(NPC_NAME_UNIQUE)+"::OnBye");
        01871_Demure_BlackBox();
    }
    end;

OnBye:
    Zeny += rand2(100, 300)*max(1, @d_mode2);
    warp "018-7-1", 23, 22;
    deltimer strnpcinfo(NPC_NAME_UNIQUE)+"::OnBeet";
    specialeffect(FX_FANFARE, SELF, getcharid(3));
    end;

// Summon second form (give 5 seconds to players to move)
// FIXME: @d_* might not be set
OnDem1:
    if (!playerattached()) {
        consolebug "ERROR - PLAYER NOT ATTACHED, INVALID KILL, CANNOT RESOLVE MAP.";
        end;
    }
    getmapxy(.@m$, .@x, .@y, 0);
    .@n$ = "Demure, Queen of Dragons : ";
    mapannounce(getmap(), .@n$+"You think you've won...?", bc_map | bc_pc);
    sleep2(1000);
    mapannounce(getmap(), .@n$+"That would be lame; After all, did you forget that...", bc_map | bc_pc);
    sleep2(2000);
    mapannounce(getmap(), .@n$+"I *am* the Queen of Dragons??", bc_map | bc_pc);
    sleep2(1000);
    // Read data, summon dragons...
    for (.@i = 0; .@i < max(1, @d_count*@d_mode3/2); .@i++) {
        .@mob = any(EarthFairy, FireFairy, WaterFairy, WindFairy, GreenDragon);
        .@m = areamonster(.@m$, .@x-10, .@y-10, .@x+10, .@y+10, strmobinfo(1, .@mob), .@mob, 1);
        .@p = getunitdata(.@m, UDT_MODE);
        setunitdata(.@m, UDT_MODE, (.@p | MD_AGGRESSIVE));
    }
    areamonster(.@m$, .@x-5, .@y-5, .@x+5, .@y+5, strmobinfo(1, GreenDragon), GreenDragon, 1);

    // And finally, change form (ressurect)
    .@m=monster(.@m$, 87, 57, strmobinfo(1, DemureSecondForm), DemureSecondForm, 1, strnpcinfo(NPC_NAME_UNIQUE)+"::OnDem2");
    if (@d_count) {
        .@p=getunitdata(.@m, UDT_MAXHP);
        // If player is solo'ing, put more ATK and less HP
        .@hp=.@p*@d_count*@d_mode3;
        if (@d_mode1 == MODE_SOLO) {
            .@hp = .@hp * 3 / 5;
            // Raise ASPD based on difficulty
            setunitdata(.@m, UDT_ADELAY,
                        getunitdata(.@m, UDT_ADELAY) - (@d_mode3 * 25));
        }
        setunitdata(.@m, UDT_MAXHP, .@hp);
        setunitdata(.@m, UDT_HP,    .@hp);
    }
    mapannounce(getmap(), .@n$+"I'll leave a 2x2 black square where you used to be!", bc_map | bc_pc);
    end;

OnInit:
    .distance = 4;
    .sex = G_FEMALE;
    npcsit;
    end;
}

