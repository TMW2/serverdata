// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    015-8 Sagratha Cave Configuration File

015-8	mapflag	zone	MMO
015-8-1	mapflag	zone	MMO

015-8,99,179,0	script	#Exit0158	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    .@q=getq(HurnscaldQuest_Sagratha);
    .@q2=getq2(HurnscaldQuest_Sagratha);
    // Cheater Detected
    if (!MAGIC_LVL || .@q < 3) {
        setq HurnscaldQuest_Sagratha, 0, 0, 0;
        sc_end SC_CASH_PLUSEXP;
        sc_end SC_OVERLAPEXPUP;
        sc_start SC_OVERLAPEXPUP, 300000, -20;
        warp "Save", 0, 0;
        return false;
    }

	mesc l("Return to Sagratha's House?");
	if (askyesno() == ASK_YES) {
		.@mapn$=SaggyInstCheck();
	    warp .@mapn$, 33, 35;
		changemusic .@mapn$, "eric_matyas_ghouls.ogg";
	}
	closeclientdialog;
	close;
}

