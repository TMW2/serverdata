// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 010-2: Desert Mountains conf

010-2,209,70,0	script	#010-2_209_70	NPC_CHEST,{
	TreasureBox();
	specialeffect(.dir == 0 ? 24 : 25, AREA, getnpcid()); // closed ? opening : closing
	close;
OnInit:
	.distance=2;
	end;
}
