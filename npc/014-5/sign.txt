// TMW2 Script.
// Author:
//    Jesusalva
// Description:
//    Random Sign nobody bothers reading.

014-5,179,29,0	script	Sign#014517929	NPC_SWORDS_SIGN,{
    mesc l("Welcome to the Greenlands, the place cursed by the Monster King to be ice forever, and now known as Icelands.");
    next;
    mesc l("It's advised to come ready for the cold, the hungry beasts, and other dangers snow may bring.");
    if (getgmlevel()) {
        mesc l("Blame @@, missing map between 014-5 and 019-1, solely to make trip longer.", any("jesusalva", "saulc", "lawncable", "mishana", "dragonstar", "4144", "everyone", "developers", "the Monster King", strcharinfo(0))), 1;
    }
    /*
    next;
    mesc l("WARNING: Path temporaly closed due strong snowstorm."), 3;
    */
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 3;
    end;
}
