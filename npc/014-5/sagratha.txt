// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Sagratha's House Door
//
// getq(HurnscaldQuest_Sagratha)
//  Field 1
//      0 - Sagratha is home and annyoed
//      1 - Wyara sent player saggy way
//      2 - Rumor Confirmed, check again
//  Field 2
//      RESERVED - INSTANCE ID
//  Field 3
//      During stage 1 and 2: Holds if player looked everywhere
//      During stage 3 and 4: Control puzzles
//      During stage 5 boss fight

// Check instances and rebuild if needed
// Returns map name
// SaggyInstCheck( {house=true} )
function	script	SaggyInstCheck	{
	.@house=getarg(0, true);

	.@q2=getq2(HurnscaldQuest_Sagratha);
	// Map name limit: 4 chars (sgt1)
	.@mapn$="sgt1@"+getcharid(0);
	.@map2$="sgt2@"+getcharid(0);
	if (!(isinstance(.@q2) && .@q2 != 0)) {
		.@inst = instance_create("Sagratha House "+getcharid(0), getcharid(3), IOT_CHAR);
	    instance_attachmap("014-5-1", .@inst, false, .@mapn$);
	    //instance_attachmap("015-8", .@inst, false, .@mapn$);
	    instance_attachmap("015-8-1", .@inst, false, .@map2$);
	    // Instance lasts one hour
		instance_set_timeout(3600, 3600, .@inst);
		instance_init(.@inst);
	    setq2 HurnscaldQuest_Sagratha, .@inst;
	}

    // It broke
    if (getmapinfo(MAPINFO_SIZE_X, .@mapn$) <= 0) {
        setq2 HurnscaldQuest_Sagratha, 0;
        // Infinite Loop?
        return callfunc("SaggyInstCheck", .@house);
    }

	if (.@house) {
		return .@mapn$;
	} else {
		return .@map2$;
	}
}

014-5,122,137,0	script	Sagratha Door	NPC_HIDDEN,0,0,{
    end;

L_Magicless:
    npctalk3 l("A light magic barrier prevents you from entering.");
    dispbottom l("If I only knew some magic...");
    end;

L_Lockpick:
    mesn;
    mesc l("The door is locked!");
    select
        l("Knock on the door?"),
        l("Attempt to lockpick?"),
        l("Leave it alone?");
    mes "";

    if (@menu == 1) {
        /*
        if (getq(HurnscaldQuest_InjuriedMouboo) == 2)) {
            mesn strcharinfo(0);
            mesc l("Nobody answers.");
            mes "";
        }
        */
        mesn l("Sagratha");
        mesq l("No, I don't need monster repellent nor anything! Go away!");
        close;
    } else if (@menu == 2) {
        if (LockPicking(4, 5)) {
            getexp BaseLevel*4, 5;
            mesn;
            mesc l("*click*");
            next;
            mesn l("Sagratha");
            mesq l("Who is there? Are they trying to break my lock again?!");
            next;
            select
                l("Run away?"),
                l("Stay there?");
            mes "";

            if (@menu == 2) {
                mesn l("Sagratha");
                mesq l("Oh my Jesusalva - You broke my lock! Why?!");
                next;
                select
                    l("I need to talk with you!"),
                    l("No reason.");
                mes "";
                if (@menu == 1) {
                    mesn l("Sagratha");
                    // I don't take strangers as students.
                    mesq l("No, I don't teach magic to strangers. No, I'm not interested in @@ affairs.", get_race());
                    next;
                    mesn l("Sagratha");
                    mesq l("Yes, I know when next Alliance's Council meeting will be. No, I don't need anything.");
                    next;
                    mesn l("Sagratha");
                    mesq l("Now begone.");
                } else {
                    mesn l("Sagratha");
                    mesq l("What?! Do my house look like the place to you pratice your thief skills?!");
                    mesq col(l("DIE!"), 1);
                    specialeffect(312, SELF, getcharid(3));
                    percentheal -40, -100;
                }
            }

        } else {
            mesn l("Sagratha");
            mesq l("Who is there? Are they trying to break my lock again?!");
            mesq col(l("DIE!"), 1);
            specialeffect(312, SELF, getcharid(3));
            percentheal -40, -100;
        }
    }
    close;

L_Open:
    mesn;
    mesc l("The door is locked!");
    select
        l("Knock on the door?"),
        l("Attempt to lockpick?"),
        l("Say that Mouboos are Cute?"),
        l("Leave it alone?");
    mes "";
    if (@menu == 4)
        close;
    if (@menu == 3) {
        mesc l("Surprisingly, nothing happens.");
        next;
        mesc l("A close inspection suggests the lock was busted. You approach to inspect.");
        next;
    }
    mesc l("*CREAK*");
    mesc l("The pressure you did made the door burst open. It is unusually quiet inside...");
    next;
    //enable_items();
    //mesc l("WARNING: Save your game now."), 3;
    //mesc l("WARNING: Change your equipment now."), 3;
    //next;
    //disable_items();
    /*
	if (has_instance("014-5-1") == "") {
		.@inst = instance_create("Sagratha House", getcharid(3), IOT_CHAR);
        instance_attachmap("014-5-1", .@inst);
        // 20 minutes (1200s) inside, or 5 minutes (300s) outside
		instance_set_timeout(1200, 30, .@inst);
		instance_init(.@inst);
    }
    warp "014-5-1", 33, 44;
    //addmapmask instance_mapname("014-5-1"), 1;
    changemusic "014-5-1", "eric_matyas_ghouls.ogg";
    */
	.@mapn$=SaggyInstCheck();
    warp .@mapn$, 33, 44;
    changemusic .@mapn$, "eric_matyas_ghouls.ogg";
    closeclientdialog;
    close;

L_Welcome:
    warp "014-5-1", 33, 44;
    dispbottom l("Mouboos are cute");
    end;

OnTouch:
    .@q=getq(HurnscaldQuest_Sagratha);
    if (!MAGIC_LVL) goto L_Magicless;
    if (.@q >= 1 && .@q < 6) goto L_Open;
    if (.@q >= 6) goto L_Welcome;

    // No special condition
    goto L_Lockpick;
    end;

}
