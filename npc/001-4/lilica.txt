// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Lilica is TMW-BR Scary Scary Easter Bunny and will help your trading stuff.
// Data layout:
//    SQuest_Easter
//      (year of eggshell hat, strange coin limit, limited shop purchases)
//      Strage Coin Limit: 500, or last year + 100, the smaller of both

001-4,139,151,0	script	Lilica#easter	NPC_EASTER,{
    mesn;
    mesq lg("Ah, traveller! I am Lilica the Scary Bunny! I exchange many @@ and @@ for neat rewards!","Ah, traveller! I am Lilica the Scary Bunny! I exchange many @@ and @@ for neat rewards!", getitemlink(SilverEasteregg), getitemlink(GoldenEasteregg));
    next;
    mesn;
    mesq l("Golden Eggs are used for the grand collector prize. Ah, I love Easter! I loooooooove it!");
    mesc l("Note: Golden and Silver Eggs are deleted after the next event end.");

    menu
        l("Scary..........."), -,
        l("Trade Silver Eggs"), L_Silver,
        l("Trade Golden Eggs"), L_Golden,
        rif(getq(SQuest_Easter) == gettime(7), l("Limited Shop")), L_Limited,
        rif(getq(SQuest_Easter) != gettime(7), l("I want an Eggshell Hat!")), L_Quest,
        l("View LeaderBoard"), L_Leader;
    close;

L_Silver:
    openshop "#eastershop1";
    closedialog;
    close;

L_Golden:
    mes l("You can still purchase %d %s.", getq2(SQuest_Easter),
                                           getitemlink(StrangeCoin));
    next;
    openshop "#eastershop2";
    closedialog;
    close;

L_Leader:
    mesc l("Leaderboard is refresh daily at 1 AM!");
	mes("1."+$@easter_name$[0]+" ("+$@easter_value[0]+")");
	mes("2."+$@easter_name$[1]+" ("+$@easter_value[1]+")");
	mes("3."+$@easter_name$[2]+" ("+$@easter_value[2]+")");
	mes("4."+$@easter_name$[3]+" ("+$@easter_value[3]+")");
	mes("5."+$@easter_name$[4]+" ("+$@easter_value[4]+")");
	mes("6."+$@easter_name$[5]+" ("+$@easter_value[5]+")");
	mes("7."+$@easter_name$[6]+" ("+$@easter_value[6]+")");
	mes("8."+$@easter_name$[7]+" ("+$@easter_value[7]+")");
	mes("9."+$@easter_name$[8]+" ("+$@easter_value[8]+")");
	mes("10."+$@easter_name$[9]+" ("+$@easter_value[9]+")");
    close;

L_Quest:
    setarray .@Seasonal, EggshellHat, VioletEggshellHat, YellowEggshellHat, RedEggshellHat, GreenEggshellHat, CyanEggshellHat, OrangeEggshellHat, BlueEggshellHat;
    .@Hat=.@Seasonal[gettime(7)%8]; // Magically choose the hat from the array
    mesn;
    mesq l("Good choice! This year we're having a @@!", getitemlink(.@Hat));
    next;
    mesn;
    mesq l("As usual, you can get only one hat yearly, for the symbolic amount of 40 @@ and 10 @@!", getitemlink(GoldenEasteregg), getitemlink(SilverEasteregg));
    next;
    menu
        l("Maybe later."), -,
        rif(countitem(GoldenEasteregg) >= 40 && countitem(SilverEasteregg) >= 10, l("Deal.")), L_QuestDone;
    close;

L_QuestDone:
    inventoryplace .@Hat, 1;
    delitem GoldenEasteregg, 40;
    delitem SilverEasteregg, 10;
    if (rand2(10000) < 100) goto L_Unlucky;
    setq SQuest_Easter, gettime(7), min(500, getq2(SQuest_Easter)+100), 0;
    npctalk3 l("Strange Coins stock on shops was restored!");
    getnameditem(.@Hat, strcharinfo(0));
    mesn;
    mesq l("Here you go! Happy easter! Bhop bhop!");
    close;

L_Unlucky:
    getitem GoldenGift, 1;
    mesn;
    mes l("\"Oh... Sorry, @@.", strcharinfo(0));
    mes l("But in accordance to an old %s, you were unlucky.", b(l("Community Decision")));
    mes l("This means ##BAll items were lost##b, and you need to collect EVERYTHING, again, to get the hat.");
    mes l("But, hm hm hm! I have a %s for you! It won't have what you wanted, but maybe you're lucky, after all?", getitemlink(GoldenGift));
    mes l("Better luck next time!\"");
    close;

// Leaderboard functions, and initialization
OnClock0100:
OnClock1300:
    if ($EVENT$ == "Easter") {
    	.@nb = query_sql("SELECT c.name, i.amount FROM `inventory` AS i, `char` AS c WHERE i.nameid=834 AND i.char_id=c.char_id ORDER BY i.amount DESC LIMIT 10", $@easter_name$, $@easter_value);
        initnpctimer;
    }
    end;

OnInit:
    .sex = G_OTHER;
    .distance = 5;

    if ($EVENT$ == "Easter")
        sEaster();
    end;

// Smoke Dragon Sequencers (every 2 hours in average)
OnClock0300:
OnClock0900:
OnClock1700:
    if ($EVENT$ != "Easter" || $GAME_STORYLINE < 5) end;
OnClock0500:
OnClock1100:
OnClock1900:
    if ($EVENT$ != "Easter" || $GAME_STORYLINE < 4) end;
OnClock1500:
OnClock2300:
    if ($EVENT$ != "Easter" || $GAME_STORYLINE < 3) end;
OnClock0700:
OnClock2100:
    if ($EVENT$ != "Easter" || $GAME_STORYLINE < 2) end;
    initnpctimer;
    end;

// Smoke Dragon Logic
OnTimer5000:
    mapannounce "001-4", "Lilica : Be careful - I feel dragons approaching!", bc_map;
    end;

OnSmoked:
    end;

OnTimer30000:
    // They spawn above the traps
    monster("001-4", 275, 204, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 260, 182, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 231, 173, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 199, 171, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 200, 144, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 247, 127, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 265, 135, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 271, 110, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 190,  30, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 163,  33, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 177,  49, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 164,  89, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 130,  84, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 126,  47, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 130,  32, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 107,  33, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4",  33, 141, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4",  82, 149, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4",  42, 118, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4",  24, 106, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4",  79, 163, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4",  42, 175, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4",  26, 184, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4",  32, 198, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 102, 275, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 128, 274, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 130, 256, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 128, 220, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 156, 212, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 166, 246, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 176, 270, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    monster("001-4", 160, 270, strmobinfo(1, SmokeDragon), SmokeDragon, 1, "Lilica#easter::OnSmoked");
    end;

OnTimer900000:
    mapannounce "001-4", "Lilica : I feel the dragons leaving!", bc_map;
    end;

OnTimer1000000:
    killmonster("001-4", "Lilica#easter::OnSmoked");
    stopnpctimer;
    end;

// Special shop functions
function EEX_check {
    .@tier = getarg(0); // Minimum reward level
    .@clas = getarg(1); // EEX code
    .@preq = getarg(2, 0); // Pre-requisite purchase

    if (!.@clas || .@clas == .@preq) return false; // Fail-safe

    .@s = getq3(SQuest_Easter);
    .@lv = (TOP3AVERAGELVL() / 25) + $GAME_STORYLINE;

    if (.@preq)
        return (!(.@s & .@clas) && .@lv >= .@tier && (.@s & .@preq));
    else
        return (!(.@s & .@clas) && .@lv >= .@tier);
}

function EEX_trade {
    .@clas = getarg(2); // Easter EXchange code (EEX)
    .@item = getarg(1); // Item ID
    .@pric = getarg(0); // Price in Golden Eggs

    if (!.@item || !.@clas) return; // Fail-safe
    if (countitem(GoldenEasteregg) < .@pric) {
        mesc l("You only have %d/%d %s, so the transaction was cancelled.", countitem(GoldenEasteregg), .@pric, getitemlink(GoldenEasteregg)), 1;
        close; // TERMINATE the script
    }

    inventoryplace .@item, 1; // may TERMINATE the script
    delitem GoldenEasteregg, .@pric;
    setq3 SQuest_Easter, getq3(SQuest_Easter) | .@clas;
    getitem .@item, 1;
    return;
}

L_Limited:
    mes l("You can only make each purchase only. Some purchases unlock other purchases.");
    mesc l("The limited shop will not be available once event ends."), 1;
    mesc l("Trade here will lower your ranking on Easter event. Be careful."), 1;
    mes l("Current golden eggs: %s", fnum(countitem(GoldenEasteregg)));
    menuint
        l("Nothing, thanks."), -1,
        rif(EEX_check(1, EEX_bronze),
            l("Bronze Gift (%d eggs)",   50)), EEX_bronze,
        rif(EEX_check(3, EEX_silver, EEX_bronze),
            l("Silver Gift (%d eggs)",  100)), EEX_silver,
        rif(EEX_check(5, EEX_golden, EEX_silver),
            l("Golden Gift (%d eggs)",  150)), EEX_golden,
        rif(EEX_check(7, EEX_prism, EEX_golden),
            l("Prism Gift (%d eggs)",   250)), EEX_prism,
        rif(EEX_check(9, EEX_supreme, EEX_prism),
            l("Supreme Gift (%d eggs)", 500)), EEX_supreme,

        rif(EEX_check(2,  EEX_gapple),
            l("Golden Apple (%d eggs)",    35)), EEX_gapple,
        rif(EEX_check(4,  EEX_napple, EEX_gapple),
            l("Manapple (%d eggs)",        75)), EEX_napple,
        rif(EEX_check(6,  EEX_elixir, EEX_napple),
            l("Elixir of Life (%d eggs)", 150)), EEX_elixir,
        rif(EEX_check(8, EEX_dapple, EEX_elixir),
            l("Divine Apple (%d eggs)",   300)), EEX_dapple,
        rif(EEX_check(10,  EEX_mapple, EEX_dapple),
            l("Magic Apple (%d eggs)",    300)), EEX_mapple,

        rif(EEX_check(0,  EEX_merc1),
            l("★ Mercenary (%d eggs)",  20)), EEX_merc1,
        rif(EEX_check(3,  EEX_merc2, EEX_merc1),
            l("★★ Mercenary (%d eggs)",  40)), EEX_merc2,
        rif(EEX_check(6,  EEX_merc3, EEX_merc2),
            l("★★★ Mercenary (%d eggs)",  60)), EEX_merc3,
        rif(EEX_check(9, EEX_merc4, EEX_merc3),
            l("★★★★ Mercenary (%d eggs)",  80)), EEX_merc4,
        rif(EEX_check(11,  EEX_merc5, EEX_merc4),
            l("★★★★★ Mercenary (%d eggs)", 100)), EEX_merc5,

        l("I have to go."), -1;

    mes "";
    switch (@menuret) {
    case EEX_bronze:  EEX_trade(  50, BronzeGift,  EEX_bronze); break;
    case EEX_silver:  EEX_trade( 100, SilverGift,  EEX_silver); break;
    case EEX_golden:  EEX_trade( 150, GoldenGift,  EEX_golden); break;
    case EEX_prism:   EEX_trade( 250, PrismGift,   EEX_prism); break;
    case EEX_supreme: EEX_trade( 500, SupremeGift, EEX_supreme); break;

    case EEX_gapple:  EEX_trade(  35, GoldenApple,  EEX_gapple); break;
    case EEX_napple:  EEX_trade(  75, Manapple,     EEX_napple); break;
    case EEX_elixir:  EEX_trade( 150, ElixirOfLife, EEX_elixir); break;
    case EEX_dapple:  EEX_trade( 300, DivineApple,  EEX_dapple); break;
    case EEX_mapple:  EEX_trade( 300, MagicApple,   EEX_mapple); break;

    case EEX_merc1:   EEX_trade(  20, MercBoxAA,    EEX_merc1); break;
    case EEX_merc2:   EEX_trade(  40, MercBoxBB,    EEX_merc2); break;
    case EEX_merc3:   EEX_trade(  60, MercBoxCC,    EEX_merc3); break;
    case EEX_merc4:   EEX_trade(  80, MercBoxDD,    EEX_merc4); break;
    case EEX_merc5:   EEX_trade( 100, MercBoxEE,    EEX_merc5); break;

    default: closeclientdialog; close;
    }
    mesc l("Enjoy your purchase!"), 3;
    next;
    goto L_Limited;
}

function	script	EasterCoinCheck	{
    for (.@i=0;.@i < getarraysize(@bought_nameid); .@i++) {

        if (debug || $@GM_OVERRIDE)
            debugmes("%dx %s", @bought_quantity[.@i], getitemname(@bought_nameid[.@i]));

        if (@bought_nameid[.@i] == StrangeCoin) {
            .@q2=getq2(SQuest_Easter)-@bought_quantity[.@i];
            if (.@q2 < 0) {
                dispbottom l("Attempted to buy %d/%d %s, operation cancelled.",
                             @bought_quantity[.@i], getq2(SQuest_Easter),
                             getitemlink(StrangeCoin));
                if (TUTORIAL)
                    dispbottom l("This quota is reset yearly, by completing %s's quest.", b("Lilica"));
                end;
            }
            setq2 SQuest_Easter, .@q2;
        }
    }
    return;
}

// Silver Easter Egg exchange.
001-4,139,151,0	trader	#eastershop1	NPC_HIDDEN,{
    end;

OnInit:
	tradertype(NST_CUSTOM);

	sellitem ChocolateMouboo,100;
	sellitem StrangeCoin,50;
	sellitem GoldenEasteregg,25;
	sellitem EasterEgg,5;
    end;

OnCountFunds:
	setcurrency(countitem(SilverEasteregg));
	end;

OnPayFunds:
	if( countitem(SilverEasteregg) < @price )
		end;
    // Strange Coins are different
    EasterCoinCheck();
    // Complete purchase
	delitem SilverEasteregg, @price;
	purchaseok();
	end;
}
// Golden Easter Egg exchange.
001-4,139,151,0	trader	#eastershop2	NPC_HIDDEN,{
    end;

OnInit:
	tradertype(NST_CUSTOM);
    setarray .@Seasonal, EggshellHat, VioletEggshellHat, YellowEggshellHat, RedEggshellHat, GreenEggshellHat, CyanEggshellHat, OrangeEggshellHat, BlueEggshellHat;
    .Hat=.@Seasonal[(gettime(7)+1)%8]; // Magically choose the hat from the array
    .PrevHat=.@Seasonal[(gettime(7)-1)%8]; // Magically choose the hat from the array

    // Seasonal item
	sellitem .Hat,200;
	sellitem .PrevHat,500;

    // Determine the current game shift
    // Range: 0 ~ 12 (Roughly)
    .@lv = (TOP3AVERAGELVL() / 25) + $GAME_STORYLINE;
    .@ol = max(1, 12-.@lv); // The opposite from the above function

    // Rare and not-so-rare Items
    if (.@lv >= 4)
    	sellitem MercBoxE,125*.@ol;
    if (.@lv >= 3)
    	sellitem MercBoxD,80*.@ol;
    if (.@lv >= 2)
    	sellitem MercBoxC,40*.@ol;
    if (.@lv >= 1)
    	sellitem Boots,37*.@ol;

    // These are better limited to a max amount of purchases
	sellitem BronzeGift,100;
	sellitem BunnyEars,50;
	sellitem StrangeCoin,10;
	sellitem SilverEasteregg,1;
    end;

OnCountFunds:
	setcurrency(countitem(GoldenEasteregg));
	end;

OnPayFunds:
	if( countitem(GoldenEasteregg) < @price )
		end;
    // Strange Coins are different
    EasterCoinCheck();
    // Complete purchase
	delitem GoldenEasteregg, @price;
	purchaseok();
	end;
}

