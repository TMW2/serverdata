// TMW2 Script
// Author:
//  Jesusalva, Saulc
// Description:
//  This GM NPC controls spawns and item drops on Aeros
// Monsters are sorted alphabetically, except bif

001-1,250,20,0	script	Mana Being#001-1	NPC_ALIGE_OUTSIDE_BARREL,{
    function spawner { // (memo, ID, amount)
        // First argument is a memorand for Script Writers, usually name. It must be present, but can have whatever value you want. (Unused)

        // [0] East [1] West [2] Full
        switch($@AEROS_SPWN) {
        case 1:
            areamonster("001-1", 20, 20, 140, 140, strmobinfo(1, getarg(1)), getarg(1), getarg(2), "Mana Being#001-1::OnAerosMobDeath");
            break;
        case 2:
            areamonster("001-1", 20, 20, 340, 160, strmobinfo(1, getarg(1)), getarg(1), getarg(2), "Mana Being#001-1::OnAerosMobDeath");
            break;
        default:
            areamonster("001-1", 171, 20, 340, 160, strmobinfo(1, getarg(1)), getarg(1), getarg(2), "Mana Being#001-1::OnAerosMobDeath");
            break;
        }
    }

    function mkitem { // ( ID{, Amount} )
        // [0] East [1] West [2] Full makeitem
        switch($@AEROS_SPWN) {
        case 1:
            for (.@i = 0; .@i < getarg(1,1); .@i++)
                makeitem(getarg(0), 1, "001-1", rand(20,140), rand(20,140));
            break;
        case 2:
            for (.@i = 0; .@i < getarg(1,1); .@i++)
                makeitem(getarg(0), 1, "001-1", rand(20,340), rand(20,160));
            break;
        default:
            for (.@i = 0; .@i < getarg(1,1); .@i++)
                makeitem(getarg(0), 1, "001-1", rand(171,320), rand(158,340));
            break;
        }
    }

    function buryitem { // ( ID{, Amount} )
        // [0] East [1] West [2] Full makeitem
        switch($@AEROS_SPWN) {
        case 1:
            shovel_scatter("001-1", 20, 20, 140, 140, getarg(1,1), getarg(0));
            break;
        case 2:
            shovel_scatter("001-1", 20, 20, 340, 160, getarg(1,1), getarg(0));
        default:
            shovel_scatter("001-1",171, 20, 340, 160, getarg(1,1), getarg(0));
        }
    }

    .@curmobc=mobcount("001-1", "Mana Being#001-1::OnAerosMobDeath");
    if (!is_gm()) goto L_Unauthorized;
    if ($HARDCORE) goto L_Unauthorized;

    @log_spawns=0;
    @log_mode=$@AEROS_SPWN;
    @log_ratio=$coinsrate;
    logmes "Aeros Control Panel was open.", LOGMES_ATCOMMAND;

    mesn;
    mes "Tired of walking the whole Aeros to spawn monsters, I was brought to existence.";
    mes "Monsters left: "+str(.@curmobc);

L_Menu:
    mes "";
    mes "Please select operation.";
    menu
        "Close", L_Close,
        "Start/End Event", L_EventHandler,
        "Spawn", L_Spawn,
        rif(countitem(StrangeCoin) >= 10, "Drop stuff! (10x Coins)"), L_Drop,
        "Reconfigure spawn/warp points", L_Conf,
        rif(.WALL, "Open Extension"), L_DelWall,
        rif(!.WALL, "Close Extension"), L_AddWall,
        rif(is_admin(), "Adjust coins drop rate"), L_Rate;

L_Unauthorized:
    //dispbottom l("I am too far away to talk. Weird floating thingy...");
    dispbottom l("Monsters left: @@", .@curmobc);
    end;

L_Close:
    if (@log_spawns > 0) logmes "spawned "+str(@log_spawns)+" beigns at Aeros.", LOGMES_ATCOMMAND;
    if (@log_ratio != $coinsrate) logmes "set aeros ratio from "+str(@log_ratio)+" to "+str($coinsrate), LOGMES_ATCOMMAND;
    if (@log_mode == $@AEROS_SPWN) logmes "configured aeros spawn area to "+str($@AEROS_SPWN), LOGMES_ATCOMMAND;
    logmes "Aeros Control Panel was closed normally.", LOGMES_ATCOMMAND;
    close;

L_Spawn:
    mes "";
    mes "Spawn from a preset (with intended levels) from this menu. Otherwise, use @aeros with same syntax.";
    mes "(agr) means Agressive Monsters on the set, DO NOT ABUSE.";
    next;
    menu
        "Abort", L_Menu,
        "Presets", L_Preset,
        "Normal (Lv <50)", L_Norm,
        "Normal (Lv >50)", L_Norm2,
        "Plants", L_Plants,
        "Looters", L_Sli,
        "Aggressive", L_Agr,
        //"Assistants", L_Ass,
        "GM Event Only", L_EventOnly,
        "Boss", L_Boss;

// NOTE: New Presets weren't added since ... long time ago.
L_Preset:
    select
        "Abort",                                                        // 1
        "20x Piou, Piousee, Ratto, 10x Croc",                           // 2
        "20x Little Blub, 10x Plushroom Field",                         // 3
        "(agr) 5x Tipiu, 10x Cave Maggot, 10x Bat",                    // 4
        "20x Scorpion, 10x Duck, 10x Maggot",                           // 5
        "10x Red Scorpion, 20x Fire Goblin, 5x Mana Ghost",             // 6
        "(agr) 1x Saxso Ghost, 20x House Maggot",                       // 7
        "(agr) 5x Slime Blast, 5x Red Slime, 10x White Slime",          // 8

        "(agr) 5x Mouboo, 4x Bandit, 2x Black Scorpion",                // 9
        "10x Giant Maggot, 10x Cave Snake, 10x Mana Bug",               // 10
        "1x Golden/Night Scorpion, 2x Santa Slime, 5x Copper Slime",    // 11
        "(agr) 2x Fallen Guards",                                       // 12

        "(agr) 5x Old Snake, 4x Grass Snake, 3x Snake",                 // 13
        "(agr) 4x Pollet, 3x Wolvern",                                  // 14
        "5x of each fairy",                                             // 15
        "(agr) 1x of each slime mother",                                // 16
        "(agr) 1x of each skull slime",                                 // 17

        "20x Bat, 10x Crafty, 5x Troll",                                // 18
        "(agr) 5x Forain, 5x Yeti, 5x Centaur",                         // 19
        "(agr) 10x Gobo Bear, 10x Terra, 5x Terra Protector",           // 20
        "(agr) 1x Reaper, 1x Michel, 2x JackO, 5x Skellington",         // 21
        "1x of each Pixie",                                             // 22
        "(agr) 20x Pinkie, 10x Suseran, 7x Maximus",                    // 23

        "7x3 random Chests and mimics",                                 // 24
        "10x Clover Path, 5 groups of 5 random Bifs",                   // 25
        "5x Bifs, 4 groups of 5 random Bifs";                           // 26

    switch (@menu) {
        case 1:
            goto L_Spawn;
            break;
        //"20x Piou, Piousee, Ratto, 10x Croc",                           // 2
        //"20x Little Blub, 10x Plushroom Field",                         // 3
        //"(agr) 5x Tipiu, 10x Cave Maggot, 10x Bat",                    // 4
        //"20x Scorpion, 10x Duck, 10x Maggot",                           // 5
        //"10x Red Scorpion, 20x Fire Goblin, 5x Mana Ghost",             // 6
        //"(agr) 1x Saxso Ghost, 20x House Maggot",                       // 7
        //"(agr) 5x Slime Blast, 5x Red Slime, 10x White Slime",          // 8
        case 2:
            spawner(strmobinfo(1, Piou), Piou, 20);
            spawner(strmobinfo(1, Piousee), Piousee, 20);
            spawner(strmobinfo(1, Ratto), Ratto, 20);
            spawner(strmobinfo(1, Croc), Croc, 10);
            @log_spawns=@log_spawns+70;
            break;
        case 3:
            spawner(strmobinfo(1, LittleBlub), LittleBlub, 20);
            spawner(strmobinfo(1, PlushroomField), PlushroomField, 10);
            @log_spawns=@log_spawns+30;
            break;
        case 4:
            spawner(strmobinfo(1, Tipiu), Tipiu, 5);
            spawner(strmobinfo(1, CaveMaggot), CaveMaggot, 10);
            spawner(strmobinfo(1, Bat), Bat, 10);
            @log_spawns=@log_spawns+25;
            break;
        case 5:
            spawner(strmobinfo(1, Scorpion), Scorpion, 20);
            spawner(strmobinfo(1, Duck), Duck, 10);
            spawner(strmobinfo(1, Maggot), Maggot, 10);
            @log_spawns=@log_spawns+40;
            break;
        case 6:
            spawner(strmobinfo(1, RedScorpion), RedScorpion, 10);
            spawner(strmobinfo(1, ManaBug), FireGoblin, 20);
            spawner(strmobinfo(1, ManaGhost), ManaGhost, 5);
            @log_spawns=@log_spawns+35;
            break;
        case 7:
            spawner(strmobinfo(1, SaxsoGhost), SaxsoGhost, 1);
            spawner(strmobinfo(1, HouseMaggot), HouseMaggot, 20);
            @log_spawns=@log_spawns+21;
            break;
        case 8:
            spawner(strmobinfo(1, SlimeBlast), SlimeBlast, 5);
            spawner(strmobinfo(1, RedSlime), RedSlime, 5);
            spawner(strmobinfo(1, WhiteSlime), WhiteSlime, 10);
            @log_spawns=@log_spawns+20;
            break;

        //"(agr) 5x Mouboo, 4x Bandit, 2x Black Scorpion",                // 9
        //"10x Giant Maggot, 10x Cave Snake, 10x Mana Bug",               // 10
        //"1x Golden/Night Scorpion, 2x Santa Slime, 5x Copper Slime",    // 11
        //"(agr) 2x Fallen Guards",                                       // 12
        case 9:
            spawner(strmobinfo(1, Mouboo), Mouboo, 5);
            spawner(strmobinfo(1, Bandit), Bandit, 4);
            spawner(strmobinfo(1, BlackScorpion), BlackScorpion, 2);
            @log_spawns=@log_spawns+11;
            break;
        case 10:
            spawner(strmobinfo(1, GiantMaggot), GiantMaggot, 10);
            spawner(strmobinfo(1, CaveSnake), CaveSnake, 10);
            spawner(strmobinfo(1, ManaBug), ManaBug, 10);
            @log_spawns=@log_spawns+30;
            break;
        case 11:
            spawner(strmobinfo(1, GoldenScorpion), GoldenScorpion, 1);
            spawner(strmobinfo(1, NightScorpion), NightScorpion, 1);
            spawner(strmobinfo(1, SantaSlime), SantaSlime, 5);
            spawner(strmobinfo(1, CopperSlime), CopperSlime, 10);
            @log_spawns=@log_spawns+17;
            break;
        case 12:
            spawner(strmobinfo(1, FallenGuard1), FallenGuard1, 1);
            spawner(strmobinfo(1, FallenGuard2), FallenGuard2, 1);
            @log_spawns=@log_spawns+2;
            break;

        //"(agr) 5x Old Snake, 4x Grass Snake, 3x Snake",                 // 13
        //"(agr) 4x Pollet, 3x Wolvern",                                  // 14
        //"5x of each fairy",                                             // 15
        //"(agr) 1x of each slime mother",                                // 16
        //"(agr) 1x of each skull slime",                                 // 17
        case 13:
            spawner("", OldSnake, 5);
            spawner("", GrassSnake, 4);
            spawner("", Snake, 3);
            @log_spawns=@log_spawns+12;
            break;
        case 14:
            spawner("", Pollet, 4);
            spawner("", Wolvern, 3);
            @log_spawns=@log_spawns+7;
            break;
        case 15:
            spawner("", EarthFairy,  5);
            spawner("", FireFairy,   5);
            spawner("", WaterFairy,  5);
            spawner("", WindFairy,   5);
            spawner("", NatureFairy, 5);
            @log_spawns=@log_spawns+25;
            break;
        case 16:
            spawner("", GreenSlimeMother, 1);
            spawner("", BlueSlimeMother, 1);
            spawner("", CopperSlimeMother, 1);
            spawner("", YellowSlimeMother, 1);
            spawner("", RedSlimeMother, 1);
            spawner("", ChocolateSlimeMother, 1);
            spawner("", WhiteSlimeMother, 1);
            spawner("", AzulSlimeMother, 1);
            spawner("", SeaSlimeMother, 1);
            spawner("", LavaSlimeMother, 1);
            spawner("", BlackSlimeMother, 1);
            @log_spawns=@log_spawns+11;
            break;
        case 17:
            spawner("", AzulSkullSlime, 1);
            spawner("", YellowSkullSlime, 1);
            spawner("", RedSkullSlime, 1);
            spawner("", GreenSkullSlime, 1);
            spawner("", CopperSkullSlime, 1);
            spawner("", LavaSkullSlime, 1);
            spawner("", BlackSkullSlime, 1);
            @log_spawns=@log_spawns+7;
            break;
        //"20x Bat, 10x Crafty, 5x Troll",                                // 18
        //"(agr) 5x Forain, 5x Yeti, 5x Centaur",                         // 19
        //"(agr) 10x Gobo Bear, 10x Terra, 5x Terra Protector",           // 20
        //"(agr) 1x Reaper, 1x Michel, 2x JackO, 5x Skellington",         // 21
        //"1x of each Pixie",                                             // 22
        //"(agr) 20x Pinkie, 10x Suseran, 7x Maximus",                    // 23
        case 18:
            spawner("", Bat, 20);
            spawner("", Crafty, 10);
            spawner("", Troll, 5);
            @log_spawns=@log_spawns+35;
            break;
        case 19:
            spawner("", Forain, 5);
            spawner("", Yeti, 5);
            spawner("", Centaur, 5);
            @log_spawns=@log_spawns+15;
            break;
        case 20:
            spawner("", GoboBear, 10);
            spawner("", Terranite, 10);
            spawner("", TerraniteProtector, 5);
            @log_spawns=@log_spawns+25;
            break;
        case 21:
            spawner("", Reaper, 1);
            spawner("", Michel, 1);
            spawner("", JackO, 2);
            spawner("", Skeleton, 5);
            @log_spawns=@log_spawns+9;
            break;
        case 22:
            spawner("", VanityPixie, 1);
            spawner("", HolyPixie, 1);
            spawner("", ShadowPixie, 1);
            spawner("", NulityPixie, 1);
            @log_spawns=@log_spawns+4;
            break;
        case 23:
            spawner("", Pinkie, 20);
            spawner("", PinkieSuseran, 10);
            spawner("", PinkieMaximus, 7);
            @log_spawns=@log_spawns+37;
            break;


        //"7x3 random Chests and mimics",                                 // 24
        //"10x Clover Path, 5 groups of 5 random Bifs",                   // 25
        //"5x Bifs, 4 groups of 5 random Bifs";                           // 26
        case 24:
            spawner("", rand(BronzeChest, GoldenChest), 3);
            spawner("", rand(BronzeChest, MalignChest), 3);
            spawner("", rand(BronzeMimic, GoldenMimic), 3);
            spawner("", rand(WildxChest,  MalignChest), 3);
            spawner("", rand(BronzeChest, GoldenChest), 3);
            spawner("", rand(BronzeChest, MalignChest), 3);
            spawner("", rand(BronzeMimic, GoldenMimic), 3);
            @log_spawns=@log_spawns+21;
            break;
        case 25:
            spawner(strmobinfo(1, CloverPatch), CloverPatch, 10);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            @log_spawns=@log_spawns+35;
            break;
        case 26:
            spawner(strmobinfo(1, Bif), Bif, 5);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            @log_spawns=@log_spawns+25;
            break;
    }

    if (@menu == 1) goto L_Spawn;
    mes "";
    mes "Completed.";
    mes "Total spawns: "+str(@log_spawns);
    next;
    goto L_Preset;

L_Norm:
    // option preparatives
    .@opt$="Abort";
    debugmes "Found %d entries", getarraysize(.ML_Lv50);
    for (.@i=0;.@i < getarraysize(.ML_Lv50);.@i++) {
        .@opt$+=":"+strmobinfo(1, .ML_Lv50[.@i]);
        // .ML_Lv50[.@i]);
    }
    select .@opt$;

    // Select handler
    if (@menu != 1)
        input .@c, 0, 100;
    if (.@c == 0 && @menu != 1)
        @menu=99;

    if (@menu == 1)
        goto L_Spawn;
    if (@menu == 99)
        goto L_Norm;

    // Spawn if needed
    .@i=@menu-2;
    spawner(("NORMAL 1 Monster"), .ML_Lv50[.@i], .@c);

    // Log the spawn and resume
    @log_spawns=@log_spawns+.@c;
    mes "";
    mes "Completed.";
    mes "Total spawns: "+str(@log_spawns);
    next;
    goto L_Norm;

L_Norm2:
    // option preparatives
    .@opt$="Abort";
    debugmes "Found %d entries", getarraysize(.ML_Lv99);
    for (.@i=0;.@i < getarraysize(.ML_Lv99);.@i++) {
        .@opt$+=":"+strmobinfo(1, .ML_Lv99[.@i]);
        // .ML_Lv99[.@i]);
    }
    select .@opt$;

    // Select handler
    if (@menu != 1)
        input .@c, 0, 100;
    if (.@c == 0 && @menu != 1)
        @menu=99;

    if (@menu == 1)
        goto L_Spawn;
    if (@menu == 99)
        goto L_Norm2;

    // Spawn if needed
    .@i=@menu-2;
    spawner(("NORMAL 2 Monster"), .ML_Lv99[.@i], .@c);

    // Log the spawn and resume
    @log_spawns=@log_spawns+.@c;
    mes "";
    mes "Completed.";
    mes "Total spawns: "+str(@log_spawns);
    next;
    goto L_Norm2;

L_Sli:
    // option preparatives
    .@opt$="Abort";
    debugmes "Found %d entries", getarraysize(.ML_Loot);
    for (.@i=0;.@i < getarraysize(.ML_Loot);.@i++) {
        .@opt$+=":"+strmobinfo(1, .ML_Loot[.@i]);
        // .ML_Loot[.@i]);
    }
    select .@opt$;

    // Select handler
    if (@menu != 1)
        input .@c, 0, 100;
    if (.@c == 0 && @menu != 1)
        @menu=99;

    if (@menu == 1)
        goto L_Spawn;
    if (@menu == 99)
        goto L_Sli;

    // Spawn if needed
    .@i=@menu-2;
    spawner(("LOOTER Monster"), .ML_Loot[.@i], .@c);

    // Log the spawn and resume
    @log_spawns=@log_spawns+.@c;
    mes "";
    mes "Completed.";
    mes "Total spawns: "+str(@log_spawns);
    next;
    goto L_Sli;


L_Plants:
    // option preparatives
    .@opt$="Abort";
    debugmes "Found %d entries", getarraysize(.ML_Plants);
    for (.@i=0;.@i < getarraysize(.ML_Plants);.@i++) {
        .@opt$+=":"+strmobinfo(1, .ML_Plants[.@i]);
        // .ML_Plants[.@i]);
    }
    select .@opt$;

    // Select handler
    if (@menu != 1)
        input .@c, 0, 100;
    if (.@c == 0 && @menu != 1)
        @menu=99;

    if (@menu == 1)
        goto L_Spawn;
    if (@menu == 99)
        goto L_Plants;

    // Spawn if needed
    .@i=@menu-2;
    spawner(("PLANT Monster"), .ML_Plants[.@i], .@c);

    // Log the spawn and resume
    @log_spawns=@log_spawns+.@c;
    mes "";
    mes "Completed.";
    mes "Total spawns: "+str(@log_spawns);
    next;
    goto L_Plants;


L_Agr:
    // option preparatives
    .@opt$="Abort";
    debugmes "Found %d entries", getarraysize(.ML_Aggr);
    for (.@i=0;.@i < getarraysize(.ML_Aggr);.@i++) {
        .@opt$+=":"+strmobinfo(1, .ML_Aggr[.@i]);
        // .ML_Aggr[.@i]);
    }
    select .@opt$;

    // Select handler
    if (@menu != 1)
        input .@c, 0, 100;
    if (.@c == 0 && @menu != 1)
        @menu=99;

    if (@menu == 1)
        goto L_Spawn;
    if (@menu == 99)
        goto L_Agr;

    // Spawn if needed
    .@i=@menu-2;
    spawner(("AGGRESSIVE Monster"), .ML_Aggr[.@i], .@c);

    // Log the spawn and resume
    @log_spawns=@log_spawns+.@c;
    mes "";
    mes "Completed.";
    mes "Total spawns: "+str(@log_spawns);
    next;
    goto L_Agr;









L_EventOnly:
    mesc l("WARNING: Spawn these monsters with moderation!"), 1;
    mesc l("This is just a short list to make GMs life easier!"), 1;
    mes "";
    menuint
        "Abort",            -1,
        "Bhop Fluffy",      BhopFluffy,
        "Easter Mouboo",    EasterMouboo,
        "Evil Scythe",      EvilScythe,
        "Jack'O",           JackO,
        "Magic Ratto",      MagicRatto,
        "Moonshroom",       Moonshroom,
        "Mouboo Slime",     MoubooSlime,
        "Small Magic Bif",  SmallMagicBif,
        "Magic Bif",        MagicBif,
        "Bronze Chest",     BronzeChest,
        "Bronze Mimic",     BronzeMimic,
        "Silver Chest",     SilverChest,
        "Silver Mimic",     SilverMimic,
        "Golden Chest",     GoldenChest,
        "Golden Mimic",     GoldenMimic,
        "Evil Chest",       EvilChest,
        "Big Magic Bif",    BigMagicBif;

    .@mobId=@menuret;

    if (.@mobId > 0)
        input .@c, 0, 100;
    if (.@c == 0 && .@mobId > 0)
        .@mobId = 0;

    if (.@mobId < 0)
        goto L_Spawn;
    else if (.@mobId > 0)
        spawner("", .@mobId, .@c);

    @log_spawns=@log_spawns+.@c;
    mes "";
    mes "Completed.";
    mes "Total spawns: "+str(@log_spawns);
    next;
    goto L_EventOnly;








L_Boss:
    // Boss option preparatives
    .@opt$="Abort";
    debugmes "Found %d entries", getarraysize(.ML_Boss);
    for (.@i=0;.@i < getarraysize(.ML_Boss);.@i++) {
        .@opt$+=":"+strmobinfo(1, .ML_Boss[.@i]);
        // .ML_Boss[.@i]);
    }
    select .@opt$;

    // Select handler
    if (@menu != 1)
        input .@c, 0, 100;
    if (.@c == 0 && @menu != 1)
        @menu=99;

    if (@menu == 1)
        goto L_Spawn;
    if (@menu == 99)
        goto L_Boss;

    // Spawn if needed
    .@i=@menu-2;
    spawner(("BOSS Monster"), .ML_Boss[.@i], .@c);

    // Log the spawn and resume
    @log_spawns=@log_spawns+.@c;
    mes "";
    mes "Completed.";
    mes "Total spawns: "+str(@log_spawns);
    next;
    goto L_Boss;


L_Drop:
    select
        "Abort",                                   // 1
        "Bury 10x Coins",                          // 2
        "Toothbrush",                              // 4
        "Bronze Gift",                             // 5
        "Silver Gift",                             // 6
        "Golden Gift",                             // 7
        "Prism Gift",                              // 8
        "Supreme Gift"; // default

    if (@menu != 1)
        input .@c, 0, countitem(StrangeCoin)/10;
    if (.@c == 0 && @menu != 1)
        @menu=1;

    delitem(StrangeCoin,.@c*10);
    switch (@menu) {
        case 1:
            goto L_Menu;
            break;
        case 2:
            buryitem(StrangeCoin, .@c*any(9,10,10,10,11,11)); break;
        case 3:
            mkitem(Toothbrush, .@c); break;
        case 4:
            mkitem(BronzeGift, .@c); break;
        case 5:
            mkitem(SilverGift, .@c); break;
        case 6:
            mkitem(GoldenGift, .@c); break;
        case 7:
            mkitem(PrismGift, .@c); break;
        default:
            mkitem(SupremeGift, .@c); break;

    }

    mes "";
    mes "Dropped "+.@c+" stuff.";
    next;
    goto L_Drop;

L_Rate:
    // Only Admins can change Strange Coin drop rate
    if (!is_admin())
        goto L_Menu;

    mes "";
    mes "Current drop rate: " + str($coinsrate);
    mes "Insert drop rate (from 0 to 10000, capped at 5000 or 50%). Use -1 to disable.";
    input $coinsrate;
    if ($coinsrate > 5000)
        $coinsrate=5000;
    mes "";
    mes "Ratio adjusted.";
    next;
    goto L_Menu;

L_EventHandler:
    if($@GM_EVENT) {
        announce ("The event is over!"), bc_all|bc_npc; // I haven't tested this yet.
        $@GM_EVENT=0;
        $@MK_SCENE=MK_NONE;
    } else if (!$@MK_SCENE) {
        announce ("The mana bridge to Aeros is open! To participate on event, talk to ##BSoul Menhir##b!"), bc_all|bc_npc;
        channelmes("#world", "An event is happening on Aeros! Hurry up!");
        $@GM_EVENT=1;
        $@MK_SCENE=MK_LOCKED;
    } else {
        mesc("Operation not permitted: Monster King event is in progress.", 1);
        mesc("You MUST wait.", 1);
    }
    close;

L_AddWall:
    setcells "001-1", 169, 63, 169, 76, 3, "AerosWall";
    //setwall("001-1", 169, 63, 13, DOWN, false, "AerosWall");
    .WALL=1;
    close;

L_DelWall:
    delcells "AerosWall";
    //delwall("AerosWall");
    .WALL=0;
    close;

L_Conf:
    mes "";
    mesn;
    mesq l("Current Spawn Mode: "+$@AEROS_SPWN);
    next;
    select
        l("[0] East Aeros"),
        l("[1] West Aeros"),
        l("[2] Full Aeros");
    $@AEROS_SPWN=@menu-1;
    goto L_Menu;

OnAerosMobDeath:
    if ($coinsrate < 0) end;
    // A level 100 monster can have a 4% drop bonus against a level 0 monster.
    // $coinsrate acts stablishing a basic drop value. Advised value: 600 (up to 10% for normal mobs)
    if (playerattached()) {
        if (rand(10000) <= $coinsrate + (atoi(strmobinfo(3,killedrid))*4)) {
            getmapxy(.@m$, .@x, .@y, 0);
            makeitem(StrangeCoin, 1, .@m$, .@x, .@y);
        }
    }
    end;

OnInit:
    if ($coinsrate == 0)
        $coinsrate=400; // Default value is 4% + mob str bonus

    // Monster lists
    // These arrays are filled automatically by redesign.py
    // DO NOT EDIT MANUALLY
    setarray .ML_Plants, 1021, 1058, 1098, 1099, 1100, 1101, 1102, 1103, 1104, 1105, 1106, 1107, 1108, 1109, 1110, 1111, 1112, 1113, 1114, 1115, 1116, 1117, 1118, 1134, 1135, 1136, 1140, 1141, 1142, 1143, 1144, 1145, 1146, 1147, 1148, 1149, 1150, 1156, 1157, 1158, 1170, 1188, 1189, 1226, 1227, 1228;
    setarray .ML_Boss, 1015, 1036, 1040, 1044, 1076, 1077, 1079, 1080, 1087, 1129, 1154, 1179, 1200, 1205, 1209, 1211, 1213, 1221, 1222, 1225, 1229, 1400, 1401, 1420, 1430, 1495, 1496, 1497, 1498, 1499, 1500, 1503, 1504, 1505;
    setarray .ML_Aggr, 1012, 1024, 1026, 1027, 1037, 1042, 1051, 1052, 1056, 1061, 1062, 1063, 1064, 1074, 1081, 1082, 1084, 1085, 1089, 1090, 1092, 1097, 1120, 1122, 1123, 1124, 1125, 1130, 1131, 1137, 1138, 1153, 1167, 1168, 1169, 1174, 1176, 1177, 1178, 1187, 1192, 1193, 1194, 1195, 1196, 1198, 1199, 1203, 1206, 1207, 1208, 1212, 1214, 1220, 1223, 1224, 1230, 1231, 1234, 1235, 1236, 1237, 1238, 1239, 1240, 1241, 1242, 1243, 1244, 1245, 1246, 1247, 1248, 1249, 1402, 1403, 1404, 1405, 1406, 1407, 1408, 1419, 1492, 1501, 1502;
    setarray .ML_Asst, 1003, 1007, 1016, 1023, 1038, 1056, 1065, 1066, 1072, 1083, 1085, 1086, 1089, 1090, 1094, 1155, 1175, 1178, 1180, 1181, 1182, 1183, 1184, 1185, 1186, 1187, 1190, 1191, 1192, 1195, 1201, 1202, 1230, 1232;
    setarray .ML_Loot, 1005, 1007, 1008, 1009, 1018, 1029, 1032, 1033, 1039, 1053, 1054, 1055, 1085, 1086, 1088, 1089, 1090, 1091, 1092, 1093, 1094, 1095, 1096, 1097, 1124, 1125, 1126, 1138, 1167, 1175, 1178, 1180, 1181, 1187, 1190, 1191, 1192, 1193, 1194, 1195, 1198, 1212, 1230, 1233, 1234, 1236, 1237, 1238, 1239, 1240, 1241, 1242, 1243, 1244, 1245, 1246, 1247, 1248, 1402, 1403, 1404, 1405, 1406, 1407, 1408;
    setarray .ML_Lv50, 1002, 1006, 1010, 1011, 1017, 1022, 1025, 1028, 1030, 1031, 1034, 1035, 1041, 1043, 1049, 1050, 1060, 1067, 1068, 1070, 1071, 1073, 1075, 1119, 1121, 1127, 1128, 1132, 1133, 1152, 1172, 1173, 1204, 1210, 1219;
    setarray .ML_Lv99, 1004, 1069, 1078, 1139, 1151, 1171, 1197, 1215, 1216, 1217, 1218, 1409, 1410, 1411, 1412, 1413, 1414, 1415, 1416, 1417, 1418, 1421, 1422, 1423, 1424, 1425, 1426, 1427, 1428, 1429, 1493, 1494;
    end;
}



-	script	@aeros	32767,{
    end;

OnCall:
    if (!is_gm())
        end;
    if (getmapname() != "001-1") {
        dispbottom "This command can only be used at aeros.";
        end;
    }
    if ($HARDCORE) {
        dispbottom "Aeros cannot be used on Hardcore servers.";
        end;
    }
    //.@atcmd_parameters$ = strtoupper(strip( // nah

    if (.@atcmd_numparameters != 2) {
        dispbottom "Use the numeric ID provided by the wiki. This is a safeguard to ensure you are not overkilling players.";
        dispbottom "Talk to Mana Being for a less fine-grained but much more optimized control over monsters.";
        dispbottom "This command takes exactly this syntax: Mob ID <space> amount.";
        end;
    }

        switch($@AEROS_SPWN) {
        case 1:
            areamonster("001-1", 20, 20, 140, 140, strmobinfo(1, atoi(.@atcmd_parameters$[0])), atoi(.@atcmd_parameters$[0]), atoi(.@atcmd_parameters$[1]), "Mana Being#001-1::OnAerosMobDeath");
            break;
        case 2:
            areamonster("001-1", 20, 20, 340, 160, strmobinfo(1, atoi(.@atcmd_parameters$[0])), atoi(.@atcmd_parameters$[0]), atoi(.@atcmd_parameters$[1]), "Mana Being#001-1::OnAerosMobDeath");
            break;
        default:
            areamonster("001-1", 171, 20, 340, 160, strmobinfo(1, atoi(.@atcmd_parameters$[0])), atoi(.@atcmd_parameters$[0]), atoi(.@atcmd_parameters$[1]), "Mana Being#001-1::OnAerosMobDeath");
            break;
        }
    logmes "@aeros "+strmobinfo(1, atoi(.@atcmd_parameters$[0])) + " " + .@atcmd_parameters$[1], LOGMES_ATCOMMAND;
    dispbottom "All monsters summoned.";

    end;

OnInit:
    bindatcmd "aeros", "@aeros::OnCall", 80, 99, 0;
    end;
}
