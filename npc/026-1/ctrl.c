// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//   The Impregnable Fortress Control Files
// Quest: General_Fortress
//  (MaxFloor+2, internal, internal)

026-1,60,26,0	script	Impregnable#B1F	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    if (getq(General_Fortress) > 2) goto L_Warp;
    mesc l(".:: Impregnable Fortress, %sF ::.", "B1"), 3;
    msObjective(getq(General_Fortress) == 3, l("* Obtain clearance"));
    msObjective($MK_TEMPVAR >= MKIF_LV_B2F, l("Minimum wins: %d/%d", $MK_TEMPVAR, MKIF_LV_B2F));
    mes "";
    mesc l("Hint: You might need to come back later.");
    close;

L_Warp:
    fortwarp(MKIF_LV_B2F, "026-2", 32, 96);
    end;

// Disarm & reset everything
OnMinute15:
OnMinute45:
    if ($@DATA_0261[12])
        setcells "026-1", 58, 76, 62, 76, 1, "026-1_58_76";
    if ($@DATA_0261[13])
        setcells "026-1", 25, 32, 25, 32, 1, "026-1_25_32";
    if ($@DATA_0261[14])
        setcells "026-1", 30, 32, 30, 32, 1, "026-1_30_32";
    setnpcdisplay "#026-1_24_28", NPC_SWITCH_ONLINE;
    setnpcdisplay "#026-1_26_28", NPC_SWITCH_ONLINE;
    setnpcdisplay "#026-1_62_28", NPC_SWITCH_ONLINE;
    callfunc "0261_CheckReset";
    end;
}

// Other misc controllers
function	script	0261_GateChange	{
    //if (Sp < MaxSp) end;
    if (Hp < MaxHp) {die(); end;}
    $@DATA_0261[12] = true;
    percentheal -99, -100;
    dispbottom "*snap* - Oh noes, there was a trap!";
    return;
}

function	script	0261_Flip25	{
    $@DATA_0261[13] = true;
    return;
}

function	script	0261_Flip30	{
    $@DATA_0261[14] = true;
    return;
}

function	script	0261_CheckReset	{
    if ($@DATA_0261[0] == 1 &&
        $@DATA_0261[1] == 1 &&
        $@DATA_0261[2] == 1 &&
        $@DATA_0261[3] == 1 &&
        $@DATA_0261[4] == 1 &&
        $@DATA_0261[5] == 1 &&
        $@DATA_0261[6] == 1 &&
        $@DATA_0261[7] == 1 &&
        $@DATA_0261[8] == 1 &&
        $@DATA_0261[9] == 1 &&
        $@DATA_0261[10] == 1 &&
        $@DATA_0261[11] == 1)
            .@r=true;
    if ($@DATA_0261[0])
        delcells "026-1_D0";
    if ($@DATA_0261[1])
        delcells "026-1_D1";
    if ($@DATA_0261[2])
        delcells "026-1_D2";
    if ($@DATA_0261[3])
        delcells "026-1_D3";
    if ($@DATA_0261[4])
        delcells "026-1_D4";
    if ($@DATA_0261[5])
        delcells "026-1_D5";
    if ($@DATA_0261[6])
        delcells "026-1_D6";
    if ($@DATA_0261[7])
        delcells "026-1_D7";
    if ($@DATA_0261[8])
        delcells "026-1_D8";
    if ($@DATA_0261[9])
        delcells "026-1_D9";
    if ($@DATA_0261[10])
        delcells "026-1_D10";
    if ($@DATA_0261[11])
        delcells "026-1_D11";
    deletearray $@DATA_0261;
    if (.@r) {
        $@DATA_0261[12] = true;
        delcells "026-1_58_76";
    }
    return .@r;
}

026-1,29,76,0	script	Important Note#B1F	NPC_NO_SPRITE,{
    function noteBegin;
    if ($@DATA_0261[12]) end;
    if (getq2(General_Fortress) == 0) noteBegin();
    mesc l("Flip the manaplace?"), 1;
    next;
    if (askyesno() == ASK_YES) {
        .@r=0261_CheckReset();
        if (!.@r)
            warp "003-1", 82, 119;
        else if (getq(General_Fortress) == 2)
            setq General_Fortress, 3, 0, 0;
    }
    close;

function noteBegin {
    mes l("To the monster army:");
    mes "";
    mes l("There has been a lot of movement here as of late.");
    mes l("Your Highness does not like it, and you're dirtying the floor faster than staff can clean.");
    mes "";
    mes l("Therefore, we have restricted access to and from B0F/B2F.");
    mes l("If you could not attend to the meeting (may the king forgive your soul), just restore the objects to their proper position and flip the switch on this manaplace.");
    mes "";
    mes l("Be careful to don't flip the manaplace before all objects are ordered correctly.");
    mes "";
    mes l("-- The Keeper");
    next;
    setq2 General_Fortress, 1;
    return;
}

OnInit:
    .distance=3;
    end;
}

// Flip flop
026-1,26,89,0	script	#0261_FF@0	NPC_NO_SPRITE,{
    explode(.@ni$, .name$, "@");
    .@x=atoi(.@ni$[1]);
    .@v$=sprintf("$@DATA_0261[%d]", .@x);
    .@i=getd(.@v$);
    if (.@i == 0) {
        setd(.@v$, 1);
        setcells "026-1", .x-1, .y, .x+1, .y, 2, "026-1_D"+.@x;
    } else if (.@i == 1) {
        setd(.@v$, 2);
        delcells "026-1_D"+.@x;
        setcells "026-1", .x-1, .y, .x+1, .y, 3, "026-1_D"+.@x;
    } else {
        setd(.@v$, 0);
        delcells "026-1_D"+.@x;
    }
    end;
OnInit:
    .distance=2;
    end;
}

026-1,32,89,0	duplicate(#0261_FF@0)	#0261_FF@1	NPC_NO_SPRITE
026-1,26,86,0	duplicate(#0261_FF@0)	#0261_FF@2	NPC_NO_SPRITE
026-1,32,86,0	duplicate(#0261_FF@0)	#0261_FF@3	NPC_NO_SPRITE
026-1,71,76,0	duplicate(#0261_FF@0)	#0261_FF@4	NPC_NO_SPRITE
026-1,71,81,0	duplicate(#0261_FF@0)	#0261_FF@5	NPC_NO_SPRITE
026-1,78,76,0	duplicate(#0261_FF@0)	#0261_FF@6	NPC_NO_SPRITE
026-1,78,81,0	duplicate(#0261_FF@0)	#0261_FF@7	NPC_NO_SPRITE
026-1,85,76,0	duplicate(#0261_FF@0)	#0261_FF@8	NPC_NO_SPRITE
026-1,85,81,0	duplicate(#0261_FF@0)	#0261_FF@9	NPC_NO_SPRITE
026-1,92,76,0	duplicate(#0261_FF@0)	#0261_FF@10	NPC_NO_SPRITE
026-1,92,81,0	duplicate(#0261_FF@0)	#0261_FF@11	NPC_NO_SPRITE

