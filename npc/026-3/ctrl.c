// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//   The Impregnable Fortress Control Files
// Quest: General_Fortress
//  (MaxFloor+2, internal, internal)

// 0263Event(switchID)
function	script	0263Event	{
    // Terminate the script if you already flipped it (also to stop trolls)
    if (getq(General_Fortress) != 4) end;
    if (getq2(General_Fortress) & getarg(0)) {
        dispbottom l("I have already flipped this switch earlier.");
        end;
    }
    // Flip it for good
    setq2 General_Fortress, getq2(General_Fortress) | getarg(0);
    // And if all switches are flipped...!
    if (getq2(General_Fortress) == 15) {
        setq General_Fortress, 5, 0, 0;
        specialeffect(FX_MGWARP, SELF, getcharid(3));
    }
    return;
}

// The main entrance
026-3,55,128,0	script	Impregnable#B3F	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    dispbottom l("From this point forward, you'll no longer be able to return to the previous floor.");
    if (getq(General_Fortress) > 4) goto L_Warp;
    mesc l(".:: Impregnable Fortress, %sF ::.", "B3"), 3;
    msObjective(getq(General_Fortress) == 5, l("* Flip all four switches"));
    msObjective($MK_TEMPVAR >= MKIF_LV_B4F, l("Minimum wins: %d/%d", $MK_TEMPVAR, MKIF_LV_B4F));
    mes "";
    mesc l("Hint: You cannot unflip a switch, but they unflip every 12 hours.");
    close;

L_Warp:
    // This is a more complex warp
    .@e = 0;
    do {
        if (.@e >= 30) break;
        .@x = rand2(20, 40);
        .@y = rand2(20, 40);
        .@e+=1;
    } while (!checknpccell("026-4", .@x, .@y, cell_chkpass));
    fortwarp(MKIF_LV_B4F, "026-4", .@x, .@y);
    end;

// Reset the switches
OnClock0500:
OnClock1700:
	setnpcdisplay "#026-3_129_38", NPC_SWITCH_OFFLINE;
	setnpcdisplay "#026-3_59_86",  NPC_SWITCH_OFFLINE;
	setnpcdisplay "#026-3_94_80",  NPC_SWITCH_OFFLINE;
	setnpcdisplay "#026-3_53_59",  NPC_SWITCH_OFFLINE;
    end;

OnInit:
    setarray $@MKB3FX, 66, 60,  54,  95,  80, 90, 88, 61, 73;
    setarray $@MKB3FY, 18, 87, 118, 122, 140, 88, 56, 51, 39;
    end;
}


// Miller system (otherwise, you can't reach the switches & platforms)
026-3,21,69,0	script	#0263WA+	NPC_FANCY_CIRCLE,0,0,{
    end;
OnTouch:
    // All portals are the same, anyway
    if (compare(strnpcinfo(2), "+"))
        @state += 3;
    else
        @state -= 2;
    .@index = miller_rand(@state, getcharid(0), 8);
    slide $@MKB3FX[.@index], $@MKB3FY[.@index];
    end;
}

026-3,28,136,0	duplicate(#0263WA+)	#0263WA-	NPC_FANCY_CIRCLE,0,0
026-3,45,81,0	duplicate(#0263WA+)	#0263WB+	NPC_FANCY_CIRCLE,0,0
026-3,79,95,0	duplicate(#0263WA+)	#0263WB-	NPC_FANCY_CIRCLE,0,0
026-3,48,100,0	duplicate(#0263WA+)	#0263WC+	NPC_FANCY_CIRCLE,0,0
026-3,66,127,0	duplicate(#0263WA+)	#0263WC-	NPC_FANCY_CIRCLE,0,0
026-3,83,126,0	duplicate(#0263WA+)	#0263WD+	NPC_FANCY_CIRCLE,0,0
026-3,107,110,0	duplicate(#0263WA+)	#0263WD-	NPC_FANCY_CIRCLE,0,0
026-3,117,95,0	duplicate(#0263WA+)	#0263WE+	NPC_FANCY_CIRCLE,0,0
026-3,140,100,0	duplicate(#0263WA+)	#0263WE-	NPC_FANCY_CIRCLE,0,0
026-3,105,87,0	duplicate(#0263WA+)	#0263WF+	NPC_FANCY_CIRCLE,0,0
026-3,90,73,0	duplicate(#0263WA+)	#0263WF-	NPC_FANCY_CIRCLE,0,0
026-3,82,65,0	duplicate(#0263WA+)	#0263WG+	NPC_FANCY_CIRCLE,0,0
026-3,76,52,0	duplicate(#0263WA+)	#0263WG-	NPC_FANCY_CIRCLE,0,0
026-3,44,41,0	duplicate(#0263WA+)	#0263WH+	NPC_FANCY_CIRCLE,0,0
026-3,49,68,0	duplicate(#0263WA+)	#0263WH-	NPC_FANCY_CIRCLE,0,0
026-3,74,35,0	duplicate(#0263WA+)	#0263WI+	NPC_FANCY_CIRCLE,0,0
026-3,71,42,0	duplicate(#0263WA+)	#0263WI-	NPC_FANCY_CIRCLE,0,0

