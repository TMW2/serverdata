// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//   Lilit's access

018-5,100,52,0	script	Great Tree	NPC_NO_SPRITE,{
    function gtLocked;
    function gtFirstTime;
    function gtAccessOK;

    .@q = getq(LilitQuest_Access);

    if (JobLevel < 40) gtLocked();
    switch (.@q) {
        case 2:
            gtAccessOK(); break;
        case 1:
            gtFirstTime(); break;
        default:
            gtLocked();
    }
    close;

function gtFirstTime {
    if (getq(LoFQuest_Fairy) >= 3 &&
        getq(LilitQuest_Shaabty) >= 4 &&
        getq(LilitQuest_Ivanize) >= 2) {
        mesn l("Yetifly the Mighty");
        mesq l("%s... While you don't have the title of %s nor the flower of fae, nor have anything remotely important on your persona...", strcharinfo(0), b(l("Friend of the Fairies")));
        next;
        mesn l("Yetifly the Mighty");
        mesq l("...I cannot fail to notice your deeds. Very well. Under my own authority, I authorize you to climb this holy tree.");
        next;
        mesn l("Yetifly the Mighty");
        mesq l("We shall be waiting for your arrival.");
        next;
        closeclientdialog;
        gtAccessOK();
        return;
    }
    mesn l("Yetifly the Mighty");
    mesq l("Your persistence is amusing. However, you're yet to acquire the title of %s.", b(l("Friend of the Fairies")));
    next;
    mesn l("Yetifly the Mighty");
    mesq l("Bring me the proof of your devotion, and I shall arrange you an audience with the Queen.");
    return;
}

function gtLocked {
    mesn;
    mesq l("The door is locked.");
    close;
}

function gtAccessOK {
    warp "018-7", 52, 33;
    return;
}

OnInit:
    .sex = G_OTHER;
    .distance = 1;
    end;
}

