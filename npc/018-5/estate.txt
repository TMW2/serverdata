// TMW2: Moubootaur Legends scripts.
// Author:
//    Jesusalva
// Description:
//    Real Estate System

// ID: 7
// $ESTATE_OWNER[.id] → Account ID owner of the Real Estate
// $ESTATE_OWNERNAME$[.id] → Human readable name of Real Estate owner
// $ESTATE_RENTTIME[.id] → When the rent will expire
// $ESTATE_MOBILIA_2[.id] → Bitmask of mobilia currently purchased on Monster Collision (6) (Use on walls only)
// $ESTATE_MOBILIA_4[.id] → Bitmask of mobilia currently purchased on Air Collision (2)
// $ESTATE_MOBILIA_8[.id] → Bitmask of mobilia currently purchased on Water Collision (3)
// $ESTATE_MOBILIA_32[.id] → Bitmask of mobilia currently purchased on Yellow Collision (4)
// $ESTATE_MOBILIA_64[.id] → Bitmask of mobilia currently purchased on Normal Collision (1)
// $ESTATE_MOBILIA_128[.id] → Bitmask of mobilia currently purchased on Player Collision (5)
// $ESTATE_PASSWORD$[.id] → Password to enter the estate. If it is "", then no password required
// Note: GMs and Administrators can always use super password "mouboo" to enter a locked estate
// $ESTATE_DOORBELL[.id] → If doorbell is disabled (enabled by default)

// REAL_ESTATE_CREDITS → Credits equivalent to GP the player have. Will be used first.

// The sign is the main controller
018-5,84,52,0	script	Sign#RES_0185	NPC_SWORDS_SIGN,{
    if ($ESTATE_RENTTIME[.id] < gettimetick(2))
        goto L_RentAvailable;

    if ($ESTATE_OWNER[.id] == getcharid(3))
        goto L_Manage;

    if (is_admin() && $@GM_OVERRIDE)
        goto L_Manage;

    mesc l("This estate currently belongs to @@.", $ESTATE_OWNERNAME$[.id]);
    mesc l("Press the doorbell?");
    next;
    if (askyesno() == ASK_YES)
        doevent "Doorbell#RES_0208::OnDoorbell";
    close;

L_RentAvailable:
    realestate_rent(.id, .price);
    close;

L_Manage:
    realestate_manage(.id, (.price*7/10));
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 3;

    // Estate Settings
    .id=7; // Estate ID
    .price=60000; // Monthly rent price. Renew is only 70% from this value.
    end;

}

// Door entrance
018-5,85,52,0	script	#RES_0185	NPC_HIDDEN,0,0,{
    end;
OnTouch:
    if ($ESTATE_RENTTIME[.id] < gettimetick(2))
        goto L_RentAvailable;

    if ($ESTATE_OWNER[.id] == getcharid(3) || $ESTATE_PASSWORD$[.id] == "")
        goto L_Warp;

    mesc l("The door is locked");
    next;
    mesc l("However, it can be unlocked if you know the password:");
    if (is_gm()) mesc l("You can use super password \"mouboo\" to unlock the door."), 1;
    input .@password$;
    // GMs can use super password "mouboo"
    if (.@password$ == $ESTATE_PASSWORD$[.id] || (is_gm() && .@password$ == "mouboo"))
        goto L_Warp;
    close;

L_Warp:
    warp "018-5-6", 33, 33;
    closeclientdialog;
    close;

L_RentAvailable:
    dispbottom l("This estate is available for rent, talk to the sign to rent it.");
    close;

OnInit:
    // Estate Settings
    .id=7; // Estate ID
    end;

}

