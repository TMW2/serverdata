// TMW-2 script.
// Author:
//    Jesusalva
// Description:
//    More specific Recipe Books in TMW2

// callfunc("TerraniteBlueprint")
function	script	TerraniteBlueprint	{
    // Lucky roll (0.25%)
    if (rand2(2500) < 1) {
        getitem AncientBlueprint, 1;
        dispbottom l("Wait a minute... This is written in Mananese! I can't read!");
        return;
    }

    // Setup
    array_push(.@recipes, CraftRockKnife);       // Weapon: Rock Knife
    array_push(.@recipes, CraftTerraniteArmor);  // TerraniteArmor
    array_push(.@recipes, CraftTerranitePants);  // TerranitePants
    array_push(.@recipes, CraftTerraniteHelmet); // TerraniteHelmet
    //array_push(.@recipes, CraftTerranite); // TerraniteGloves (?)(X)(TODO)
    //array_push(.@recipes, CraftTerranite); // TerraniteBoots (?)(X)(TODO)

    // Now you'll learn some recipe!
    .@rcp=any_of(.@recipes);

	// Double precision failsafe
    if (RECIPES_EQUIPMENT[.@rcp])
        .@rcp=any_of(.@recipes);

	// Maybe you already knew it?
    if (RECIPES_EQUIPMENT[.@rcp]) {
        .@mpot=rand2(2000, 5000);
        dispbottom l("It was a recipe you already knew... (+ @@ Mobpt)", .@mpot);
        getexp (BaseLevel+JobLevel)*rand2(1,.@rarity), JobLevel+rand2(1,.@rarity);
        // Give you some Monster Points to use with Intense Beard
        // You do NOT need to be registered with Aidan for this.
        Mobpt+=.@mpot;
    } else {
        dispbottom l("Learned a new recipe!");
        RECIPES_EQUIPMENT[.@rcp]=true;
    }
    return;
}
/////////////////////////////////////////////////////////////////////////////////

// callfunc("LegendaryBlueprint")
function	script	LegendaryBlueprint	{
    // Unlucky roll (0.25%)
    if (rand2(2500) < 1) {
        getitem AncientBlueprint, 1;
        dispbottom l("Wait a minute... This is written in Mananese! I can't read!");
        return;
    }

    // Setup
    array_push(.@recipes, CraftSkypiercer);         // Weapon: Sky Piercer
    array_push(.@recipes, CraftSaviorShield);       // Shield: Savior Shield
    //array_push(.@recipes, CraftSaviorArmor);        // Savior Armor
    //array_push(.@recipes, CraftSaviorPants);        // Savior Pants
    //array_push(.@recipes, CraftSaviorBoots);        // Savior Boots
    //array_push(.@recipes, CraftSavior);             // Savior Helmet
    //array_push(.@recipes, CraftSavior);             // Savior Gloves (?)(X)

    // Now you'll learn some recipe!
    .@rcp=any_of(.@recipes);

	// Double precision failsafe
    if (RECIPES_EQUIPMENT[.@rcp])
        .@rcp=any_of(.@recipes);

	// Maybe you already knew it?
    if (RECIPES_EQUIPMENT[.@rcp]) {
        .@mpot=rand2(200000, 500000);
        dispbottom l("It was a recipe you already knew... (+ @@ Mobpt)", .@mpot);
        getexp rand(100000, 200000), rand(10000, 25000);
        // Give you some Monster Points to use with Intense Beard
        // You do NOT need to be registered with Aidan for this.
        Mobpt+=.@mpot;
    } else {
        dispbottom l("Learned a new recipe!");
        RECIPES_EQUIPMENT[.@rcp]=true;
    }
    return;
}


