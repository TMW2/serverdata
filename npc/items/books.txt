// TMW-2 script.
// Author:
//    Jesusalva
//    gumi
//    Tirifto
// Description:
//    Books used by TMW-2. Some are from evol.

function	script	FishingBook	{
    narrator S_FIRST_BLANK_LINE | S_LAST_NEXT,
        l("To get started with fishing, you'll need two things: a fishing rod and a bait."),
        l("You just need one fishing rod, although you should take more than one single bait.");

    @menu = 0; // reset for the rif

    do
    {
        narrator S_NO_NPC_NAME,
            l("Please select a chapter:");

        mes "";

        select
            rif2(1, true, l("Ch 1 — Fishing apparatus")),
            rif2(2, true, l("Ch 2 — Baits")),
            rif2(3, true, l("Ch 3 — Location")),
            rif2(4, true, l("Ch 4 — Casting")),
            rif2(5, true, l("Ch 5 — Reeling")),
            l("Close");
        mes "";

        switch(@menu)
        {
            case 1:
                narrator S_LAST_NEXT,
                    l("You'll want your fishing rod to be flexible but solid."),
                    l("Comfortable grip is important especially for newcomers, since they'll be holding it for quite a while.");
                break;
            case 2:
                narrator S_LAST_NEXT,
                    l("You can use many diverse items to lure fishes."),
                    l("Most common and widely popular in the fish realm are @@ and pieces of @@.",
                        getitemlink(SmallTentacles), getitemlink(Bread)),
                    l("Some types of fish also enjoy @@ quite a bit.",
                        getitemlink(Aquada)),
                    l("Some people, however, prefer to fish with more unorthodox baits, such as @@ or @@.",
                        getitemlink(RoastedMaggot), getitemlink(CaveSnakeTongue)),
                    l("Other food can be used as a bait, too.");
                break;
            case 3:
                narrator S_LAST_NEXT,
                    l("Find yourself a nice dry spot on a coast where you can easily reach into deep water."),
                    l("Fishing next to shallow water is not going to work well, because fishes seldom go there."),
                    l("You can easily identify fishing spots, small bubbles and fishes are visible from the surface."),
                    l("Don't forget to come as close as possible to these spots!");
                break;
            case 4:
                narrator S_LAST_NEXT,
                    l("Toss the hook into deep water by clicking on where you want to cast it."),
                    l("Make sure to put on a bait after you click, though!"),
                    l("After that, stay still and be patient, but also alert!");
                break;
            case 5:
                narrator S_LAST_NEXT,
                    l("To successfully catch a fish, you need to pull up your hook by clicking it, right after it submerges."),
                    l("Should you be too quick or wait too long, you will most likely fail.");
                break;
        }
    } while (@menu != 6);
    return;
}

-	script	#Book-Fishing1	NPC_HIDDEN,{

    function read_book {
        setnpcdialogtitle l(.book_name$);
        FishingBook();
        closeclientdialog;
        end;
    }

OnShelfUse:
    if (openbookshelf())
        read_book;
    bye;

OnUse:
    if (openbook())
        read_book;
    bye;

OnInit:
    .book_name$ = getitemname(FishingGuideVolI);
    .sex = G_OTHER;
    .distance = 1;
    end;
}



function	script	PetcaringBook	{
    select
        l("General Information"),
        l("Pet Summary");
    mes "";
    if (@menu == 1) {
        narrator 1,
    l("So you have now a pet, who is loyal to you. It'll follow you everywhere, but there are two things you must know."),
    l("Do not let intimacy and hunger get to zero. If any of those get to zero, it'll leave you forever."),
    l("Pets must keep a strict diet. Pious eats Piberries, Bhoppers eat Aquadas, and Maggots eats Bug Legs."),
    l("White Cats drink Milk, Forest Mushroom eats Moss, Black Cats eats marshmallow. Keep in mind whatever they eat."),
    l("However, you should only give food when it's hungry, otherwise it'll believe you're a bad owner and intimacy will decrease."),
    l("Dying will also decrease the pet intimacy, and there are bonuses when your intimacy is high!"),
    l("To perform most actions, like feeding and renaming, just right-click it. You can even put it back on the egg if its following gets too annoying. When in the egg, they will not feel hunger."),
    l("Give your pet a nice name, and keep it healthy, and you'll be a successful pet owner!"),
    l("Some pets will also collect loot for you, right click on it so it drop whatever it is holding for you."),
    l("...And if you're still trying to check your pet stats, just hover it with your mouse. Thanks."),
    l("-- Animals Protection Agency of Hurnscald");
    } else {
        mes l("%s", getitemlink(PiouEgg));
        mesc b(l("Acquisition: ")) + l("Login Bonus");
        mesc b(l("Food: ")) + getitemlink(Piberries);
        mesc b(l("Bonus: ")) + l("Loot 10, Luck +2, Luck Dance");

        dnext;
        mes "";
        mes l("%s", getitemlink(Ratte));
        mesc b(l("Acquisition: ")) + l("Unobtanium");
        mesc b(l("Food: ")) + getitemlink(Cheese);
        mesc b(l("Bonus: ")) + l("Loot 3, Dex +5");

        dnext;
        mes "";
        mes l("%s", getitemlink(DuckEgg));
        mesc b(l("Acquisition: ")) + l("Grand Hunter Quest");
        mesc b(l("Food: ")) + getitemlink(CherryCake);
        mesc b(l("Bonus: ")) + l("Loot 3, Steal +15%, Passive HP Regen (1)");

        dnext;
        mes "";
        mes l("%s", getitemlink(FluffyEgg));
        mesc b(l("Acquisition: ")) + l("Grand Hunter Quest");
        mesc b(l("Food: ")) + getitemlink(LettuceLeaf);
        mesc b(l("Bonus: ")) + l("Loot 3, Max MP +250");

        dnext;
        mes "";
        mes l("%s", getitemlink(MaggotCocoon));
        mesc b(l("Acquisition: ")) + l("Grand Hunter Quest");
        mesc b(l("Food: ")) + getitemlink(BugLeg);
        mesc b(l("Bonus: ")) + l("Loot 3, Max HP +250");

        dnext;
        mes "";
        mes l("%s", getitemlink(BatEgg));
        mesc b(l("Acquisition: ")) + l("Grand Hunter Quest");
        mesc b(l("Food: ")) + getitemlink(RoastedMaggot);
        mesc b(l("Bonus: ")) + l("Loot 3, ASPD +5%");

        dnext;
        mes "";
        mes l("%s", getitemlink(ForestShroomEgg));
        mesc b(l("Acquisition: ")) + l("Grand Hunter Quest");
        mesc b(l("Food: ")) + getitemlink(Moss);
        mesc b(l("Bonus: ")) + l("Loot 3, STR +4, AGI +1");

        dnext;
        mes "";
        mes l("%s", getitemlink(MoggunEgg));
        mesc b(l("Acquisition: ")) + l("Grand Hunter Quest");
        mesc b(l("Food: ")) + getitemlink(MoubooSteak);
        mesc b(l("Bonus: ")) + l("Loot 3, DEF +5");

        dnext;
        mes "";
        mes l("%s", getitemlink(TamedSnakeEgg));
        mesc b(l("Acquisition: ")) + l("Grand Hunter Quest");
        mesc b(l("Food: ")) + getitemlink(MoubooSteak);
        mesc b(l("Bonus: ")) + l("Loot 3, Evasion +7");

        dnext;
        mes "";
        mes l("%s", getitemlink(AggressiveSnakeEgg));
        mesc b(l("Acquisition: ")) + l("Grand Hunter Quest");
        mesc b(l("Food: ")) + getitemlink(FluoPowder);
        mesc b(l("Bonus: ")) + l("Loot 3, Evasion +5, Str +1");

        dnext;
        mes "";
        mes l("%s", getitemlink(DragonHorn));
        mesc b(l("Acquisition: ")) + l("Grand Hunter Quest");
        mesc b(l("Food: ")) + getitemlink(Dragonfruit);
        mesc b(l("Bonus: ")) + l("Loot 4, Str +1, Gold Drop (4%)");

        dnext;
        mes "";
        mes l("%s", getitemlink(BhopEgg));
        mesc b(l("Acquisition: ")) + l("Easter Top 1 Prize");
        mesc b(l("Food: ")) + getitemlink(Aquada);
        mesc b(l("Bonus: ")) + l("Loot 3, Luck +5, Luck Dance, Passive MP Regen (1)");

        dnext;
        mes "";
        mes l("%s", getitemlink(DoggyDog));
        mesc b(l("Acquisition: ")) + l("Valentine Top 1 Prize");
        mesc b(l("Food: ")) + getitemlink(AnimalBones);
        mesc b(l("Bonus: ")) + l("Loot 3, STR +5, Vitality Dance, Passive HP Regen (1)");

        dnext;
        mes "";
        mes l("%s", getitemlink(CattyCat));
        mesc b(l("Acquisition: ")) + l("Christmas Top 1 Prize");
        mesc b(l("Food: ")) + getitemlink(Milk);
        mesc b(l("Bonus: ")) + l("Loot 3, Agi +5, Agility Dance, Passive MP Regen (1)");

        dnext;
        mes "";
        mes l("%s", getitemlink(BlackyCat));
        mesc b(l("Acquisition: ")) + l("Magic Olympics Top 1 Prize");
        mesc b(l("Food: ")) + getitemlink(Mashmallow);
        mesc b(l("Bonus: ")) + l("Loot 3, Int +5, Agi +1");

        dnext;
        mes "";
        mes l("%s", getitemlink(PinkieCrystal));
        mesc b(l("Acquisition: ")) + l("Ultra Rare Drop");
        mesc b(l("Food: ")) + getitemlink(CherryCake);
        mesc b(l("Bonus: ")) + l("Loot 3, Agi +5");

        next;
        mes l("-- Animals Protection Agency of Hurnscald");
    }
    return;
}

-	script	#Book-Petcaring	NPC_HIDDEN,{
    function read_book {
        PetcaringBook();
        close;
    }

OnShelfUse:
    @book_name$ = .bookname$;
    if (openbookshelf ())
        read_book;
    close;
OnUse:
    @book_name$ = .bookname$;
    if (openbook ())
        read_book;
    close;
OnInit:
    .bookname$ = "Fluffy Animals who Love Their Owners";
    .sex = G_OTHER;
    .distance = 1;
    end;
}























-	script	#Book-JGrimorium	NPC_HIDDEN,{
    end;

function myself {
    // TODO: Save the variables in temp vars
    // If getarg(1) is not your charid, detach
    // Then attach the getarg(1) instead
    // Display then using the temp vars
    // For scrying, myself(false, .@me)
    // While still using attachrid
    .@all = getarg(0, true);
    .@who = getarg(1, getcharid(3));
    .@why = getcharid(3);
    .@adm = is_admin();

    /* Basic Data */
    detachrid();
    attachrid(.@who);
    // No scrying
    if (GSET_NOSCRY && .@who != .@why && !.@adm) {
        detachrid();
        attachrid(.@why);
        return 1;
    }
    // Basic data
    .@name$ = strcharinfo(0, "error", .@who);
    .@staff = is_staff();
    .@sponsor = is_sponsor();
    .@party$ = (getcharid(1) ? strcharinfo(1) : "");
    .@guild$ = (getcharid(2) ? strcharinfo(2) : "");
    .@clan$  = (getcharid(5) ? strcharinfo(4) : "");
    .@married = getpartnerid();
    .@legend = islegendary();
    if (getpetinfo(0)) {
        .@pet = true;
        .@pet_name$ = getpetinfo(2);
        .@pet_type$ = getpetinfo(1);
    } else {
        .@pet = false;
    }
    if (gethominfo(0)) {
        .@homun = true;
        .@hc_name$ = gethominfo(2);
    } else {
        .@homun = false;
    }
    .@born = #REG_DATE;
    detachrid();
    attachrid(.@why);

    mes ".:: " + .@name$ + " ::.";
    if (.@staff)
        mesc l("%s is currently a staff member.", .@name$), 3;
    else if (.@sponsor)
        mesc l("%s is currently sponsoring the High Alliance.", .@name$), 3;
    mes "";
    if (.@party$ != "")
        mesc l("Party Name: @@", .@party$);
    if (.@guild$ != "")
        mesc l("Guild Name: @@", .@guild$);
    if (.@clan$ != "")
        mesc l("Clan Name: @@", .@clan$);
    if (.@married)
        mesc l("Civil status: Married");
    else
        mesc l("Civil status: Single");
    if (.@legend)
        mesc l("%s is a legendary hero.", .@name$), 2;
    if (.@pet)
        mesc l("Proud owner of %s the %s.", .@pet_name$, .@pet_type$);
    if (.@homun)
        mesc l("Proud owner of %s the Homunculus.", .@hc_name$);
    mesc l("Born %s ago", FuzzyTime(.@born));
    dnext;

    /* Magic Data */
    detachrid();
    attachrid(.@who);
    .@lvl = MAGIC_LVL;
    .@rank$ = academicrank();
    if (.@all) {
        .@rp$ = fnum(MAGIC_RP);
        .@sp  = sk_points();
        .@msp = sk_maxpoints();
    } else {
        .@rp$ = "?";
    }
    detachrid();
    attachrid(.@why);

    mes ".:: " + l("Magic Status") + " ::.";
    mesc l("Current magic rank: %d", .@lvl);
    if (.@all)
     mesc l("You have @@/@@ magic skill points available.",
            b(.@sp), .@msp);
    mesc l("Your current scholar rank: %s (%s Research Points)",
            .@rank$, .@rp$);
    if (.@who == .@why)
        ShowAbizit(true);
    dnext;


    /* Rogue Data */
    detachrid();
    attachrid(.@who);
    .@rank$ = thiefrank();
    if (.@all) {
        .@exp = THIEF_EXP;
        .@rank= THIEF_RANK;
    }
    detachrid();
    attachrid(.@why);

    mes ".:: " + l("Rogue Status") + " ::.";
    mesc l("Your current rank: %s", .@rank$);
    if (.@all && .@exp > (.@rank*2)**5)
        mesc l("An upgrade is available."), 2;
    dnext;


    /* Craft Data */
    detachrid();
    attachrid(.@who);
    .@skill = getskilllv(TMW2_CRAFT);
    .@score = CRAFTING_SCORE_COMPLETE/40;
    .@milis = (CRAFTING_SCORE_COMPLETE%40)*25/10; // <- or * 2.5 because base
    detachrid();
    attachrid(.@why);

    mes ".:: " + l("Crafting Status") + " ::.";
    mesc l("Skill Level: %d", .@skill);
    mesc l("Crafting Score: %d.%02d", .@score, .@milis);
    dnext;




    /* Misc Data */
    detachrid();
    attachrid(.@who);
    if (.@all) {
        .@mpt = Mobpt;
        .@gp = (Zeny+BankVault);
    }
    .@die = PC_DIE_COUNTER;
    .@reborn = REBIRTH;
    .@honor = HONOR;
    .@kills = MONSTERS_KILLED;
    .@gid = getcharid(2);
    .@afk_h = AFKING/1200;
    .@afk_m = AFKING%1200/60*3;
    detachrid();
    attachrid(.@why);

    mes ".:: " + l("Miscellaneous Status") + " ::.";
    if (.@all)
     mesc l("Monster Points: %s", fnum(.@mpt));
    mesc l("Times died: %s", fnum(.@die));
    mesc l("Times reborn: %d", .@reborn);
    if (.@all)
     mesc l("Total Gold: %s", fnum(.@gp));
    mesc l("Honor Points: %s", fnum(.@honor));
    mesc l("Monsters killed: %s", fnum(.@kills));
    if (.@gid > 0) {
        .@pos=getguildrole(.@gid, .@who);
        mesc l("Current Guild: %s", getguildname(.@gid));
        mesc l("Guild Master: @@", getguildmaster(.@gid));
        if (.@all)
         mesc l("You are the guild's \"%s\", and you contribute with %02d%% EXP.",
                getguildpostitle(.@gid, .@pos),
                getguildpostax(.@gid, .@pos));
    }
    mesc l("Total time AFK'ed in Tulimshar: %d hours and %d minutes",
            .@afk_h, .@afk_m);
    if (.@all) {
        .@trait$ = "";
        if (PCBONUS & PCB_ATKBONUS) .@trait$ += "ATK+ ";
        if (PCBONUS & PCB_DEFBONUS) .@trait$ += "DEF+ ";
        if (PCBONUS & PCB_EVDBONUS) .@trait$ += "EVA+ ";
        if (PCBONUS & PCB_HITBONUS) .@trait$ += "HIT+ ";
        if (PCBONUS & PCB_HPBONUS) .@trait$ += "MHP+ ";
        if (PCBONUS & PCB_MPBONUS) .@trait$ += "MMP+ ";
        if (PCBONUS & PCB_MATKBONUS) .@trait$ += "MATK+ ";
        if (PCBONUS & PCB_MDEFBONUS) .@trait$ += "MDEF+ ";
        if (PCBONUS & PCB_CRITBONUS) .@trait$ += "CRI+ ";
        if (PCBONUS & PCB_ALLSTATS) .@trait$ += "STAT+ ";
        if (PCBONUS & PCB_ASPDBONUS) .@trait$ += "ASPD+ ";
        if (PCBONUS & PCB_WSPDBONUS) .@trait$ += "WALK+ ";
        if (PCBONUS & PCB_WEIGHTBONUS) .@trait$ += "WEIGHT+ ";
        if (PCBONUS & PCB_RANGEMASTER) .@trait$ += "RANGE+ ";
        if (PCBONUS & PCB_EXPBONUS) .@trait$ += "EXP+ ";
        if (PCBONUS & PCB_NOKNOCKBACK) .@trait$ += "No-Knockback ";
        if (PCBONUS & PCB_DOUBLEATK) .@trait$ += "Double-Attack ";
        if (PCBONUS & PCB_SPLASHMASTER) .@trait$ += "AoE ";
        if (PCBONUS & PCB_LEGENDARY) .@trait$ += "Legendary ";
        mesc l("Traits: %s", .@trait$);
    }
    dnext;


    /* Records Data */
    detachrid();
    attachrid(.@who);
    .@candor = CRAZYPOINTS;
    .@bloodbath = gettimetick(2)+SCANDORPTS;
    .@ctf = CAPTURE_FLAG;
    .@cod = getq2(LoFQuest_COD);
    .@merc = MERCENARY_DAILYQUEST;
    .@udt = UDTRANK;
    .@egg = getq3(General_EasterEggs);
    detachrid();
    attachrid(.@why);
    mes ".:: " + l("Personal Records") + " ::.";
    mesc l("Candor Battle Score: %s", fnum(.@candor));
    mesc l("Candor Bloodbath Score: %s", FuzzyTime(.@bloodbath));
    mesc l("Times won Capture the Flag: %s", fnum(.@ctf));
    mesc l("Times won Call of Dusty: %s", fnum(.@cod));
    mesc l("Mercenary Quests completed: %s", fnum(.@merc));
    mesc l("Doppelganger Waves Won: %s", fnum(.@udt));
    mesc l("Easter Eggs found: %d", .@egg);
    dnext;


    /* Feat Data */
    detachrid();
    attachrid(.@who);
    .@yeti = YETIKING_WINNER;
    .@hh = HEROESHOLD_WINNER;
    .@reborn = REBIRTH_WINNER;
    .@quirin = QUIRINO_WINNER;
    .@gemini = GEMINI_WINNER;
    .@ghq = GHQ_WINNER;
    .@doct = EPISODE_WINNER;
    .@fort = FORT_1ST_VISIT;
    .@seal = MOUBOOTAUR_WINNER;
    .@king = MK_WINNER;
    detachrid();
    attachrid(.@why);
    mes ".:: " + l("Personal Feats") + " ::.";
    if (.@yeti)
        mesc l("Cleared the Yeti King Challenge %s ago", FuzzyTime(.@yeti));
    if (.@hh)
        mesc l("Cleared Heroes Hold %s ago", FuzzyTime(.@hh));
    if (.@gemini)
        mesc l("Cleared Gemini Sisters Quest %s ago", FuzzyTime(.@gemini));
    if (.@reborn)
        mesc l("First reborn %s ago", FuzzyTime(.@reborn));
    if (.@quirin)
        mesc l("Won Quirino Voraz Arena %s ago", FuzzyTime(.@quirin));
    if (.@ghq)
        mesc l("First Grand Hunter challenge cleared %s ago", FuzzyTime(.@ghq));
    if (.@doct)
        mesc l("Completed The Episode of Ozthokk %s ago", FuzzyTime(.@doct));
    if (.@fort)
        mesc l("First visit to Fortress Is. %s ago", FuzzyTime(.@fort));
    if (.@seal)
        mesc l("Defeated the Moubootaur (Sealed) %s ago", FuzzyTime(.@seal));
    if (.@king)
        mesc l("Defeated the Monster King %s ago", FuzzyTime(.@king));

    /* Heroic Data */
    detachrid();
    attachrid(.@who);
    .@candor = (reputation("Candor") >= 100);
    .@tulim = (reputation("Tulim") >= 100);
    .@halin = (reputation("Halin") >= 100);
    .@hurns = (reputation("Hurns") >= 100);
    .@lof   = (reputation("LoF") >= 100);
    .@nival = (reputation("Nival") >= 100);
    .@frost = (reputation("Frostia") >= 100);
    .@forte = (reputation("Fortress") >= 100);
    detachrid();
    attachrid(.@why);
    if (.@candor)
        mesc l("%s Hero", l("Candor"));
    if (.@tulim)
        mesc l("%s Hero", l("Tulimshar"));
    if (.@halin)
        mesc l("%s Hero", l("Halinarzo"));
    if (.@hurns)
        mesc l("%s Hero", l("Hurnscald"));
    if (.@lof)
        mesc l("%s Hero", l("Land Of Fire"));
    if (.@nival)
        mesc l("%s Hero", l("Nivalis"));
    if (.@frost)
        mesc l("%s Hero", l("Frostia"));
    if (.@forte)
        mesc l("%s Hero", l("Fortress Town"));
    // TODO: Total players invited to ML
    // TODO: Houses owned
    // TODO: Times elected
    // TODO: Admin of how many towns?
    // TODO: First election won date
    // TODO: Most used skill
    // TODO: Remember the position attained on previous events
    //mes ".:: " + l("Achievements") + " ::.";
    return 0;
}

OnScry:
    // Lack the book
    if (!countitem(JesusalvaGrimorium) && !is_staff())
        end;
    // Doesn't owns the book
    if (ST_TIER < 10 && getskilllv(TMW2_SKILLPERMIT) < 3 && !is_staff())
        end;
    .@w$ = implode(.@atcmd_parameters$, " ");
    if (.@w$ == "" || .@w$ == "NULL")
        .@w$ = strcharinfo(0);
    .@me = getcharid(3);
    .@id = getcharid(3, .@w$);
    if (!.@id) {
        mesc l("The requested char \"%s\" is not online or does not exist.", .@w$), 1;
        close;
    }
    /* attachrid() + mes() is a no-go for this */
    setnpcdialogtitle sprintf("@scry %s", .@w$);
    if (myself(is_admin(), .@id)) {
        mesc l("%s has protected themselves from prying eyes. Your scry attempt failed.", .@w$), 1;
    }
    close;

function read_book {
    // Doesn't owns the book
    if (ST_TIER < 10 && getskilllv(TMW2_SKILLPERMIT) < 3) {
        percentheal 0, -20;
        dispbottom l("The book rejects you, draining your mana!");
        end;
    }

    setnpcdialogtitle l(.book_name$);

    narrator S_FIRST_BLANK_LINE | S_LAST_NEXT,
        l("I, second sage of Fate, write this book. The knowledge on it shall guide you to the Secret Of Mana.");

    @menu = 0; // reset for the rif

    do
    {
        narrator S_NO_NPC_NAME,
            l("Please select a chapter:");

        mes "";

        select
            rif2(1, MAGIC_LVL, l("Ch 1 — Prologue")),
            rif2(2, MAGIC_EXP, l("Ch 2 — About Magic Skills")),
            rif2(3, MAGIC_LVL, l("Ch 3 — Status Ailments")),
            rif2(4, true, l("Ch 4 — Information About You")),
            rif2(5, true, l("Ch 5 — Information About Others")),
            rif2(6, true, l("Open Fishing Book")),
            rif2(7, true, l("Open Petcaring Book")),
            rif2(8, getq(LoFQuest_Pets), l("List of Unlocked Pets and Food")),
            rif2(9, CRAFTQUEST, l("Open Recipe Book")),
            rif2(10, true, l("Read Rules")),
            rif2(11, getskilllv(TMW2_COOKING), l("Open Cooking Manual")),
            rif2(12, getq(General_Narrator) >= 16, l("Open Homunculus Manual")),
            l("Close");
        mes "";

        switch(@menu)
        {
            case 1:
                mesc l("You have @@/@@ magic skill points available.", b(sk_points()), sk_maxpoints());
                mesc l("Your current scholar rank: %s (%d Research Points)",
        academicrank(), fnum(MAGIC_RP));
                next;
                narrator S_LAST_NEXT,
                    l("Mana is something which existed since the being, but nobody knows much about."),
                    l("This book will write itself, and reveal you the Secret Of Mana."),
                    l("Give it time, increase your magic power, and you'll find out the truth."),
                    l("You are a @@º degree mage. This book allows you many new possibilities.", MAGIC_LVL);
                break;
            case 2:
                narrator S_LAST_NEXT,
                    l("Re-casting the same magic spell or skill won't give you magic experience."),
                    l("Summoning and Homunculus (H) skills can be raised from skill window directly.");
                // TODO: We could show you all the skills via loop? Meh

                ShowAbizit(true);
                next;
                break;
            case 3:
                mes l("There are several minor status conditions, which may buff or debuff you.");
                mes l("An example is dec agi, which lowers your agility.");
                mes l("The most aggressive and main ones are:");
                next;
                mesf("##B%s##b - %s", l("Blind"), l("Acc. and Evade -25%%"));
                mesf("##B%s##b - %s", l("Burning"), l("Damage over time, MDF -25%%"));
                mesf("##B%s##b - %s", l("Curse"), l("ATK = 25%%, LUK = 0, Slow down"));
                mesf("##B%s##b - %s", l("Freeze"), l("Can't move, DEF-, no evade, Water element"));
                mesf("##B%s##b - %s", l("Poison"), l("DEF-, Damage over time, no MP regen"));
                mesf("##B%s##b - %s", l("Silence"), l("Can't use skills"));
                mesf("##B%s##b - %s", l("Sleep"), l("Can't move nor attack, crit def -100%"));
                mesf("##B%s##b - %s", l("Stone"), l("Can't move, DEF-, Damage over time, Earth element"));
                mesf("##B%s##b - %s", l("Stun"), l("Can't move nor evade."));
                next;
                mes l("There are also less common ailments:");
                next;
                mesf("##B%s##b - %s", l("Bleed"), l("Lethal damage over time, no regen."));
                mesf("##B%s##b - %s", l("Confuse"), l("Random movement and extra desync"));
                mesf("##B%s##b - %s", l("Cold"), l("Total slow down, DEF-, may freeze"));
                mesf("##B%s##b - %s", l("Deadly Poison"), l("Lower MAXHP, Damage over time"));
                mesf("##B%s##b - %s", l("Deep Sleep"), l("Can't chat, recover HP over time"));
                mesf("##B%s##b - %s", l("Fear"), l("Can't move, Acc. and Evade -20%%"));
                next;
                break;
            case 4:
                myself();
                break;
            case 5:
                mesc l("You can scry other players with: %s", b("@scry"));
                mesc l("To scry Jesusaves, for example, you would do:");
                mes b("@scry Jesusaves");
                mes "";
                mesc l("- Players can prevent being scry'ed with %s", b("@ucp"));
                mesc l("- Some information like money won't be available.");
                mesc l("- Target player must be online.");
                next;
                break;
            case 6:
                FishingBook();
                break;
            case 7:
                PetcaringBook();
                break;
            case 8:
                mesf(".:: %s ::.", l("Grand Hunter Quest"));
                if (PDQ_CheckGHQ(Maggot) >= 10000)
                    mesf("%s - %s",
                        getmonsterlink(Maggot), getitemlink(BugLeg));
                if (PDQ_CheckGHQ(ForestMushroom) >= 10000)
                    mesf("%s - %s",
                        getmonsterlink(ForestMushroom), getitemlink(Moss));
                if (PDQ_CheckGHQ(Fluffy) >= 10000)
                    mesf("%s - %s",
                        getmonsterlink(Fluffy), getitemlink(LettuceLeaf));
                if (PDQ_CheckGHQ(Duck) >= 10000)
                    mesf("%s - %s",
                        getmonsterlink(Duck), getitemlink(CherryCake));
                if (PDQ_CheckGHQ(Bat) >= 10000)
                    mesf("%s - %s",
                        getmonsterlink(Bat), getitemlink(RoastedMaggot));
                if (PDQ_CheckGHQ(Moggun) >= 10000)
                    mesf("%s - %s",
                        getmonsterlink(Moggun), getitemlink(MoubooSteak));
                if (#LOGIN_ALLTIME >= 6)
                    mesf("%s - %s",
                        getmonsterlink(Piou), getitemlink(Piberries));
                next;
                mesf(".:: %s ::.", l("Special Event Pets"));
                if (countitem(Ratte))
                    mesf("%s - %s",
                        getmonsterlink(Ratto), getitemlink(Cheese));
                if (countitem(BhopEgg))
                    mesf("%s - %s",
                        getmonsterlink(BhopFluffy), getitemlink(Aquada));
                if (countitem(DoggyDog))
                    mesf("%s - %s",
                        getmonsterlink(Toto), getitemlink(AnimalBones));
                if (countitem(CattyCat))
                    mesf("%s - %s",
                        getmonsterlink(WhiteCat), getitemlink(Milk));
                if (countitem(BlackyCat))
                    mesf("%s - %s",
                        getmonsterlink(BlackCat), getitemlink(Mashmallow));
                next;
                break;
            case 9:
                closeclientdialog;
                doevent("#RecipeBook::OnUse");
                end;
                break;
            case 10:
                GameRules();
                break;
            case 11:
                closeclientdialog;
                doevent("Ashley Loisl::OnCookHelp");
                end;
            case 12:
                closeclientdialog;
                doevent("Notebook#MKHB::OnBookRead");
                end;
            default:
                close;
        }
    } while (true);

    end;
}

OnShelfUse:
    if (openbookshelf())
        read_book();
    bye;

OnUse:
    read_book;
    bye;

OnInit:
    .book_name$ = getitemname(JesusalvaGrimorium);
    .sex = G_OTHER;
    .distance = 1;
    bindatcmd "scry", "#Book-JGrimorium::OnScry", 0, 0, 0;
    end;
}

