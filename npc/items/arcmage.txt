// TMW-2 script.
// Author:
//    Jesusalva
// Description:
//    Card boxsets, shout out for arcmage.org

// Create a random card, with rares
function	script	MakeRandomArcmageCard	{
    array_push(.@arcmagecards, NatureCard);
    array_push(.@arcmagecards, NinjaCard);
    array_push(.@arcmagecards, MageCard);
    array_push(.@arcmagecards, DruidCard);
    array_push(.@arcmagecards, ClericCard);
    array_push(.@arcmagecards, KnightCard);
    array_push(.@arcmagecards, HeroCard);
    array_push(.@arcmagecards, NecromancerCard);

    .@r=rand(0,10000);
    // 5% chances of a rare card
    // Actually only 1/3 of this rate is effective
    // Therefore real chances are of about 1.67%
    if (.@r < 500+(JobLevel*2)) {
        array_push(.@arcmagecards, SpeedCard);
        array_push(.@arcmagecards, ReflectCard);
        array_push(.@arcmagecards, PowerCard);
        array_push(.@arcmagecards, WallCard);
    }
    // You may get the S Card if you are lucky.
    // However, less than 1/2 of this rate is
    // effective, and it is not so flexible.
    // 0.05% at start and +0.1% per rebirth
    if (.@r < 5+(REBIRTH*10)) {
        array_push(.@arcmagecards, NatureCardS);
        array_push(.@arcmagecards, NinjaCardS);
        array_push(.@arcmagecards, MageCardS);
        array_push(.@arcmagecards, DruidCardS);
        array_push(.@arcmagecards, ClericCardS);
        array_push(.@arcmagecards, KnightCardS);
        array_push(.@arcmagecards, HeroCardS);
        array_push(.@arcmagecards, NecromancerCardS);
        // These cards should NOT drop their S versions
        array_push(.@arcmagecards, SpeedCard);
        array_push(.@arcmagecards, ReflectCard);
        array_push(.@arcmagecards, PowerCard);
        array_push(.@arcmagecards, WallCard);
    }

    getitem any_of(.@arcmagecards), 1;
    return;
}

