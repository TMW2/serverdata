// TMW2 scripts.
// Authors:
//    Jesusalva
//    TMW Org.
// Description:
//    Resting workers

020-7,46,30,6	script	Miro	NPC_BLUESAGEWORKER_MA,{
    function askQuestion;
    mesn;
    mesq l("I'm so tired... Slimes everywhere... Chaos everywhere... Yawn...");
    next;
    mesn;
    mesq l("After cleaning for twelve hours straight, they allowed me to rest a bit.");
    .@q=getq(NivalisQuest_BlueSage);
    if (BSQuestion(.@q))
        askQuestion(BSQuestion(.@q));
    if (.@q == 12) {
        next;
        mesn;
        mesq l("You did a good job too. Rest a bit, too. There's still one chair.");
    }
    close;

function askQuestion {
    .@qs=getarg(0);
    do {
        .@q=getq(NivalisQuest_BlueSage);
        .@q2=getq2(NivalisQuest_BlueSage);
        .@q3=getq3(NivalisQuest_BlueSage);
        next;
        select
            rif(.@qs & BS_QVISITOR, l("Do you know anything about the strange visitor?")),
            rif(.@qs & BS_QHELPER, l("What's your opinion of Peetu and his work?")),
            any(l("I need to leave."), l("See you."), l("Bye."));
        mes "";
        switch (@menu) {
            case 1:
                mesn;
                mesq l("With a mask? I really don't know. I'm usually in the workshop and most of the visitors spend their time in the library. Sometimes they come to have a quick look in the workshop too, but I don't really pay attention to that, so... I can't recall a visitor with a mask, sorry.");
                break;
            case 2:
                mesn;
                mesq l("Peetu? He is very cautious. It is not like him to make mistakes. After all, he is a elf. Elves are really careful with their jobs.");
                next;
                mesn;
                mesq l("If Nikolai's helpers weren't so carefully chosen, I'd think this was some kind of a bad joke from someone.");
                if (!(.@q3 & .bsId))
                    setq3 NivalisQuest_BlueSage, .@q3 | .bsId;
                break;
        }
    } while (@menu != 3);
    close;
}

OnInit:
    .bsId=BS_NPC01;
    .sex=G_MALE;
    .distance=5;
    npcsit;
    end;
}










































020-7,50,31,2	script	Lenita	NPC_BLUESAGEWORKER_FA,{
    function askQuestion;
    .@q=getq(NivalisQuest_BlueSage);
    // Teuvo said you're helping to collect some of the missing bookpages to repair the books. That's very kind of you! It's so much work to do, so every bit of help is welcome.
    // Wow, Ensio told me you collected so many of the lost book pages.
    if (.@q == 12) {
        mesn;
        mesq l("You did a good job. Rest a bit. There's still one chair.");
        close;
    }
    mesn;
    mesq l("Oh, hello. You didn't choose a good time to visit. The library is a total mess. The slimes got out of control and ate most of the books.");
    next;
    mesn;
    mesq l("We're trying to repair some of the valuable and important books. It's so much work!");
    next;
    mesn;
    mesq l("I was up until late last night, and woke up so early today... I'm having a break right now.");
    if (BSQuestion(.@q))
        askQuestion(BSQuestion(.@q));
    close;

function askQuestion {
    .@qs=getarg(0);
    do {
        .@q=getq(NivalisQuest_BlueSage);
        .@q2=getq2(NivalisQuest_BlueSage);
        .@q3=getq3(NivalisQuest_BlueSage);
        next;
        select
            rif(.@qs & BS_QVISITOR, l("Do you know anything about the strange visitor?")),
            rif(.@qs & BS_QHELPER, l("What's your opinion of Peetu and his work?")),
            any(l("I need to leave."), l("See you."), l("Bye."));
        mes "";
        switch (@menu) {
            case 1:
                mesn;
                mesq l("With a mask? Yeah, I remember. That was a strange guy. He came to visit the library, but he lurked around at the workshop area and they had to send him back to the books.");
                if (!(.@q2 & .bsId))
                    setq2 NivalisQuest_BlueSage, .@q2 | .bsId;
                break;
            case 2:
                mesn;
                mesq l("Mh, I can't really tell since I usually work in the library while he works at the workshop. Maybe you should ask around among the people who work there.");
                break;
        }
    } while (@menu != 3);
    close;
}

OnInit:
    .bsId=BS_NPC01;
    .sex=G_FEMALE;
    .distance=5;
    npcsit;
    end;
}


