// TMW 2 Script
// Author:
//  Jesusalva
//  Micksha
// Description:
//  Controls sewers.
//  FIXME: People should not be able to return here once they leave to 042-10
//  Spawn monsters and respawns them.

// A simple random treasure chest - to be sure players were introduced to this
// awesome system. Same rules as any treasure box still applies.
042-7,22,30,0	script	#chest_0427	NPC_CHEST,{
	KamelotTreasure(4);
	specialeffect(.dir == 0 ? 24 : 25, AREA, getnpcid()); // closed ? opening : closing
	close;

OnInit:
	.distance = 2;
	end;

OnInstanceInit:
    // Yes, we just hope it works out of box
    explode(.@map$, .map$, "@");
    .@g=atoi(.@map$[1]);
    if (.@g < 1) {
        consolewarn "[ERROR] [KAMELOT] Unable to spawn for Kamelot %s", .map$;
        debugmes "[ERROR] [KAMELOT] Using dummy data (returned: %d)", .@g;
        .@g=0;
    }
    debugmes "Spawning monsters for guild %d", .@g;
    .@mx=getguildavg(.@g);

    // Corritors
    KamelotCaveSpawn(2, 18, 52,  30,  80, .@mx, "042-7");
    KamelotCaveSpawn(3, 20, 44,  58,  60, .@mx, "042-7");
    KamelotCaveSpawn(4, 40, 55,  70,  85, .@mx, "042-7");
    KamelotCaveSpawn(5, 50, 57,  70,  85, .@mx, "042-7");

    // Boss Chamber
    KamelotCaveSpawn(8, 19, 20, 56, 44, .@mx, "042-7");
    KamelotBoss("042-7", 30, 30, .@mx+1, .name$);

    // Boss monster
    // TODO

    // Neutral monsters
    areamonster(.map$, 20, 20, 70, 80, strmobinfo(1, YellowSlime), YellowSlime, 5);
    areamonster(.map$, 20, 20, 70, 80, strmobinfo(1, ManaGhost), ManaGhost, max(1, .@mx/10));
    areamonster(.map$, 20, 20, 70, 80, strmobinfo(1, CaveMaggot), CaveMaggot, 30);

    // Bonus monsters
    if (!rand2(2))
        areamonster(.map$, 45, 20, 70, 80, strmobinfo(1, MagicBif), MagicBif, 2);
    if (!rand2(2))
        areamonster(.map$, 20, 20, 70, 80, strmobinfo(1, GoldenChest), GoldenChest, 1);
    if (!rand2(2))
        areamonster(.map$, 20, 20, 70, 80, strmobinfo(1, SilverChest), SilverChest, 2);
    if (!rand2(2))
        areamonster(.map$, 20, 20, 70, 80, strmobinfo(1, BronzeChest), BronzeChest, 3);
    end;

OnKillBoss:
    if (!playerattached())
        goto OnRespawn;
    // Maybe a reward is due
    .@g=getcharid(2);
    if (.@g < 1) die();
    getitem GuildCoin, 2+min(13, $KAMELOT_MX[.@g]/10);
    getexp $KAMELOT_MX[.@g]*14, $KAMELOT_MX[.@g]*8;
    mapannounce getmap(), strcharinfo(0)+" has defeated the boss!", 0;
    .@delay=max(3000, 21000-$KAMELOT_PC[.@g]*1000);
    goto OnRespawn;

OnKillMob:
    if (!playerattached())
        goto OnRespawn;
    // Maybe a reward is due
    .@g=getcharid(2);
    if (.@g < 1) die();
    getexp $KAMELOT_MX[.@g]*7, $KAMELOT_MX[.@g]*4;
    .@delay=max(7000, 42000-$KAMELOT_PC[.@g]*2000);
    // FALLTHROUGH

OnRespawn:
    .@delay=(.@delay ? .@delay : 7000);
    sleep(.@delay);
    // Yes, we just hope it works out of box
    explode(.@map$, .map$, "@");
    .@g=atoi(.@map$[1]);
    if (.@g < 1) {
        consolewarn "[ERROR] [KAMELOT] Unable to respawn for Kamelot %s", .map$;
        .@g=0;
    }
    KamelotCaveSpawn(1, 20, 20, 115, 100, $KAMELOT_MX[.@g], "042-7");
    end;

}


// The exit only works before chest is looted
042-7,21,78,0	script	#KDoor0427	NPC_HIDDEN,3,0,{
    end;

OnTouch:
    .@g=getcharid(2);
    if (.@g < 1) die();
    if ($KAMELOT_KEYMASK[.@g] & 4) goto L_NoAccess;
    warp "042-5@"+.@g, 56, 21;
    end;


L_NoAccess:
    dispbottom l("OH NOES! The ceiling seems to have collapsed when the chest was open! We are forced to go forward!!");
    end;
}

