// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Pinkie Emperor Boss

025-2-3,0,0,0	script	#BossCtrl_025-2-3	NPC_HIDDEN,{
    end;

// Respawn every hour
OnTimer3600000:
    stopnpctimer;
OnInit:
    setarray .xp, 268, 55, 371, 482, 212;
    setarray .yp,  90, 45,  38, 114, 148;
    .@tg=rand2(getarraysize(.xp));
    monster "025-2-3", .xp[.@tg], .yp[.@tg], strmobinfo(1, PinkieEmperor), PinkieEmperor, 1, "#BossCtrl_025-2-3::OnBossDeath";
    end;

OnBossDeath:
    initnpctimer;
    .@party=getcharid(1);
    getitem StrangeCoin, 1;
    announce_bosskill(getmap(), PinkieEmperor);
    if (.@party > 0) {
        partytimer("025-2-3", 200, "#BossCtrl_025-2-3::OnBossReward", .@party);
    } else {
        addtimer(200, "#BossCtrl_025-2-3::OnBossReward");
    }
    callfunc "02524_Revenge_BlackBox";
    fix_mobkill(PinkieEmperor);
    specialeffect(FX_FANFARE, AREA, getcharid(3));
    end;

OnBossReward:
    if (ispcdead()) end;
    getitem StrangeCoin, getmapusers("025-2-3");
    end;
}
