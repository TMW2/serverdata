// Moubootaur Legends Script
// Author:
//    Jesusalva
// Description:
//  Guild Facility - Guild Vault

guilds,35,35,0	script	Guild Vault	NPC_NO_SPRITE,{
    .@gid=getcharid(2);
    .@role=getguildrole(.@gid, getcharid(3));
    do
    {
        mesn;
        mesc l("This vault currently has %d GP inside.", format_number($GUILD_BANK[.@gid]));
        select
            l("Okay, laters"),
            l("Donate GP"),
            rif(.@role == GPOS_TREASURER || .@role <= GPOS_VICELEADER, l("Withdraw GP"));
        mes "";
        switch (@menu)
        {
            case 1:
                close;
            case 2:
                input .@mx;
                if (Zeny < .@mx || .@mx < 0)
                {
                    mesc l("Invalid amount!"), 1;
                }
                else
                {
                    Zeny=Zeny-.@mx;
                    $GUILD_BANK[.@gid]+=.@mx;
                    mesc l("Donation successful!"), 3;
                }
                break;
            case 3:
                input .@mx;
                if ($GUILD_BANK[.@gid] < .@mx || .@mx < 0)
                {
                    mesc l("Invalid amount!"), 1;
                }
                else
                {
                    $GUILD_BANK[.@gid]-=.@mx;
                    Zeny=Zeny+.@mx;
                    mesc l("Money withdrawn!"), 3;
                }
                break;
        }

    } while (true);
    close;

OnInit:
    .distance=2;
    end;
}

