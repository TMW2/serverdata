// The Mana World rEvolt scripts.
// Author:
//    Jesusalva
// Description:
//    Core for rescuing Rossy. Integrated into Inspector Quest for ML.
//    So yes - the SLIMES kidnapped Rossy this time. Poor Rossy!
018-8	mapflag	zone	MMO

017-2-1,63,23,0	script	#RitualCaveE	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    if (getq(LoFQuest_Inspector) < ROSSY_QUESTST && !is_sponsor()) {
        dispbottom l("You see a \"DO NOT TRESPASS\" sign. For a short moment you consider ignoring it, but you notice an odd eye-shaped device next to it, so you reconsider.");
    } else if (getq(LoFQuest_Inspector) == ROSSY_QUESTST) {
        // LoFQuest_Inspector
        // (MAIN QUEST, CAVE CONTROL, INSTANCE CONTROL)
        .@inst=getq3(LoFQuest_Inspector);
        .@mapn$="ross@"+getcharid(0);
        if (isinstance(.@inst) && instanceowner(.@inst) == getcharid(3))
        {
            // Reset instance duration
		    //instance_set_timeout(ROSSY_INSTIME, ROSSY_INSTIME, .@inst);
            // FIXME: Clean unkilled monsters
        } else {
		    .@inst = instance_create("ROSSY "+getcharid(0), getcharid(3), IOT_CHAR);
	        instance_attachmap("018-8", .@inst, false, .@mapn$);
	        // Instance lasts 20 minutes
		    instance_set_timeout(ROSSY_INSTIME, ROSSY_INSTIME, .@inst);
		    instance_init(.@inst);
	        setq3 LoFQuest_Inspector, .@inst;
            // Reset quest progress
	        setq2 LoFQuest_Inspector, 0;
            @rossylock=false;
        }
        // Warp to instanced map
        warp .@mapn$, 129, 112;
    } else {
        warp "018-8", 130, 112;
    }
    end;
}

018-8,129,94,0	script	Clauquer	NPC_CAUL,{
    function clauquerPrologue;

    // LoFQuest_Inspector
    // (MAIN QUEST, CAVE CONTROL, INSTANCE CONTROL)
    .@q=getq(LoFQuest_Inspector);
    .@q2=getq2(LoFQuest_Inspector);
    .@inst=getq3(LoFQuest_Inspector);

    // Instance zero is reserved
    if (instance_id() <= 0 || .@q != ROSSY_QUESTST) {
        mesn;
        mesq l("Huh? Why are you here? Get out.");
        close;
    }
    mesn;
    mesq l("Hello stranger, are you lost?");
    if (.@q < ROSSY_QUESTST)
        close;
    if (!.@q2) {
        clauquerPrologue();
        close;
    }
    next;
    .@caves=bitmask_count(.@q2)-1;
    mesn;
    if (.@q2 > ROSSY_BOSSCAVE || .@q > ROSSY_QUESTST)
        .@msg$=l("Good job rescuing the little girl!");
    else if (.@q2 == ROSSY_READY)
        .@msg$=l("I am amazed... You eliminated all the 9 energy sources. Now is the difficult part. To save the girl, you need to eliminate the last and most powerful barrier. Good Luck!");
    else if (.@q2 <= ROSSY_PROLOGUE)
        .@msg$=l("Well... You didn't eliminate any of the barriers, so that means there are still 9 caves and the tenth one, the strongest...");
    else
        .@msg$=l("You are doing good... If I am right, you already eliminated %d of the 9 energy sources of the barrier that protects the cave in which the girl is being kept captive.", .@caves);

    mesq .@msg$;
    if (.@q == 16 && .@caves < ROSSY_BOSSCAVE) {
        next;
        select
            l("Thanks."),
            l("But please, I really need your help!");
        mes "";
        if (@menu == 2) {
            mesn;
            mesq l("Well, I already told you what I knew, but repeating never hurts, I guess. So, to nullify the barrier.");
            next;
            clauquerPrologue();
        }
    }
    close;

function clauquerPrologue {
    // LoFQuest_Inspector
    // (MAIN QUEST, CAVE CONTROL, INSTANCE CONTROL)
    .@q=getq(LoFQuest_Inspector);
    .@q2=getq2(LoFQuest_Inspector);
    .@inst=getq3(LoFQuest_Inspector);

    // Prologue
    if (!(.@q2 & ROSSY_PROLOGUE)) {
        next;
        mesn;
        mesq l("Don't answer— I know you are looking for someone... A girl, is that right?");
        next;
        select
            l("I must admit you're right, but how come you..."),
            l("I'm not lost and I don't need help, bye.");
        mes "";
        if (@menu == 2)
            return;
        mesn;
        mesq l("How do I know you're looking for her? Well, because I am a very good observer. I know you were helping the police. Besides, I saw the girl. I also heard her. She screamed, and probably still screams like hell. Moreover, I don't think you would enter these caves just for fun!");
        next;
        select l("Of course not.");
        mes "";
        mesn;
        mesq l("I can see that you really want to save the girl. She is trapped in a place inside one of these caves, protected by a magic barrier created by a fey spirit. To save her, you have to annulate the energy sources of this barrier. They are located in 10 different spots, each one in a different cave.");
        next;
        select l("But how can I annulate this barrier?");
        mes "";
    }
    // Common
    mesn;
    mesq l("It is simple, you just have to place a %s on the exact center of the energy source. Once you do this, its entire power will transmute into some terrible monsters. If you defeat them, this source of energy will be close to nothing and the barrier will be weaker.", getitemlink(PileOfAsh));
    next;
    mesn;
    mesq l("If my observations are correct, the girl is trapped in a cave protected by a barrier fed by 9 different power sources. And inside this cave there is another barrier, fed by a single, but very strong, power source. The girl is inside that cave, protected by that strong barrier.");
    next;
    mesn;
    mesq l("Be prepared before you enter one of these caves, because once you put the root in the power source, you won't be able to get out. Also, remember that you can't stay in these caves for a long time, because of the foul air.");
    mesc l("If you take more than %d minutes between a boss fight and other, you'll faint and the quest will reset!!", (ROSSY_INSTIME / 60)), 1;
    next;
    // Prologue (FIXME)
    if (!(.@q2 & ROSSY_PROLOGUE)) {
        mesn;
        mesq l("Now I should tell you more about how that girl got into that cave and warn you about what you are going to face there...");
        next;
        select l("Please go on.");
        mes "";
        mesn;
        mesq l("I advise you to be mentally prepared for this rescue operation. You'll be fighting a selection of monsters in these caves with mixed elements. Mostly slimes, but with exceptions.");
        next;
        mesn;
        mesq l("Given the girl headed to the ritual room, she'll likely be sacrificed. However, hope is not yet lost. If you were to, say, defeat the great slime there, before it is fully awakened, the girl would be saved... Time, however, is not on your favor and is of essence. And I am no fighter, so I cannot help you.");
        next;
    }
    mesn;
    mesq l("Oh, and if you really plan to rescue the girl, I advise you to use potions for every single battle. You might want to take some food with you too. Don't be afraid to bring too much with you, these battles take time to end.");
    if (REBIRTH)
        mesc l("Clauquer clearly is not aware that you were reborn. These fights will likely be a breeze to someone so overpowered as you, but caution is never too much.");
    else if (BaseLevel >= 90)
        mesc l("Clauquer clearly is not aware of your level. These fights will likely be a breeze to someone so overpowered as you, but caution is never too much.");
    next;
    mesn;
    mesq l("I think you're now ready to explore these caves and fight. Good luck rescuing the girl!");

    // Renew the timer the first time you see the prologue
    if (!.@q2) {
        instance_set_timeout(ROSSY_INSTIME, ROSSY_INSTIME, .@inst);
        addtimer2(300000, "#RossyQuest::OnTick1");
        deltimer("#RossyQuest::OnTick2");
        deltimer("#RossyQuest::OnTick3");
        deltimer("#RossyQuest::OnTick4");
        deltimer("#RossyQuest::OnTick5");
    }
    setq2 LoFQuest_Inspector, .@q2 | ROSSY_PROLOGUE;
    return;
}

OnInit:
    .distance = 4;
    end;
}
018-8,92,89,0	script	Summoning Altar#Rossy	NPC_NO_SPRITE,{
    .@q2=getq2(LoFQuest_Inspector);
    // Not ready
    if (.@q2 < ROSSY_READY) {
        mesn;
        mesq l("Better not touch this.");
        close;
    }
    // Complete
    if (.@q2 & ROSSY_BOSSCAVE) {
        dispbottom l("You find someone hiding behind the altar.");
        enablenpc instance_npcname("Rossy");
        disablenpc instance_npcname("Summoning Altar#Rossy");
        end;
    }
    // Battle
    mesc l("This is the boss room! Be prepared for the worst!!"), 1;
    callfunc("RossyCave", ROSSY_BOSSCAVE);
    close;

OnInit:
    .distance = 4;
    end;

// Time to configure the map
// Note: Should we add Bifs and Shadow Herbs? Or no need?
OnInstanceInit:
    .@m$=instance_mapname("018-8");
    // Rebuild spawns
    // WARNING: Originals were generated automatically!
    // print('\tareamonster(.@m$, %d, %d, %d, %d, strmobinfo(1, %s), %s, %d);' % (obj.x, obj.y, obj.x+obj.w, obj.y+obj.h, obj.name.replace(' ', ''), obj.name.replace(' ', ''), obj.max_beings))
    areamonster(.@m$, 30, 113, 37, 127, strmobinfo(1, RedMushroom), RedMushroom, 5);
	areamonster(.@m$, 102, 116, 116, 124, strmobinfo(1, RedMushroom), RedMushroom, 5);
	areamonster(.@m$, 79, 144, 89, 150, strmobinfo(1, RedMushroom), RedMushroom, 7);
	areamonster(.@m$, 158, 87, 168, 99, strmobinfo(1, RedMushroom), RedMushroom, 7);
	areamonster(.@m$, 34, 28, 37, 32, strmobinfo(1, BlackSlime), BlackSlime, 1);
	areamonster(.@m$, 160, 124, 165, 127, strmobinfo(1, OldSnake), OldSnake, 4);
	areamonster(.@m$, 155, 163, 156, 170, strmobinfo(1, OldSnake), OldSnake, 4);
	areamonster(.@m$, 128, 146, 133, 149, strmobinfo(1, OldSnake), OldSnake, 4);
	areamonster(.@m$, 120, 61, 125, 64, strmobinfo(1, OldSnake), OldSnake, 4);
	areamonster(.@m$, 138, 64, 148, 71, strmobinfo(1, DarkLizard), DarkLizard, 10);
	areamonster(.@m$, 129, 102, 140, 108, strmobinfo(1, DarkLizard), DarkLizard, 6);
	areamonster(.@m$, 67, 114, 77, 116, strmobinfo(1, BlackScorpion), BlackScorpion, 3);
	areamonster(.@m$, 109, 145, 113, 155, strmobinfo(1, BlackScorpion), BlackScorpion, 5);
	areamonster(.@m$, 68, 84, 71, 91, strmobinfo(1, BlackScorpion), BlackScorpion, 4);
	areamonster(.@m$, 113, 60, 136, 68, strmobinfo(1, RedMushroom), RedMushroom, 7);
	areamonster(.@m$, 199, 68, 207, 77, strmobinfo(1, DarkLizard), DarkLizard, 5);
	areamonster(.@m$, 160, 47, 177, 52, strmobinfo(1, DarkLizard), DarkLizard, 5);
	areamonster(.@m$, 119, 200, 139, 204, strmobinfo(1, BlackScorpion), BlackScorpion, 5);
	areamonster(.@m$, 34, 193, 45, 202, strmobinfo(1, BlackScorpion), BlackScorpion, 5);
	areamonster(.@m$, 26, 115, 31, 132, strmobinfo(1, BlackScorpion), BlackScorpion, 5);
	areamonster(.@m$, 82, 60, 103, 67, strmobinfo(1, RedMushroom), RedMushroom, 5);
	areamonster(.@m$, 38, 193, 54, 197, strmobinfo(1, RedMushroom), RedMushroom, 5);
	areamonster(.@m$, 114, 199, 126, 202, strmobinfo(1, OldSnake), OldSnake, 4);
	areamonster(.@m$, 106, 30, 116, 37, strmobinfo(1, DarkLizard), DarkLizard, 5);
	areamonster(.@m$, 78, 128, 89, 149, strmobinfo(1, OldSnake), OldSnake, 4);
	areamonster(.@m$, 117, 141, 152, 152, strmobinfo(1, DarkLizard), DarkLizard, 10);
	areamonster(.@m$, 117, 101, 135, 112, strmobinfo(1, OldSnake), OldSnake, 7);
    /*
	areamonster(.@m$, 101, 27, 104, 31, strmobinfo(1, BlackSlime), BlackSlime, 1);
	areamonster(.@m$, 165, 28, 168, 32, strmobinfo(1, BlackSlime), BlackSlime, 1);
	areamonster(.@m$, 199, 67, 202, 71, strmobinfo(1, BlackSlime), BlackSlime, 1);
	areamonster(.@m$, 199, 109, 202, 113, strmobinfo(1, BlackSlime), BlackSlime, 1);
	areamonster(.@m$, 197, 194, 200, 198, strmobinfo(1, BlackSlime), BlackSlime, 1);
	areamonster(.@m$, 133, 199, 136, 203, strmobinfo(1, BlackSlime), BlackSlime, 1);
	areamonster(.@m$, 34, 194, 37, 198, strmobinfo(1, BlackSlime), BlackSlime, 1);
	areamonster(.@m$, 26, 111, 29, 115, strmobinfo(1, BlackSlime), BlackSlime, 1);
    */

    // Yay yay yay
    end;
}

/////////////////////////////////////////////////////////
// Main script controller for Rossy Quest
-	script	#RossyQuest	NPC_HIDDEN,{
    end;

OnStage2:
    if (!playerattached()) end; // ERROR
    if (!@rossylock) end; // Invalid
    if (mobcount(getmap(), "#RossyQuest::OnStage2")) end; // Still working
    .@q2=getq2(LoFQuest_Inspector);
    setq2 LoFQuest_Inspector, .@q2 | ROSSY_CAVE1;
    goto OnBeat;

OnStage4:
    if (!playerattached()) end; // ERROR
    if (!@rossylock) end; // Invalid
    if (mobcount(getmap(), "#RossyQuest::OnStage4")) end; // Still working
    .@q2=getq2(LoFQuest_Inspector);
    setq2 LoFQuest_Inspector, .@q2 | ROSSY_CAVE2;
    goto OnBeat;

OnStage8:
    if (!playerattached()) end; // ERROR
    if (!@rossylock) end; // Invalid
    if (mobcount(getmap(), "#RossyQuest::OnStage8")) end; // Still working
    .@q2=getq2(LoFQuest_Inspector);
    setq2 LoFQuest_Inspector, .@q2 | ROSSY_CAVE3;
    goto OnBeat;

OnStage16:
    if (!playerattached()) end; // ERROR
    if (!@rossylock) end; // Invalid
    if (mobcount(getmap(), "#RossyQuest::OnStage16")) end; // Still working
    .@q2=getq2(LoFQuest_Inspector);
    setq2 LoFQuest_Inspector, .@q2 | ROSSY_CAVE4;
    goto OnBeat;

OnStage32:
    if (!playerattached()) end; // ERROR
    if (!@rossylock) end; // Invalid
    if (mobcount(getmap(), "#RossyQuest::OnStage32")) end; // Still working
    .@q2=getq2(LoFQuest_Inspector);
    setq2 LoFQuest_Inspector, .@q2 | ROSSY_CAVE5;
    goto OnBeat;

OnStage64:
    if (!playerattached()) end; // ERROR
    if (!@rossylock) end; // Invalid
    if (mobcount(getmap(), "#RossyQuest::OnStage64")) end; // Still working
    .@q2=getq2(LoFQuest_Inspector);
    setq2 LoFQuest_Inspector, .@q2 | ROSSY_CAVE6;
    goto OnBeat;

OnStage128:
    if (!playerattached()) end; // ERROR
    if (!@rossylock) end; // Invalid
    if (mobcount(getmap(), "#RossyQuest::OnStage128")) end; // Still working
    .@q2=getq2(LoFQuest_Inspector);
    setq2 LoFQuest_Inspector, .@q2 | ROSSY_CAVE7;
    goto OnBeat;

OnStage256:
    if (!playerattached()) end; // ERROR
    if (!@rossylock) end; // Invalid
    if (mobcount(getmap(), "#RossyQuest::OnStage256")) end; // Still working
    .@q2=getq2(LoFQuest_Inspector);
    setq2 LoFQuest_Inspector, .@q2 | ROSSY_CAVE8;
    goto OnBeat;

OnStage512:
    if (!playerattached()) end; // ERROR
    if (!@rossylock) end; // Invalid
    if (mobcount(getmap(), "#RossyQuest::OnStage512")) end; // Still working
    .@q2=getq2(LoFQuest_Inspector);
    setq2 LoFQuest_Inspector, .@q2 | ROSSY_CAVE9;
    goto OnBeat;

OnStage1024:
    if (!playerattached()) end; // ERROR
    if (!@rossylock) end; // Invalid
    if (mobcount(getmap(), "#RossyQuest::OnStage1024")) end; // Still working
    .@q2=getq2(LoFQuest_Inspector);
    setq2 LoFQuest_Inspector, .@q2 | ROSSY_BOSSCAVE;
    goto OnBeat;

OnBeat:
    @rossylock = false;
    dispbottom l("*** Cave Cleared ***");
    delcells "Rossy"+getcharid(0)+"Wall";
    .@inst = getq3(LoFQuest_Inspector);
    instance_set_timeout(ROSSY_INSTIME, ROSSY_INSTIME, .@inst);
    if (.@q2 < ROSSY_BOSSCAVE)
        addtimer2(300000, "#RossyQuest::OnTick1");
    else
        deltimer("#RossyQuest::OnTick1");
    deltimer("#RossyQuest::OnTick2");
    deltimer("#RossyQuest::OnTick3");
    deltimer("#RossyQuest::OnTick4");
    deltimer("#RossyQuest::OnTick5");
    end;

/* ****************************************** */
OnTick1:
    //dispbottom l("Time remaining: %d minutes", 15);
    //addtimer2(300000, "#RossyQuest::OnTick2");
    //end;
    //FALLTHROUGH;

OnTick2:
    dispbottom l("Time remaining: %d minutes", 10);
    addtimer2(300000, "#RossyQuest::OnTick3");
    end;

OnTick3:
    dispbottom l("Time remaining: %d minutes", 5);
    addtimer2(240000, "#RossyQuest::OnTick4");
    end;

OnTick4:
    dispbottom l("Time remaining: 1 minute");
    addtimer2(45000, "#RossyQuest::OnTick5");
    end;

OnTick5:
    dispbottom l("Time remaining: %d seconds!", 15);
    end;

}

/////////////////////////////////////////////////////////
// RossyCave(Cave ID)
function	script	RossyCave	{
    // Fight ongoing - cannot trigger
    if (@rossylock)
        return false;

    // Check quest consistency
    // LoFQuest_Inspector
    // (MAIN QUEST, CAVE CONTROL, INSTANCE CONTROL)
    .@q=getq(LoFQuest_Inspector);
    .@q2=getq2(LoFQuest_Inspector);
    .@inst=getq3(LoFQuest_Inspector);

    if (instance_id() != .@inst)
        return false;
    if (.@q != ROSSY_QUESTST)
        return false;
    if (!.@q2) {
        dispbottom l("This looks DANGEROUS! Maybe someone around here can tell me what I'm supposed to do.");
        return false;
    }
    if (.@q2 & .caveid)
        return false;

    // ---------------
    // Main node setup
    .@c=getarg(0);
    .@m$=getmap();
    .@n$="#RossyQuest::OnStage"+.@c;

    // Obtain and Write Configuration
    switch (.@c) {
    case ROSSY_CAVE1:
        .@boss=BlueSlimeMother;
        .@mobc=rand2(3,5);
        .@x1=85;.@y1=21;.@x2=108;.@y2=39;
        .@x3=109;.@y3=33; .@x4=109; .@y4=36;
        break;
    case ROSSY_CAVE2:
        .@boss=RedSlimeMother;
        .@mobc=rand2(3,5);
        .@x1=191;.@y1=103;.@x2=205;.@y2=122;
        .@x3=201;.@y3=123; .@x4=201; .@y4=123;
        break;
    case ROSSY_CAVE3:
        .@boss=MurdererScorpion;
        .@mobc=rand2(2,4);
        .@x1=26;.@y1=23;.@x2=43;.@y2=36;
        .@x3=38;.@y3=37; .@x4=38; .@y4=37;
        break;
    case ROSSY_CAVE4:
        .@boss=YellowSlimeMother;
        .@mobc=rand2(6,12);
        .@x1=180;.@y1=176;.@x2=206;.@y2=207;
        .@x3=179;.@y3=200; .@x4=179; .@y4=200;
        break;
    case ROSSY_CAVE5:
        .@boss=FireSkull;
        .@mobc=rand2(6,8);
        .@x1=98;.@y1=188;.@x2=141;.@y2=207;
        .@x3=97;.@y3=190; .@x4=97; .@y4=197;
        break;
    case ROSSY_CAVE6:
        .@boss=FireFairy;
        .@mobc=rand2(5,7);
        .@x1=19;.@y1=98;.@x2=36;.@y2=136;
        .@x3=37;.@y3=125; .@x4=37; .@y4=125;
        break;
    case ROSSY_CAVE7:
        .@boss=DustRifle;
        .@mobc=rand2(5,7);
        .@x1=22;.@y1=181;.@x2=50;.@y2=205;
        .@x3=51;.@y3=192; .@x4=51; .@y4=197;
        break;
    case ROSSY_CAVE8:
        .@boss=HoodedNinja;
        .@mobc=rand2(3,5);
        .@x1=158;.@y1=20;.@x2=176;.@y2=35;
        .@x3=175;.@y3=36; .@x4=177; .@y4=36;
        break;
    case ROSSY_CAVE9:
        .@boss=GreenSlimeMother;
        .@mobc=rand2(5,7);
        .@x1=187;.@y1=58;.@x2=208;.@y2=85;
        .@x3=186;.@y3=82; .@x4=186; .@y4=85;
        break;
    case ROSSY_BOSSCAVE:
        .@boss=GreatMoubooSlime;
        .@mobc=rand2(3,5);
        .@x1=84;.@y1=85;.@x2=101;.@y2=101;
        .@x3=92;.@y3=102; .@x4=93; .@y4=102;
        break;
    default:
        consolemes(CONSOLEMES_ERROR, "INVALID ROSSY CAVE ID - "+.@c+" - SCRIPT ABORTED");
        return false;
    }

    // This is cast from a dialog so prompt comes before any advanced calculation
    mesc l("Drop a %s here to summon monsters.", getitemlink(PileOfAsh));
    if (.@c == ROSSY_BOSSCAVE)
        mesc l("This is the master's room seal, be careful!"), 1;
    else
        mesc l("Weaken all nine seals to be able to undo the final one.");
    select
        rif(countitem(PileOfAsh), l("Place pile of ashes")),
        l("Run away in fear");
    mes "";
    closeclientdialog;
    if (@menu == 2)
        return false;

    // Delete the item and lock for the quest
    delitem PileOfAsh, 1;
    @rossylock = true;

    // Spawn underlings
    .@mobc += any(1,2); // Moubootaur Legends difficulty FIX
    for (.@i=0; .@i < .@mobc; .@i++) {
        // Summon some slimes, mostly, but some other nasties as well
        .@mobId=any(BlueSlime, Bluepar, YellowSlime, LavaSlime, SeaSlime, RedMushroom, AngryYellowSlime, Snake, RobinBandit, BlackSlime); // Mobs lv 30~40
        .@mob=areamonster(.@m$, .@x1, .@y1, .@x2, .@y2, strmobinfo(1, .@mobId), .@mobId, 1, .@n$);
        // Make it aggresive
        .@bmod=getunitdata(.@mob, UDT_MODE);
        setunitdata(.@mob, UDT_MODE, .@bmod|4); // 4 = MD_AGGRESSIVE
        // Make it healthy
        .@bhp= getunitdata(.@mob, UDT_MAXHP);
        setunitdata(.@mob, UDT_MAXHP,    .@bhp*12/10);
        setunitdata(.@mob, UDT_HP,       .@bhp*12/10);
    }

    // Spawn boss
    .@mob=areamonster(.@m$, .@x1, .@y1, .@x2, .@y2, strmobinfo(1, .@boss), .@boss, 1, .@n$);

    // Obain boss basic status...
    .@bhp= getunitdata(.@mob, UDT_MAXHP);
    .@bat1=getunitdata(.@mob, UDT_ATKMIN);
    .@bat2=getunitdata(.@mob, UDT_ATKMAX);
    .@bhit=getunitdata(.@mob, UDT_HIT);
    .@bmod=getunitdata(.@mob, UDT_MODE);
    .@bmod=.@bmod|4; // 4 = MD_AGGRESSIVE

    // ...And Tweak Battle Status
    setunitdata(.@mob, UDT_MAXHP,    .@bhp*14/10);
    setunitdata(.@mob, UDT_HP,       .@bhp*14/10);
    setunitdata(.@mob, UDT_ATKMIN,   .@bat1*7/10);
    setunitdata(.@mob, UDT_ATKMAX,   .@bat2*7/10);
    setunitdata(.@mob, UDT_HIT,      .@bhit*2);
    setunitdata(.@mob, UDT_MODE,     .@bmod|32); // 32 = MD_BOSS

    // Build the wall so you cannot leave :>
    setcells getmap(), .@x3, .@y3, .@x4, .@y4, 3, "Rossy"+getcharid(0)+"Wall";
    return true;
}

/////////////////////////////////////////////////////////
// Position the triggers

// Begin Cave Code
018-8,98,27,0	script	#RossyCave01	NPC_SUMMONING_CIRC,{
    if (RossyCave(.caveid))
        disablenpc instance_npcname(.name$);
    close;

OnInit:
    disablenpc "#RossyCave01";
OnInstanceInit:
    .distance=1;
    .caveid=ROSSY_CAVE1;
    end;
}

// Begin Cave Code
018-8,199,109,0	script	#RossyCave02	NPC_SUMMONING_CIRC,{
    if (RossyCave(.caveid))
        disablenpc instance_npcname(.name$);
    close;

OnInit:
    disablenpc "#RossyCave02";
OnInstanceInit:
    .distance=1;
    .caveid=ROSSY_CAVE2;
    end;
}

// Begin Cave Code
018-8,35,26,0	script	#RossyCave03	NPC_SUMMONING_CIRC,{
    if (RossyCave(.caveid))
        disablenpc instance_npcname(.name$);
    close;

OnInit:
    disablenpc "#RossyCave03";
OnInstanceInit:
    .distance=1;
    .caveid=ROSSY_CAVE3;
    end;
}

// Begin Cave Code
018-8,198,193,0	script	#RossyCave04	NPC_SUMMONING_CIRC,{
    if (RossyCave(.caveid))
        disablenpc instance_npcname(.name$);
    close;

OnInit:
    disablenpc "#RossyCave04";
OnInstanceInit:
    .distance=1;
    .caveid=ROSSY_CAVE4;
    end;
}

// Begin Cave Code
018-8,133,200,0	script	#RossyCave05	NPC_SUMMONING_CIRC,{
    if (RossyCave(.caveid))
        disablenpc instance_npcname(.name$);
    close;

OnInit:
    disablenpc "#RossyCave05";
OnInstanceInit:
    .distance=1;
    .caveid=ROSSY_CAVE5;
    end;
}

// Begin Cave Code
018-8,27,104,0	script	#RossyCave06	NPC_SUMMONING_CIRC,{
    if (RossyCave(.caveid))
        disablenpc instance_npcname(.name$);
    close;

OnInit:
    disablenpc "#RossyCave06";
OnInstanceInit:
    .distance=1;
    .caveid=ROSSY_CAVE6;
    end;
}

// Begin Cave Code
018-8,31,192,0	script	#RossyCave07	NPC_SUMMONING_CIRC,{
    if (RossyCave(.caveid))
        disablenpc instance_npcname(.name$);
    close;

OnInit:
    disablenpc "#RossyCave07";
OnInstanceInit:
    .distance=1;
    .caveid=ROSSY_CAVE7;
    end;
}

// Begin Cave Code
018-8,165,26,0	script	#RossyCave08	NPC_SUMMONING_CIRC,{
    if (RossyCave(.caveid))
        disablenpc instance_npcname(.name$);
    close;

OnInit:
    disablenpc "#RossyCave08";
OnInstanceInit:
    .distance=1;
    .caveid=ROSSY_CAVE8;
    end;
}

// Begin Cave Code
018-8,201,69,0	script	#RossyCave09	NPC_SUMMONING_CIRC,{
    if (RossyCave(.caveid))
        disablenpc instance_npcname(.name$);
    close;

OnInit:
    disablenpc "#RossyCave09";
OnInstanceInit:
    .distance=1;
    .caveid=ROSSY_CAVE9;
    end;
}

// --------------------------------------------------------------------
018-8,92,90,0	script	Rossy	NPC_ANGELA2,{
    // Check quest consistency
    // LoFQuest_Inspector
    // (MAIN QUEST, CAVE CONTROL, INSTANCE CONTROL)
    .@q=getq(LoFQuest_Inspector);
    .@q2=getq2(LoFQuest_Inspector);
    .@inst=getq3(LoFQuest_Inspector);

    if (instance_id() != .@inst)
        end;
    if (!.@q2)
        end;

    // A bug!
    if (.@q != ROSSY_QUESTST) {
        disablenpc instance_npcname("Rossy", .@inst);
        end;
    }

    // Another bug!!
    if (!(.@q2 & ROSSY_BOSSCAVE)) {
        disablenpc instance_npcname("Rossy", .@inst);
        end;
    }

    // Prepare rewards
    inventoryplace SilverBossGift, 1;

    // Run epilogue
    mesn;
    mesq l("I heard some battle noises a few minutes ago. Are you here to save me?");
    next;
    select
        l("Of course I'm here to save you! I work for the police!");
    mes "";
    mesn;
    mesc l("Rossy starts scavenging the fallen slime.");
    mesq l("Whenever you defeat a boss monster, sometimes they drop gifts. The more powerful the boss, the better the gift.");
    next;
    mesn;
    mesq l("Here you go!");
    getitem SilverBossGift, 1;
    setq LoFQuest_Inspector, 8, 0, .@inst;
    next;
    mesn;
    mesq l("Thanks for helping me... I'll be on my way now.");
    next;
    select
        l("Go safely!"),
        l("Do you need an escort?"),
        l("Who kidnapped you and why, anyway?");
    mes "";
    if (@menu == 2) {
        mesn;
        mesq l("I'll be fine, but thanks for the concern.");
        mesc l("Apparently, her feet was not as hurt as it originally seemed.");
    } else if (@menu == 3) {
        // This opens path for additional questline
        mesn;
        mesq l("Actually... I don't remember. I'm not sure why.");
        next;
        mesn;
        mesq l("But it was big and scary! I haven't seen it since I entered this cave, though...");
        next;
        mesn;
        mesq l("It probably went away after seeing your bravure, tee-hee!");
        next;
        mesn;
        mesq l("I must go now. I've already spent too much time here already.");
    }
    disablenpc instance_npcname("Rossy", .@inst);
    close;

OnInit:
    disablenpc "Rossy";
    .distance=4;
    end;

OnInstanceInit:
    disablenpc instance_npcname("Rossy");
    .distance=4;
    end;
}


