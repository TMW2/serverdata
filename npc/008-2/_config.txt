// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 008-2: 2nd Floor - Party Dungeon conf

008-2,80,208,0	script	#008-2_80_208	NPC_HIDDEN,{
	end;
OnDisable:
	delcells "008-2_80_208"; end;
OnEnable:
OnInit:
	setcells "008-2", 80, 208, 88, 208, 1, "008-2_80_208";
}

008-2,106,237,0	script	#008-2_106_237	NPC_SWITCH_ONLINE,{
	if (getnpcclass() == NPC_SWITCH_OFFLINE)
		end;
	callfunc "massprovoke", 12;
	setnpcdisplay "#008-2_106_237", NPC_SWITCH_OFFLINE;
	end;
OnInit:
	.distance=2;
}

008-2,66,143,0	script	#008-2_66_143	NPC_SWITCH_OFFLINE,{
	if (getnpcclass() == NPC_SWITCH_ONLINE)
		end;
	callfunc "grenade", 12, 10000;
	setnpcdisplay "#008-2_66_143", NPC_SWITCH_ONLINE;
	end;
OnInit:
	.distance=2;
}

008-2,111,166,0	script	#008-2_111_166	NPC_SWITCH_ONLINE,{
	if (getnpcclass() == NPC_SWITCH_OFFLINE)
		end;
	callfunc "ALCReset";
	setnpcdisplay "#008-2_111_166", NPC_SWITCH_OFFLINE;
	end;
OnInit:
	.distance=2;
}

008-2,135,21,0	script	#008-2_135_21	NPC_HIDDEN,2,1,{
	end;
OnTouch:
	callfunc "massprovoke", 12;
	end;
}

008-2,54,237,0	script	#008-2_54_237	NPC_HIDDEN,2,1,{
	end;
OnTouch:
	doevent "#008-2_80_208::OnDisable";
	end;
}

008-2,72,247,0	script	#008-2_72_247	NPC_TRAP_ONLINE,0,0,{
	mesn strcharinfo(0);
	mesq l("Something seems off with that!");
	close;
OnTouch:
OnTouchNPC:
	IronTrap(200, 0, 1);
	end;
}

008-2,87,240,0	script	#008-2_87_240	NPC_TRAP_ONLINE,0,0,{
	mesn strcharinfo(0);
	mesq l("Something seems off with that!");
	close;
OnTouch:
OnTouchNPC:
	IronTrap(200, 0, 1);
	end;
}

008-2,46,211,0	script	#008-2_46_211	NPC_TRAP,0,0,{
	mesn strcharinfo(0);
	mesq l("Something seems off with that!");
	close;
OnTouch:
OnTouchNPC:
	IronTrap(200, 10, 2);
	end;
OnTimer10000:
	stopnpctimer; setnpctimer 0; setnpcdisplay "#008-2_46_211", NPC_TRAP; end;
}

008-2,85,117,0	script	#008-2_85_117	NPC_TRAP,0,0,{
	mesn strcharinfo(0);
	mesq l("Something seems off with that!");
	close;
OnTouch:
OnTouchNPC:
	IronTrap(400, 15, 3);
	end;
OnTimer15000:
	stopnpctimer; setnpctimer 0; setnpcdisplay "#008-2_85_117", NPC_TRAP; end;
}

008-2,59,222,0	script	#008-2_59_222	NPC_HIDDEN,{
	end;
OnDisable:
	delcells "008-2_59_222"; end;
OnEnable:
OnInit:
	setcells "008-2", 59, 222, 67, 222, 1, "008-2_59_222";
}

008-2,32,99,0	script	#008-2_32_99	NPC_SWITCH_ONLINE,{
	if (getnpcclass() == NPC_SWITCH_OFFLINE)
		end;
	doevent "#008-2_59_222::OnDisable";
	setnpcdisplay "#008-2_32_99", NPC_SWITCH_OFFLINE;
	end;
OnInit:
	.distance=2;
}

008-2,87,221,0	script	#008-2_87_221	NPC_CHEST,{
	TreasureBox();
	specialeffect(.dir == 0 ? 24 : 25, AREA, getnpcid()); // closed ? opening : closing
	close;
OnInit:
	.distance=3;
	end;
}
