// TMW-2 Script.
// Author:
//    Jesusalva
// Notes:
//    Homage to Warp Mage and Crimson Feltchstick for their time spent in jail

sec_pri,22,21,0	script	Warp Feltchstick	NPC_PLAYER,{
    mesn;
    mesq l("Carry on, criminal. I am %s and this jail was entrusted to me by kytty herself.", .name$);
    if (!getstatus(SC_JAILED,0))
        mesc l("...Actually, shouldn't you be outside? I'm pretty sure you already served your sentence, something must have gone wrong, please report."), 1;
    next;
    if ($HARDCORE && getstatus(SC_JAILED,0)) {
        // Hardcore Server Logic (DOES NOT WORK well for some reason?)
        .@v1=getstatus(SC_JAILED, 1);
        .@v2=getstatus(SC_JAILED, 2);
        .@v3=getstatus(SC_JAILED, 3);
        .@v4=getstatus(SC_JAILED, 4);
        .@ti=getstatus(SC_JAILED, 5);
        if (.@v1 <= 1) end;
        .@ti += 120000; // Penalty time for changing point: 2 minutes
        .@v1 += 2;
        mesc l("Do you want me to change where you'll respawn once you finish serving your sentence? This may increase the sentence duration, so be careful."), 1;
        select
            l("I'm fine, thanks."),
            l("Change to the Soul Menhir, please.");
        mes "";
        closeclientdialog;
        if (!getstatus(SC_JAILED,0)) end;
        switch (@menu) {
        case 2:
            // When renewing status' information: val3 = map_index+1 val4 = coords
            // coords are done with &0xFFFF and <<16
            // (22&0xFFFF|22<<16)
            // ...However, @v4 has other stuff, so at the moment only save point
            sc_start4 SC_JAILED, .@ti, .@v1, .@v2, getmapinfo(MAPINFO_ID, "000-1")+1, .@v4, 10000, SCFLAG_FIXEDRATE|SCFLAG_FIXEDTICK|SCFLAG_NOAVOID;
            break;
        }
    } else {
        // Official Server logic
        closeclientdialog;
        // TODO: Say some random anecdote, maybe steal from Confused Tree + Elli
        // Using npctalk3
        legiontalk();
    }
    close;

OnInit:
    .sex = G_MALE;
    .distance = 14;
    .@npcId = getnpcid(.name$);

    setunitdata(.@npcId, UDT_HEADTOP, PrsmHelmet);
    setunitdata(.@npcId, UDT_HEADMIDDLE, JeansChaps);
    setunitdata(.@npcId, UDT_HEADBOTTOM, Beard);
    setunitdata(.@npcId, UDT_WEAPON, CopperArmor);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 2);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    npcsit;
    end;
}

