// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 018-1-1: Sincerity Island Cave mobs
018-1-1,53,51,14,12	monster	Silkworm	1034,10,30000,3000
018-1-1,43,35,0,0	monster	Evil Mushroom	1042,3,40000,5000
018-1-1,27,31,0,0	monster	Evil Mushroom	1042,3,40000,5000
018-1-1,50,49,30,30	monster	Bat	1039,10,60000,10000
018-1-1,65,52,12,24	monster	Chocolate Slime	1180,6,30000,60000
018-1-1,49,65,27,10	monster	Wicked Mushroom	1176,4,30000,3000
018-1-1,35,39,13,17	monster	Lavern	1175,6,30000,3000
018-1-1,71,33,0,0	monster	Evil Mushroom	1042,3,40000,5000
018-1-1,49,63,30,10	monster	Shadow Plant	1189,4,60000,10000
018-1-1,53,48,27,21	monster	Black Mamba	1174,4,60000,10000
018-1-1,50,50,29,29	monster	Vicious Squirrels	1187,9,60000,10000
