// TMW2 scripts.
// Author:
//    gumi
//    Reid
//    Saulc
//    Jesusalva
// Description:
//    Water animations, splash, fishes, etc...

012-1,119,54,0	script	#Hurn_WAM0	NPC_WATER_SPLASH,{

    fishing(1,
            CommonCarp,
            GrassCarp,
            BottleOfWoodlandWater);

    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;
}

012-1,137,58,0	duplicate(#Hurn_WAM0)	#Hurn_WAM1	NPC_WATER_SPLASH
012-1,137,88,0	duplicate(#Hurn_WAM0)	#Hurn_WAM2	NPC_WATER_SPLASH
012-1,59,12,0	duplicate(#Hurn_WAM0)	#Hurn_WAM3	NPC_WATER_SPLASH
012-1,138,31,0	duplicate(#Hurn_WAM0)	#Hurn_WAM4	NPC_WATER_SPLASH

