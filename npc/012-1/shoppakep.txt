// TMW2 Script.
// Author:
//    Jesusalva
// Description:
//    Random Hurnscald Shopper with random overpriced stuff and gifts

012-1,58,64,0	script	Shoppa Kep	NPC_SHOPPAKEP,{
    mesn;
    mesq lg("Welcome! Only the finest wares!");
    next;
    closedialog;
    npcshopattach(.name$);
    shop .name$;
    close;

    function ShoppaKepItem {
    .@i=rand2(0,100);
    if (.@i > 75)
        return Aquada;
    else if (.@i < 35)
        return Manana;
    else
        return HalfEggshell;
    }

OnInit:
    .CurrentItem=ShoppaKepItem();
    .ThisRebootAmou=rand2(30,70);

    .sex = G_FEMALE;
    .distance = 5;


    tradertype(NST_MARKET);
    sellitem CottonTrousers, 80000, 1;
    sellitem CottonSkirt, 40000, 1;
    sellitem MiniSkirt, 35000, 1;
    sellitem ShortTankTop, 20000, 1;
    sellitem CottonGloves, 20000, 1;
    sellitem CottonBoots, 10000, 1;

    sellitem PinkieLeg, 500, 1;
    sellitem Bread, -1, 50;
    sellitem Piberries, -1, 30;
    sellitem Plushroom, -1, 3;
    sellitem Chagashroom, -1, 3;

    sellitem PiouFeathers, -1, .ThisRebootAmou;
    sellitem .CurrentItem, -1, rand2(1,4);

    end;

OnClock1759:
    restoreshopitem PinkieLeg, 500, 1;
OnClock0546:
    restoreshopitem CottonTrousers, 80000, 1;
    restoreshopitem CottonSkirt, 40000, 1;
    restoreshopitem MiniSkirt, 35000, 1;
    restoreshopitem ShortTankTop, 20000, 1;
    restoreshopitem CottonGloves, 20000, 1;
    restoreshopitem CottonBoots, 10000, 1;

    restoreshopitem Bread, -1, 50;
    restoreshopitem Piberries, -1, 30;
    restoreshopitem Plushroom, -1, 3;
    restoreshopitem Chagashroom, -1, 3;

    restoreshopitem PiouFeathers, -1, .ThisRebootAmou;

    stopselling(.CurrentItem);
    .CurrentItem=ShoppaKepItem();
    sellitem .CurrentItem, -1, rand2(1,4);
    end;

// Pay your taxes!
OnBuyItem:
    debugmes("Purchase confirmed");
    PurchaseTaxes();
    end;

OnSellItem:
    debugmes("Sale confirmed");
    SaleTaxes();
    end;
}

