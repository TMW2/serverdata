// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Wizard at Magic Academy Entrance
// TODO: Training for Academy Students

017-0,245,223,0	script	Red Wizard	NPC_RED_WIZARD_F,{
    mesn;
    mesq l("Hello. I am the instructor assigned to the magic range training field.");
    if (!MAGIC_LVL)
        close;
    next;
    mesn;
    mesq l("Do you wish to return to the Academy?");
    next;
    select
        l("Not yet."),
        l("Yes please.");
    mes "";
    closeclientdialog;
    if (@menu == 2)
        warp "027-1", 46, 91;
    close;

OnInit:
    .distance = 4;
    .sex = G_FEMALE;
    end;
}

