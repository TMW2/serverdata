// TMW-2 Script.
// Author:
//    Jesusalva
// Description:
//    Leather Trousers quest (lv 48)
// Variable:
//    LilitQuest_Shaabty
//      0 - Not enough level
//      1 - Quest assigned
//      2 - Requested the material
//      3 - Craft is in progress
//      4 - Quest Complete
// Var2:
//      Controls how many snakes you killed down there.
//      It counts regardless of quest state.
// Var3:
//      Timer for state 3

018-5-3,29,38,0	script	Shaabty	NPC_FAIRY_A,{
    function LTask;
    .@q=getq(LilitQuest_Shaabty);
    switch (.@q) {
        case 0:
            goto L_Start; break;
        case 1:
            goto L_Return; break;
        case 2:
            goto L_Craft; break;
        case 3:
            goto L_Craft2; break;
        case 4:
            goto L_Start; break;
    }
    Exception(l("Invalid quest state: @@", .@q), RB_DEFAULT|RB_SPEECH|RB_ISFATAL^RB_DISPBOTTOM);
    close;

L_Start:
    mesn l("Shaabty the Fairy");
    mesq l("I always wanted to live in this town. I'm glad I managed to move from LoF Village to here.");
    next;
    mesn l("Shaabty the Fairy");
    mesq l("A shame there are snakes below the ground. I really, really hate snakes, like every fairy.");
    if (BaseLevel < 45 || .@q >= 3)
        close;
    next;
    mesn l("Shaabty the Fairy");
    mesq l("Maybe you could do me a favour, though?");
    setq1 LilitQuest_Shaabty, 1;
    LTask();
    close;

function LTask {
    mesn l("Shaabty the Fairy");
    mesq l("I would love to see the skin of two @@.", getmonsterlink(MountainSnake));
    next;
    mesn l("Shaabty the Fairy");
    mesq l("They live below this town. Can you do this for me?");
    next;
    return;
}

L_Return:
    LTask();
    .@q2=getq2(LilitQuest_Shaabty);
    // 0.98% DR (skin) - Reference is 0.32%
    // 2 snake skins should take ~200 kills
    if (countitem(MountainSnakeSkin) >= 2) {
        if (.@q2 < 100) {
            mesn l("Shaabty the Fairy");
            mesq l("I see two snake skins, but you didn't killed them down there. That defeats the whole purpose.");
        } else {
            goto L_Finish;
        }
    } else {
        mesn l("Shaabty the Fairy");
        mesq l("That's not a couple of skins.");
    }
    close;

L_Finish:
    mesc l("Deliver @@ @@ to @@?", 2, getitemlink(MountainSnakeSkin), .name$), 1;
    next;
    if (askyesno() == ASK_YES) {
        delitem MountainSnakeSkin, 2;
        setq1 LilitQuest_Shaabty, 2;
        getexp 20000, 200;
        mesn l("Shaabty the Fairy");
        mesq l("Thanks. Please come back later, there is something I want to do for you.");
    }
    close;

L_Craft:
    mesn l("Shaabty the Fairy");
    mesq l("So, I was thinking in crafting a @@ for you.", getitemlink(LeatherTrousers));
    next;
    mesn l("Shaabty the Fairy");
    mes l("For that, I'll need @@/@@ @@.", countitem(LeatherPatch), 10, getitemlink(LeatherPatch));
    mes l("And a commission of @@/@@ GP.", format_number(Zeny), format_number(8000));
    if (countitem(LeatherPatch) < 10 ||
        Zeny < 8000)
        close;
    next;
    mesc l("Pay for her handi-work?");
    if (askyesno() == ASK_YES) {
        delitem LeatherPatch, 10;
        Zeny-=8000;
        setq1 LilitQuest_Shaabty, 3;
        //setq3 LilitQuest_Shaabty, gettimeparam(GETTIME_HOUR)+2;
        setq3 LilitQuest_Shaabty, gettimetick(2)+7200;
        mesn l("Shaabty the Fairy");
        mesq l("Thanks! Please come back in two hours, then I'll have the trousers ready.");
    }
    close;

// Each time you bother her, she loses the line;
L_Craft2:
    .@q3=getq3(LilitQuest_Shaabty);
    if (gettimetick(2) < .@q3) {
        mesn l("Shaabty the Fairy");
        .@mg$=any(l("stop interrupting me while I make your trousers."), l("stop making me lose the line by talking to me."));
        mesq l("Please be patient and "+.@mg$);
        mesc l("Remaining time: @@", FuzzyTime(.@q3));
        setq3 LilitQuest_Shaabty, .@q3+rand2(1,3);
    } else {
        inventoryplace LeatherTrousers, 1;
        getitem LeatherTrousers, 1;
        getexp 0, 1000;
        setq3 LilitQuest_Shaabty, 0;
        setq1 LilitQuest_Shaabty, 4;
        mesn l("Shaabty the Fairy");
        mesq l("Here you go. Please enjoy and thanks for making this town better to live. Maybe one day the snakes leave ^.^");
    }
    close;

/////////////////////////////////////////////////////////
OnKillSnake:
    .@q1=getq(LilitQuest_Shaabty);
    .@q2=getq2(LilitQuest_Shaabty);
    setq2 LilitQuest_Shaabty, .@q2+1;
    if (.@q1 == 1) {
        if (! (.@q2+1) % 10)
            dispbottom l("@@ @@ killed.", .@q2+1, getmonsterlink(MountainSnake));
    }
    fix_mobkill(MountainSnake);
    end;

OnInit:
    .sex = G_OTHER;
    .distance = 5;
    end;
}
