// TMW2 Scripts
// Author:
//    Jesusalva, Saulc
// Description:
//      Slot Machine for bets in Aethyr, for the King of Gambling
//      Rare Reward: Imperial Crown

031-4,96,81,0	script	Slot Machine#031-4	NPC_NO_SPRITE,{
    function spin;
    function symbol {
        switch (getarg(0)) {
        case 1:
            mesn "%%A";
            break;
        case 2:
            mesn "%%B";
            break;
        case 3:
            mesn "%%C";
            break;
        case 4:
            mesn "%%D";
            break;
        case 5:
            mesn "%%E";
            break;
        case 6:
            mesn "%%F";
            break;
        case 7:
            mesn "7";
            break;
        case 8:
            mesn "%%8";
            break;
        case 9:
            mesn "%%9";
            break;
        case 10:
            mesn "%%0";
            break;
        case 11:
            mesn "%%1";
            break;
        case 12:
            mesn "%%2";
            break;
        case 13:
            mesn "%%3";
            break;
        case 14:
            mesn "%%4";
            break;
        case 15:
            mesn "%%5";
            break;
        case 16:
            mesn "%%6";
            break;
        case 17:
            mesn "%%7";
            break;
        case 18:
            mesn "%%:";
            break;
        case 19:
            mesn "%%;";
            break;
        default:
            mesn "%%@";
            break;
        }
        return;
    }

    function dismiss {
        mesc b(l("NOT CLASSY ENOUGH!")), 1;
        mesc l("You need to acquire a %s from a Casino around the world to use this slot machine.", getitemlink(getarg(0))), 1;
        close;
        return;
    }

L_Menu:
    // Unlock the seven Casinos around the world
    if (!countitem(Monocle)) dismiss(Monocle);
    if (!countitem(Bloodstone)) dismiss(Bloodstone);
    if (!countitem(PaperBag)) dismiss(PaperBag);
    if (!countitem(TrapperHat)) dismiss(TrapperHat);
    if (!countitem(SantaHat)) dismiss(SantaHat);
    if (!countitem(Beard)) dismiss(Beard);
    if (!countitem(AxeHat)) dismiss(AxeHat);
    // Oooh, so you did it, didn't you?
    mesn;
    mesc l("Thou whom hast cleared the seven casinos in the world, may challenge the ultimate slot machine."), 1;
    if (!gethominfo(0)) { mesc l("Acquire a Homunculus to witness it."); close; }
    if (homstatus()) { mesc l("Bring your Homunculus to witness it."); close; }
    mesc l("Just one coin for spin."), 1;
    next;
    menu
        rif(countitem(CasinoCoins) >= 1, l("Spin!")), L_Spin,
        rif(countitem(CasinoCoins) >= 10, l("Spin 10 times!")), L_Spin10,
        rif(countitem(CasinoCoins) >= 100, l("Spin 100 times!")), L_Spin100,
        l("Prizes"), L_Info,
        l("Leave"), -;
    close;

L_Info:
    mes "";
    mesc l("Prizes:");
    mes l("##9 77777: @@.", getitemlink(ImperialCrown));
    if (countitem(Blanket))
        mesc l("Five equal: %s.", getitemlink(Bathrobe));
    else
        mesc l("Five equal: %s.", l("20 Strange Coin + 15000 Homun EXP"));
    mesc l("Four equal: @@.", l("up to 5 Strange Coin + 5000 Homun EXP"));
    mesc l("Three equal: @@.", l("up to 5 Casino Coins + 500 Homun EXP"));
    mesc l("Two equal: %s.", l("50 GP + 50 Homunculus EXP"));
    mes "";
    mesc l("Two pairs count as three equal. A \"full house\" count as four equal.");
    next;
    goto L_Menu;


L_Spin:
    spin();
    next;
    goto L_Menu;

L_Spin10:
    for (.@i=0;.@i < 10;.@i++) {
        spin();
        dnext;
    }
    next;
    goto L_Menu;

L_Spin100:
    freeloop(true);
    for (.@i=0;.@i < 100;.@i++) {
        spin();
        dnext;
    }
    freeloop(false);
    next;
    goto L_Menu;

function spin {
    mesc l("Spinning...");
    dnext;
    delitem CasinoCoins, 1;
    .@a=rand2(20-(@sfcasino/100));
    .@b=rand2(20-(@sfcasino/100));
    .@c=rand2(20-(@sfcasino/100));
    .@d=rand2(20-(@sfcasino/100));
    .@e=rand2(20-(@sfcasino/100));
    symbol(.@a);
    symbol(.@b);
    symbol(.@c);
    symbol(.@d);
    symbol(.@e);

    // Check how many symbols you got identical
    // This is not the same as normal slot machines!
    .@s = 1;
    if (.@a == .@b || .@a == .@c || .@a == .@d || .@a == .@e)
        .@s += 1;
    if (.@b == .@c || .@b == .@d || .@b == .@e)
        .@s += 1;
    if (.@c == .@d || .@c == .@e)
        .@s += 1;
    if (.@d == .@e)
        .@s += 1;

    // Randomness correction, but always keep the "7" in the list
    // (Should be 1200 but we used 1000 instead to keep the odds against you)
    @sfcasino=min(1000, @sfcasino+1);

    mes "";
    mesn;

    switch (.@s) {
    case 5:
        if (.@a == 7) {
            getitem ImperialCrown, 1;
            kamibroadcast(b(sprintf("%s has acquired the %s!", strcharinfo(0), getitemlink(ImperialCrown))), "JACKPOT");
            mesc b(l("Jackpot! Thou hast been crowned eternal as the gambler king!")), 4;
        } else {
            if (countitem(Blanket))
                getitem Bathrobe, 1;
            else
                getitem StrangeCoin, 20;
            Zeny += 350;
            gethomunexp 15000;
            mesc b(l("Thou hast been lucky and a %s prize was given to thee.", l("major"))), 4;
        }
        break;
    case 4:
        Zeny += 250;
        getitem StrangeCoin, rand2(1,5);
        gethomunexp 5000;
        mesc l("Thou hast been lucky and a %s prize was given to thee.", l("normal")), 3;
        break;
    case 3:
        Zeny += 150;
        getitem CasinoCoins, rand2(1,5);
        gethomunexp 500;
        mesc l("Thou hast been lucky and a %s prize was given to thee.", l("small")), 2;
        break;
    case 2:
        Zeny += 50;
        gethomunexp 50;
        mesc l("Thou hast been lucky and a %s prize was given to thee.", l("minor")), 6;
        break;
    default:
        mesc l("It wasn't this time..."), 9;
    }
    return;
}

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;
}

