// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Nurse.

024-8,73,26,0	script	Frostia's Nurse	NPC_ELF_F,{
    Nurse(.name$, 10, 5);
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonSkirt);
    setunitdata(.@npcId, UDT_HEADMIDDLE, TneckSweater);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 12);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 14);

    .sex = G_FEMALE;
    .distance = 5;
    end;
}

