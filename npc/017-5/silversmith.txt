// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Crafts silver objects

017-5,24,25,0	script	Silversmith	NPC_SILVERSMITH,{
    showavatar NPC_SILVERSMITH;
    goto L_Menu;

    // blacksmith_create( BaseItem1, Amount, BaseItem2, Amount, PrizeItem, Price )
    function blacksmith_create {
        .@base1=getarg(0);
        .@amon1=getarg(1);
        .@base2=getarg(2);
        .@amon2=getarg(3);
        .@prize=getarg(4);
        .@price=getarg(5);

		.@price=POL_AdjustPrice(.@price);

        mesn;
        mesq l("Do you want to craft @@? For that I will need:", getitemlink(.@prize));
        mesc l("@@/@@ @@", countitem(.@base1), .@amon1, getitemlink(.@base1));
        mesc l("@@/@@ @@", countitem(.@base2), .@amon2, getitemlink(.@base2));
        mesc l("@@/@@ GP", format_number(Zeny), format_number(.@price));

        select
            l("Yes"),
            l("No");

        if (@menu == 2)
            return;

        if (countitem(.@base1) >= .@amon1 &&
            countitem(.@base2) >= .@amon2 &&
            Zeny >= .@price) {
            inventoryplace .@prize, 1;
            delitem .@base1, .@amon1;
            delitem .@base2, .@amon2;
			POL_PlayerMoney(.@price);
            getitem .@prize, 1;
            .@xp=getiteminfo(.@base1, ITEMINFO_SELLPRICE)*.@amon1+getiteminfo(.@base2, ITEMINFO_SELLPRICE)*.@amon2;
            .@xp=.@xp*2/3;
            getexp .@xp, rand(1,10);

            mes "";
            mesn;
            mesq l("Many thanks! Come back soon.");
        } else {
            speech S_FIRST_BLANK_LINE,// | S_LAST_NEXT,
                    l("You don't have enough material, sorry.");
        }
        return;
    }


L_Menu:
    mesn l("Smith Silvers");
    mesq l("Hello, I am your local silversmith, here for all of your smithing needs!");
    next;
    select
        l("Nothing, sorry!"),
        l("I'd like my Crozenite Clover Silvered."),
        l("Silver Ring!"),
        l("Miere Cleaver!"),
        l("Broadsword!");

    switch (@menu) {
    case 2:
        blacksmith_create(SilverIngot, 3, CrozeniteFourLeafAmulet, 1, SilverFourLeafAmulet, 500);
        break;
    case 3:
        blacksmith_create(SilverIngot, 4, TinIngot, 2, SilverRing, 1000);
        break;
    case 4:
        blacksmith_create(SilverIngot, 12, Coal, 8, MiereCleaver, 8000);
        break;
    case 5:
        blacksmith_create(SilverIngot, 27, Coal, 20, Broadsword, 15000);
        break;
    }
    close;

OnInit:
    .sex=G_MALE;
    .distance=5;
    end;
}

