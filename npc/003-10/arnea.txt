// TMW2 Scripts
// Author:
//    4144
//    Jesusalva
// Description:
//    Arena for Duels and PVP (003-13,31,31)

003-10,26,60,0	script	Arnea	NPC_ELF_F,{
    npctalk3 l("Hello!");
	if (gettime(GETTIME_MONTH) == DECEMBER)
		mes("It's December! The arena will close for new year, be warned!");

L_Menu:
    mes "";
    mesn;
    mesq l("Welcome to the Arena. Select your action");
    menu
        l("Create new arena"), L_NewArena,
        l("Join existing arena"), L_JoinArena,
        rif(is_staff(), l("Debug Information")), L_Debug,
        l("Information"), L_Info,
        l("Hall Of Honor"), L_Scoreboards,
        l("Leave"), L_Quit;

L_NewArena:
    .@price=.price / max(1, reputation("Tulim"));
    mes "";
    mesn;
    if (Zeny < .@price) {
        mesq l("You need @@ GP to use this arena.", .@price);
        goto L_Menu;
    }

    mesq l("Okay, which arena will you rent? Cost is @@ GP.", .@price);
    menu
        rif(Zeny > .@price, l("Rent arena")), -,
        l("Give Up"), L_Quit;

    mes "";
    mesn;
    mesq l("Please type a password for your Arena, it must be unique.");
    input .@user_password$;

    // Leaving blank will close
    if (.@user_password$ == "")
        close;

    .@m = htget($@ARENAS, .@user_password$, -1);
    if (.@m > 0) {
        mes "";
        mesn;
        mesq l("Sorry, this password was already used on another arena.");
        next;
        /*
        mesn;
        mesq l("You'll need to think on a new password!");
        next;
        mesn;
        mesq l("If you don't know what to use, try using the current date or something.");
        next;
        */
        mesn;
        mesq l("Now, let's try again.");
        next;
        goto L_NewArena;
    }


    // XXX - Important Note - XXX
    // map name MUST be only 4 chars long (eg. "abcd") on char instances
    .@ID=.curinst;
    .curinst+=1;
    htput($@ARENAS, .@user_password$, .@ID);

    .@MAP$="ARENA@"+str(.@ID);

    // Create the arena
    .@INSTID = instance_create("ARX@"+(.@ID), 0, IOT_NONE);
    .@instanceMapName$ = instance_attachmap("003-13", .@INSTID, 0, .@MAP$);
    instance_set_timeout(1800, 1800, .@INSTID);
    instance_init(.@INSTID);

    // You are only charged once arena is all set
    Zeny=Zeny-.@price;

    dispbottom l("Arena created, it can be used for 30 minutes.");
    dispbottom l("Room password: @@", .@user_password$);
    mes "";
    mesn;
    mes l("Arena created, it can be used for 30 minutes.");
    mes l("Room password: @@", .@user_password$);
    if (is_staff()) mes l("Inst @@ Map @@", .@INSTID, .@instanceMapName$);
    next;

    goto L_Menu;


L_JoinArena:
    // FIXME
    /*
    warp "003-13", 31, 31;
    close;
    */
    mes "";
    mesn;
    mesq l("Okay, to join an arena, you need the unique password. Leave blank if you don't know.");

    input .@user_password$;
    .@m = htget($@ARENAS, .@user_password$, -1);
    if (.@m > 0) {
        /*
        if(has_instance2("ARENA@"+.@m) >= 0) {
            warp "ARENA@"+str(.@m), 31,31;
        */
        if (.@m < .curinst-2000) {
            // Delete the arena entry so this doesn't happens anymore
            htput $@ARENAS, .@user_password$, 0;
            // Explain this arena has expired
            mes "";
            mesn;
            mesq l("Sorry, that arena is already closed.");
            next;
            mesn;
            mesq l("All arenas stay open for only 30 minutes after being purchased.");
        } else {
            closeclientdialog;
            // If the arena has expired, this will fail.
            // But because dialog was closed, players won't notice anything.
            // It won't have any visual effect, just like when you don't use a password.
            // However, a debug message will be printed to console.
            // If I start seeing spam of this debug message, I'll take appopriate
            // measures, including and not limited to permanent ban, including IP ban.
            warp "ARENA@"+.@m, 31, 31;
            dispbottom l("Good luck!");
        }
    }
    close;


L_Debug:
    mes "npc name: " + .name$;
    mes "npc ext name: " + .extname$;
    mes "npc id: " + .id;
    mes "npc parent id: " + .parent;
    mes "npc src id: " + .srcId;
    mes "char id 3: " + getcharid(3);
    if (instance_id() >= 0)
        mes "instance id: " + instance_id();
    goto L_Menu;

L_Info:
    mesn;
    mesq lg("Hello darling.");
    next;
    mesq l("I am @@, and I take care of the Arena.", .name$);
    next;
    mesq l("Guards use it to spar against each other on friendly matches, to see who is stronger.");
    next;
    mesq l("We arranged a small underground room for that, because the Colliseum is too far away.");
    tutmes l("If you kill an opponent stronger than you, you will gain honor points. But if the oponent is 15 levels weaker than you, it will be NEGATIVE!"), l("About Scoreboards and Honor Points");
    tutmes l("You will also LOSE honor if the opponent is below level 30. If you are a bandit (negative honor), all fights versus you will be honorable."), l("About Scoreboards and Honor Points");
    tutmes l("If you kill the same person within 30 minutes, honor will not fluctuate. The whole honor system is very experimental."), l("About Scoreboards and Honor Points");
    next;
    goto L_Menu;

L_Scoreboards:
    mesc l("All scoreboards are refreshed hourly."), 1;
    HallOfHonor();
    next;
    goto L_Menu;

L_Quit:
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, DarkHelm);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CopperArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, JeansChaps);
    setunitdata(.@npcId, UDT_WEAPON, RockKnife);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 14);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 18);

    .sex = G_FEMALE;
    .distance = 5;
    //.alwaysVisible = true; // This is dumb, why Jesusalva put it here?
    .price=1000;
    .curinst=1001;
    npcsit;

    // create hashtable
    $@ARENAS = htnew();
    // Structure:
    // Password, Instance Control ID
    end;

// Every half hour, advance curinst in 1000.
// This way, we don't need to use/rely-on hasinstance2()
OnMinute30:
OnMinute60:
    .curinst+=1000;
    end;

// Protect bugs by disabling NPC on new year. This NPC have two days off for vacations B-)
OnDay1231:
OnDay0101:
    disablenpc(.name$);
    end;

OnDay0102:
    enablenpc(.name$);
    end;

}

