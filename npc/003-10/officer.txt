// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Wooden Sword quest. Pays back earlier players

003-10,30,79,4	script	Veteran Officer	NPC_PLAYER,{
    .@q=getq(TulimsharQuest_WoodenSword);
    if (.@q >= 2 || BaseLevel < 18) {
        legiontalk;
        close;
    }
    if (.@q == 0) {
        mesn;
        mesq l("Hey. If you want to kill monsters, you need a good sword. I think, a @@ would be the perfect fit for you.", getitemlink(WoodenSword));
        next;
        mesn;
        mesq l("I think, for just 25 @@, I could arrange one for you. Eh... Just don't think that you have an amazing weapon, alright?", getitemlink(RawLog));
        next;
        mesn;
        mesq l("You can kill @@ on the desert. It's a dangerous area though, so take care.", getmonsterlink(DesertLogHead));
        setq TulimsharQuest_WoodenSword, 1;
    } else {
        mesn;
        mesq l("I think, for just 25 @@, I could arrange a @@ for you.", getitemlink(RawLog), getitemlink(WoodenSword));
    }
    mes "";
    select
        rif(countitem(RawLog) >= 25, l("Here they are!")),
        l("Maybe later.");
    mes "";
    if (@menu == 1) {
        delitem RawLog, 25;
        //getitem WoodenSword, 1;
        CsysNpcCraft(WoodenSword, IOPT_EXPGAIN, 10, IOPT_RICHNESS, 10);
        setq TulimsharQuest_WoodenSword, 2;
        getexp 580, 20;
        mesn;
        mesq lg("Here's your sword, novice. Now go kill monsters.");
    }
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, DesertHelmet);
    setunitdata(.@npcId, UDT_HEADMIDDLE, Chainmail);
    setunitdata(.@npcId, UDT_HEADBOTTOM, WoodenSword);
    setunitdata(.@npcId, UDT_WEAPON, JeansShorts);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 2);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    npcsit;
    .sex=G_MALE;
    .distance=5;
    end;
}

