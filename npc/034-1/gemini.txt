// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Gemini Sisters Quest - Forest & Desert Stage

034-1,67,48,0	script	Fake Manastone	NPC_MANA_STONE,{
    if (instance_id() < 0 || getcharid(1) < 1) end;
    GeminiCheck(1);
    .@p=getcharid(1);
    mesc l("This is a weird stone. It looks like a Mana Stone from afar, but anyone can tell it is fake.");
    if (strcharinfo(0) != getpartyleader(.@p)) {
        mesc l("It may be dangerous. I better ask %s to check it instead.", getpartyleader(.@p));
        close;
    }
    switch ($@VALIA_STATUS[.@p]) {
    case 1:
    case 2:
        mesc l("It seems to be a mechanism of some kind, but it is missing a Runestone.");
        mesc l("Maybe one of the slimes dropped it.");
        $@VALIA_STATUS[.@p] = 2;
        break;
    case 3:
        mesc l("You insert the Runestone on it and hear a sound.");
        mesc l("Something changed; We should see what is.");
        $@VALIA_STATUS[.@p] = 4;
        break;
    default:
        mesc l("I already did everything I could with this. What am I waiting for?");
        break;
    }
    close;

OnInit:
OnInstanceInit:
    .distance = 2;
    end;
}

034-1,66,45,0	script	#GeminiExit1	NPC_HIDDEN,0,0,{
    end;
OnTouch:
    if (instance_id() < 0 || getcharid(1) < 1) end;
    GeminiCheck(1);
    .@p=getcharid(1);
    if ($@VALIA_STATUS[.@p] < 4) {
        dispbottom l("There seems to be sort of lock preventing you from passing.");
        end;
    }
    slide 118, 55;
    end;
}


034-1,169,24,0	script	#GeminiExit2	NPC_HIDDEN,1,0,{
    end;
OnTouch:
    if (instance_id() < 0 || getcharid(1) < 1) end;
    GeminiCheck(4);
    .@p=getcharid(1);
    if ($@VALIA_STATUS[.@p] < 6) {
        if (countitem(SealedSoul) >= 7) {
            mesc l("Do you want to use the souls to unlock the passage?"), 1;
            next;
            if (askyesno() == ASK_YES) {
                delitem SealedSoul, 7;
                closeclientdialog;
                if ($@VALIA_STATUS[.@p] == 4) {
                    .@u=monster(getmap(), 163, 26, strmobinfo(1, JackO), JackO, 1);
                    unittalk(.@u, "Souls... Feed me Souls... Nooooooooowwww!!!!");
                    $@VALIA_STATUS[.@p]=5;
                } else {
                    dispbottom l("The waterfall open, and you may now pass.");
                    $@VALIA_STATUS[.@p]=6;
                    close;
                }
            }
            close;
        }
        dispbottom l("A powerful magic barrier prevents passage. %d %s should suffice to dispel... Maybe.", 7, getitemlink(SealedSoul));
        end;
    }
    warp "val2@"+.@p, 24, 59;
    end;
}

