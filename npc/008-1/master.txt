// TMW-2 Script
// Author:
//  Jesusalva
// Description:
//  This NPC is a work on progress. It measures all players equal.
// Controls the first floor, but not many things to control.

008-1,48,117,0	script	#FPDM12	NPC_HIDDEN,0,0,{
    end;
OnTouch:
    // IMPORTANT: Prevent party master from returning here
    getmapxy(.@m$, .@x, .@y, 0, getpartyleader(getcharid(1)));
    if (.@m$ ~= "008-*" &&
        .@m$ != "008-0" &&
        .@m$ != "008-1" &&
        BaseLevel > 20 &&
        mobcount("008-1", "First Dungeon Master::OnBossDeath") == 0 &&
        @pmloop) {
        dispbottom l("Go and follow @@, your party leader!", getpartyleader(getcharid(1)));
        warp "008-2", 135, 20;
    } else if (BaseLevel > 20) {
        dispbottom l("If I bring @@, my party leader, and the boss is defeated, I can go you in.", getpartyleader(getcharid(1)));
    } else {
        mesc l("I cannot pass, because I am only level @@.", BaseLevel);
        mesc l("Newbies can only get past their limits once, with their party leader level help! If they die, they can't rejoin!");
        // I thought it would be better to allow first passage based on leader level.
        // Parties are not meant to be permanent, and this helps a lot.
    }
    end;
}

008-1,48,117,0	script	First Dungeon Master	NPC_BRGUARD_SWORD,{
    // Double-check, you should have been flung out long ago.
    if (getcharid(1) <= 0 || @pmloop <= 0) {
        mesn;
        mesq l("Something seems wrong.");
        warp "SavePoint",0,0;
        close;
    }
    if (mobcount("008-1", "First Dungeon Master::OnBossDeath") > 0) {
        npctalk3 l("What are you doing? Fight!");
        end;
    }

    mesn;
    mesq l("Compliments reaching this far, @@ from the @@ party!", strcharinfo(0), getpartyname(getcharid(1)));
    next;

    mesn;
    mesq l("I can summon the BOSS for the level 0~20 area.");
    next;
    if (strcharinfo(0) != getpartyleader(getcharid(1))) goto L_NotYou;
    if (BaseLevel < 20) goto L_TooWeak;
    mesn;
    mesq l("He is strong though, so keep your party together!");
    next;
    // Get info about your party, and backup it
    getpartymember(getcharid(1));
	.@count = $@partymembercount;
	copyarray(.@name$[0], $@partymembername$[0], $@partymembercount);
	copyarray(.@aid[0], $@partymemberaid[0], $@partymembercount);
	copyarray(.@cid[0], $@partymembercid[0], $@partymembercount);
    .@count_online=0;

    if (.@count < 3 && !$@GM_OVERRIDE) goto L_TooSmall;
    mesn;
    mesc l("Are you and your party ready?"), 1;
    if (askyesno() != ASK_YES)
        close;

    // Loop though party to see if you can start
	for (.@i = 0; .@i < .@count; ++.@i) {
        // Online?
		if (isloggedin(.@aid[.@i], .@cid[.@i])) {
            getmapxy(.@m$, .@x, .@y, 0, .@name$[.@i]);
            // Here?
            if (.@m$ == .map$)
    			.@count_online++;
        }
    }

    // How many are logged in and here?
    if (.@count_online < 3 && !$@GM_OVERRIDE) goto L_TooSmall;

    // TODO: Query if exp sharing is enabled
    // TODO: Instance for party
    // TODO: Second Floor
    npctalk l("@@: Fight!", getpartyname(getcharid(1)));
    monster "008-1", rand(37,54), rand(109,122), "First Dungeon Boss", Sarracenus, 1, "First Dungeon Master::OnBossDeath";
    close;










// Only the party leader can start this.
L_NotYou:
    mesn;
    dispbottom l("Go and fetch @@, the party leader!", getpartyleader(getcharid(1)));
    close;

// Minimum 3 players
L_TooSmall:
    mesn;
    mesq l("However, I need to see at least three volunteers here, to allow you in.");
    close;

// Must have level to face boss
L_TooWeak:
    mesn;
    mesq l("This is the end for your party, as the leader doesn't have sufficient level.");
    close;

// Second Floor special monsters
OnReward:
    Zeny=Zeny+200;
    getexp 200, 20;
    dispbottom l("Reward: 200 GP, 200 XP");

// Boss death causes 008-2 to be set
OnBossDeath:
    // Give every party member in map a reward
    partytimer("008-1", 200, "First Dungeon Master::OnReward", getcharid(1));

    // Warp everyone and add timers
    warpparty("008-2", 135, 20, getcharid(1), "008-1", true);

    // Bonus Monsters
    monster("008-1", 104, 238, "Time Bonus", NightScorpion, 1, "Party Master::OnMobTime");
    monster("008-1", 85, 117, "Time Bonus", BlackScorpion, 1, "Party Master::OnMobTime");

    // Five Chests
    monster("008-2", 70, 239, "Mysterious Chest", any(BronzeChest, BronzeMimic), 1);
    monster("008-2", 70, 241, "Mysterious Chest", any(BronzeChest, BronzeMimic), 1);
    monster("008-2", 70, 243, "Mysterious Chest", any(BronzeChest, BronzeMimic, SilverChest, SilverMimic), 1);
    monster("008-2", 70, 245, "Mysterious Chest", any(BronzeChest, BronzeMimic), 1);
    monster("008-2", 70, 247, "Mysterious Chest", any(BronzeChest, BronzeMimic), 1);
    end;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}

