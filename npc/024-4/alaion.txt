// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Random shops

024-4,36,23,0	script	Alaion	NPC_ELF,{
    npcshopattach(.name$);
    shop .name$;
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, any(Chainmail, LightPlatemail, CopperArmor, AssassinChest));
    setunitdata(.@npcId, UDT_HEADBOTTOM, any(RaidTrousers, LeatherTrousers, JeansChaps, BromenalPants, AssassinPants, ChainmailSkirt));
    setunitdata(.@npcId, UDT_WEAPON, AncientSword);
    setunitdata(.@npcId, UDT_HAIRSTYLE, rand2(5,15));
    setunitdata(.@npcId, UDT_HAIRCOLOR, rand2(2,10));

    .sex = G_MALE;
    .distance = 4;
    sleep(SHOPWAIT);
    tradertype(NST_MARKET);

    sellitem Halberd, -1, 1;
    sellitem Broadsword, -1, 1;
    sellitem Kitana, -1, 1;
    sellitem ShortSword, -1, 1;
    sellitem Backsword, -1, 1;
    sellitem ShortGladius, -1, 1;
    end;

OnClock1250:
OnClock0112:
    restoreshopitem Halberd, 1;
    restoreshopitem Broadsword, 1;
    restoreshopitem Kitana, 1;
    restoreshopitem ShortSword, 1;
    restoreshopitem Backsword, 1;
    restoreshopitem ShortGladius, 1;
    end;

// Pay your taxes!
OnBuyItem:
    debugmes("Purchase confirmed");
    PurchaseTaxes();
    end;

OnSellItem:
    debugmes("Sale confirmed");
    SaleTaxes();
    end;
}

