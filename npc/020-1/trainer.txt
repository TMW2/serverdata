// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Mercenary Trainer

020-1,39,89,0	script	Mercenary Trainer	NPC_REDY_MALE_SWORD,{
    mesn;
    mesq l("Hello, I am a sword to hire, a Mercenary Trainer and Chief.");
    next;
    mesn;
    mesq l("Do you want to hire a mercenary? Or perhaps get a card so you can invoke them later? I can even make them stronger if you wish.");
    next;
    goto L_Main;

L_Main:
    select
        l("Hire a mercenary"),
        l("Buy a mercenary card"),
        l("Evolve mercenaries"),
        l("Bye.");
    mes "";
    switch (@menu) {
        // Hire Card
        case 1:
            menuint
                l("[%d GP] [Lv 1~25] Hire for one hour", 2500), 1,
                l("[%d GP] [Lv 26~40] Hire for one hour", 7500), 2,
                l("[%d GP] [Lv 41~60] Hire for one hour", 15000), 3,
                l("[%d GP] [Lv 61~79] Hire for one hour", 30000), 4,
                l("[%d GP] [Lv 80~100] Hire for one hour", 50000), 5,
                l("I've changed my mind"), 0;
            switch (@menuret) {
                case 1:
                    .@gp=max(2000, POL_AdjustPrice(2500));
                    if (Zeny < .@gp) {
                        mesc l("You cannot pay."), 1;
                        next;
                    } else {
                        .@mid=merc_randid(0, 0, 0, 0, 1000);
                        mercenary_create(.@mid, 3600000);
                        POL_PlayerMoney(.@gp);
                    }
                    break;
                case 2:
                    .@gp=max(7000, POL_AdjustPrice(7500));
                    if (Zeny < .@gp) {
                        mesc l("You cannot pay."), 1;
                        next;
                    } else {
                        .@mid=merc_randid(0, 0, 0, 1000, 0);
                        mercenary_create(.@mid, 3600000);
                        POL_PlayerMoney(.@gp);
                    }
                    break;
                case 3:
                    .@gp=max(14000, POL_AdjustPrice(15000));
                    if (Zeny < .@gp) {
                        mesc l("You cannot pay."), 1;
                        next;
                    } else {
                        mercenary_create merc_randid(0, 0, 1000, 0, 0), 3600000;
                        POL_PlayerMoney(.@gp);
                    }
                    break;
                case 4:
                    .@gp=max(27000, POL_AdjustPrice(30000));
                    if (Zeny < .@gp) {
                        mesc l("You cannot pay."), 1;
                        next;
                    } else {
                        mercenary_create merc_randid(0, 1000, 0, 0, 0), 3600000;
                        POL_PlayerMoney(.@gp);
                    }
                    break;
                case 5:
                    .@gp=max(45000, POL_AdjustPrice(50000));
                    if (Zeny < .@gp) {
                        mesc l("You cannot pay."), 1;
                        next;
                    } else {
                        mercenary_create merc_randid(1000, 0, 0, 0, 0), 3600000;
                        POL_PlayerMoney(.@gp);
                    }
                    break;
                default:
                    break;
            }
            break;
        // Buy Card
        case 2:
            npcshopattach(.name$);
            openshop;
            closedialog;
            close;
            break;
        // Evolve Card
        case 3:
            mesc l("ERROR: Unimplemented Function"), 1;
            break;
        // Leave
        default:
            closeclientdialog;
            goodbye;
            close;
            break;
    }
    goto L_Main;

OnInit:
	tradertype(NST_MARKET);
    sellitem MercBoxEE, 25000, 1;
    sellitem MercBoxDD, 15000, 2;
    sellitem MercBoxCC, 7500, 3;
    sellitem MercBoxBB, 3750, 4;
    sellitem MercBoxAA, 1250, 5;

    .distance=5;
    .sex=G_MALE;
	end;

OnClock0001:
OnClock1201:
    restoreshopitem MercBoxEE, 25000, 1;
    restoreshopitem MercBoxDD, 15000, 2;
    restoreshopitem MercBoxCC, 7500, 3;
    restoreshopitem MercBoxBB, 3750, 4;
    restoreshopitem MercBoxAA, 1250, 5;
    end;

// Pay your taxes!
OnBuyItem:
    debugmes("Purchase confirmed");
    PurchaseTaxes("Nival");
    end;

OnSellItem:
    debugmes("Sale confirmed");
    PurchaseTaxes("Nival");
    end;

}

