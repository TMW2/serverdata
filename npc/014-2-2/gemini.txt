// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Gemini Sisters Quest - Central Core

034-1,0,0,0	script	#Gemini	NPC_HIDDEN,{
    end;

OnCoreInit:
    debugmes "CORE INIT, Inst = %d", instance_id();
    // Oops! You do not have enough level!
    if (BaseLevel < 30) {
        warp "014-2-2", 36, 24;
        end;
    }
    // Disable the cart
    if ($HARDCORE) {
        if (isequipped(RentCart)) {
            unequipbyid(RentCart);
            setcart 0;
        }
        // Remove the cart
        if (countitem(RentCart)) {
            delitem RentCart, countitem(RentCart);
            dispbottom l("The %s was destroyed!", getitemlink(RentCart));
        }
    }
    .@p = getcharid(1);
    instance_attach($@VALIA_INST[.@p]);
    @mystatus = 0;
    sleep2(200);
    if (!$@VALIA_STATUS[.@p])
        $@VALIA_STATUS[.@p] = 1;
    // FALLTHROUGH
OnCore:
    if (!compare(getmap(), "val")) end;
    .@p = getcharid(1);
    instance_attach($@VALIA_INST[.@p]);
    // Always true on first interaction
    // This handles new stages for person
    if (@mystatus < $@VALIA_STATUS[.@p]) {
        // No retroaction!
        switch ($@VALIA_STATUS[.@p]) {
        case 1: // Quest initiated
            // Spawn monsters
            areamonster("val1@"+.@p, 20, 20, 60, 60, strmobinfo(1, GreenSlime), GreenSlime, (BaseLevel/15), "Valia::OnGSlime");
            areamonster("val1@"+.@p, 20, 20, 70, 60, strmobinfo(1, LogHead), LogHead, (BaseLevel/15), "Valia::OnLHead");
            areamonster("val1@"+.@p, 20, 20, 70, 60, strmobinfo(1, ForestMushroom), ForestMushroom, (BaseLevel/12), "Valia::OnFShroom");
            areamonster("val1@"+.@p, 20, 20, 70, 60, strmobinfo(1, RedMushroom), RedMushroom, 1+(BaseLevel/30), "Valia::OnRShroom");
            break;
        case 2: // Looking for a runestone
            if (@mystatus)
                getexp 4000, 100;
            break;
        case 3: // Runestone was found
            if (@mystatus)
                getexp 8000, 200;
            break;
        case 4: // Desert unlocked, but no clue on what to do
            dispbottom l("You hear a noise from distance.");
            // Spawn monsters
            areamonster("val1@"+.@p, 100, 20, 170, 60, strmobinfo(1, CaveSnake), CaveSnake, 3, "Valia::OnCSnake");
            areamonster(getmap(), 100, 20, 170, 60, strmobinfo(1, MagicGoblin), MagicGoblin, 1, "Valia::OnMGoblin");
            // These refuse to respawn but are numerous
            // Thus far, the quest is still easy. But soon it won't be.
            areamonster(getmap(), 100, 20, 170, 60, strmobinfo(1, MountainSnake), MountainSnake, 1+(BaseLevel/24));
            areamonster(getmap(), 100, 20, 170, 60, strmobinfo(1, DarkLizard), DarkLizard, 1+(BaseLevel/20));
            areamonster(getmap(), 100, 20, 170, 60, strmobinfo(1, WickedMushroom), WickedMushroom, 1+(BaseLevel/32));
            areamonster(getmap(), 100, 20, 170, 60, strmobinfo(1, EliteDuck), EliteDuck, 1);
            break;
        case 5: // JackO shows up
            if (@mystatus)
                getexp 16000, 400;
            break;
        case 6: // Passage Open
            if (countitem(SealedSoul))
                delitem SealedSoul, countitem(SealedSoul);
            dispbottom l("You hear waterfall opening in distance.");
            if (@mystatus)
                getexp 32000, 800;
            break;
        case 7: // Ambushed & Captured
            if (!is_gm())
                nude();
            @lockpicks=false;
            // Prision Area
            .@m=any(Bandit, RobinBandit, SleepingBandit);
            areamonster("val2@"+.@p, 52, 93, 70, 98, strmobinfo(1, .@m), .@m, 1);
            .@m=any(Bandit, RobinBandit, SleepingBandit);
            areamonster("val2@"+.@p, 52, 93, 70, 98, strmobinfo(1, .@m), .@m, 1);
            // Ante-chamber
            areamonster("val2@"+.@p, 69, 96, 85, 107, strmobinfo(1, Bandit), Bandit, 1);
            .@m=any(Bandit, RobinBandit, SleepingBandit, RobinBandit);
            areamonster("val2@"+.@p, 69, 96, 85, 107, strmobinfo(1, .@m), .@m, 1);
            // Reception & Lord Desk
            areamonster("val2@"+.@p, 50, 30, 80, 75, strmobinfo(1, Bandit), Bandit, (BaseLevel/15));
            areamonster("val2@"+.@p, 50, 30, 80, 75, strmobinfo(1, RobinBandit), RobinBandit, (BaseLevel/25));
            // Whole Map
            areamonster("val2@"+.@p, 20, 24, 78, 72, strmobinfo(1, Archant), Archant, (BaseLevel/8));
            // Great Hall
            areamonster("val2@"+.@p, 25, 24, 50, 45, strmobinfo(1, RobinBandit), RobinBandit, (BaseLevel/22));
            areamonster("val2@"+.@p, 25, 24, 50, 45, strmobinfo(1, Assassin), Assassin, (BaseLevel/20));
            areamonster("val2@"+.@p, 25, 24, 50, 45, strmobinfo(1, Bandit), Bandit, (BaseLevel/18));
            areamonster("val2@"+.@p, 25, 24, 50, 45, strmobinfo(1, HoodedNinja), HoodedNinja, 1);
            break;
        case 8: // Bandit Key retrieved
            if (@mystatus)
                getexp 64000, 1600;
            break;
        case 9: // Door seal broken
            if (@mystatus)
                getexp 128000, 3200;
            break;
        case 10: // Luvia first cutscene
            // Limit exports of Lazurite outside the area
            if (countitem(LazuriteShard) > 2)
                delitem LazuriteShard, countitem(LazuriteShard)/2;
            // These two remain for internal quests
            if (countitem(LazuriteCrystal))
                delitem LazuriteCrystal, countitem(LazuriteCrystal);
            if (countitem(LazuriteHeart))
                delitem LazuriteHeart, countitem(LazuriteHeart);
            break;
        case 11: // Survived cutscene
            if (@mystatus)
                getexp 256000, 6400;
            break;
        case 12: // Storage cutscene
            break;
        case 13: // Survived storage
            if (@mystatus)
                getexp 384000, 9600;
            break;
        case 14: // Luvia Showdown cutscene
            break;
        case 15: // Defeated Luvia and now on pursuit!
            if (@mystatus)
                getexp 512000, 12800;
            break;
        case 16: // Cooldown in progress
            break;
        }
        @mystatus = $@VALIA_STATUS[.@p];
    }
    addtimer(2000, "#Gemini::OnCore");
    end;
}

// Max experience gain: 1,404,000 XP and 35,100 JXP

