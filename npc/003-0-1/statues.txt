// TMW-2 Script
// Author:
//    Jesusalva
// Description:
//    These statues are of great honor to whoever have their name written in them!

003-0-1,63,30,0	script	Fortune Statue	NPC_STATUE_BANKER,{
    if (.rate_limit >= gettimetick(2))
        end;
    .rate_limit=gettimetick(2);

    HallOfFortune();
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    .rate_limit=0;
    end;
}

003-0-1,53,30,0	script	Strength Statue	NPC_STATUE_GUARD,{
    if (.rate_limit >= gettimetick(2))
        end;
    .rate_limit=gettimetick(2);

    HallOfLevel();
    next;
    HallOfJob();
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    .rate_limit=0;
    end;
}

003-0-1,56,26,0	script	Hero Statue	NPC_STATUE_ANDREI,{

    mes l("This statue was built for memory of Andrei Sakar, the greatest hero this world has even seen.");
    mes l("For defending Hurnscald alone and saving all its inhabitants.");
    mes l("For fighting against the Monster King once and getting out alive to tell the story.");
    mes l("For all his great deeds, and thousands of lives he saved, this statue is in his honor.");
    if ($MOST_HEROIC$ == "")
        goto L_Fame;
    next;
    mes l("Also in honor of @@, who did a great act of bravery recently. May they keep protecting our world!", $MOST_HEROIC$);
    // TODO: Must find a better place for this
    next;
    mes l("And in honor of all brave LoF players, to be known to all, the fluffly hunters.");
    mes "BunnyBear (239) - 2017-11-07 10:04:29";
    mes "Scorpius (190) - 2017-01-09 21:33:00";
    mes "Billr (177) - 2016-05-21 23:53:22";
    mes "Naburudanga (153) - 2017-07-28 22:14:07";
    mes "Axzell (150) - 2017-01-09 22:23:00";

L_Fame:
    next;
    mesq l("All hail the ones who proven their worth before the whole Alliance!");

    HallOfHonor();
    HallOfGuild();
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;
}

003-0-1,60,26,0	script	Worker Statue	NPC_STATUE_CONTRIBUTOR,{

    HallOfSponsor();
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;
}


003-0-1,68,28,0	script	#Statue2018	NPC_STATUE_2018,{

    HallOf2018();
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;
}

003-0-1,67,27,0	script	#Statue2019	NPC_STATUE_2019,{

    HallOf2019();
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;
}

003-0-1,68,26,0	script	#Statue2020	NPC_STATUE_2020,{

    HallOf2020();
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;
}

003-0-1,67,25,0	script	#Statue2021	NPC_STATUE_2021,{

    HallOf2021();
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;
}

