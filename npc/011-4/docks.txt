// TMW2 Scripts
// There are three docks here, one which returns you to 014-2 and two which dismount
// Those which dismount have two NPCs nearby to confirm the operation. If it did not
// succeed (back'n'forth), you sink. Remember you MUST use a slide
// Having a rowboat is never checked here, it can safely expire.
// setmount 3; setmount 0;

011-4,124,21,0	script	#Dock0114A	NPC_HIDDEN,0,1,{
    end;
OnTouch:
    warp "014-2", 25, any(96, 97);
    sleep2(10);
    setmount 0;
    end;
}


011-4,85,67,0	script	#Dock0114B1	NPC_HIDDEN,0,1,{
    end;
OnTouch:
    dispbottom l("You are now at Graveyard Island.");
    slide 82, 65;
    sleep2(10);
    setmount 0;
    end;
}

011-4,83,67,0	script	#Dock0114B2	NPC_HIDDEN,0,1,{
    end;
OnTouch:
    slide 85, 65;
    sleep2(10);
    setmount 3;
    end;
}


011-4,236,72,0	script	#Dock0114C1	NPC_HIDDEN,0,1,{
    end;
OnTouch:
    dispbottom l("You are now at Elli's Tower.");
    slide 239, 70;
    sleep2(10);
    setmount 0;
    end;
}

011-4,238,72,0	script	#Dock0114C2	NPC_HIDDEN,0,1,{
    end;
OnTouch:
    slide 236, 70;
    sleep2(10);
    setmount 3;
    end;
}

