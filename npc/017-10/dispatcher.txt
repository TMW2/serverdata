// TMW2/LoF scripts.
// Authors:
//    Jesusalva
// Description:
//    Deploys an Homunculus in a mission during your offline time
// Variables
//    HOMUN_DEPLOYDATE
//      When the Homunculus is scheduled to arrive
//    HOMUN_DEPLOYFIELD
//      ID of the field which Homunculus was deployed

017-10,61,34,0	script	Erik	NPC_JOURNALMAN,{
    function lootField;
    inspectorQuest();
    mesn;
    mesq l("Hello. My name is Erik, and I have Homunculus go out in errands in my stead.");

    // If you haven't summoned yet, or killed it...
    if (!gethominfo(0)) {
        mesc l("You do not own an Homunculus."), 1;
        close;
    }

    // Check what your homunculus is up to
    switch (homstatus()) {
        case 0:
            mesc l("Your homunculus is ready!"), 2;
            break;
        case 1:
            mesc l("Your homunculus is sleeping! Wake them up!"), 3;
            close;
            break;
        case 2:
            mesc l("Your homunculus is already on a mission!"), 1;

            // Homunculus should return from mission
            if (gettimetick(2) > HOMUN_DEPLOYDATE) {
                select
                    l("Recall");
                .@xp=lootField(HOMUN_DEPLOYFIELD);
                recallhomunculus();
                gethomunexp(.@xp);
                .@lv=gethominfo(6);
                getexp 0, (.@lv*min(10000, gethominfo(3))/10000)*5;
                close;
            }
            mesc l("It should be back in @@", FuzzyTime(HOMUN_DEPLOYDATE)), 1;
            next;
            mesn;
            mesq l("If you want, I can warp you back to the Professor in Tulimshar while you wait... But I'll charge you %d GP.", .warpfee);
            if (Zeny < .warpfee) close;
            next;
            mesc l("Warp back to the carpet?"), 1;
            if (askyesno() == ASK_YES) {
                Zeny -= .warpfee;
                warp "003-0-1", 35, 36;
                dispbottom "Erik : "+l("Fly away!");
            }
            closeclientdialog;
            close;
            break;
        default:
            Exception("ERROR! homstatus code "+homstatus(), RB_DEFAULT|RB_SPEECH|RB_ISFATAL);
            break;
    }

    // This is a bug: They are ready but shouldn't be.
    if (gettimetick(2) < HOMUN_DEPLOYDATE) {
        Exception("ERROR: YOU WERE BANNED FROM USING ERIK SERVICES. (might be a bug).", RB_SPEECH|RB_ISFATAL);
        close;
    }

L_Main:
    select
        l("Deploy to Piou Fields [1h]"),
        l("Deploy to Herb Fields [1h]"),
        rif(gethominfo(6) >= 10, l("Deploy to Ore Mines [2h]")),
        rif(gethominfo(6) >= 10, l("Deploy to Rare Mines [2h]")),
        rif(gethominfo(6) >= 15, l("Deploy to Slime Nest [2h]")),
        rif(gethominfo(6) >= 30, l("Deploy to Snake Pit [3h]")),
        rif(gethominfo(6) >= 45 && $GAME_STORYLINE >= 3, l("Deploy to Dangerous Area [6h]")),
        rif(gethominfo(6) >= 60 && $GAME_STORYLINE >= 4, l("Deploy to Monster King Basement [8h]")),
        rif(gethominfo(6) >= 80 && $GAME_STORYLINE >= 5, l("Deploy to Moubootaur's Domain [12h]")),
        l("Deploy to The Market [1h]"),
        l("Don't deploy");
    mes "";
        // Common drops: At least 90% drop
        // Rare drops: 5% or less drop rate
    switch (@menu) {
        case 1:
            mesc ".:: " + l("Piou Fields") + " ::.";
            mesc l("The most basic grinding camp for your homunculus.");
            mesc l("Average EXP: "+1800), 3;
            mesc l("Common Drops: "+
                    getitemlink(PiouFeathers));
            mesc l("Regular Drops: "+
                    getitemlink(PiouLegs)+
                    getitemlink(HalfEggshell));
            mesc l("Rare Drops: "+
                    getitemlink(GoldenApple));
            next;
            .@dest=HCD_PIOUFIELDS;
            .@hours=1;
            break;
        case 2:
            mesc ".:: " + l("Herb Fields") + " ::.";
            mesc l("If you need more herbs, the best place to look.");
            mesc l("Average EXP: "+1200), 3;
            mesc l("Common Drops: "+
                    getitemlink(ArtichokeHerb));
            mesc l("Regular Drops: "+
                    getitemlink(CobaltHerb)+
                    getitemlink(GambogeHerb)+
                    getitemlink(MauveHerb)+
                    getitemlink(AlizarinHerb));
            mesc l("Rare Drops: "+
                    getitemlink(ReedBundle));
            next;
            .@dest=HCD_HERBFIELDS;
            .@hours=1;
            break;
        case 3:
            mesc ".:: " + l("Ore Mines") + " ::.";
            mesc l("Send your Homunculus to mineration!");
            mesc l("Average EXP: "+2400), 3;
            mesc l("Common Drops: "+
                    getitemlink(Coal));
            mesc l("Regular Drops: "+
                    getitemlink(IronOre)+
                    getitemlink(CopperOre)+
                    getitemlink(SilverOre)+
                    getitemlink(GoldOre));
            mesc l("Rare Drops: "+
                    getitemlink(TinOre)+
                    getitemlink(LeadOre)+
                    getitemlink(TitaniumOre)+
                    getitemlink(IridiumOre)+
                    getitemlink(PlatinumOre));
            next;
            .@dest=HCD_OREMINES;
            .@hours=2;
            break;
        case 4:
            mesc ".:: " + l("Rare Mines") + " ::.";
            mesc l("Send your Homunculus to mineration!");
            mesc l("Average EXP: "+2500), 3;
            mesc l("Common Drops: "+
                    getitemlink(Pearl));
            mesc l("Regular Drops: "+
                    getitemlink(Diamond)+
                    getitemlink(Ruby)+
                    getitemlink(Emerald)+
                    getitemlink(Sapphire)+
                    getitemlink(Topaz)+
                    getitemlink(Amethyst));
            mesc l("Rare Drops: "+
                    getitemlink(LightGreenDiamond)+
                    getitemlink(BlackPearl)+
                    getitemlink(BlueManaPearl));
            next;
            .@dest=HCD_RAREMINES;
            .@hours=2;
            break;
        case 5:
            mesc ".:: " + l("Slime Nest") + " ::.";
            mesc l("Send your Homunculus to a slime nest!");
            mesc l("Average EXP: "+3600), 3;
            mesc l("Common Drops: "+
                    getitemlink(MaggotSlime)+
                    getitemlink(Arrow));
            mesc l("Regular Drops: "+
                    getitemlink(IronArrow)+
                    getitemlink(TreasureKey)+
                    getitemlink(CactusDrink)+
                    getitemlink(CoinBag)+
                    getitemlink(CasinoCoins)+
                    getitemlink(Coal)+
                    getitemlink(Dagger)+
                    getitemlink(Candy)+
                    getitemlink(ChocolateBar));
            mesc l("Rare Drops: "+
                    getitemlink(Milk)+
                    getitemlink(IcedBottle));
            next;
            .@dest=HCD_SLIMENEST;
            .@hours=2;
            break;
        case 6:
            mesc ".:: " + l("Snake Pit") + " ::.";
            mesc l("Send your Homunculus to hold off snakes!");
            mesc l("Average EXP: "+format_number(5400)), 3;
            mesc l("Common Drops: "+
                    getitemlink(SnakeEgg)+
                    getitemlink(SnakeTongue));
            mesc l("Regular Drops: "+
                    getitemlink(SnakeSkin)+
                    getitemlink(BlackMambaEgg));
            mesc l("Rare Drops: "+
                    getitemlink(JeansShorts)+
                    getitemlink(DivineApple)+
                    getitemlink(Kanabo));
            next;
            .@dest=HCD_SNAKEPIT;
            .@hours=3;
            break;
        case 7:
            mesc ".:: " + l("Dangerous Area") + " ::.";
            mesc l("Send your Homunculus to loot the Impregnable Fortress!");
            mesc l("Average EXP: "+format_number(140000)), 3;
            mesc l("Common Drops: None");
            mesc l("Regular Drops: "+
                    getitemlink(MoubooSteak)+
                    getitemlink(HastePotion)+
                    getitemlink(StrengthPotion)+
                    getitemlink(SmallMushroom)+
                    getitemlink(Grenade)+
                    getitemlink(MercBoxA)+
                    getitemlink(AncientBlueprint)+
                    getitemlink(Lockpicks)+
                    getitemlink(StatusResetPotion)+
                    getitemlink(DungeonMap)+
                    getitemlink(TreasureMap));
            mesc l("Rare Drops: "+
                    getitemlink(MagicApple));
            next;
            .@dest=HCD_DANGERAREA;
            .@hours=6;
            break;
        case 8:
            mesc ".:: " + l("Monster King Basement") + " ::.";
            mesc l("Send your Homunculus to one of the most dangerous...!");
            mesc l("Average EXP: "+format_number(750000)), 3;
            mesc l("Common Drops: None");
            mesc l("Regular Drops: "+
                    getitemlink(Curshroom)+
                    getitemlink(HastePotion)+
                    getitemlink(StrengthPotion)+
                    getitemlink(SmallMushroom)+
                    getitemlink(Grenade)+
                    getitemlink(AncientBlueprint)+
                    getitemlink(DungeonMap)+
                    getitemlink(TreasureMap)+
                    getitemlink(CrazyRum)+
                    getitemlink(MercBoxB));
            mesc l("Rare Drops: "+
                    getitemlink(WhiskeyAle)+
                    getitemlink(GoldenApple)+
                    getitemlink(DivineApple)+
                    getitemlink(MagicApple));
            next;
            .@dest=HCD_MKBASEMENT;
            .@hours=8;
            break;
        case 9:
            mesc ".:: " + l("Moubootaur's Domain") + " ::.";
            mesc l("Send your Homunculus to THE most dangerous...!");
            mesc l("Average EXP: "+format_number(1500000)), 3;
            mesc l("Common Drops: "+
                    getitemlink(Wurtzite)+
                    getitemlink(ChickenLeg)+
                    getitemlink(Tomato));
            mesc l("Regular Drops: "+
                    getitemlink(Quill)+
                    getitemlink(DodgeDonut)+
                    getitemlink(MintDonut)+
                    getitemlink(ChocolateDonut)+
                    getitemlink(StrawberryDonut)+
                    getitemlink(SparklyChocolateDonut)+
                    getitemlink(SparklyStrawberryDonut)+
                    getitemlink(SkullBloodyMug)+
                    getitemlink(PurificationPotion)+
                    getitemlink(DeathPotion)+
                    getitemlink(CrazyRum)+
                    getitemlink(MercBoxC));
            mesc l("Rare Drops: "+
                    getitemlink(Setzer)+
                    getitemlink(TortugaShell)+
                    getitemlink(SaviorBlueprint)+
                    getitemlink(DivineApple)+
                    getitemlink(MagicApple));
            next;
            .@dest=HCD_MOUBOOTAUR;
            .@hours=12;
            break;
        case 10:
            mesc ".:: " + l("The Market") + " ::.";
            mesc l("Send your Homunculus to make money!");
            mesc l("Average EXP: "+900), 3;
            mesc l("Maximum Profit: @@ GP", format_number(.zenycap)), 3;
            mesc l("Common Drops: "+
                    getitemlink(Piberries));
            mesc l("Regular Drops: "+
                    getitemlink(Bread)+
                    getitemlink(Cheese)+
                    getitemlink(Beer));
            mesc l("Rare Drops: "+
                    getitemlink(TreasureMap)+
                    getitemlink(DungeonMap));
            next;
            .@dest=HCD_MARKET;
            .@hours=1;
            break;
        default:
            close;
    }

    // Confirmation
    mesc l("Really deploy your homunculus?"), 1;
    if (askyesno() == ASK_NO)
        goto L_Main;

    // Deploy Homunculus (if possible)
    if (!homstatus() && gethominfo(0)) {
        deployhomunculus();
        mesc l("Deployed!"), 2;
        HOMUN_DEPLOYFIELD=.@dest;
        HOMUN_DEPLOYDATE=gettimetick(2)+(60*60*.@hours);
    }
    close;

// lootField (Field ID)
// Returns amount of Homunculus EXP to get
function lootField {
    // Ensure you have free space
    inventoryplace Iten, 3, NPCEyes, 9;
    .@lv=gethominfo(6);

    // Base EXP is based on Homunculus level and intimacy
    .@val=.@lv*min(10000, gethominfo(3))/10000;

    switch (getarg(0)) {
    case HCD_PIOUFIELDS:
        .@val+=1800;
        anyloot(PiouFeathers, .@lv, 10000,
                PiouLegs, max(1, .@lv/2), 8000,
                HalfEggshell, .@lv, 6000,
                GoldenApple, 1, 100);
        break;
    case HCD_HERBFIELDS:
        .@val+=1200;
        anyloot(ArtichokeHerb, .@lv, 10000,
                CobaltHerb, max(1, .@lv/2), 6000,
                GambogeHerb, max(1, .@lv/2), 6000,
                MauveHerb, max(1, .@lv/2), 6000,
                AlizarinHerb, max(1, .@lv/2), 6000,
                ShadowHerb, max(1, .@lv/3), 4000,
                ReedBundle, 2, 800);
        break;
    case HCD_OREMINES:
        .@val+=2400;
        .@mk = ($FORTRESS_STATE ? 10 : 5);
        anyloot(Coal, .@lv, 9999,
                IronOre, max(1, .@lv/2), 2000,
                CopperOre, max(1, .@lv/2), 1000,
                SilverOre, max(1, .@lv/3), 700,
                GoldOre, max(1, .@lv/4), 450,
                TinOre, max(1, .@lv/5), 350,
                LeadOre, max(1, .@lv/6), 300,
                TitaniumOre, max(1, .@lv/8), 10 * .@mk,
                (REBIRTH ? IridiumOre : TitaniumOre), max(1, .@lv/10), 8 * .@mk,
                (REBIRTH ? PlatinumOre : IridiumOre), 1, 2 * .@mk);
        break;
    case HCD_RAREMINES:
        .@val+=2500;
        anyloot(Pearl, 1, 9999,
                Diamond, limit(1, .@lv/2, 3), 2000,
                Ruby, limit(1, .@lv/2, 3), 1700,
                Emerald, limit(1, .@lv/2, 3), 1400,
                Sapphire, limit(1, .@lv/2, 3), 1100,
                Topaz, limit(1, .@lv/2, 3), 800,
                Amethyst, limit(1, .@lv/2, 3), 500,
                LightGreenDiamond, 1, 100,
                BlackPearl, 1, 10,
                BlueManaPearl, 1, 1);
        break;
    case HCD_SLIMENEST:
        .@val+=3600;
        anyloot(MaggotSlime, .@lv, 10000,
                Arrow, .@lv, 9000,
                IronArrow,  .@lv, 4000,
                TreasureKey, max(1, .@lv/10), 6000,
                CactusDrink, limit(1, .@lv/10, 3), 4000,
                CoinBag,     limit(1, .@lv/8, 5), 4000,
                CasinoCoins, limit(1, .@lv/6, 10), 3200,
                Coal,        limit(1, .@lv/10, 3), 3000,
                Dagger,      limit(1, .@lv/15, 3), 3000,
                Candy,       limit(1, .@lv/5, 6), 2000,
                ChocolateBar, limit(1, .@lv/20, 3), 1000,
                Milk, 1, 200,
                IcedBottle, 1, 60);
        break;
    case HCD_SNAKEPIT:
        .@val+=5400;
        anyloot(SnakeEgg, limit(1, .@lv/8, 10), 9600,
                SnakeTongue, limit(1, .@lv/16, 5), 9500,
                SnakeSkin, limit(1, .@lv/20, 3), 7000,
                BlackMambaEgg, limit(1, .@lv/40, 3), 4000,
                JeansShorts, 1, 300,
                DivineApple, 1, 10,
                Kanabo, 1, 1); // Teasing Xanthem for mapping here
        break;
    case HCD_DANGERAREA:
        .@val+=140000;
        anyloot(HastePotion, limit(1, .@lv/20, 10), 4000,
                StrengthPotion, limit(1, .@lv/20, 10), 4000,
                SmallMushroom, limit(1, .@lv/18, 8), 4000,
                Grenade, limit(1, .@lv/28, 6), 3000,
                MoubooSteak, limit(1, .@lv/28, 6), 2500,
                MercBoxA, 1, 2000,
                AncientBlueprint, 1, 1500,
                Lockpicks, limit(1, .@lv/30, 4), 1000,
                StatusResetPotion, 1, 1000,
                DungeonMap, 1, 700,
                TreasureMap, 1, 700,
                MagicApple, 1, 40);
        break;
    case HCD_MKBASEMENT:
        .@val+=750000;
        anyloot(HastePotion, limit(1, .@lv/20, 10), 6000,
                StrengthPotion, limit(1, .@lv/20, 10), 6000,
                Curshroom, limit(1, .@lv/28, 6), 4500,
                SmallMushroom, limit(1, .@lv/18, 8), 4500,
                Grenade, limit(1, .@lv/28, 6), 3500,
                AncientBlueprint, 1, 2500,
                DungeonMap, 1, 2000,
                TreasureMap, 1, 2000,
                CrazyRum, 1, 1500,
                MercBoxB, 1, 1000,
                WhiskeyAle, 1, 400,
                GoldenApple, 1, 80,
                DivineApple, 1, 60,
                MagicApple, 1, 45);
        break;
    case HCD_MOUBOOTAUR:
        .@val+=1500000;
        anyloot(Tomato, limit(1, rand2(.@lv/20), 8), 9500,
                Wurtzite, limit(1, rand2(.@lv/25), 6), 9500,
                ChickenLeg, limit(1, rand2(.@lv/25), 6), 9500,
                Quill, 1, 6500,
                SkullBloodyMug, limit(1, .@lv/28, 5), 4200,
                ChocolateDonut, limit(1, .@lv/24, 5), 4000,
                StrawberryDonut, limit(1, .@lv/24, 5), 4000,
                DodgeDonut, limit(1, .@lv/22, 5), 3500,
                MintDonut, limit(1, .@lv/22, 5), 3500,
                SparklyChocolateDonut, limit(1, .@lv/26, 5), 3000,
                SparklyStrawberryDonut, limit(1, .@lv/26, 5), 3000,
                DeathPotion, limit(1, .@lv/41, 3), 2000,
                PurificationPotion, limit(1, .@lv/42, 3), 1800,
                CrazyRum, limit(1, .@lv/44, 3), 1500,
                MercBoxC, 1, 1000,
                TortugaShell, 10, 80,
                DivineApple, 1, 70,
                MagicApple, 1, 50,
                Setzer, 1, 6,
                SaviorBlueprint, 1, 1);
        break;
    case HCD_MARKET:
        .@val+=36;
        // Sending a fresh homun in market may yield no GP.
        Zeny+=min(.zenycap, 1000*.@lv*min(10000, gethominfo(3))/100000);
        anyloot(Piberries, .@lv, 9000,
                Bread, limit(1, .@lv/3, 12), 7000,
                Cheese, .@lv, 5000,
                Beer, limit(1, .@lv/10, 10), 1200,
                TreasureMap, 1, 420,
                DungeonMap, 1, 280);
        break;
    }
    // A final boost for sake of randomness
    .@val+=rand2(0, 10+gethominfo(6));
    return .@val;
}

OnInit:
    .sex=G_MALE;
    .distance=5;
    // Old Maximum Profit: 10,000 GP
    // Coin Bag farming: 180 mobs, at 5% drop rate: 5,000 GP max

OnSun1751:
    .zenycap=7500*min(1, ($GAME_STORYLINE+1)/2);
    .warpfee=$GAME_STORYLINE*100;
    end;

}

