// TMW2 script
// Author:
//   Saulc
//   Jesusalva
// Description:
//    Handles pirate attacks

016-7,0,0,0	script	#MarineShipAttack	NPC_HIDDEN,117,29,{
    end;

OnEvent:
    warp "016-7", 31, 33;
    dispbottom l("##1The ship is under a pirate's attack! ##BKill all or survive!");
    addtimercount "#MarineShip::OnEvent", 10000;
    addtimer 2000, "#MarineShipAttack::OnStart";
    end;

OnStart:
    // Define number of pirates based on player level.
    // Player count is not important here, as this is run for every player.
    .@c=max(1, (BaseLevel/15));
    areamonster "016-7", 23, 25, 51, 30, strmobinfo(1, OceanPirate), OceanPirate, .@c, "#MarineShipAttack::OnPirateDie";
    end;

OnPirateDie:
    addtimercount "#MarineShip::OnEvent", 5000;
    if (!mobcount("016-7", "#MarineShipAttack::OnPirateDie")) {
        addtimercount "#MarineShip::OnEvent", 15000;
        mapannounce "016-7", l("The pirate ship captain appear! TAKE CARE!!"), bc_map;
        .@pirate=monster("016-7", 38, 27, "Pirate Captain", OceanPirate, 1, "#MarineShipAttack::OnVictory");
        sc_start SC_INCMHP, 900000, 1000, 10000, SCFLAG_FIXEDTICK|SCFLAG_NOAVOID|SCFLAG_NOICON, .@pirate;
        sc_start SC_ATTHASTE_POTION1, 900000, 40, 10000, SCFLAG_FIXEDTICK|SCFLAG_NOAVOID|SCFLAG_NOICON, .@pirate;
        sc_start SC_INCHIT, 900000, 1000, 10000, SCFLAG_FIXEDTICK|SCFLAG_NOAVOID|SCFLAG_NOICON, .@pirate;
    }
    end;

OnVictory:
    if (rand2(10000) < REBIRTH+((readparam2(bLuk) + readparam2(bVit)) / 2))
        getitem PirateBandana, 1;
    areatimer("016-7", 23, 25, 52, 35, 10, "#MarineShipAttack::OnReward");
    end;

OnReward:
    // That would give 10% from missing exp, but I didn't like it.
    //getexp ((NextBaseExp-BaseExp)/10), 0;
    addtimercount "#MarineShip::OnEvent", 3500;

    // Lv 40 rewards: 400 exp, 40 jxp, 200~600 GP
    getexp BaseLevel*10, BaseLevel;
    Zeny=Zeny+BaseLevel*rand2(5,15);
    // 7% chance to get Crazy Rum
    if (rand2(10000) < 700+(readparam2(bLuk)*3))
        getitem CrazyRum, 1;
    dispbottom l("Congratulations!");
    if (isin("016-7", 23, 25, 52, 35))
        addtimer(3000, "#MarineShipAttack::OnResumeTravel");
    end;

OnResumeTravel:
    if (@timer_navio_running)
        warp "016-6", 31, 33;
    end;
}

