// TMW2/LoF Script.
// Author:
//    Jesusalva
// Notes:
//    Based on BenB idea.

018-2-4,23,24,0	script	Vault#01824a	NPC_NO_SPRITE,{
    LootableVault(2, 3, "01824");
    close;

OnInit:
    .distance=3;
    end;

OnClock0201:
OnClock1216:
    $VAULT_01824+=rand2(30,65);
    end;
}


018-2-4,23,45,0	duplicate(Vault#01824a)	Vault#01824b	NPC_NO_SPRITE

