// TMW2 Script.
// Author:
//    Jesusalva
// Description:
//    Part of Helena's quest
//    Gives spoilers about what Jesusalva plans on doing with Bryant

015-2,28,199,0	script	Helena's Teammate	NPC_ORC,{
    .@q=getq(HurnscaldQuest_Bandits);
    if (.@q == 5) goto L_SilverKey;
    if (.@q == 6) goto L_BanditLord;
    mesn;
    mesq lg("Welcome.");
    mesq l("This small island is a good place to rest, as the monsters here are not very threatening.");
    close;

L_SilverKey:
    mesn;
    mesq l("Don't say anything, I can smell the scent of Helena's hair on you.");
    next;
    inventoryplace SilverKey, 1, PiberriesInfusion, 3;
    getitem SilverKey, 1;
    getitem PiberriesInfusion, 3;
    setq HurnscaldQuest_Bandits, 6;
    mesn;
    mesq l("I guess she sent you to kill another Bandit Lord, right? I'll give you the key for his room.");
    next;
    mesn;
    mesq l("It is pretty close to this island. I advise you to use a good weapon, like a well-made sword, and heal yourself often.");
    next;
    mesn;
    mesq l("I gave you 3 @@. Use them in this fight, or you're doomed to fail.", getitemlink(PiberriesInfusion));
    close;

L_BanditLord:
    mesn;
    mesq l("What are you waiting for? Go kill the bandit lord.");
    next;
    mesn strcharinfo(0);
    select
        l("I'm going, don't worry."),
        l("There was nobody on the Bandit Lord's room.");
    if (@menu == 1)
        close;
    mes "";
    mesn;
    mesq l("...Have you even gone there yet?");
    next;
    mesn;
    mesq l("This is not The Mana World. My name is not Bryant.");
    next;
    mesn;
    mesq l("In fact, Bryant is my brother, and he lives in a cave, and he probably has a high level quest too, but that's not important now.");
    next;
    mesn;
    mesq l("Go kill the Bandit Lord!");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, FairyHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, ForestArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, JeansChaps);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 24);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 1);

    .sex = G_MALE;
    .distance = 5;
    end;
}
