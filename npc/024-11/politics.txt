// TMW2 Scripts
// Author:
//    Jesusalva
// Description:
//    Town Administrator file, see npc/functions/politics.txt
// User variables:
//    #POL_APPLYWEEK = Week of last application
//    #POL_VOTEDAY = Day of last vote

024-11,24,30,0	script	Frostia Office	NPC_POLITICS,{
do
{
    mesc ".:: "+l("Frostia Townhall")+" ::.", 2;
    mesc l("Current Town Administrator: ")+$FROSTIA_MAYOR$, 3;
    if (Class != Elven)
        mesc l("Only elves may run to Town Admin Office in Frostia!"), 1;
    else
        mesc l("Hey, you're an elf, cool! But you still cannot run for office here!"), 1;
    close;
    POL_TownInfo("FROSTIA");
    mesc l("Application fee: @@ GP", .applytax);
    next;
    select
        l("Information"),
        rif(strcharinfo(0) == $FROSTIA_MAYOR$, l("Manage Town")),
        rif(#POL_APPLYWEEK != gettimeparam(GETTIME_WEEKDAY), l("Apply for the office!")),
        l("View Candidate List and cast a vote"),
        l("[Quit]");

    switch (@menu) {
        case 1:
            POL_Information();
            break;
        case 2:
            POL_Manage("FROSTIA");
            break;
        case 3:
            // array_push might be too sensible for getd/setd
            if (Zeny < .applytax)
                break;
            Zeny-=.applytax;
            $FROSTIA_MONEY+=.applytax;
            #POL_APPLYWEEK=gettimeparam(GETTIME_WEEKDAY);
            array_push($FROSTIA_CANDIDATE$, strcharinfo(0));
            array_push($FROSTIA_VOTES, 0);
            mesc l("Application successful!"), 3;
            next;
            break;
        case 4:
            POL_Candidate("FROSTIA");
            break;
        default:
            close;
    }
} while (true);
end;

OnInit:
    .applytax=50;
    .distance=4;
    end;
}

