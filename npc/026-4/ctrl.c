// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//   The Impregnable Fortress Control Files
// Quest: General_Fortress
//  (MaxFloor+2, internal, internal)

// This script must set 026-4 up on init, and refresh it when needed
-	script	Impregnable#B4F	NPC_HIDDEN,{
    function _moveNpc;
    end;

// Initialize the dungeon
OnInit:
    .InstId = false;
    .npc_x = 0;
    .npc_y = 0;
    // The 19 mobs which compose the Bloody List
    setarray $@MKB4F_MOBS, NightmareDragon, Snail, WhirlyBird, PinkieSuseran, Jhon, Mandragora, PinkieMaximus, Junglefowl, Tengu, Moubi, SuperiorShroom, Nutcracker, Golem, GreenhornAbomination, ShadowTortuga, FireElement, WaterElement, EarthElement, WindElement;
    sleep(6000); // Delay 6s before initialization
    .InstId = CreateMaze(IOT_NONE, MAZE_SIZE_X, "026-4");
    instance_set_timeout(0, 0, .InstId);
    instance_init(.InstId);
    debugmes "Impregnable Fortress B4F (%d): Init OK at %d", .InstId, gettimetick(0);
    setmapflag("026-4", mf_zone, "SuperMMO");
    goto OnMon2359;

OnMon2359:
    if (!.InstId) end;
    // Reload .InstId (weekly takeover)
    debugmes "Reloading B4F... %d", gettimetick(0);
    killmonsterall("026-4");
    MazeMobs(145, false, 18, "026-4", 50); // Initiate Lv 145 mobs
    MazeMobs(115, false, 52, "026-4", 50); // Initiate Lv 115 mobs
    debugmes "Reloaded B4F, %d", gettimetick(0);
    // Move NPCs around
    goto OnClock1843;

OnClock1843:
    if (!.InstId) end;
    // Movement will need to happen here (daily modifications)
    // We need however to validate the coordinates they're jumping to...
    _moveNpc("026-4");
    unitwarp(getnpcid("#026-4Switch_1"), "026-4", .npc_x, .npc_y);
    _moveNpc("026-4");
    unitwarp(getnpcid("#026-4Switch_2"), "026-4", .npc_x, .npc_y);
    _moveNpc("026-4");
    unitwarp(getnpcid("#026-4Switch_3"), "026-4", .npc_x, .npc_y);
    _moveNpc("026-4");
    unitwarp(getnpcid("#026-4Switch_4"), "026-4", .npc_x, .npc_y);
    _moveNpc("026-4");
    unitwarp(getnpcid("#026-4Switch_5"), "026-4", .npc_x, .npc_y);
    _moveNpc("026-4");
    unitwarp(getnpcid("#026-4Gate"), "026-4", .npc_x, .npc_y);
    end;

function _moveNpc {
    // Try to warp randomly, up to 30 attempts
    .@e=0;
    .@mx=getmapinfo(MAPINFO_SIZE_X, getarg(0))-20;
    .@my=getmapinfo(MAPINFO_SIZE_Y, getarg(0))-20;
    do {
        if (.@e >= 30)
            break;
        .npc_x = rand2(20, .@mx);
        .npc_y = rand2(20, .@my);
        .@e+=1;
    } while (!checknpccell(getarg(0), .npc_x, .npc_y, cell_chkpass));
    return;
} // _moveNpc

}


// The switches main code
000-0,0,0,0,	script	#026-4Switch_1	NPC_SWITCH_OFFLINE,{
    // Retrieve my own metadata
    .@n$=strnpcinfo(0, "_0");
    explode(.@ni$, .@n$, "_");
    .@id=atoi(.@ni$[1]);
    .@bit = (2 ** (.@id - 1));
    // Do not react if already done
    if (getq(General_Fortress) != 5)
        end;
    // If already flipped, then don't do pointless things
    if (getq2(General_Fortress) & .@bit) {
        dispbottom l("I already flipped this switch, no point flipping it again.");
        end;
    }
    // After used by someone, can't be used by others for ~30 seconds
	if (getnpcclass() == NPC_SWITCH_ONLINE)
		end;
    // The Bloody Switch: Assign random monster (Miller + char ID + Switch ID)
    // Maze mobs should count (and qualify for mob points)
    // All target mobs should be Lv 100+
    .@minKills = 10;
    .@mobId = $@MKB4F_MOBS[miller_rand(.@id, getcharid(0), 18)];
    mesn l("The Bloody Switch");
    mes l("Our master respects %s, so those whom wish to meet him, the following shall they prove:", b(l("strength")));
    mesc l("Kill %d/%d %s in this session.", @bloodykill[.@mobId], .@minKills, getmonsterlink(.@mobId)), 1;
    if (@bloodykill[.@mobId] < .@minKills)
        close;
    next;
    // Mark this switch as done
    setq2 General_Fortress, getq2(General_Fortress) | .@bit;
    dispbottom l("You hear a *clank*, it must have worked.");
    // Check if this was the last switch
    if (getq2(General_Fortress) == 31) {
        dispbottom l("This was the last one of them. Now to find the door...");
        setq General_Fortress, 6, 0, 0;
        specialeffect(FX_MGWARP, SELF, getcharid(3));
    }
    // Final decorators
    closeclientdialog;
	setnpcdisplay .name$, NPC_SWITCH_ONLINE;
    sleep(30000); // Detach the player (instead of sleep2)
	setnpcdisplay .name$, NPC_SWITCH_OFFLINE;
	end;
OnInit:
	.distance=3;
    end;
}

000-0,0,0,0	duplicate(#026-4Switch_1)	#026-4Switch_2	NPC_SWITCH_OFFLINE
000-0,0,0,0	duplicate(#026-4Switch_1)	#026-4Switch_3	NPC_SWITCH_OFFLINE
000-0,0,0,0	duplicate(#026-4Switch_1)	#026-4Switch_4	NPC_SWITCH_OFFLINE
000-0,0,0,0	duplicate(#026-4Switch_1)	#026-4Switch_5	NPC_SWITCH_OFFLINE

// The main NPC which lets you out of here
000-0,0,0,0,	script	#026-4Gate	NPC_ICE_CAGE,0,0,{
    if (getq(General_Fortress) > 5) goto L_Warp;
    dispbottom l("After flipping all %s switches, I should go there.", l("five"));
    end;

OnTouch:
    if (getq(General_Fortress) > 5) goto L_Warp;
    mesc l(".:: Impregnable Fortress, %sF ::.", "B4"), 3;
    msObjective(getq(General_Fortress) == 6, l("* Flip all five switches"));
    msObjective($MK_TEMPVAR >= MKIF_LV_B5F, l("Minimum wins: %d/%d", $MK_TEMPVAR, MKIF_LV_B5F));
    mes "";
    mesc l("Hint: The Bloody Switches.");
    close;

L_Warp:
    // This is a more complex warp
    .@e = 0;
    do {
        if (.@e >= 30) break;
        .@x = rand2(20, 40);
        .@y = rand2(20, 40);
        .@e+=1;
    } while (!checknpccell("026-5", .@x, .@y, cell_chkpass));
    fortwarp(MKIF_LV_B5F, "026-5", .@x, .@y);
    end;

OnInit:
	.distance=3;
    end;
}

