// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Part of Speed Run Quest, and an unplanned assassin quest.

004-2,61,118,0	script	Pachua	NPC_INDIGENOUS_OLD,1,1,{
    if (@pachua == 1) goto OnTouch;
    mesn;
    mesq l("Howdy! Mirio and I scout this canyon to protect our clan. I am also a retired assassin, maybe someday I can exchange some knowledge with you! Hahaha!");
    if (getq(LoFQuest_EPISODE) == 9 &&
        countitem(Honey) >= 10 &&
        countitem(ElixirOfLife)) {
        next;
        select
            l("Nice."),
            l("[10x Honey, 1x Elixir of Life] The Shadow Tortuga won the race against the Panthom Lord.");
        mes "";
        if (@menu == 2) {
            mesc l("%s lifts an eyebrow to you.", .name$);
            next;
            inventoryplace ArcmageBoxset, 1;
            mesn;
            mesq l("So they need me again. Alright. Tell them the following:");
            mes b(l("The crow took off to his last flight."));
            next;
            delitem Honey, 10;
            delitem ElixirOfLife, 1;
            getitem ArcmageBoxset, 1;
            setq LoFQuest_EPISODE, 10;
            mesn;
            mesq l("And you, take this %s. It is a reward for doing the dirty job. You would think old age would let you retire, hahaha!", getitemlink(ArcmageBoxset));
            close;
        }
    }
    close;

OnTouch:
    if (@pachua != 1) end;
    // If you're disarmed (riding?) apply a small time penalty
    // checkmount() might not work? untested?
    if (getequipid(EQI_HAND_R) < 1 || getequipid(EQI_HAND_L) < 1) {
        setq2 HalinarzoQuest_Speed, getq2(HalinarzoQuest_Speed) - 30;
    }
    message strcharinfo(0), "Pachua quickly inhales from his pipe and releases a ring of smoke towards the sky!";
    @pachua = 2;
    end;

OnInit:
    .distance = 4;
    .sex = G_MALE;
    end;
}

