// TMW2 Script.
// Author:
//    Jesusalva
// Description:
//    Error Handler

004-2,64,63,0	script	Sign#HalinRoute	NPC_SWORDS_SIGN,{
    mesc "← "+l("Tulimshar");
    mesc "→ "+l("Canyon - Safe Route");
    mesc "↓ "+l("Canyon - Settlement Route");
    mes "";
    mesc l("\"Follow the light.\" - Weary traveler");
    mesc l("\"Those whom believe the Pink Moouboo wear rock knifes at the entrances.\" - Aahna");
    mesc l("\"Those whom stray from the light shall met a quick death.\" - Saulc, from the 'Blame Saulc' famous book"), 1;
    tutmes l("If you fail to see two statues while trying to go to Halinarzo, backtrack. Do note that caves without torches ARE NOT SAFE!");
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 3;
    end;
}

004-2,81,23,0	script	Sign#TempBugfix	NPC_SWORDS_SIGN,{
    if (countitem(DesertTablet)) goto L_Tablet;
    if (MAGIC_LVL >= 7) goto L_RawPower;
    mesc l("Impossible to read.");
    close;

L_Tablet:
    mes l("The %s shines with a strange light...", getitemlink(DesertTablet));
    next;
    closeclientdialog;
    cwarp "001-3", 117, 135;
    close;

L_RawPower:
    mes l("You can sense powerful cloaking magic emanating from this sign. With your superior magic, you dismiss the enchantment, and see what the cliffs have been hiding all along...");
    next;
    closeclientdialog;
    cwarp "001-3", 117, 135;
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 3;
    end;
}

