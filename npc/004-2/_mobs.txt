// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 004-2: Desert Mountains mobs
004-2,0,0,0,0	monster	Maggot	1030,100,35000,150000
004-2,118,37,17,15	monster	Scorpion	1071,14,35000,150000
004-2,61,43,10,22	monster	Scorpion	1071,6,35000,150000
004-2,111,58,4,4	monster	Mister Prickel	1436,1,35000,35000
004-2,118,100,4,4	monster	Snake	1122,3,35000,150000
004-2,87,54,9,4	monster	Snake	1199,1,35000,150000
004-2,77,66,14,11	monster	Scorpion	1071,8,35000,150000
004-2,94,56,42,32	monster	Desert Log Head	1127,30,35000,150000
004-2,56,98,1,1	monster	Duck	1029,1,35000,150000
004-2,71,100,28,10	monster	Red Scorpion	1072,10,35000,150000
004-2,146,28,3,2	monster	Yellow Slime	1091,1,35000,150000
004-2,143,47,6,18	monster	Scorpion	1071,9,35000,150000
004-2,140,57,9,8	monster	Snake	1122,2,35000,150000
004-2,142,96,3,5	monster	Mountain Snake	1123,1,35000,300000
004-2,131,75,17,9	monster	Snake	1122,3,35000,150000
004-2,70,106,20,14	monster	Mister Prickel	1436,5,35000,35000
