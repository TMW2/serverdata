// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Gemini Sisters Quest - Part D: Final Chamber

034-4,144,82,0	script	#GeminiFExit	NPC_HIDDEN,0,0,{

OnWumpus:
    GeminiCheck(15);
    .@q = getq2(HurnscaldQuest_Gemini);
    // Note: Only one Wumpus Egg can be exchanged for a Sunny Crystal
    if (.@q == 0 || .@q == 2 || (.@q > 5 && !(.@q % 3)))
        getitem WumpusEgg, 1;
    else
        getitem LightGreenDiamond, 1;
    getexp 0, rand2(7500, 9999);
    setq2 HurnscaldQuest_Gemini, .@q + 1;
    @forced_sick$ = "";
    end;

OnRw:
    getitem StrangeCoin, 1;
    if (!GEMINI_WINNER)
        GEMINI_WINNER = gettimetick(2);
    X24Event(X24_GEMINI);
    end;

OnExit:
    .@p=getcharid(1);
    partytimer(MAZE_MAP$, 10, "#GeminiFExit::OnRw", getcharid(1));
    if ($GEMINI_WINNER$ == "") {
        $GEMINI_WINNER$=strcharinfo(0);
        channelmes("#world", $GEMINI_WINNER$+" is the first player to finish Gemini Sisters Quest!! GG, dude! %%N");
        announce "All hail ##B"+$GEMINI_WINNER$+"##b, first to complete the ##3Gemini Sisters Quest!", bc_all|bc_npc;
        getitem PrismGift, 1;
        mesc l("CONGRATULATIONS! You are the first player to finish Gemini Sisters quest!!"), 2;
        mesc l("You just gained a Prism Gift for your bravery!"), 2;
        next;
    }
    sleep2(400);
    if ($GAME_STORYLINE >= 5) {
        warp "023-3-1", 45, 77;
        warpparty("023-3-1", 45, 77, getcharid(1), MAZE_MAP$, true);
    } else {
        warp "014-2-2", 35, 20;
        warpparty("014-2-2", 35, 20, getcharid(1), MAZE_MAP$, true);
    }
    end;

OnTouch:
    if (instance_id() < 0 || getcharid(1) < 1) end;
    GeminiCheck(15);
    // Only the party leader go ahead
    if (strcharinfo(0) != getpartyleader(getcharid(1))) {
	    mes l("Only %s has the key.", getpartyleader(getcharid(1)));
        close;
    }

    mesc l("Are you sure you want to leave?"), 1;
    mesc l("You, and everyone on the party, won't be able to return."), 1;
    if (askyesno() == ASK_NO) close;
    closeclientdialog;

    // Create maze and populate (From 45x45 to 60x60)
    CreateMaze(IOT_CHAR, MAZE_SIZE_M | MAZE_SIZE_G);
    .@mx=getmapinfo(MAPINFO_SIZE_X, MAZE_MAP$)-20;
    .@my=getmapinfo(MAPINFO_SIZE_Y, MAZE_MAP$)-20;
    .@tl=(20-.@mx)*(20-.@my) * 3 / 10; // Total tiles + collision guess
    .@tl=(.@tl / 10) + 1; // Monster density is a bit random

    /* *** Copied from 006-5/groata.txt & 018-2-2/main.txt! *** */
    .@mb[0] = MagicGoblin;
    .@mb[1] = CaveMaggot;
    array_push(.@mb, BronzeChest);
    array_push(.@mb, SmallMagicBif);
    array_push(.@mb, Bif);
    array_push(.@mb, RobinBandit);
    array_push(.@mb, SilverChest);
    array_push(.@mb, DustGatling);
    array_push(.@mb, MagicBif);
    array_push(.@mb, DustRifle);
    array_push(.@mb, DustRevolver);
    array_push(.@mb, GoldenChest);
    array_push(.@mb, GreatMoubooSlime);
    array_push(.@mb, Piousse);
    array_push(.@mb, ManaPiou);
    array_push(.@mb, ForestPiou);
    array_push(.@mb, HouseMaggot);
    array_push(.@mb, LittleYellowSlime);
    array_push(.@mb, MoubooSlime);
    array_push(.@mb, SmallFrog);
    array_push(.@mb, BigFrog);
    array_push(.@mb, Lavern);
    array_push(.@mb, LittleRedSlime);
    array_push(.@mb, ChocolateSlime);
    array_push(.@mb, Duck);
    array_push(.@mb, Bat);
    array_push(.@mb, CaveMaggot);
    array_push(.@mb, ManaGhost);
    array_push(.@mb, ManaBug);
    array_push(.@mb, FireGoblin);
    array_push(.@mb, ViciousSquirrel);
    array_push(.@mb, RedScorpion);
    array_push(.@mb, WhiteSlime);
    array_push(.@mb, AzulSlime);
    array_push(.@mb, DesertLogHead);
    array_push(.@mb, RedSlime);
    array_push(.@mb, DesertBandit);
    array_push(.@mb, Sarracenus);
    array_push(.@mb, IceMaggot);
    array_push(.@mb, VampireBat);
    array_push(.@mb, Bandit);
    array_push(.@mb, Assassin);
    array_push(.@mb, Skeleton);
    array_push(.@mb, CaveSnake);
    array_push(.@mb, GreenSlime);
    array_push(.@mb, CopperSlime);
    array_push(.@mb, YellowSlime);
    array_push(.@mb, SantaSlime);
    array_push(.@mb, LavaSlime);
    array_push(.@mb, Bluepar);
    array_push(.@mb, DeathCat);
    array_push(.@mb, Moggun);
    array_push(.@mb, RedMushroom);
    array_push(.@mb, CandiedSlime);
    array_push(.@mb, OldSnake);
    array_push(.@mb, GrassSnake);
    array_push(.@mb, Snake);
    array_push(.@mb, BlackSlime);
    array_push(.@mb, Pollet);
    array_push(.@mb, PiouKnight);
    array_push(.@mb, Shrewboo);
    array_push(.@mb, Wolvern);
    array_push(.@mb, FireSkull);
    array_push(.@mb, DarkLizard);
    array_push(.@mb, ArmoredSkeleton);
    array_push(.@mb, BlackScorpion);
    array_push(.@mb, ElectroWorm);
    array_push(.@mb, EarthFairy);
    array_push(.@mb, FireFairy);
    array_push(.@mb, WaterFairy);
    array_push(.@mb, WindFairy);
    array_push(.@mb, PoisonFairy);
    array_push(.@mb, MountainSnake);
    array_push(.@mb, HoodedNinja);
    array_push(.@mb, ForestMushroom);
    array_push(.@mb, GoldenScorpion);
    array_push(.@mb, FallenGuard2);
    array_push(.@mb, WickedMushroom);
    array_push(.@mb, Archant);
    array_push(.@mb, Scar);
    array_push(.@mb, Crafty);
    array_push(.@mb, Forain);
    array_push(.@mb, GreenDragon);
    array_push(.@mb, Michel);
    array_push(.@mb, Troll);
    array_push(.@mb, EliteDuck);
    array_push(.@mb, AzulSkullSlime);
    array_push(.@mb, Moonshroom);
    array_push(.@mb, RedSkullSlime);
    array_push(.@mb, Terranite);
    array_push(.@mb, JackO);
    array_push(.@mb, BlackMamba);
    array_push(.@mb, GreenSkullSlime);
    array_push(.@mb, BloodyMouboo);
    array_push(.@mb, GoboBear);
    array_push(.@mb, TerraniteProtector);
    array_push(.@mb, WhirlyBird);

    /* Spawn them and make hostile */
    freeloop(true);
    for (.@i = 0; .@i < 1+(.@tl); .@i++) {
        .@mid = any_of(.@mb);
        .@m=areamonster(MAZE_MAP$, 20, 20, .@mx, .@my, strmobinfo(1, .@mid), .@mid, 1);
        set_aggro(.@m);
    }
    freeloop(false);

    // Spawn & Configure the boss monster
    // Defeating the boss yields a bonus
    .@mob=areamonster(MAZE_MAP$, 60, 60, .@mx-40, .@my-40, "Wumpus?", PanthomLord, 1, "#GeminiFExit::OnWumpus");
    setunitdata(.@mob, UDT_MAXHP,    250000);
    setunitdata(.@mob, UDT_HP,       250000);
    setunitdata(.@mob, UDT_SPEED,       275);
    setunitdata(.@mob, UDT_HIT,        3500);
    setunitdata(.@mob, UDT_DMOTION,      50);

    // NOTE: Once you exit, put the quest in cooldown for 4 hours as well.
    .@mx = getmapinfo(MAPINFO_SIZE_X, MAZE_MAP$); .@my = getmapinfo(MAPINFO_SIZE_Y, MAZE_MAP$);
    .@mob=monster(MAZE_MAP$, .@mx-25, .@my-25, "Exit", FortressGate, 1, "#GeminiFExit::OnExit");
    setunitdata(.@mob, UDT_MAXHP,     50000);
    setunitdata(.@mob, UDT_HP,        50000);

    // Miscellaneous data
    .@old$=getmap();
    .@p=getcharid(1);
    InitMaze(7200, false);
    $@VALIA_STATUS[.@p]=16;
    sleep2(500);
    changeplayermusic "Arabesque.ogg";
    dispbottom l("It was a trap! We must escape!");
    $@VALIA_MAP$[.@p]=getmap();
    partytimer(.@old$, 2000, "#GeminiFExit::OnSick", .@p);
    sleep2(2000);
    @forced_sick$ = getmap();
	doevent "#DungeonCore::OnSick";
    end;

OnSick:
    .@p=getcharid(1);
    MAZE_MAP$ = $@VALIA_MAP$[.@p];
    // Find random, warpable coordinates
    .@e=0; .@x=0; .@y=0;
    .@mx=40; .@my=40;
    do {
        .@x = rand2(20, .@mx);
        .@y = rand2(20, .@my);
        .@e += 1;
        if (.@e > 30) {
            consolebug("Too many failures at Maze \"%s\"! Trying anyway!", MAZE_MAP$);
            break;
        }
    } while (!checknpccell(MAZE_MAP$, .@x, .@y, cell_chkpass));

    warp MAZE_MAP$, .@x, .@y;
    sleep2(500);
    changeplayermusic "Arabesque.ogg";
    dispbottom l("It was a trap! We must escape!");
    sleep2(2000);
    @forced_sick$ = getmap();
	doevent "#DungeonCore::OnSick";
    end;
}

034-4,146,83,0	script	Chest#gemini	NPC_CHEST,{
    if (instance_id() < 0 || getcharid(1) < 1) end;
    GeminiCheck(15);
    // Already taken
    if (@mystatus > 20) {
        dispbottom l("I already took my share from this chest.");
        end;
    }
    // One prize per person
    inventoryplace Iten, 1;
    @mystatus = 99;
    // Non party leaders receive "less"
    if (strcharinfo(0) != getpartyleader(getcharid(1))) {
            Mobpt+=7500;
            getexp 96000, 900;
            dispbottom l("I found %s and %s!", "7,500 "+l("monster points"), "96,000 "+l("experience points"));
            end;
    }
    // Party leaders receive "more"
    .@q = getq(HurnscaldQuest_Gemini);
    setq HurnscaldQuest_Gemini, .@q + 1;
    //getitem RentCart, 1; // Not rented
    switch (.@q) {
    case 1:
        getitem SarabArmlet, 1;
        dispbottom l("I found %s!", l("a(n) ")+getitemlink(SarabArmlet));
        break;
    case 2:
        getitem StrangeCoin, 150;
        getitem MysteriousFruit, 1;
        dispbottom l("I found %s and %s!", "150 "+getitemlink(StrangeCoin), ("a(n) ")+getitemlink(MysteriousFruit));
        break;
    default:
        Mobpt+=7500;
        getexp 96000, 900;
        dispbottom l("I found %s and %s!", "7,500 "+l("monster points"), "96,000 "+l("experience points"));
        end;
    }
    end;

OnInit:
    .distance=2;
    end;
}


