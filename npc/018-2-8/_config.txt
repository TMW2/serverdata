// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 018-2-8: Heroes Hold SS - B3F conf

018-2-8,108,60,0	script	#018-2-8_108_60	NPC_CHEST,{
	TreasureBox();
	specialeffect(.dir == 0 ? 24 : 25, AREA, getnpcid()); // closed ? opening : closing
	close;
OnInit:
	.distance=3;
	end;
}

018-2-8,113,57,0	script	#018-2-8_113_57	NPC_HIDDEN,{
	end;
OnDisable:
	delcells "018-2-8_113_57"; end;
OnEnable:
OnInit:
	setcells "018-2-8", 113, 57, 115, 57, 1, "018-2-8_113_57";
}

018-2-8,113,64,0	script	#018-2-8_113_64	NPC_HIDDEN,{
	end;
OnDisable:
	delcells "018-2-8_113_64"; end;
OnEnable:
OnInit:
	setcells "018-2-8", 113, 64, 115, 64, 1, "018-2-8_113_64";
}

018-2-8,89,35,0	script	#018-2-8_89_35	NPC_HIDDEN,{
	end;
OnDisable:
	delcells "018-2-8_89_35"; end;
OnEnable:
OnInit:
	setcells "018-2-8", 89, 35, 91, 35, 1, "018-2-8_89_35";
}

018-2-8,69,47,0	script	#018-2-8_69_47	NPC_HIDDEN,{
	end;
OnDisable:
	delcells "018-2-8_69_47"; end;
OnEnable:
OnInit:
	setcells "018-2-8", 69, 47, 69, 49, 1, "018-2-8_69_47";
}
