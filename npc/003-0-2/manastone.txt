// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Mana Stone owned by the Magic Council
//
//  Variables:
//      $MANA_BINT => Base Intelligence for Mana Stone
//      $MANA_BLVL => Base Level for Mana Stone
//      $MANA_JLVL => Base Job Level for Mana Stone
//      .int       => Int Increment
//      .lvl       => Lvl Increment
//      .jlvl      => Jlv Increment

003-0-2,35,19,0	script	Mana Stone#Tulim	NPC_MANA_STONE,{

    // You need 50% more base level
    if (BaseLevel < $MANA_BLVL*15/10) goto L_NotWorthy;
    mesn;
    mes l("The mighty Mana Stone does not reacts against you.");
    mes l("Although this particular one seems to hate everyone and everything, it recognizes your strength.");
    mes l("If you fell ready, perhaps you should touch it?");
    mes "";
    menu
        l("Touch it!"), L_Level,
        l("Take it!"), L_NotWorthy2,
        l("Break it!"), L_NotWorthy2,
        l("Leave it alone!"), -;
    close;



L_NotWorthy2:
    npctalk3 l("You are not worthy!");
    percentheal -20, -50;
    if (!MAGIC_LVL)
        dispbottom l("I should train my intelligence, have full MP, and don't neglect even Job Level.");
    end;

L_Level:
    // See functions/util.txt for *mstone() details
    if (mstone(0)) goto L_LevelUp;
    if (mstone(1)) goto L_LevelUp;
    if (mstone(2)) goto L_LevelUp;
    if (mstone(3)) goto L_LevelUp;
    if (mstone(4)) goto L_LevelUp;
    if (mstone(5)) goto L_LevelUp;
    if (mstone(6)) goto L_LevelUp;
    if (MAGIC_LVL >= 7) npctalk3 l("You already got all power I could grant you!");
    if (is_gm()) percentheal -20, -50;
    if (MAGIC_LVL >= 7 || is_gm()) close;

L_NotWorthy:
    if (readparam(Sp) != readparam(MaxSp))
        dispbottom l("I must have full MP to touch it... Which I don't.");
    else
        dispbottom l("I should train my intelligence, and level up, both my base as my Job Level.");
    npctalk3 l("You are not worthy!");
    percentheal min(-10, -70+BaseLevel), min(-10, -100+BaseLevel);
    end;

L_LevelUp:
    mes "";
    mes l("A great rush of mana flows though you.");
    if (!MAGIC_LVL) mes l("Magic Power is granted to you, but you die from it.");
    if (MAGIC_LVL) mes l("More Magic Power is granted to you, but you die from it.");
    MAGIC_LVL = MAGIC_LVL+1;
    sk_lvup(AL_DP);
    die();
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 2;
    end;
}
