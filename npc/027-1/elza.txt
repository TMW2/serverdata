// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Forgetful

027-1,104,61,0	script	Elza	NPC_DARK_SORCERER_F,{
    mesn l("%s, the Forgetful", .name$);
    mesq l("What was I doing before...? I totally forgot!");
    if (any(true, false))
        mesc l("Have you seen my brother %s?", any("Enzo", "Ezra"));
    next;
    ForgetfulNPC(.name$, CLASS_SCHOLARSHIP);
    close;

OnInit:
    .sex = G_FEMALE;
    .distance = 5;
    end;

}

