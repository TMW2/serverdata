// TMW-2 Script
// Author:
//    Jesusalva

009-4,39,46,0	script	Halinarzo's Nurse	NPC_FEMALE,{
    Nurse(.name$, 10, 5);
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    // I am too lazy to dress every NPC I add >.<
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, MiniSkirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, ShortTankTop);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 12);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 9);

    .sex = G_FEMALE;
    .distance = 5;
    end;

}

