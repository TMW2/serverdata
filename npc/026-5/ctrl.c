// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//   The Impregnable Fortress Control Files
// Quest: General_Fortress
//  (MaxFloor+2, internal, internal)

// This script must set 026-5 up on init, and refresh it when needed
// This one also contains a blackbox
-	script	Impregnable#B5F	NPC_HIDDEN,{
    function _moveNpc;
    end;

// On Tuesday, the map changes
//OnTue0000:
OnMon2358:
    if (!.InstId) end;
    // TODO: Reload .InstId
    // Check if this is causing time leak
    debugmes "Reloading B5F... %d", gettimetick(0);
    killmonsterall("026-5");
    MazeMobs(145, false,  32, "026-5", 65); // Initiate Lv 145 mobs
    MazeMobs(175, false,  96, "026-5", 30); // Initiate Lv 175 mobs
    debugmes "Reloaded B5F, %d", gettimetick(0);
    goto OnClock1843;

// Initialize the dungeon
OnInit:
    .InstId = false;
    sleep(7000); // Delay 7s before initialization
    .InstId = CreateMaze(IOT_NONE, MAZE_SIZE_A, "026-5");
    instance_set_timeout(0, 0, .InstId);
    instance_init(.InstId);
    debugmes "Impregnable Fortress B5F (%d): Init OK", .InstId;
    setmapflag("026-5", mf_zone, "SuperMMO");
    goto OnMon2358;

OnClock1843:
    if (!.InstId) end;
    // Movement will need to happen here (daily modifications)
    // We need however to validate the coordinates they're jumping to...
    _moveNpc("026-5");
    unitwarp(getnpcid("#026-5Switch_1"), "026-5", .npc_x, .npc_y);
    _moveNpc("026-5");
    unitwarp(getnpcid("#026-5Switch_2"), "026-5", .npc_x, .npc_y);
    _moveNpc("026-5");
    unitwarp(getnpcid("#026-5Switch_3"), "026-5", .npc_x, .npc_y);
    _moveNpc("026-5");
    unitwarp(getnpcid("#026-5Switch_4"), "026-5", .npc_x, .npc_y);
    _moveNpc("026-5");
    unitwarp(getnpcid("#026-5Switch_5"), "026-5", .npc_x, .npc_y);
    _moveNpc("026-5");
    unitwarp(getnpcid("#026-5Switch_6"), "026-5", .npc_x, .npc_y);
    _moveNpc("026-5");
    unitwarp(getnpcid("#026-5Switch_7"), "026-5", .npc_x, .npc_y);
    _moveNpc("026-5");
    unitwarp(getnpcid("#026-5Gate"), "026-5", .npc_x, .npc_y);
    end;

function _moveNpc {
    // Try to warp randomly, up to 30 attempts
    .@e=0;
    .@mx=getmapinfo(MAPINFO_SIZE_X, getarg(0))-20;
    .@my=getmapinfo(MAPINFO_SIZE_Y, getarg(0))-20;
    do {
        if (.@e >= 30)
            break;
        .npc_x = rand2(20, .@mx);
        .npc_y = rand2(20, .@my);
        .@e+=1;
    } while (!checknpccell(getarg(0), .npc_x, .npc_y, cell_chkpass));
    return;
} // _moveNpc

}
// The switches main code
000-0,0,0,0,	script	#026-5Switch_1	NPC_SWITCH_OFFLINE,{
    // Retrieve my own metadata
    .@n$=strnpcinfo(0, "_0");
    explode(.@ni$, .@n$, "_");
    .@id=atoi(.@ni$[1]);
    .@bit = (2 ** (.@id - 1));
    // Do not react if already done
    if (getq(General_Fortress) != 6)
        end;
    // If already flipped, then don't do pointless things
    if (getq2(General_Fortress) & .@bit) {
        dispbottom l("I already flipped this switch, no point flipping it again.");
        end;
    }
    // After used by someone, can't be used by others for ~30 seconds
	if (getnpcclass() == NPC_SWITCH_ONLINE)
		end;
    // The Riddled Switch: Assign random question (Miller + char ID + Switch ID)
    // Maze mobs should count (and qualify for mob points)
    // All target mobs should be Lv 100+
    .@minKills = 10;
    mesn l("The Riddled Switch");
    mes l("Our master respects %s, so those whom wish to meet him, the following shall they prove:", b(l("knowledge")));
    // Blackbox will question and validate the answer
    if (!0235_Riddle_BlackBox(.@id)) {
        mesc l("The answer, apparently, was incorrect."), 1;
        close;
    }
    // Mark this switch as done
    setq2 General_Fortress, getq2(General_Fortress) | .@bit;
    dispbottom l("You hear a *clank*, it must have worked.");
    // Check if this was the last switch
    if (getq2(General_Fortress) == 127) {
        dispbottom l("This was the last one of them. Now to find the door...");
        setq General_Fortress, 7, 0, 0;
        specialeffect(FX_MGWARP, SELF, getcharid(3));
    }
    // Final decorators
    closeclientdialog;
	setnpcdisplay .name$, NPC_SWITCH_ONLINE;
    sleep(30000); // Detach the player (instead of sleep2)
	setnpcdisplay .name$, NPC_SWITCH_OFFLINE;
	end;
OnInit:
	.distance=3;
    end;
}

000-0,0,0,0	duplicate(#026-5Switch_1)	#026-5Switch_2	NPC_SWITCH_OFFLINE
000-0,0,0,0	duplicate(#026-5Switch_1)	#026-5Switch_3	NPC_SWITCH_OFFLINE
000-0,0,0,0	duplicate(#026-5Switch_1)	#026-5Switch_4	NPC_SWITCH_OFFLINE
000-0,0,0,0	duplicate(#026-5Switch_1)	#026-5Switch_5	NPC_SWITCH_OFFLINE
000-0,0,0,0	duplicate(#026-5Switch_1)	#026-5Switch_6	NPC_SWITCH_OFFLINE
000-0,0,0,0	duplicate(#026-5Switch_1)	#026-5Switch_7	NPC_SWITCH_OFFLINE

// The main NPC which lets you out of here
000-0,0,0,0,	script	#026-5Gate	NPC_ICE_CAGE,0,0,{
    if (getq(General_Fortress) > 6) goto L_Warp;
    dispbottom l("After flipping all %s switches, I should go there.", l("seven"));
    end;
OnTouch:
    if (getq(General_Fortress) > 6) goto L_Warp;
    mesc l(".:: Impregnable Fortress, %sF ::.", "B5"), 3;
    msObjective(getq(General_Fortress) == 7, l("* Flip all seven switches"));
    msObjective($MK_TEMPVAR >= MKIF_LV_B6F, l("Minimum wins: %d/%d", $MK_TEMPVAR, MKIF_LV_B6F));
    mes "";
    mesc l("Hint: The Riddled Switches.");
    close;

L_Warp:
    fortwarp(MKIF_LV_B6F, "026-6", 100, 90);
    end;

OnInit:
	.distance=3;
    end;
}

