// TMW-2 Script
// Author:
//    Jesusalva
// Description:
//    This stuff should be moved somewhere else...

003-1,89,109,0	script	Michel	NPC_BACCHUS,{

    mesn;
    mesq l("These people have lots of unspent Strange Coins! Waw!");

    HallOfCoins();

    // Anyone with GM Level, staff or not, is allowed to latest GM logs
    if (!getgmlevel()) goto L_Main;

    next;
    HallOfGMLog();

    // Only Admins are allowed to change the Referral Scoreboard
    if (!is_admin()) goto L_Main;
    if (is_admin()) goto L_GMOnce;
    close;

L_Main:
    if (!$REFERRAL_IDS[#REFERRAL_PROG])
        close;
    next;
    mesn;
    mesq l("Hey, I see you have appointed @@ players to this game. Good job!", $REFERRAL_IDS[#REFERRAL_PROG]);
    if ($REFERRAL_IDS[#REFERRAL_PROG] > 20)
        mesc l("Uhm, just remember it'll be manually verified before prizes are given... Don't even think on cheating!"), 1;
    close;

L_GMOnce:
    next;
    .@nb = query_sql("SELECT SUM(value) FROM `mapreg` WHERE varname='$REFERRAL_IDS' LIMIT 2", .@value);
    @total=.@value[0];
L_GM:
    mesc "Referral Event status: " + ($REFERRAL_ENABLED ? "##2ACTIVE##0" : "##1INACTIVE##0");
    mesc "Total refers count: "+@total;
    mes "";
    select
        "Close",
        "Toggle Referral Event",
        "Scoreboard",
        "CLEAR REFERRAL ID ARRAY",
        "Close";
    mes "";
    switch (@menu) {
        case 2:
            $REFERRAL_ENABLED=!$REFERRAL_ENABLED; logmes "Enabled REFER event.", LOGMES_ATCOMMAND; break;
        case 3:
            HallOfReferral(); break;
        case 4:
            mesc "Are you sure?", 1;
            next;
            if (askyesno() == ASK_YES) {
                deletearray($REFERRAL_IDS);
                logmes "##1Deleted PERMANENTLY the REFER event scoreboard.##0", LOGMES_ATCOMMAND;
                .@nb = query_sql("SELECT SUM(value) FROM `mapreg` WHERE varname='$REFERRAL_IDS' LIMIT 2", .@value);
                @total=.@value[0];
            }
        default: close;
    }
    next;
    goto L_GM;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}
