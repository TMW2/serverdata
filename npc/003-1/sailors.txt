// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    The major reward from this one is knowledge about secret passages on Tulimshar.
//    The quest pays what you spend, except for travel fees. The real reward here
//    is knowledge, the fishing rod you'll need to make anyway, 150 XP and 20 JExp.
//  1 - First Quest assigned
//  2 - First Quest Complete
//  3 - Second Quest assigned
//  4 - Second Quest Complete

003-1,112,93,0	script	Sailors#003-1	NPC_ELVEN_MAN_TRADER_SITTING,{
    if (BaseLevel < 12) goto L_Complete;
    .@q = getq(TulimsharQuest_Sailors);
    if (.@q == 4)
        goto L_Complete;

    if (.@q == 1)
        goto L_Report1;
    if (.@q == 3)
        goto L_Report2;
    mesn;
    mesq lg("Hey, pal. This spot is very good, I can see the whole town from here.");
    next;
    mesn;
    mesq lg("I am a trader from Nard's ship. We actually need supplies. Help us, and I'll help you.");
    next;
    mesn strcharinfo(0);
    menu
        rif(.@q == 0, l("What do you need?")), L_Quest1,
        rif(.@q == 2, l("What do you need?")), L_Quest2,
        l("Maybe another time."), -;
    close;

L_Complete:
    hello;
    end;

L_Quest1:
    mes "";
    mesn;
    mesq l("You're willing to help? Perfect. We need @@ @@, @@ @@ and @@ @@ for travel.", 1, getitemlink(FishBox), 2, getitemlink(CroconutBox), 3, getitemlink(CottonCloth));
    next;

    mesn;
    mesq l("Cotton Cloth is sold at Candor, Croconut Boxes can be bought at the market. Fish, however, is a little more tricky.");
    next;

    mesn;
    mesq l("If you hug the wall, there's a secret passage somewhere there. Well, there are many secret passages at Tulimshar.");
    next;

    mesn;
    mesq l("You will find a fisher, maybe he is having a good time and can sell you a few boxes. And remember to keep your eye open.");
    setq TulimsharQuest_Sailors, 1;
    next;

    mesn;
    mesq l("There are secret caves on this city. In fact, Tulimshar holds many misteries.");
    mesc l("Take care: Secret passages can be dangerous places (or not).");
    tutmes l("You can get Croconut from Crocotree, and fill @@ purchased in the market.", getitemlink(EmptyBox)), l("Protip"), false;
    close;

L_Report1:
    mesn;
    mes l("* %d/%d %s", countitem(FishBox),     1, getitemlink(FishBox));
    mes l("* %d/%d %s", countitem(CroconutBox), 2, getitemlink(CroconutBox));
    mes l("* %d/%d %s", countitem(CottonCloth), 3, getitemlink(CottonCloth));
    next;

    select
        l("[Deliver all goods.]"),
        l("[Maybe another time.]");
    mes "";
    if (@menu == 2)
        close;

    if (countitem(FishBox) < 1 ||
        countitem(CroconutBox) < 2 ||
        countitem(CottonCloth) < 3
    ) {
        goto L_Liar;
    }
    inventoryplace Dagger, 1, CottonShirt, 1;
    delitem FishBox, 1;      // 7.500 GP → 3.750 GP
    delitem CroconutBox, 2;  // 12.000 GP → 10.600~11.800 GP
    delitem CottonCloth, 3; // 2.000 GP
    getexp 1508, 15;
    Zeny=Zeny+3750;   // Profit: 1800 GP → 2.400~1.200 GP
    getitem Dagger, 1;
    getitem CottonShirt, 1;
    setq TulimsharQuest_Sailors, 2;
    mesn;
    mesq lg("Many thanks, your help has been invaluable. We're now capable to travel at ease.");
    next;
    mesn;
    mesq l("Take care. Here, take this @@. You can sell it for some quick-spot cash, or use it as a weapon.", getitemlink(Dagger));
    close;

// Second Quest
L_Quest2:
    mes "";
    mesn;
    mesq l("You're willing to help? Perfect. We need @@ @@, @@ @@ and @@ @@ for travel.", 2, getitemlink(FishBox), 5, getitemlink(CroconutBox), 7, getitemlink(CottonCloth));
    next;
    mesn;
    mesq l("Cotton Cloth is sold at Candor, Croconut Boxes can be bought at the market. Fish, however, is a little more tricky.");
    next;
    mesn;
    mesq l("If you hug the wall, there's a secret passage somewhere there. Well, there are many secret passages at Tulimshar.");
    next;
    mesn;
    mesq l("You will find a fisher, maybe he is having a good time and can sell you a few boxes. And remember to keep your eye open.");
    setq TulimsharQuest_Sailors, 3;
    next;
    mesn;
    mesq l("There are secret caves on this city. In fact, Tulimshar holds many misteries.");
    mesc l("Take care: Secret passages can be dangerous places (or not).");
    close;

L_Report2:
    mesn;
    mes l("* %d/%d %s", countitem(FishBox),     2, getitemlink(FishBox));
    mes l("* %d/%d %s", countitem(CroconutBox), 5, getitemlink(CroconutBox));
    mes l("* %d/%d %s", countitem(CottonCloth), 7, getitemlink(CottonCloth));
    next;

    select
        l("[Deliver all goods.]"),
        l("[Maybe another time.]");
    mes "";
    if (@menu == 2)
        close;

    if (countitem(FishBox) < 2 ||
        countitem(CroconutBox) < 5 ||
        countitem(CottonCloth) < 7
    ) {
        goto L_Liar;
    }
    delitem FishBox, 2;      // 7.500 GP → 3.750 GP
    delitem CroconutBox, 5;  // 12.000 GP → 10.600~11.800 GP
    delitem CottonCloth, 7; // 2.000 GP
    getexp 1508, 15;
    Zeny=Zeny+15750;   // Profit: 1800 GP → 2.400~1.200 GP
    setq TulimsharQuest_Sailors, 4;
    mesn;
    mesq lg("Many thanks, your help has been invaluable. We're now capable to travel at ease.");
    next;
    mesn;
    mesq l("Take care. This is a dangerous world, but I think that worse than being attacked by sea monsters, is starving from hunger!");
    close;


L_Liar:
    mesn;
    mesq l("Liar. Where are the goods? Are you trying to cheat me?!");
    next;

    mesn;
    mesq l("You know Jesus Saves hates cheaters, right? If Saulc didn't asked me to double-check stuff...");
    next;

    mesn;
    mesq l("For short, you would have lost all your items, wouldn't get anything, and there would be no refunds.");
    next;

    mesn;
    mesq l("That's just a friendly advise. That's how things works around here.");
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 5;
    end;
}
