// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Controls events, part of Aurora Event System
//
// See also: functions/aurora.txt, functions/seasons.txt, command/event.txt,
// event.txt, functions/soulmenhir.txt and, of course, the event maps (if any).
//
// Script Variables:
//      Q_AuroraEvent
//          Quest Variable: FYEVENT_CYCLE, Score, ClaimedControl
//
// TODO: Use duplicate() command to make it available in other towns as well

// Easter
003-1,47,53,0	script	Aurora	NPC_FEMALE,{
    function handleEaster;
    function handleValentine;
    function handleStPatrick;

    // Aurora Event functions
    function auroraRankings;
    function auroraCurrentRankings;
    function auroraSubmit;
    function auroraListRewards;

    // Handle annuals
    //.@v_stday = getvariableofnpc(.valentine_stday, "#EventCore");
    //.@v_stmon = getvariableofnpc(.valentine_stmon, "#EventCore");
    .@v_endday = getvariableofnpc(.valentine_endday, "#EventCore");
    .@v_endmon = getvariableofnpc(.valentine_endmon, "#EventCore");

    //.@e_stday = getvariableofnpc(.easter_stday, "#EventCore");
    //.@e_stmon = getvariableofnpc(.easter_stmon, "#EventCore");
    .@e_endday = getvariableofnpc(.easter_endday, "#EventCore");
    .@e_endmon = getvariableofnpc(.easter_endmon, "#EventCore");

    .@dy=gettime(GETTIME_DAYOFMONTH);
    .@mo=gettime(GETTIME_MONTH);

    // Debug Overrides
    if ($@DEBUG_OD)
        .@dy=$@DEBUG_OD;
    if ($@DEBUG_OM)
        .@mo=$@DEBUG_OM;

    // Annual rewards can only be claimed until the month ends
    if ($EVENT$ == "Patrick")
        handleStPatrick();
    else if (.@mo == .@v_endmon && .@dy > .@v_endday)
        handleValentine();
    else if (.@mo == .@e_endmon && .@dy > .@e_endday)
        handleEaster();

    // Another event is going on, smoothly handle it
    if ($EVENT$ != "")
        goto L_Aurora;
    else
        mesc l("Currently, there is no event going on."), 1;
    close;


// OnRestore causes OnInit to start again
OnRestore:
    setnpcdisplay .name$, "Aurora", NPC_FEMALE;
OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, MiniSkirt);
    setunitdata(.@npcId, UDT_HEADMIDDLE, RedStockings);
    //setunitdata(.@npcId, UDT_HEADBOTTOM, BlueRoseHat);
    setunitdata(.@npcId, UDT_WEAPON, UglyChristmasSweater); // (Blue) Bathrobe?
    setunitdata(.@npcId, UDT_HAIRSTYLE, any(8, 8, 8, 20, 20, 11));
    setunitdata(.@npcId, UDT_HAIRCOLOR, 7);
    .sex = G_FEMALE;
    .distance = 5;
    end;

// Override for Valentine Day - There should be no Aurora
OnValentine:
    .@npcId = getnpcid(.name$);
    setnpcdisplay .name$, "Demure#ValentineFinal", NPC_FEMALE;
    //.@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, Cap);
    setunitdata(.@npcId, UDT_HEADMIDDLE, RedStockings);
    setunitdata(.@npcId, UDT_HEADBOTTOM, BunnyEars);
    setunitdata(.@npcId, UDT_WEAPON, GMRobe);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 14);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 18);
    end;
























/////////////////////////////////////////////////////////////////////////////////
function handleEaster {
    // Handle rewards before anything else
    if (EASTER_YEAR != gettime(GETTIME_YEAR)) {
        EASTER_YEAR=gettime(GETTIME_YEAR);
        if (strcharinfo(0) == $@easter_name$[0]) {
            makepet BhopFluffy;
            mesn;
            mesc l("For the first place in Easter, you gained a Bhopper Fluffy."), 3;
            mesc l("Remember to give it a balanced diet of Aquadas to make it happy."), 3;
            #EVFEATS = #EVFEATS | EVFEAT_EASTER;
            next;
        } else {
            .@pos=array_find($@easter_name$, strcharinfo(0));
            // 0 (aka top 1) is not an appliable winner
            if (.@pos > 0) {
                // Reverse it so top 10 value is 2, and top 2 value is 10.
                .@pos=11-.@pos;
                getitem StrangeCoin, .@pos*10;
            }
        }
    }
    mesn;
    mesq l("Easter is over! I am the last chance to get rid of eggs!!");
    mesc l("Note: Golden and Silver Eggs are deleted after the next event end."), 1;
    // Heartbeat
    select
        l("Trade Silver Eggs"),
        l("Trade Golden Eggs"),
        l("View LeaderBoard"),
        l("Thanks Lilica.");
    mes "";
    switch (@menu) {
    case 1:
        openshop "#eastershop1";
        closedialog;
        break;
    case 2:
        openshop "#eastershop2";
        closedialog;
        break;
    case 3:
        mesn l("Easter @@", gettime(GETTIME_YEAR));
	    mes("1."+$@easter_name$[0]+" ("+$@easter_value[0]+")");
	    mes("2."+$@easter_name$[1]+" ("+$@easter_value[1]+")");
	    mes("3."+$@easter_name$[2]+" ("+$@easter_value[2]+")");
	    mes("4."+$@easter_name$[3]+" ("+$@easter_value[3]+")");
	    mes("5."+$@easter_name$[4]+" ("+$@easter_value[4]+")");
	    mes("6."+$@easter_name$[5]+" ("+$@easter_value[5]+")");
	    mes("7."+$@easter_name$[6]+" ("+$@easter_value[6]+")");
	    mes("8."+$@easter_name$[7]+" ("+$@easter_value[7]+")");
	    mes("9."+$@easter_name$[8]+" ("+$@easter_value[8]+")");
	    mes("10."+$@easter_name$[9]+" ("+$@easter_value[9]+")");
        break;
    }
    return;
}
























/////////////////////////////////////////////////////////////////////////////////
function handleValentine {
    mesn;
    mesq l("Valentine Day is over!");
    next;
    mes "##B"+l("Top 10 - Valentine Day")+"##b";
    mes("1." +$@valentine_name$[0]+" ("+$@valentine_value[0]+")");
    mes("2." +$@valentine_name$[1]+" ("+$@valentine_value[1]+")");
    mes("3." +$@valentine_name$[2]+" ("+$@valentine_value[2]+")");
    mes("4." +$@valentine_name$[3]+" ("+$@valentine_value[3]+")");
    mes("5." +$@valentine_name$[4]+" ("+$@valentine_value[4]+")");
    mes("6." +$@valentine_name$[5]+" ("+$@valentine_value[5]+")");
    mes("7." +$@valentine_name$[6]+" ("+$@valentine_value[6]+")");
    mes("8." +$@valentine_name$[7]+" ("+$@valentine_value[7]+")");
    mes("9." +$@valentine_name$[8]+" ("+$@valentine_value[8]+")");
    mes("10."+$@valentine_name$[9]+" ("+$@valentine_value[9]+")");

    if (#VALENTINE_SENT+#VALENTINE_OPENED <= 0)
        return;
    next;

    // Handle rewards
    #VALENTINE_SENT=0;
    #VALENTINE_OPENED=0;
    #VALENTINE_RECEIVED=0;
    copyarray(.@name$[0], $@valentine_name$[0], 10);
    if (strcharinfo(0) == .@name$[0]) {
        makepet DoggyDog;
        getitem PrismGift, 1;
        getitem StrangeCoin, 10;
        #EVFEATS = #EVFEATS | EVFEAT_VALENTINE;
    } else if (strcharinfo(0) == .@name$[1] || strcharinfo(0) == .@name$[2]) {
        getitem PrismGift, 1;
        getitem GoldenGift, 1;
        getitem StrangeCoin, 10;
    } else if (strcharinfo(0) == .@name$[3] || strcharinfo(0) == .@name$[4]) {
        getitem GoldenGift, 1;
        getitem SilverGift, 1;
        getitem StrangeCoin, 10;
    } else if (strcharinfo(0) == .@name$[5] || strcharinfo(0) == .@name$[6]) {
        getitem SilverGift, 1;
        getitem BronzeGift, 1;
        getitem StrangeCoin, 10;
    } else if (strcharinfo(0) == .@name$[7] || strcharinfo(0) == .@name$[8] || strcharinfo(0) == .@name$[9]) {
        getitem BronzeGift, 1;
        getitem StrangeCoin, 10;
    } else {
        getitem StrangeCoin, 5;
    }

    getexp #VALENTINE_SENT+#VALENTINE_RECEIVED, #VALENTINE_SENT;
    Zeny=Zeny+#VALENTINE_OPENED;

    if (strcharinfo(0) == .@name$[0])
        mesc l("You gained a @@ for the #1 place on the event. Remember to feed it @@, or it may run away from you.", getitemlink(DoggyDog), getitemlink(AnimalBones));
    return;
}
























/////////////////////////////////////////////////////////////////////////////////
function handleStPatrick {
    // Check if St. Patrick day is over D:
    if ($EVENT$ != "Patrick")
        goto OnRestore;
    // Okay, it is still St. Patrick :3
    mesn;
    mesc l("It's St. Patrick Event!"), 3;
    mes l("At 00:00, 06:00, 12:00, 15:00, 18:00 and 21:00 server time");
    mes l("Several special clovers will show up at forests.");
    next;
    mes l("They have 10x more chance to drop a @@, so it is a great deal!", getitemlink(FourLeafClover));
    mes l("Also, hidden in a forest which is not hot nor cold, is the Gold Pot Cauldron...");
    mes l("You can get daily something from it, but unless you're green like me, you will have no luck...");
    next;
    return;
}
























/////////////////////////////////////////////////////////////////////////////////
L_Aurora:
    // Define script variables
    .@WHAT$=l("event");

    // Fill them with specific details, if available
    if ($EVENT$ == "Expo") {
        .@WHAT$=l("world expo");
    } else if ($EVENT$ == "Fishing") {
        .@WHAT$=l("golden fish hunt");
    } else if ($EVENT$ == "Kamelot") {
        .@WHAT$=l("kamelot raid");
    } else if ($EVENT$ == "Regnum") {
        .@WHAT$=l("regnum's blessing");
    } else if ($EVENT$ == "Mining") {
        .@WHAT$=l("miners union request");
    } else if ($EVENT$ == "Candor") {
        .@WHAT$=l("candor battle season");
    } else if ($EVENT$ == "Celestia") {
        .@WHAT$=l("yeti king hunt season");
    } else if ($EVENT$ == "Gemini") {
        .@WHAT$=l("gemini season");
    } else if ($EVENT$ == "Rebirth") {
        .@WHAT$=l("rebirth season");
    } else if ($EVENT$ == "Siege") {
        .@WHAT$=l("siege season");
    } else if ($EVENT$ == "Tower") {
        .@WHAT$=l("dream tower apparition");
    } else if ($EVENT$ == "Raid") {
        .@WHAT$=l("boss raid");
    } else if ($EVENT$ == "Olympics") {
        .@WHAT$=l("magic olympics");
    }

    mesn;
    mesq l("Hello! I am %s, and I oversee the %s!", .name$, .@WHAT$);
    next;
    mesn;
    mesq l("So, how can I help you today?");
    do
    {
    next;
    select
        l("Event Details"),
        l("Event Ranking Rewards"),
        rif(FYEventUsesRanking(), l("Current Rankings")),
        rif(FYEventUsesRanking(), l("List & Claim rewards")),
        l("That's all, thanks!");
    mes "";
    switch (@menu) {
    case 1:
        EventHelp();
        if (FYEventUsesRanking()) {
            mesc l("You must claim all rewards and use any event item BEFORE it ends."), 1;
            mesc l("Left-overs will be deleted shortly after."), 1;
            mesc l("Any eventual ranking reward will be sent by the banker's mail."), 1;
        } else {
            mesn;
            mesq l("Have fun!");
        }
        break;

    case 2:
        auroraRankings();
        break;

    case 3:
        auroraCurrentRankings();
        break;

    case 4:
        auroraSubmit();
        auroraListRewards();
        break;

    default:
        close;
    }
    // The code block is done
    } while (true);
close;
























/////////////////////////////////////////////////////////////////////////////////
function auroraRankings {
    setnpcdialogtitle l("Aurora Events")+" - "+$EVENT$;
    setskin "aurora_"+$EVENT$;
    mes "There is no ranking information available for this event.";
    select("Ok");
    setskin "";
    clear;
    auroraCurrentRankings(); // More to silence a bug than whatelse
    return;
}


function auroraCurrentRankings {
    if (FYEventUsesRanking()) {
        HallOfAurora();
    } else {
        mesn;
        mesq l("Personally, I like unranked events more than ranked ones...");
    }
    return;
}


function auroraListRewards {
    .@s=getq2(Q_AuroraEvent);
    .@r=getq3(Q_AuroraEvent);
    mesn;
    freeloop(true);
    for (.@i=0; .@i < getarraysize($FYREWARD_PT); .@i++) {
        mes l("%s %d pts - %d %s",
        (.@r > .@i ? "%%A" : (.@r == .@i ? "%%E" : "%%B")), // Status Indicator
        $FYREWARD_PT[.@i], $FYREWARD_AM[.@i], getitemlink($FYREWARD_ID[.@i]));
        // You're at this milestone? Hmm
        if (.@r == .@i) {
            // Your score is enough: rank you up
            if (.@s >= $FYREWARD_PT[.@i]) {
                inventoryplace $FYREWARD_ID[.@i], $FYREWARD_AM[.@i];
                getitem $FYREWARD_ID[.@i], $FYREWARD_AM[.@i];
                .@r+=1;
                setq3 Q_AuroraEvent, .@r;
            }
        }
    }
    freeloop(false);
    return;
}


function auroraSubmit {
    .@q2=getq2(Q_AuroraEvent);
    FYE_Submit();
    mesc l("Event score: %d -> %d", .@q2, getq2(Q_AuroraEvent));
    return;
}























// DO NOT REMOVE
}

