// TMW2 script.
// Author:
//    Saulc
//    Jesusalva
// Description:
//    Jakod replaces Luca. As there are no focus skills, Jakod teaches player about
//    magic and is their tutor on the matters of the Mana Stone.

003-1,109,129,0	script	Jakod	NPC_PLAYER,{


    mesn;
    if (BaseLevel < $MANA_BLVL) mesq l("Hello there. I am looking for strong people, but you are not strong enough.");
    if (BaseLevel < $MANA_BLVL) close;

    mes l("\"Ah, hello there, @@. You've grown quite skilled lately.", strcharinfo(0));
    if (MAGIC_LVL) {
        mes l("I also see you have a level @@ magic skill!\"", MAGIC_LVL);
        next;
        mes l("Did you got an @@ yet? That book have great insight on several details concerning mana and its usages!", getitemlink(JesusalvaGrimorium));
        next;
        mes l("Anyway, you can see how skilled using Mana Magic with @@. Mana Magic is the one which requires concentration, to bring things into existence, for example.", b("@abizit"));
        next;
        mes l("Well, if you ever need help, do not hesit reading the wiki ;-)");
        close;
    } else {
        mes l("But you lack magic! That's a pity.\"");
        select
            l("How do I get magic?"),
            l("Good bye.");
        if (@menu == 2)
            close;
        mes "";
    }

    mes "";
    mesn;
    mesq l("Well, the first thing would be to get access to a Mana Stone. Halinarzo had the biggest mana stone mines, but it's now depleted.");
    next;
    mesn;
    mes l("\"That's the only hard part. As long that you do not neglect Intelligence nor Job level...");
    mes l("...If you're lucky, you can touch it and receive magic power.\"");
    next;
    mesn;
    mesq l("Having magic power is useless by itself, so you must visit the Academy. I guess that involves approval and acknowledgment of the Mage Council that you are a mage!");
    next;
    mesn;
    mesq l("Keep in mind, the more levels and intelligence you have, more likely the Mana Stone will grant you more Magic Power. But that means nothing.");
    next;
    mesn;
    mesq l("Because in the end, you are in the hands of your class leader!"); // Rare: Some loner NPCs may grant you EXTRA skills beyond that.
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, ForestArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, JeansShorts);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 14);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 13);
    //setunitdata(.@npcId, UDT_HAIRSTYLE, 24);
    //setunitdata(.@npcId, UDT_HAIRCOLOR, 2);

    .sex = G_MALE;
    .distance = 3;
    end;
}
