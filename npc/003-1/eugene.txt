// TMW2 scripts.
// Authors:
//    Reid
//    Travolta
//    Saulc
// Description:
//    Fishman NPC
// Quest variable:
//    TulimsharQuests_Fishman
// Quest stages:
//    0 - not started
//    1 - Eugene asked for items
//    2 - completed

003-1,80,127,0	script	Eugene	NPC_EUGENE,{

    narrator S_LAST_NEXT,
        l("You see a raijin boy, sitting on the edge of the dock."),
        l("He's holding a fishing rod, while gazing out at the sea.");

    .@q = getq(TulimsharQuests_Fishman);
    if (.@q == 1) goto L_CheckItems;
    if (.@q == 2) goto L_QuestDone;

    speech S_LAST_BLANK_LINE,
        l("Ahoi."),
        l("Hey, check out my brand new fishing rod. I bought it just today."),
        l("I was so excited, I wanted to try it as soon as possible."),
        l("So in a hurry, I forgot to take enough bait for fishing."),
        lg("Be a friend and bring me @@ @@.", "Be a friend and bring me @@ @@.", .BaitCount, getitemlink(.BaitID));

    switch (
        select(l("I'll be back in no time."),
               l("Sorry, I'm doing other things at the moment."))) {
    case 1:
        setq TulimsharQuests_Fishman, 1;
        speech S_FIRST_BLANK_LINE,
               l("Thank you. I'll wait here.");
        next;
        mesc l("Protip: %s are dropped by %s. Those monsters help each other, so don't attack them when they are in packs.",
               getitemlink(.BaitID), getmonsterlink(LittleBlub));
        close;
    default:
        speech S_FIRST_BLANK_LINE,
               l("But I have no %s... %%J", getitemlink(.BaitID));
        close;
    }

L_CheckItems:
    if (countitem(.BaitID) < .BaitCount)
    {
        speech
            l("Sorry, but you don't have what I need."),
            l("I need @@ @@.", .BaitCount, getitemlink(.BaitID));
        close;
    }

    inventoryplace FishingRod, 1, FishingGuideVolI, 1;

    speech
        l("That's exactly what I needed!"),
        l("To thank you, accept my old fishing rod."),
        l("It's not as good as my new one, but still very useful."),
        l("Just look at that water! There's a whole bunch of fish down there."),
        l("Oh, and you will need this book too, it will help you learn the basics of fishing."),
        lg("You might even get lucky, and get a @@.",
           "You might even get lucky, and get a @@.", getitemlink(GrassCarp)),
        l("Have a good time fishing!");

    delitem .BaitID, .BaitCount;
    getitem FishingRod, 1;
    getitem FishingGuideVolI, 1;
    getexp 62, 5;
    setq TulimsharQuests_Fishman, 2, 99, gettimeparam(GETTIME_DAYOFMONTH);
    close;

L_QuestDone:
    // Idea for future: Eugene telling fishman jokes.
    speech
        l("Ahoy, @@!", strcharinfo(0)),
        l("Are the fish biting today?");

    select
        l("Yes, everything is going great, thank you!"),
        l("Actually, I have bad luck. Could you sell me a box full of fresh fish?");

    switch (@menu) {
    case 1:
        .@q3 = getq3(TulimsharQuests_Fishman);
        // Time check/fix
        if (.@q3 < gettimeparam(GETTIME_DAYOFMONTH))
            setq TulimsharQuests_Fishman, 2, 0, gettimeparam(GETTIME_DAYOFMONTH);

        .@maxCarps = 1 + (BaseLevel/8);
        .@q2 = getq2(TulimsharQuests_Fishman);
        if (.@q2 < .@maxCarps) {
            mesn;
            mesq l("Maybe I'm using a too low quality bait, I can't catch a %s.", getitemlink(GrassCarp));
            next;

            goto L_Sidequest;
        }

        speech S_FIRST_BLANK_LINE,
               l("Glad to hear. I swear, the fish I picked before you arrived was THAT big!");
        break;
    default:
        speech S_FIRST_BLANK_LINE,
               l("Earlier I hadn't any, but now that I have the baits, I will be glad to sell some to you!");
        npcshopattach(.name$);
        openshop;
        closedialog;
    }
    close;

L_Sidequest:
    mesn;
    mesq l("I offer you %d GP for one. What do you say?", .SuperPrice);
    if (!countitem(GrassCarp))
        close;
    if (askyesno() == ASK_NO)
        close;
    delitem GrassCarp, 1;
    Zeny+=.SuperPrice;

    .@q2 += 1;
    setq2 TulimsharQuests_Fishman, .@q2;

    // Maybe you can repeat, if so, do this immediately
    if (.@q2 < .@maxCarps) {
        mesn;
        mesc l("Eugene bows politely, but he thinks he can use more.");
        goto L_Sidequest;
    }

    // Maxed for the day, give you a hint
    mesn;
    mesq l("Thanks. Maybe I should stop using Maggot Slime as a bait.");
    close;

OnInit:
    .BaitID = SmallTentacles;
    .BaitCount = 10;
    // 600% profit (normal is 500%, but fishing is harder)
    .SuperPrice = getiteminfo(GrassCarp, ITEMINFO_SELLPRICE)*6;

    tradertype(NST_MARKET);
    sellitem SmallFishingNet, -1, 1;
    sellitem FishBox, -1, 5;
    sellitem CommonCarp, -1, 3;

    .sex = G_MALE;
    .distance = 6;
    end;

OnClock0611:
OnClock1200:
OnClock1801:
OnClock0003:
    restoreshopitem SmallFishingNet, 1;
    restoreshopitem FishBox, 5;
    restoreshopitem CommonCarp, 3;
    end;

// Pay your taxes!
OnBuyItem:
    debugmes("Purchase confirmed");
    PurchaseTaxes();
    end;

OnSellItem:
    debugmes("Sale confirmed");
    SaleTaxes();
    end;
}

