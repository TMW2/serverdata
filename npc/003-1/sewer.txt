// TMW2 scripts.
// Author:
//    Jesusalva
// Notes:
//    TW: Tulimshar, West
//    TE: Tulimshar, East

003-1,56,84,0	script	#Sewer-TW	NPC_NO_SPRITE,{
    if (getq(TulimsharQuest_Sewers) == 0 && !countitem(MirrorLakeArmor)) {
        dispbottom l("The sewer mouth is locked.");
        end;
    }
    if (BaseLevel < 25 && !countitem(MirrorLakeArmor)) end;

    mesc l("Descend into Tulimshar sewers?");
    if (askyesno() == ASK_YES) {
        closedialog;
        warp "003-1-1", 57, 100;
        dispbottom l("Blame Saulc.");
        end;
    }
    close;
}

003-1,115,111,0	script	#Sewer-TE	NPC_NO_SPRITE,{
    if (getq(TulimsharQuest_Sewers) == 0 && !countitem(MirrorLakeArmor)) {
        dispbottom l("The sewer mouth is locked.");
        end;
    }
    if (BaseLevel < 25 && !countitem(MirrorLakeArmor)) end;

    mesc l("Descend into Tulimshar sewers?");
    if (askyesno() == ASK_YES) {
        closedialog;
        warp "003-1-1", 143, 128;
        end;
    }
    close;
}

003-1,73,133,0	script	#ToTheater	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    warp "003-9-1", 27, 37;
    RegEasterEgg(EE_THEATER, 2);
    end;
}
