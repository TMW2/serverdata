// TMW-2 Script
// Author:
//  Jesusalva
//  Pookie
// Description:
//  This NPC serves no purpose currently. She is here to make Tulimshar more crowded.
//  Now a believer to announce Halinarzo and Alvasus Quest.
// TODO: During rainy days, she could ask for an Umbrella

003-1,53,128,0	script	Aahna	NPC_ELVEN_FEMALE,{
    mesn;
    mesq l("Hello, What a lovely day for a stroll.");
    next;
    mesn;
    mesq l("I heard every Sunday there's a party at Halinarzo Church, but to get there you need to pass through the Desert Canyon.");
    next;
    mesn;
    mesq l("I hope one day, they decide build a church here. It's a pain to travel there every time...");
    close;

OnInit:
    .sex = G_FEMALE;
    .distance = 5;
    end;

// Server Happy Hour
// Used to be Sunday 18:00~20:00
// Now it is Sunday 16:00~20:00
OnSun1600:
    if (debug) end;
    $@EXP_EVENT=rand2(15, 20);
    if ($@EXP_EVENT > 15 && $@EXP_EVENT != 20)
        $@EXP_EVENT=rand2(15, 20);
    $@EXP_EVENT_TIME=any(2,2,2,3,3,4);
    donpcevent "@exprate::OnPlayerCall";
    end;

OnSun0000:
    disablenpc .name$;
    end;

OnMon0000:
    enablenpc .name$;
    end;
}
