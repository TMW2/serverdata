#!/bin/bash

find -H . -type f -name "*.txt" -exec sed -i 's/[[:blank:]]*$//' {} \;

export RES=$(git diff --name-only)
if [[ -n "${RES}" ]]; then
	echo "Extra spaces before new lines detected in files:"

	# Don't know if this is possible to do via git alone:
	line_number=0
	file_path="unknown"
	while read -r line; do
		if [[ $line =~ ^"--- "(.*) ]]; then
			file_path="${BASH_REMATCH[1]}"
			file_path="${file_path#*a/}"
		elif [[ $line =~ ^"@@ -"([0-9]*).* ]]; then
			line_number="${BASH_REMATCH[1]}"
		elif [[ $line =~ ^"-"(.*) ]]; then
			old_line="${BASH_REMATCH[1]}"
			printf "%s:%s %s\n" "$file_path" "$line_number" "$old_line"
		fi
	done < <(git diff --unified=0)
	exit 1
fi
