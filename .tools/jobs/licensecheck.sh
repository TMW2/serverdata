#!/bin/bash

source ./.tools/scripts/init.sh

aptget_update
aptget_install git-core gcc ca-certificates grep python2

do_init_data

ls

do_init_tools

cd tools/licensecheck
#ls

cp -r ../../TMW2/serverdata/npc npc

#echo "ls .."
#ls ..
#echo "ls ../.."
#ls ../..
#echo "ls ../../TMW2"
#ls ../../TMW2
#echo "ls ../../TMW2/serverdata"
#ls ../../TMW2/serverdata
#echo "ls ../../TMW2/serverdata/npc"
#ls ../../TMW2/serverdata/npc

./serverdata.py npc/ # >license.log

#export RES=$(cat license.log)
#if [[ -n "${RES}" ]]; then
#    echo "Detected missing licenses."
#    cat license.log
#    echo "Estimated total missing licenses:"
#    wc -l license.log
#    exit 1
#fi
echo "Exiting with status zero because licensecheck errors are not considered critical"
exit 0
