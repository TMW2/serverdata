# Server Data Repository

Please ensure you are following [Development Instructions](https://tmw2.org/development.php).
If you could not find some package, you can try apt-cache to find it's real name.

Most common required invokation is:

```
apt-cache search libpcre3
apt-cache search mariadb
```

Install the appropriate packages, including the "-dev" version if available, and
use the following command to build everything:

```
make asan
```

If you do not have memory to spare, you can use `make new`, and it'll make a light
build. However, shall any problem happen with Evol engine, you will most likely
be on the dark.

It will output an "OK" at the end, if everything ran fine. For support head to [#tmw2-dev](http://web.libera.chat/?channels=#tmw2-dev)
Libera Chat IRC channel.

----

**Default administrative user:** `admin`
**Default administrative pass:** `admin`
