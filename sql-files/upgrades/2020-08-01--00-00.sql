#1596240000

-- This file is part of Hercules.
-- http://herc.ws - http://github.com/HerculesWS/Hercules
--
-- Copyright (C) 2019-2020 Hercules Dev Team
--
-- Hercules is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-- MOUBOOTAUR LEGENDS
-- Merge a lot of updates and drop stuff I don't need.

-- ---------------------------------------------------------------------------
-- Update: 2019-10-05--19-01
-- ---------------------------------------------------------------------------
-- Adds new total_tick column
ALTER TABLE `sc_data` ADD COLUMN `total_tick` INT(11) NOT NULL AFTER `tick`;

-- Copy current tick to total_tick so players doesn't lose their current
-- status_changes, although those will still appear wrong until they end
UPDATE `sc_data` SET `total_tick` = `tick`; 

INSERT INTO `sql_updates` (`timestamp`) VALUES (1570309293);

-- ---------------------------------------------------------------------------
-- Update: 2019-10-12--14-21
-- ---------------------------------------------------------------------------
ALTER TABLE `picklog` MODIFY `type` enum('M','P','L','T','V','S','N','C','A','R','G','E','B','O','I','X','D','U','K','Y','Z','W','Q','J','H','@','0','1','2', '3') NOT NULL DEFAULT 'P';
INSERT INTO `sql_updates` (`timestamp`) VALUES (1570870260);

-- ---------------------------------------------------------------------------
-- Update: 2020-01-24--01-09
-- ---------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `npc_expanded_barter_data` (
  `name` VARCHAR(24) NOT NULL DEFAULT '',
  `itemId` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `amount` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `zeny` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyId1` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyAmount1` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyRefine1` INT(11) NOT NULL DEFAULT '0',
  `currencyId2` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyAmount2` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyRefine2` INT(11) NOT NULL DEFAULT '0',
  `currencyId3` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyAmount3` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyRefine3` INT(11) NOT NULL DEFAULT '0',
  `currencyId4` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyAmount4` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyRefine4` INT(11) NOT NULL DEFAULT '0',
  `currencyId5` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyAmount5` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyRefine5` INT(11) NOT NULL DEFAULT '0',
  `currencyId6` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyAmount6` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyRefine6` INT(11) NOT NULL DEFAULT '0',
  `currencyId7` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyAmount7` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyRefine7` INT(11) NOT NULL DEFAULT '0',
  `currencyId8` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyAmount8` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyRefine8` INT(11) NOT NULL DEFAULT '0',
  `currencyId9` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyAmount9` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyRefine9` INT(11) NOT NULL DEFAULT '0',
  `currencyId10` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyAmount10` INT(11) UNSIGNED NOT NULL DEFAULT '0',
  `currencyRefine10` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`, `itemid`, `zeny`,
    `currencyId1`, `currencyAmount1`, `currencyRefine1`,
    `currencyId2`, `currencyAmount2`, `currencyRefine2`,
    `currencyId3`, `currencyAmount3`, `currencyRefine3`,
    `currencyId4`, `currencyAmount4`, `currencyRefine4`
)
) ENGINE=MyISAM;
INSERT INTO `sql_updates` (`timestamp`) VALUES (1579817630);

-- ---------------------------------------------------------------------------
-- Update: 2020-05-10--23-11
-- ---------------------------------------------------------------------------
-- Add separate tables for global integer and string variables.
CREATE TABLE IF NOT EXISTS `map_reg_num_db` (
  `key` VARCHAR(32) BINARY NOT NULL DEFAULT '',
  `index` INT UNSIGNED NOT NULL DEFAULT '0',
  `value` INT NOT NULL DEFAULT '0',
  PRIMARY KEY (`key`, `index`)
) ENGINE=MyISAM;
CREATE TABLE IF NOT EXISTS `map_reg_str_db` (
  `key` VARCHAR(32) BINARY NOT NULL DEFAULT '',
  `index` INT UNSIGNED NOT NULL DEFAULT '0',
  `value` VARCHAR(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`key`, `index`)
) ENGINE=MyISAM;

-- Copy data from mapreg table to new map_reg_*_db tables.
INSERT INTO `map_reg_num_db` (`key`, `index`, `value`) SELECT `varname`, `index`, CAST(`value` AS SIGNED) FROM `mapreg` WHERE NOT RIGHT(`varname`, 1)='$';
INSERT INTO `map_reg_str_db` (`key`, `index`, `value`) SELECT `varname`, `index`, `value` FROM `mapreg` WHERE RIGHT(`varname`, 1)='$';

-- Remove mapreg table. I don't want that.
-- DROP TABLE IF EXISTS `mapreg`;

-- Add update timestamp.
INSERT INTO `sql_updates` (`timestamp`) VALUES (1589145060);


-- ---------------------------------------------------------------------------
-- Index
-- ---------------------------------------------------------------------------
INSERT INTO `sql_updates` (`timestamp`) VALUES (1596240000);

